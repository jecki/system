# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
'''
See :func:`turn_gravis_in_acute`.
'''
import re
import unicodedata

def get_greek_acute_gravis_pairs(
        unicoderange: range = range(17 * 2 ** 16),
        ) -> 'Generator[Tuple[str, str]]':
    '''
    For every Greek character that has a codepoint within
    :param:`unicoderange`, and is or incorporates an acute,
    search the corresponding Greek gravis character. If there
    is one, yield this pair of gravis and acute character.
    '''
    for i in unicoderange:
        try:
            char = chr(i)
            # If ``i`` excedes the number of unicode codepoints,
            # an error is thrown.
        except ValueError:
            break
        try:
            name = unicodedata.name(char)
        # Move on if ``char`` has no unicode name.
        except ValueError:
            continue
        if name.startswith('GREEK ') and ' OXIA' in name:
            gravis_name = name.replace(' OXIA', ' VARIA')
            try:
                gravis_char = unicodedata.lookup(gravis_name)
            # Move on if the new name has no character associated.
            except KeyError:
                continue
            yield (gravis_char, char)

def turn_gravis_in_acute(
        text: str,
        accent_pairs: 'Dict[str, str]' =
            dict(get_greek_acute_gravis_pairs()),
        last_form_regex: 'Pattern[str]' =
            re.compile(r'\b(\w+)(\W*)$'),
        ) -> str:
    '''
    In the last word form of :param:`text`, replace any character which is
    or incorporates a Greek gravis with the corresponding character being
    or incorporating an acute.

    :param accent_pairs: See :func:`get_greek_acute_gravis_pairs`.
    :param last_form_regex: with its first group, matches the form
        that is the object of the replacement; with its second group,
        matches any following part of :param:`text`. This part is not
        changed.

    .. testcode::
        from _greek_accent import turn_gravis_in_acute
        for example in ('τὰ λοιπὰ?', 'τὰ λοιπὰ'):
            print(turn_gravis_in_acute(example))

    .. testoutput::
        τὰ λοιπά?
        τὰ λοιπά
    '''
    def convert(match: 'Match[str]') -> str:
        form, tail = match.group(1, 2)
        for char in form:
            if char in accent_pairs:
                form = form.replace(char, accent_pairs[char])
        return form + tail
    return last_form_regex.sub(convert, text)
