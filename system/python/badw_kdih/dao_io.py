# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
'''
Data access for the KdiH project of the Bavarian Academy of Sciences.
'''
import os
import re
import sys
import time
import xml.etree.ElementTree as ET
from functools import partial

try:
    import pymysql as mysql
    from pymysql import IntegrityError
except ImportError:
    pass

import __init__
import file_io
import xmlhtml
from compare import even_out, even_out_fast, get_sortstring
from context import DBCon
from portal import dao_io

class DB(dao_io.Folder):
    def __init__(
            self,
            config_path: str = __file__ + '/../../../../configuration.utf-8.ini',
            glosses_path: str = __file__ + '/../../../../glosses.xml',
            is_refreshing: bool = True,
            ) -> None:
        '''
        The docstring of :class:`dao_io.Folder` is relevant.
        If :param:`is_refreshing` equals ``True``, the background process
        :func:`refresh_forever` is started during initialization.

        :param config_path: path to a file containing the configuration.
        :param glosses_path: path to the file containing all placeholder
            keywords and their corresponding translations, which are inserted
            into the served page according to the currently chosen language.
        '''
        # To use ET or xmlhtml in templates without import or parameter:
        self.ET = ET
        self.xmlhtml = xmlhtml

        self.multisite = False

        config_path = os.path.abspath(config_path)
        glosses_path = os.path.abspath(glosses_path)

        self.config = config = file_io.get_config(config_path)

        self.perma_url = config['links']['url']

        paths = file_io.get_abspaths(config, config_path)
        self.paths = paths
        self.path = paths['public_path'] + '/'
        self.repros_path = paths['repros_path'] + '/'
        db_access_path = paths['db_access_path'] + '/'

        self.default_page_id = tuple(config['default_page_id'])[0]
        self.default_lang_id = tuple(config['default_lang_id'])[0]
        # For compatibility with multisite instances:
        self.default_lang_ids = {'': self.default_lang_id}

        self.glosses = self.read_glosses(glosses_path)
        self.connect = get_connect(config, db_access_path)

        if is_refreshing:
            self.process = subprocess.Popen(
                    [
                        sys.executable,
                        __file__,
                        str(os.getpid()),
                        config_path,
                        self.repros_path,
                    ],
                    )

    def get_entry(
            self,
            kind: str,
            stoff: str,
            unter: 'Optional[str]',
            ein: 'Optional[str]',
            ) -> 'Tuple[Dict[str, str], str]':
        text_lang_id = self.default_lang_id
        with closing(self.connect()) as connection:
            cur = connection.cursor()
            if kind in {'druck', 'handschrift'}:
                cur.execute(r"""
                        select * from t_eintraege
                        join auth_permission
                            on auth_permission.record_id = t_eintraege.id
                        where is_active = 'T'
                        and auth_permission.table_name = 't_eintraege'
                        and auth_permission.group_id = 41
                        and f_stoffgr_nr = %s
                        and f_untergr_nr = %s
                        and f_hs_position_nr = %s
                        limit 1
                        """, stoff, unter, ein)
            elif kind == 'stoffgruppe':
                cur.execute(r"""
                        select * from t_stoffgruppen
                        join auth_permission
                            on auth_permission.record_id = t_stoffgruppen.id
                        where is_active = 'T'
                        and auth_permission.table_name = 't_stoffgruppen'
                        and auth_permission.group_id = 41
                        and f_stoffgr_nr = %s
                        limit 1
                        """, stoff)
            elif kind == 'untergruppe':
                cur.execute(r"""
                        select * from t_untergruppen
                        join auth_permission
                            on auth_permission.record_id = t_untergruppen.id
                        where is_active = 'T'
                        and auth_permission.table_name = 't_untergruppen'
                        and auth_permission.group_id = 41
                        and f_stoffgr_nr = %s
                        and f_untergr_nr = %s
                        limit 1
                        """, stoff, unter)
            else:
                return {}, text_lang_id
            entry = cur.fetchone() or {}
        return entry, self.default_lang_id

    def get_index(
            self,
            kind: str,
            p_tags_re: 'Pattern[str]' =
                re.compile(r'''(?s)</?p\b(?:[^"'>]*?|".*?"|'.*?')*?>'''),
            blank_re: 'Pattern[str]' = re.compile(r'\s+'),
            lit_erase_re: 'Pattern[str]' = re.compile(r'(^2(?=vl$)|\bvon\s+)'),
            ) -> 'Generator[Dict[str, str]]':
        '''
        Yield all database entities of a certain :param:`kind`.
        The format is apt for a datatable.
        '''
        with closing(self.connect()) as connection:
            cur = connection.cursor()
            if kind == 'stoffgruppen':
                cur.execute(r"""
                        select
                            f_stoffgr_nr as sn,
                            f_stoffgr_ueberschrift as su,
                            f_band_nr as bn
                        from t_stoffgruppen
                        join auth_permission
                            on auth_permission.record_id = t_stoffgruppen.id
                        where is_active = 'T'
                        and auth_permission.table_name = 't_stoffgruppen'
                        and auth_permission.group_id = 41
                        """)
                for row in cur.fetchall():
                    bn = row['bn'] or ''
                    sn = row['sn'] or ''
                    su = row['su'] or ''
                    su = blank_re.sub(' ', p_tags_re.sub(' ', su)).strip()
                    if sn:
                        yield { str(rank): item for rank, item in enumerate((
                                {
                                    '_': sn,
                                    'sort': get_sortstring(sn)
                                },
                                {
                                    '_': '<strong><a href="../stoffgruppe/{sn}">{su}</a></strong>'\
                                        .format(sn = sn, su = su),
                                    'sort': get_sortstring(even_out(su))
                                },
                                {
                                    '_': bn,
                                    'sort': get_sortstring(bn)
                                },
                                )) }
            elif kind == 'untergruppen':
                cur.execute(r"""
                        select
                            t_stoffgruppen.f_stoffgr_nr as sn,
                            t_stoffgruppen.f_stoffgr_ueberschrift as su,
                            t_untergruppen.f_untergr_nr as un,
                            t_untergruppen.f_untergr_ueberschrift as uu,
                            t_stoffgruppen.f_band_nr as bn
                        from t_stoffgruppen
                        join t_untergruppen
                            on t_untergruppen.f_stoffgr_nr = t_stoffgruppen.f_stoffgr_nr
                        join auth_permission
                            on auth_permission.record_id = t_untergruppen.id
                        where t_stoffgruppen.is_active = 'T'
                        and t_untergruppen.is_active = 'T'
                        and auth_permission.table_name = 't_untergruppen'
                        and auth_permission.group_id = 41
                        """)
                for row in cur.fetchall():
                    bn = row['bn'] or ''
                    sn = row['sn'] or ''
                    su = row['su'] or ''
                    su = blank_re.sub(' ', p_tags_re.sub(' ', su)).strip()
                    un = row['un'] or ''
                    uu = row['uu'] or ''
                    uu = blank_re.sub(' ', p_tags_re.sub(' ', uu)).strip()
                    nr = sn + '.' + un
                    if sn and un:
                        yield { str(rank): item for rank, item in enumerate((
                                {
                                    '_': nr,
                                    'sort': get_sortstring(nr)
                                },
                                {
                                    '_': '<strong><a href="../untergruppe/{sn}/{un}">{uu}</a></strong>'\
                                        .format(sn = sn, un = un, uu = uu),
                                    'sort': get_sortstring(even_out(uu))
                                },
                                {
                                    '_': '<a href="../stoffgruppe/{sn}">{su}</a>'\
                                        .format(sn = sn, su = su),
                                    'sort': get_sortstring(even_out(su))
                                },
                                {
                                    '_': bn,
                                    'sort': get_sortstring(bn)
                                },
                                )) }
            elif kind == 'handschriften' or kind == 'drucke':
                if kind == 'handschriften':
                    col = 'f_string'
                    name = 'Handschrift'
                    link = 'handschrift'
                else:
                    col = 'f_signatur'
                    name = 'Druck'
                    link = 'druck'
                cur.execute(r"""
                        select
                            t_stoffgruppen.f_stoffgr_nr as sn,
                            t_stoffgruppen.f_stoffgr_ueberschrift as su,
                            t_untergruppen.f_untergr_nr as un,
                            t_untergruppen.f_untergr_ueberschrift as uu,
                            t_eintraege.f_hs_position_nr as en,
                            t_eintraege.{col} as eu,
                            t_eintraege.f_datum as edat,
                            t_eintraege.f_band_nr as bn
                        from t_stoffgruppen
                        join t_untergruppen
                            on t_untergruppen.f_stoffgr_nr = t_stoffgruppen.f_stoffgr_nr
                        join t_eintraege
                            on t_eintraege.f_stoffgr_nr = t_stoffgruppen.f_stoffgr_nr
                            and t_eintraege.f_untergr_nr = t_untergruppen.f_untergr_nr
                        join auth_permission
                            on auth_permission.record_id = t_eintraege.id
                        where t_stoffgruppen.is_active = 'T'
                        and t_untergruppen.is_active = 'T'
                        and t_eintraege.is_active = 'T'
                        and auth_permission.table_name = 't_eintraege'
                        and auth_permission.group_id = 41
                        and t_eintraege.f_bestand_typ = '{name}'
                        """.format(col = col, name = name))
                for row in cur.fetchall():
                    bn = row['bn'] or ''
                    sn = row['sn'] or ''
                    su = row['su'] or ''
                    su = blank_re.sub(' ', p_tags_re.sub(' ', su)).strip()
                    un = row['un'] or ''
                    uu = row['uu'] or ''
                    uu = blank_re.sub(' ', p_tags_re.sub(' ', uu)).strip()
                    en = row['en'] or ''
                    eu = row['eu'] or ''
                    eu = blank_re.sub(' ', p_tags_re.sub(' ', eu)).strip()
                    edat = row['edat'] or ''
                    nr = sn + '.' + un + '.' + en
                    if sn and un and en:
                        yield { str(rank): item for rank, item in enumerate((
                                {
                                    '_': nr,
                                    'sort': get_sortstring(nr)
                                },
                                {
                                    '_': '<strong><a href="../{link}/{sn}/{un}/{en}">{eu}</a></strong>'\
                                        .format(link = link, sn = sn, un = un, en = en, eu = eu),
                                    'sort': get_sortstring(even_out(eu))
                                },
                                {
                                    '_': edat,
                                    'sort': get_sortstring(even_out(edat)),
                                },
                                {
                                    '_': '<a href="../untergruppe/{sn}/{un}">{uu}</a>'\
                                        .format(sn = sn, un = un, uu = uu),
                                    'sort': get_sortstring(even_out(uu))
                                },
                                {
                                    '_': '<a href="../stoffgruppe/{sn}">{su}</a>'\
                                        .format(sn = sn, su = su),
                                    'sort': get_sortstring(even_out(su))
                                },
                                {
                                    '_': bn,
                                    'sort': get_sortstring(bn)
                                },
                                )) }
            elif kind == 'literatur':
                cur.execute(r"""
                        select
                            f_kurztitel as lk,
                            f_volltitel as lv,
                            f_band as lb,
                            f_stoffgruppe as lsg
                        from t_lit
                        where is_active = 'T'
                        """)
                for row in cur.fetchall():
                    lk = row['lk'] or ''
                    lv = row['lv'] or ''
                    lb = row['lb'] or ''
                    lsg = row['lsg'] or ''
                    lk = blank_re.sub(' ', p_tags_re.sub(' ', lk)).strip()
                    lv = blank_re.sub(' ', p_tags_re.sub(' ', lv)).strip()
                    if lk:
                        yield { str(rank): item for rank, item in enumerate((
                                {
                                    '_': lk,
                                    'sort': get_sortstring(
                                        lit_erase_re.sub('', even_out(lk)))
                                },
                                {
                                    '_': lv,
                                    'sort': get_sortstring(even_out(lv))
                                },
                                {
                                    '_': lb,
                                    'sort': get_sortstring(lb)
                                },
                                {
                                    '_': lsg,
                                    'sort': get_sortstring(lsg)
                                },
                                )) }
            else:
                raise Exception('Unknown :param:`kind` of index: ' + str(kind))

def get_connect(
        config: 'configparser.ConfigParser',
        db_access_path: str,
        db_name_key: str = 'name',
        ) -> partial:
    '''
    Get an object that, when called, establishes a connection to a database
    with the specifications given in :param:`config`.

    :param db_access_path: file storing a hash of user name and password
        that will be used to access the database.
    :param db_name_key: key term on which the db section of the configuration
        returns the database name. This parameter can be used to dynamically
        select one certain database out of several.
    '''
    dbuser, dbpassword = file_io.get_keys(db_access_path)
    return partial(
            mysql.connect,
            host        = config['db']['host'],
            port        = int(config['db']['port']),
            user        = dbuser,
            passwd      = dbpassword,
            db          = config['db'][db_name_key],
            charset     = config['db']['charset'],
            unix_socket = config['db']['unix_socket'] if sys.platform == 'linux'
                          else None,
            cursorclass = mysql.cursors.DictCursor,
            )

def refresh_forever(
        pid: int,
        config_path: str,
        ) -> None:
    '''
    Refresh or clean up data in an ongoing background process.

    - Exit, when no process with :param`pid` is running anymore.
      This should be the process ID of the process having started this function.
    - Remove occurrences of :html:`<span ...></span>`.
    - Remove artifacts caused by pasting from an office application.
    - Remove width and height settings of table cells: They result from a buggy
      feature of the MC-Editor.
    - Start anew.

    Any occurring errors or issues are written into a :class:`file_io.Log`
    instance.

    :param config_path: absolute path to a file containing the configuration,
        e.g. the path to the very database being refreshed in this function.
    '''
    log = file_io.Log(__file__)
    sys.stderr = log
    config = file_io.get_config(config_path)
    paths = file_io.get_abspaths(config, config_path)
    connect = get_connect(config, paths['db_access_path'])
    while True:
        with DBCon(connect()) as (connection, cur):
            cur.execute(r"""
                    select table_name from information_schema.tables
                    where table_schema = %s
                    """, (config['db']['name'],))
            for result in cur.fetchall():
                table = result['table_name']
                if table.startswith('auth_') or table.endswith('_archive'):
                    continue
                cur.execute("describe " + table)
                cols = cur.fetchall()
                if 'id' not in { col['Field'] for col in cols }:
                    continue
                cols = tuple(
                        (col['Field'], col['Type']) for col in cols
                        if ('text' in col['Type'].lower() or
                            'varchar' in col['Type'].lower())
                        and col['Type'].lower() != 'id'
                        )
                if not cols:
                    continue
                colnames = ', '.join( name for name, _ in cols )
                colsets = ' = %s, '.join( name for name, _ in cols ) + ' = %s'
                cur.execute("select id from " + table)
                rows = cur.fetchall()
                for row in rows:
                    row_id = row['id']
                    # Prevent writing until we have written, so that
                    # rather we get overwritten in a race condition.
                    cur.execute("""
                            select {colnames} from {table} where id = %s
                            LOCK IN SHARE MODE
                            """.format(colnames = colnames, table = table),
                            (row_id,))
                    try:
                        result = cur.fetchone()
                        olds = []
                        news = []
                        for colname, coltype in cols:
                            old = result[colname]
                            olds.append(old)
                            new = refresh(old, colname, coltype) ###########################
                            news.append(new)
                        if news != olds:
                            news.append(row_id)
                            cur.execute(r"""
                                    update {table} set {colsets} where id = %s
                                    """.format(table = table, colsets = colsets),
                                    news)
                            connection.commit()
                            exit(0)
                    except Exception as e:
                        log.write(e)
                        connection.rollback()
                    time.sleep(0.01)

if __name__ == '__main__':
    if len(sys.argv) > 2:
        pid = int(sys.argv[1])
        config_path = sys.argv[2]
    else:
        # For testing.
        pid = os.getpid()
        config_path = os.path.abspath(__file__ + '/../../../../../../6.1.7 Projekte und Arbeitsplanung/KdiH/seite_kdih/configuration.utf-8.ini')
    refresh_forever(pid, config_path)
