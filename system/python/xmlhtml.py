# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2013 ff.
'''
For processing and generating XML / HTML, especially for converting
from and to convenient data structures.
'''
import html
import html.parser
import json
try:
    import regex as re
except ImportError:
    import re
import urllib.request
import xml.etree.ElementTree as ET
from collections import deque
from collections import OrderedDict
from copy import deepcopy

try:
    import _markupbase
    html.parser.HTMLParser.parse_html_declaration = \
            _markupbase.ParserBase.parse_declaration
except Exception as e:
    print(e)
    print('Thus, a declaration with inner lesser-than will break.')

# HTML self-closing tags according to
# http://www.w3.org/TR/2014/CR-html5-20140731/syntax.html#void-elements
HTML_VOIDS = {
        'area',
        'base',
        'br',
        'col',
        'embed',
        'hr',
        'img',
        'input',
        'keygen',
        'link',
        'meta',
        'param',
        'source',
        'track',
        'wbr',
        }

# The following expressions can be used after applying :func:`milestone`:
POS   = r'(?P<pos>[se])'
ID    = r'(?P<id>[^-]+)-'
NAME  = r'(?P<name>[^\S/>!?]*)' # ``'>!?'`` are not strictly necessary.
ATTRS = r'(?P<attrs>(?: [^=]+="[^"]*")*)'
TAG   = r'(?P<tag><{}{}{}{}/>)'.format(POS, ID, NAME, ATTRS)
START = r'(?P<start><s{}{}{}/>)'.format(ID, NAME, ATTRS)
END   = r'(?P<end><e(?P=id)-(?P=name)/>)'
# The following expressions are of a more general use.
# For the second, you need the regex engine with named recursive patterns.
T     = r'''(?:"[^"]*"|'[^']*'|[^>/])*'''
INNER = r'''(?P<inner> # Note: You must use the verbose mode!
        (?:
            <!--(?:(?!-->).)*-->| # Comment
            <!\[(?:(?!\]>).)*\]>| # Unknown Declaration / CDATA
            <!(?P<dd>(?:<(?&dd)>|"[^"]*"|'[^']*'|[^<>])*)>| # Declaration / DTD
            <\?(?:(?!\?>).)*\?>|  # Declaration / Processing Instruction
            <{T}/\s*>|            # Self-closing tag
            <{T}>(?&inner)</{T}>| # Inner Element
            [^<]                  # Inner Text
        )*)'''.format(T = T)
INNER2 = INNER.replace(r'inner', 'inner2').replace('dd', 'dd2')
INNER3 = INNER2.replace(r'inner2', 'inner3').replace('dd2', 'dd3')

class EscapeError(Exception):
    '''
    Base class for exceptions to be raised when a term is meant for an
    unescapable part of XML or HTML and contains characters which make
    the end of the part ambiguous.

    For details, see the warning in the docstring of :func:`serialize`.
    '''
    def __str__(self) -> str:
        return (self.message + ':\n' + self.term)
class CommentEscapeError(EscapeError):
    def __init__(self, term: str):
        self.message = '`-->` appears in a comment'
        self.term = term
class DeclarationEscapeError(EscapeError):
    def __init__(self, term: str):
        self.message = 'There is a `>` in a document declaration '\
                'that is not balanced and not part of an attribute value'
        self.term = term
class DeclarationInstructionEscapeError(EscapeError):
    def __init__(self, term: str):
        self.message = '`?>` appears in a `<?...?>` part '\
                '(i.e. an XML declaration or an XML processing instruction)'
        self.term = term
class ProcessingInstructionEscapeError(EscapeError):
    def __init__(self, term: str):
        self.message = '`>` appears in an HTML processing instruction'
        self.term = term
class UnknownDeclarationEscapeError(EscapeError):
    def __init__(self, term: str):
        self.message = '`]>` appears in a `<![...]>` part (i.e. CDATA)'
        self.term = term

class FragmentTextExtractor(html.parser.HTMLParser):
    '''
    Parser to extract blocks of inner text along with fragment ids and
    certain status flags from the elements of an XML or xHTML document:

    .. important::
        Text before or after the root element is ignored.
        Text which is directly or indirectly embedded in the root element
        but neither directly nor indirectly embedded in an element with an
        id attribute **is** extracted with the fragment id being the empty
        string.

    These extracts can be useful for feeding a full-text search.

    The document is given to the parser with its method `parse`. Then, the
    extracts are stored in :attr:`FragmentTextExtractor.extracts`, which is
    a deque of 3-tuples, each containing a string as fragment id, a string as
    text and a dictionary of strings as status flag.

    Whether the inner text of an element is treated as a fragment (and not only
    as the part of a fragment), is determined by :param:`__init__.fragment_tags`
    and :param:`__init__.fragment_tags_attrs`: It is treated as a fragment,
    iff the tag of the element appears in the first parameter or the the tag
    appears in the second parameter *and* the set of pairs given alongside
    with each tagname in the second parameter is a subset of the key-value
    pairs of the attribute list of the element.

    The status flag is determined by :param:`tracked_attrs`: If this parameter
    is e.g. ``{'class': ('source', 'non-source')}``, a status flag of each
    fragment stores the current or, if not present, the inherited
    **value of the attribute** `class`, **if** it is `source` or `non-source`.

    .. important::
        In order to specify an attribute without value (a boolean attribute),
        specify the value as the empty string.

    .. testcode::
        import xmlhtml

        doc = '<article>\
                <section class="non-source">\
                 <h1>Wessobrunner Gebet</h1>\
                 <p id="1">The following poem is one of the most beautiful and most enigmatic OHG texts.</p>\
                </section>\
                <section class="xyz source">\
                 <h2 id="2">De poeta</h2>\
                 <p id="3">Dat <span class="non-source">ga</span>fregin ih mit firahim &hellip;</p>\
                </section>\
               </article>'

        parser = xmlhtml.FragmentTextExtractor(tracked_attrs = {'class': ('source', 'non-source')})
        parser.parse(doc)

        for extract in parser.extracts:
            print(extract)

    .. testoutput::
        ('', 'Wessobrunner Gebet', {'class': ['non-source']})
        ('1', 'The following poem is one of the most beautiful and most enigmatic OHG texts.', {'class': ['non-source']})
        ('', '                                                  ', {'class': ['non-source']})
        ('2', 'De poeta', {'class': ['source']})
        ('3', 'Dat gafregin ih mit firahim …', {'class': ['source']})
        ('', '                                                  ', {'class': ['source']})
        ('', '                                               ', {})

    '''
    def __init__(
            self,
            tracked_attrs: 'Dict[str, Sequence[str]]' = {'': ('',)},
            fragment_tags: 'Sequence[str]' = (
                'article',
                'aside',
                'blockquote',
                'button',
                'canvas',
                'caption',
                'dd',
                'div',
                'dt',
                'embed',
                'fieldset',
                'figcaption',
                'figure',
                'footer',
                'form',
                'h1',
                'h2',
                'h3',
                'h4',
                'h5',
                'h6',
                'header',
                'li',
                'map',
                'object',
                'output',
                'p',
                'pre',
                'section',
                'textarea',
                'video',
                'th',
                'tr',
                ),
            fragment_tags_attrs: 'Dict[str, AbstractSet[Tuple[str, str]]]' = {
                'small': {('class', 'note')},
                },
            ):
        '''
        :param tracked_attrs: See the docstring of the class.
        '''
        super().__init__(convert_charrefs = True)
        self.extracts       = deque()
        self.chunks         = deque([deque()])
        self.fragment_ids   = deque([''])
        self.tracked_attrs  = tracked_attrs
        self.flags          = deque([{}])
        self.fragment_flags = deque([True])
        self.fragment_tags  = fragment_tags
        self.fragment_tags_attrs = fragment_tags_attrs

    def handle_data(self, data: str):
        '''
        Append a text part to the current row of text parts.
        Text parts before or after the root element are ignored.
        '''
        self.chunks[-1].append(data)

    def handle_endtag(self, tag: str):
        '''
        Make a new extract, if the closed element is a to be treated
        as a fragment (and not only as a part of a fragment).
        '''
        if self.fragment_flags.pop():
            self.extracts.append((
                    self.fragment_ids.pop(),
                    ''.join(self.chunks.pop()),
                    self.flags.pop()
                    ))
        else:
            self.flags.pop()

    def handle_starttag(
            self,
            tag: str,
            attrs: 'List[Tuple[str, Optional[str]]]',
            ):
        '''
        If the new element is to be treated as a fragment (and not only as a
        part of the current fragment), append a new row, in which the text
        part of this element will be put. Memoize this status, the `id`
        value, if one is present, and the current or inherited status of
        the attribute values which are to be tracked.
        '''
        attrs_dict = dict(attrs)
        attrs_split = []
        for key, value in attrs:
            for subvalue in (value or '').split():
                attrs_split.append((key, subvalue))
        if tag in self.fragment_tags or (
                tag in self.fragment_tags_attrs and
                self.fragment_tags_attrs[tag].issubset(attrs_split)):
            self.fragment_flags.append(True)
            self.chunks.append(deque())
            if 'id' in attrs_dict:
                self.fragment_ids.append(attrs_dict['id'])
            else:
                self.fragment_ids.append(self.fragment_ids[-1])
        else:
            self.fragment_flags.append(False)
        flags = {}
        for key, status in self.tracked_attrs.items():
            if key in attrs_dict:
                values = [ val for _, val in attrs_split if val in status ]
                if values:
                    flags[key] = values
            if (key not in flags) and (key in self.flags[-1]):
                flags[key] = self.flags[-1][key]
        self.flags.append(flags)

    def parse(self, doc: str) -> None:
        self.feed(doc)
        self.close()

class Itemizer(html.parser.HTMLParser):
    '''
    Parser to convert an XML or HTML doc to a deque of items. An item represents
    a part of the document, e.g. a tag or a passage of text between tags. Every
    item is a list of eight fields storing the information about the item as
    follows:

    0. a tag name, if the item is a starttag or a self-closing tag, else the
       empty string.
    1. a list of tuples, which are attribute names and values, if the item is
       a starttag or startendtag and has attributes, otherwise an empty list.
    2. a tag name, if the item is an endtag or a self-closing tag, else the
       empty string.
    3. a string representing a passage of text between two non-text
       items or the empty string. The string is unescaped (i.e. de-escaped).
    4. a comment or the empty string.
    5. a processing instruction or the empty string.
    6. a declaration or the empty string.
    7. an unknown declaration or the empty string.

    .. note::
        A self-closing `<tag/>` yields:
        ``['tag', [], 'tag', '', '', '', '', '']``

    So you can conveniently iterate through the document, e.g.::

        for start, attrs, end, text, com, pi, decl, undecl in parser.items:
            if any((com, pi, decl, undecl)):
                pass
            elif start:
                ...

    .. important::
        - All tags and attribute names are inevitably converted to lower case!
        - An attribute without a value (a boolean attribute) is read with
          the value being ``None`` and is written with the value being ``''``.
        - If :param:`__init__.balance` is ``True`` (the default value),
          the stack :attr:`.items` is made balanced in the following way:

          - Missing endtags are inserted at the last possible place, i.e.,
            either at the very end of the input or immediately before an endtag
            occurs whose opening counterpart occured before the starttag whose
            closing counterpart is missing.
          - Endtags not (or: no longer) matching a starttag are omitted.

        - Tags, comments, processing instructions and declarations are stored
          without the surrounding delimiters. Except an XML declaration or an
          XML processing instruction: Here, the trailing `?` is kept in order
          to preserve the difference to an HTML processing instruction.

    .. note::
        The conversion is done incrementally and iteratively.

    .. testcode::
        import xmlhtml

        doc = """
                <tr><td><p align="left">
                  paragraph&apos;s text<br/>
                  <a></a>paragraph&#x2019;s text</tr>
                </non-match>end."""

        parser = xmlhtml.Itemizer()
        parser.parse(doc)
        for item in parser.items:
            print(item)

    .. testoutput::
        ['', [], '', '\n                ', '', '', '', '']
        ['tr', [], '', '', '', '', '', '']
        ['td', [], '', '', '', '', '', '']
        ['p', [('align', 'left')], '', '', '', '', '', '']
        ['', [], '', "\n                  paragraph's text", '', '', '', '']
        ['br', [], 'br', '', '', '', '', '']
        ['', [], '', '\n                  ', '', '', '', '']
        ['a', [], '', '', '', '', '', '']
        ['', [], 'a', '', '', '', '', '']
        ['', [], '', 'paragraph’s text', '', '', '', '']
        ['', [], 'p', '', '', '', '', '']
        ['', [], 'td', '', '', '', '', '']
        ['', [], 'tr', '', '', '', '', '']
        ['', [], '', '\n                end.', '', '', '', '']
    '''
    def __init__(self, balance: bool = True):
        super().__init__(convert_charrefs = True)
        self.items     = deque()
        self.open_tags = deque()
        self.balance   = balance

    def close(self):
        if self.balance:
            while self.open_tags:
                self.items.append(
                        ['', [], self.open_tags.pop(), '', '', '', '', ''])
        html.parser.HTMLParser.close(self)

    def serialize(self) -> str:
        '''(Cf. :func:`serialize`.)'''
        return ''.join(self.serialize_items())

    def serialize_items(self) -> 'Deque[str]':
        '''(Cf. :func:`serialize_items`.)'''
        return serialize_items(self.items)

    def handle_comment(self, comment: str):
        self.items.append(['', [], '', '', comment, '', '', ''])

    def handle_data(self, data: str):
        if self.items and self.items[-1][3]:
            self.items[-1][3] += data
        else:
            self.items.append(['', [], '', data, '', '', '', ''])

    def handle_decl(self, decl: str):
        self.items.append(['', [], '', '', '', '', decl, ''])

    def handle_endtag(self, tag: str):
        if not self.balance:
            self.items.append(['', [], tag, '', '', '', '', ''])
        elif tag in self.open_tags:
            while self.open_tags:
                open_tag = self.open_tags.pop()
                self.items.append(['', [], open_tag, '', '', '', '', ''])
                if open_tag == tag:
                    break

    def handle_pi(self, pi: str):
        self.items.append(['', [], '', '', '', pi, '', ''])

    def handle_starttag(
            self,
            tag: str,
            attrs: 'List[Tuple[str, Optional[str]]]',
            ):
        self.open_tags.append(tag)
        self.items.append([tag, attrs, '', '', '', '', '', ''])

    def handle_startendtag(
            self,
            tag: str,
            attrs: 'List[Tuple[str, Optional[str]]]',
            ):
        self.items.append([tag, attrs, tag, '', '', '', '', ''])

    def parse(self, doc: str) -> None:
        self.feed(doc)
        self.close()

    def unknown_decl(self, decl: str):
        self.items.append(['', [], '', '', '', '', '', decl])

class ItemizerID(Itemizer):
    '''
    The same as :class:`Itemizer`, but with a further field:

    8. an integer:

       - greater than zero for starttags and for matching endtags.
         **A matching endtag has the same integer as the belonging starttag.**
         The numbering starts with ``1`` for the first starttag and is
         incremented by ``1`` for every further starttag.
       - zero in all other cases:
         startendtags, declarations, processing instructions and text.

    So you may iterate over the document with::

        for start, attrs, end, text, com, pi, decl, undecl, num in parser.items:
                ...

    .. important::
        The stack :attr:`.items` is made balanced as if :class:`Itemizer`
        is initialized with :param:`Itemizer.__init__.balance` being ``True``
        (the default value).

        For further details, see the docstring of :class:`Itemizer`.
    '''
    def __init__(self):
        super().__init__(balance = True)
        self.num = 0
        self.nums = deque()

    def close(self):
        while self.open_tags:
            self.items.append([
                    '', [], self.open_tags.pop(), '', '', '', '', '',
                    self.nums.pop()])
        html.parser.HTMLParser.close(self)

    def handle_comment(self, comment: str):
        self.items.append(['', [], '', '', comment, '', '', '', 0])

    def handle_data(self, data: str):
        if self.items and self.items[-1][3]:
            self.items[-1][3] += data
        else:
            self.items.append(['', [], '', data, '', '', '', '', 0])

    def handle_decl(self, decl: str):
        self.items.append(['', [], '', '', '', '', decl, '', 0])

    def handle_endtag(self, tag: str):
        if tag in self.open_tags:
            while self.open_tags:
                open_tag = self.open_tags.pop()
                self.items.append(
                        ['', [], open_tag, '', '', '', '', '', self.nums.pop()])
                if open_tag == tag:
                    break

    def handle_pi(self, pi: str):
        self.items.append(['', [], '', '', '', pi, '', '', 0])

    def handle_starttag(
            self,
            tag: str,
            attrs: 'List[Tuple[str, Optional[str]]]',
            ):
        self.open_tags.append(tag)
        self.num += 1
        self.nums.append(self.num)
        self.items.append([tag, attrs, '', '', '', '', '', '', self.num])

    def handle_startendtag(
            self,
            tag: str,
            attrs: 'List[Tuple[str, Optional[str]]]',
            ):
        self.items.append([tag, attrs, tag, '', '', '', '', '', 0])

    def parse(self, doc: str) -> None:
        self.feed(doc)
        self.close()

    def unknown_decl(self, decl: str):
        self.items.append(['', [], '', '', '', '', '', decl, 0])

class JSONEncoderExtended(json.JSONEncoder):
    '''
    Extends the built-in :class:`json.JSONEncoder`, so that all iterables can
    be converted.
    '''
    def default(self, obj: 'Any'):
        if not isinstance(obj, (str, dict, list, tuple)) \
                and isinstance(obj, Iterable):
            return list(obj)
        return json.JSONEncoder.default(self, obj)

class Replacer(html.parser.HTMLParser):
    '''
    Parser to replace text between tags, to replace tag names and tag
    attributes, comments, doctype declarations, bracket declarations
    and processing instructions.

    It can be used e.g. for cleaning up an HTML document, after which
    the document can further be processed via XML parsing.
    See :func:`normalize`.

    The replacement is done via functions specified as parameters in
    :meth:`__init__`. The default values of the parameters just remove
    all tags, comments, declarations and processing instructions.

    The document is given to the parser as a string passed to
    :meth:`parse`. Then, the resulting parts are stored in
    :attr:`results`, which is a deque of strings.

    The usage is further illustrated by the testcode below.

    .. important::
        - All tags and attribute names are inevitably converted to lower case!
        - An attribute without a value (a boolean attribute) is read with
          the value being ``None`` and is written with the value being ``''``.
        - Missing endtags are inserted at the last possible place, i.e.,
          either at the very end of the input or immediately before an endtag
          occurs whose opening counterpart occured before the starttag whose
          closing counterpart is missing.
        - Endtags not (or: no longer) matching a starttag are omitted.
        - :param:`__init__.replace_element` is executed before
          :param:`__init__.replace_tag`.

    .. note::
        - It is possible to change a starting tag into a self-closing tag
          and vice versa. Compare the example in the testcode below.
        - If a tag name is replaced with the empty string, any matching start
          tag and endtag is replaced by an empty string, i.e. removed; tags
          and text within the element, however, are not affected by that.
        - Similarly, if a function to handle comments or declarations or
          processing instructions returns an empty string, the comment,
          declaration or instruction is removed. If the function returns
          a non-empty string, this string is delimited (e.g. by `<!--`
          and `-->`, if it is a comment) and included in the results.
        - In order to alter or remove a whole element, use the callable
          parameter `replace_element` of :meth:`__init__`:
          If this function returns ``None``, nothing happens, but if it
          returns a string, the whole element is replaced by this string,
          which can be an empty one.
        - If the list of key-value tuples which contains the attribute
          keys and values is replaced with an empty list, no attribute
          is written in the tag.
        - The function to replace text between tags is not called with the
          *whole* sequence from a starttag to the corresponding endtag,
          but only with the *fragment* reaching from some last tag to some
          next tag.
        - This fragment is unescaped before entering the function and
          escaped after leaving the function.

    .. warning::
        Some possible parts of an XML or HTML document have special rules.
        The pertinent parts and the rules to be followed are given in the
        warning of the docstring of :func:`serialize`.

        If you return a value for such a part (in the functions described
        in the docstring of :meth:`Replacer.__init__` below), you have to
        follow these rules. On escaping issues, an :class:`EscapeError`
        is raised as described in said docstring.

    .. testcode::
        import xmlhtml

        def replace_tag(tag, attrs, startend, info):
            attrs = [ (key, value) for key, value in attrs
                    if key not in {'style', 'align'} ]
            tag = tag if (attrs or tag != 'span') else ''
            startend = tag in xmlhtml.HTML_VOIDS
            return tag, attrs, startend

        def replace_text(text, info):
            return text.strip()

        text = """
                <tr>
                    <td>
                        <p align="left">
                            <span style="font-family: UglySans">
                                some text <br>
                                more text <ins/>
                            </span>
                    </td>
            """

        replacer = xmlhtml.Replacer(
                replace_tag = replace_tag,
                replace_text = replace_text,
                )
        replacer.parse(text)
        text = ''.join(replacer.results)
        print(text)

    .. testoutput::
        <tr><td><p>some text<br/>more text<ins></ins></p></td></tr>
    '''
    def __init__(
            self,
            replace_bracket_declaration: 'Callable[[str, dict], str]'
                = lambda decl, info: '',
            replace_comment:             'Callable[[str, dict], str]'
                = lambda comment, info: '',
            replace_declaration:         'Callable[[str, dict], str]'
                = lambda decl, info: '',
            replace_processing_instruct: 'Callable[[str, dict], str]'
                = lambda pi, info: '',
            replace_element: '''Callable[
                    [str, List[Tuple[str, Optional[str]]], bool, dict],
                    Optional[str]]'''
                = lambda tag, attrs, startend, info: None,
            replace_tag: '''Callable[
                    [str, List[Tuple[str, Optional[str]]], bool, dict],
                     str, List[Tuple[str, Optional[str]]], bool]'''
                = lambda tag, attrs, startend, info: ('', [], False),
            replace_text: 'Callable[[str, dict], str]'
                = lambda text, info: text,
            consider_endtag: 'Optional[Callable[[str, str, dict]]]'
                = None,
            info: dict = {},
            ):
        '''
        For the callable parameters, documented templates are given below.
        :param:`info` is the initial value of the parameter `info`, which
        is passed to these callables and may be used in them as a mutable
        memory for circumstances which are noted while processing before,
        and on which the current processing can act.
        ::

            def consider_endtag(
                    tag: str,
                    tag_new: str,
                    info: dict,
                    ) -> None:
                """
                Change (not: re-assign) :param:`info` on :param:`tag` or
                :param:`tag_new`, i.e. on the endtag before or after its
                name is changed. That can be used to control the actions
                done in the other functions.

                If :param:`tag_new` is empty, the corresponding starttag
                has been deleted. The function is not called at all:

                - if there never was a corresponding starttag.
                - for endtags within elements that has been deleted.
                - for self-closing tags (use :func:`replace_tag` instead).
                """
                return None

            def replace_bracket_declaration(
                    decl: str,
                    info: dict,
                    ) -> str:
                """Process bracket declaration :param:`decl`."""
                return decl

            def replace_comment(
                    comment: str,
                    info: dict,
                    ) -> str:
                """Process :param:`comment`."""
                return comment

            def replace_declaration(
                    decl: str,
                    info: dict,
                    ) -> str:
                """Process declaration :param:`decl`."""
                return decl

            def replace_processing_instruct(
                    pi: str,
                    info: dict,
                    ) -> str:
                """Process processing instruction :param:`pi`."""
                return pi

            def replace_element(
                    tag: str,
                    attrs: 'List[Tuple[str, Optional[str]]]',
                    startend: bool,
                    info: dict,
                    ) -> 'Optional[str]':
                """
                Process :param:`tag` and :param:`attrs`.
                A return value of ``None`` does not change the element.
                If a string is returned, the element is replaced by it
                without escaping; if the empty string is returned, the
                element is deleted.
                If the element is void, :param:`startend` is ``True``.
                """
                return None

            def replace_tag(
                    tag: str,
                    attrs: 'List[Tuple[str, Optional[str]]]',
                    startend: bool,
                    info: dict,
                    ) -> 'Tuple[str, List[Tuple[str, str]], bool]':
                """
                Process :param:`tag` and :param:`attrs`.
                If the tag is self-closing, :param:`startend` is ``True``.
                If the new tag shall be self-closing, set it to ``True``.
                """
                return tag, attrs, startend

            def replace_text(
                    text: str,
                    info: dict,
                    ) -> str:
                """
                Process and return :param:`text`.
                Unescaping and escaping is done automatically.
                """
                return text
        '''
        super().__init__(convert_charrefs = True)
        self.consider_endtag              = consider_endtag
        self.replace_bracket_declaration  = replace_bracket_declaration
        self.replace_comment              = replace_comment
        self.replace_declaration          = replace_declaration
        self.replace_element              = replace_element
        self.replace_processing_instruct  = replace_processing_instruct
        self.replace_tag                  = replace_tag
        self.replace_text                 = replace_text
        self.info                         = info
        self.results                      = deque()
        self.starttags_old                = deque()
        self.endtags_new                  = deque()
        self.in_replaced_element          = False

    def close(self):
        self.starttags_old = deque()
        while self.endtags_new:
            endtag = self.endtags_new.pop()
            if endtag is None:
                self.in_replaced_element = False
            elif not self.in_replaced_element:
                self.results.append(taggify(endtag, end = True))
        self.in_replaced_element = False
        html.parser.HTMLParser.close(self)

    def handle_comment(self, comment: str):
        if not self.in_replaced_element:
            comment = self.replace_comment(comment, self.info)
            if comment:
                if '-->' in comment:
                    raise CommentEscapeError(comment)
                self.results.append('<!--{}-->'.format(comment))

    def handle_data(self, data: str):
        if not self.in_replaced_element:
            self.results.append(escape(self.replace_text(data, self.info)))

    def handle_decl(self, decl: str):
        if not self.in_replaced_element:
            decl = self.replace_declaration(decl, self.info)
            if decl:
                if '>' in decl:
                    temp = re.sub(r'''("[^"]*"|'[^']*')''', '', decl)
                    if temp.count('<') != temp.count('>'):
                        raise DeclarationEscapeError(decl)
                self.results.append('<!{}>'.format(decl))

    def handle_endtag(self, tag: str):
        if tag in self.starttags_old:
            while self.starttags_old:
                starttag = self.starttags_old.pop()
                tag_new = self.endtags_new.pop()
                if tag_new is None:
                    self.in_replaced_element = False
                elif not self.in_replaced_element:
                    self.results.append(taggify(tag_new, end = True))
                    if self.consider_endtag:
                        self.consider_endtag(tag, tag_new, self.info)
                if starttag == tag:
                    break

    def handle_pi(self, pi: str):
        if not self.in_replaced_element:
            pi = self.replace_processing_instruct(pi, self.info)
            if pi:
                if pi[-1] == '?' and '?>' in pi:
                    raise DeclarationInstructionEscapeError(pi)
                elif '>' in pi:
                    raise ProcessingInstructionEscapeError(pi)
                self.results.append('<?{}>'.format(pi))

    def handle_starttag(
            self,
            tag: str,
            attrs: 'List[Tuple[str, Optional[str]]]',
            ):
        self.starttags_old.append(tag)
        if self.in_replaced_element:
            endtag = ''
        else:
            element = self.replace_element(tag, attrs, False, self.info)
            if element is None:
                tag, attrs, startend = \
                        self.replace_tag(tag, attrs, False, self.info)
                self.results.append(taggify(tag, attrs, startend = startend))
                if startend:
                    self.starttags_old.pop()
                    return
                endtag = tag
            else:
                self.results.append(element)
                endtag = None
                self.in_replaced_element = True
        self.endtags_new.append(endtag)

    def handle_startendtag(
            self,
            tag: str,
            attrs: 'List[Tuple[str, Optional[str]]]',
            ):
        if not self.in_replaced_element:
            ersatz = self.replace_element(tag, attrs, True, self.info)
            if ersatz is None:
                tag, attrs, startend = self.replace_tag(
                        tag, attrs, True, self.info)
                self.results.append(taggify(tag, attrs, startend = startend))
                if not startend:
                    self.results.append(taggify(tag, end = True))
            else:
                self.results.append(escape(ersatz))

    def parse(self, doc: str) -> None:
        self.feed(doc)
        self.close()

    def unknown_decl(self, undecl: str):
        if not self.in_replaced_element:
            undecl = self.replace_bracket_declaration(undecl, self.info)
            if undecl:
                if ']>' in undecl:
                    raise UnknownDeclarationEscapeError(undecl)
                self.results.append('<![{}]>'.format(undecl))

def data2json(data: 'Iterable', pretty: bool = False) -> str:
    '''Convert the given :param:`data` to JSON.'''
    if pretty:
        return json.dumps(data, cls = JSONEncoderExtended, indent = '\t')
    else:
        return json.dumps(data, cls = JSONEncoderExtended,
                separators = (',', ':'))

def elem2string(
        element: ET.Element,
        method: str = 'htmlxml',
        tailless: bool = True,
        inner: bool = False,
        short_empty_elements: bool = False,
        html_voids_re: 'Pattern[str]' = re.compile(
            '></({})\s*>'.format('|'.join(HTML_VOIDS))),
        inner_re: 'Pattern[str]' = re.compile(r'(?s)^<[^>]*>(.*)</[^>]*>$'),
        ) -> str:
    '''
    Convert :param:`element` to a string representation.

    :param method: has the same meaning as in
        :meth:`xml.etree.ElementTree.tostring`, but an additional option:

        - The value ``'text'`` leads to a string containing all texts and tails
          of the element and its descendants, unescaped and without tags.
        - The other allowed values lead to a string with escaping and with tags.
        - The additional option is ``'htmlxml'``: The resulting string conforms
          with the XML syntax, and any sequence matching :param:`html_voids_re`
          has been replaced by ``'/>'`` to make a self-closing tag, and any other
          self-closing tag is replaced with a non-self-closing tag.

    :param tailless:
        **If ``True``, the tail of the element is set to ``None``.**
        The tail is any text after the closing tag of the element up to the
        next opening or closing tag.
    :param inner: If ``True``, the tags of the element itself are not included
        **and :param:`tailless` is set to ``True``**.
    :param short_empty_elements: has the same meaning as in
        :meth:`xml.etree.ElementTree.tostring`
        and is set to ``False`` if :param:`method` is ``'htmlxml'``.
    :param voids_re: matching the strings which are to be substituted by
        ``'/>'`` in order to make self-closing tags for the HTML void elements.
        This is done if :param:`method` is ``'htmlxml'``.
        The default value matches the elements named in :const:`HTML_VOIDS`.
    :param inner_re: used if :param:`inner` is ``True``; its first group
        matches the sequence between the rightmost and the leftmost tag.
    '''
    if inner:
        tailless = True
    if tailless:
        element = deepcopy(element)
        element.tail = None
    if method == 'htmlxml':
        method = 'xml'
        short_empty_elements = False
        ensure_voids = True
    else:
        ensure_voids = False
    text = ET.tostring(element,
            encoding = 'unicode',
            method = method,
            short_empty_elements = short_empty_elements)
    if method != 'text':
        if inner:
            text = inner_re.sub('\g<1>', text)
        if ensure_voids:
            text = html_voids_re.sub('/>', text)
    return text

def elem2table(
        xmlhtml_table: 'Optional[ET.Element]',
        row_xpath: str = 'tbody/tr',
        cell_xpath: str = '*',
        get: 'Callable[[ET.Element], str]' = lambda cell: cell.text or '',
        ) -> 'Deque[Deque[str]]':
    '''
    Convert an table element (:param:`xmlhtml_table`) to a table-like
    deque of deques.

    .. note::
        The conversion is done incrementally and iteratively.

    :param row_xpath: An XPath expression taken to find all rows in
        :param:`xmlhtml_table`.
    :param cell_xpath: An XPath expression taken to find all cells within
        every found row.
    :param get: takes each cell element as argument and returns a value which
        is taken as the cell value in the deque of deques.
        In order to get all inner text of the cell, without tags, use
        ``lambda cell: ET.tostring(
            cell, method = 'text', encoding = 'unicode')``.
        In order to get all inner content of the cell, written as xHTML, use
        ``lambda cell: xmlhtml.elem2string(cell, inner = True)``.

    .. testcode::
        import xml.etree.ElementTree as ET
        import xmlhtml
        xmlhtml_table = ET.fromstring("""<table>
                <tr>
                    <td>a....</td><td>b....</td><td>c....</td><td>d....</td>
                </tr>
                <tr>
                    <td>a....</td><td>e....</td><td>f....</td><td>g....</td><td>h....</td>
                </tr>
                <tr>
                    <td>a....</td><td>e....</td><td>f....</td><td>i....</td><td>j....</td>
                </tr>
                <tr>
                    <td>a....</td><td>e....</td><td>k....</td><td>l....</td>
                </tr>
                </table>""")
        for row in xmlhtml.elem2table(xmlhtml_table, row_xpath = 'tr'):
            print(row)

    .. testoutput::
        deque(['a....', 'b....', 'c....', 'd....'])
        deque(['a....', 'e....', 'f....', 'g....', 'h....'])
        deque(['a....', 'e....', 'f....', 'i....', 'j....'])
        deque(['a....', 'e....', 'k....', 'l....'])
    '''
    table = deque()
    if xmlhtml_table:
        for row in xmlhtml_table.iterfind(row_xpath):
            table.append(deque())
            for cell in row.iterfind(cell_xpath):
                table[-1].append(html.unescape(get(cell)))
    return table

def elem2tree(
        xmlhtml_table: 'Optional[ET.Element]',
        row_xpath: str = 'tbody/tr',
        cell_xpath: str = '*',
        get: 'Callable[[ET.Element], str]' = lambda cell: cell.text or '',
        Dict: type = OrderedDict,
        leaveless: bool = False,
        ) -> 'Dict[str, Union[str, Dict]]':
    '''
    Convert an table element (:param:`xmlhtml_table`) to a tree-like
    dictionary of dictionaries.

    .. note::
        The conversion is done incrementally and iteratively.

    :param row_xpath: An XPath expression taken to find all rows in
        :param:`xmlhtml_table`.
    :param cell_xpath: An XPath expression taken to find all cells within
        every found row.
    :param get: See :func:`.elem2table`.
    :param Dict: specifies the kind of dictionaries: Choose
        `collections.OrderedDict`, if the original order of rows is to be
        preserved in the tree, choose `dict` else.
    :param leaveless: speficies the kind of tree: Choose :bool:`False`,
        if a branch shall end in ``{last_column: {}}``, and :bool:`True`,
        if a branch shall end in ``{prelast_column: last_column}``.

    .. testcode::
        import xml.etree.ElementTree as ET
        from pprint import pprint
        import xmlhtml
        xmlhtml_table = ET.fromstring('<table>\
                <tr>\
                    <td>a....</td><td>b....</td><td>c....</td><td>d....</td>\
                </tr>\
                <tr>\
                    <td>a....</td><td>e....</td><td>f....</td><td>g....</td><td>h....</td>\
                </tr>\
                <tr>\
                    <td>a....</td><td>e....</td><td>f....</td><td>i....</td><td>j....</td>\
                </tr>\
                <tr>\
                    <td>a....</td><td>e....</td><td>k....</td><td>l....</td>\
                </tr>\
                </table>')
        pprint(xmlhtml.elem2tree(xmlhtml_table, row_xpath = 'tr',
               Dict = dict))

    .. testoutput::
        {'a....': {'b....': {'c....': 'd....'},
                   'e....': {'f....': {'g....': 'h....', 'i....': 'j....'},
                             'k....': 'l....'}}}
    '''
    tree = Dict()
    if xmlhtml_table is not None:
        if leaveless:
            for row in xmlhtml_table.iterfind(row_xpath):
                subtree = tree
                for cell in row.iterfind(cell_xpath):
                    subtree = subtree.setdefault(get(cell), Dict())
        else:
            for row in xmlhtml_table.iterfind(row_xpath):
                subtree = tree
                cells = row.findall(cell_xpath)
                for cell in cells[:-2]:
                    subtree = subtree.setdefault(get(cell), Dict())
                subtree[get(cells[-2])] = get(cells[-1])
    return tree

def escape(text: str, quote: bool = True) -> str:
    '''
    Escape :param:`text` like :func:`html.escape`, just without escaping
    the apostrophe. So this cannot be used for an apostrophe-delimited
    attribute value.
    '''
    text = text.replace('&', '&amp;').replace('<', '&lt;').replace('>', '&gt;')
    if quote:
        text = text.replace('"', '&quot;')
    return text

def fragmentize(
        doc: str,
        id_attr: str = '',
        no_repeat_attrs: 'Set[str]' = {'id', 'name', 'xml:id'},
        ) -> str:
    '''
    Convert milestone ranges in :param:`doc` into fragment elements
    complying with the OHCO restriction.

    The milestones must be given as in :func:`milestone` (see the
    docstring of this function), e.g.::

        <s315-span dir="rtl"/> ... <e315-span dir="rtl"/>

    .. note::
        Attributes in the ending milestone are not used nor needed.

    If :param:`id_attr` is not the empty string, the ID in the old tag name
    (e.g. `315` in `s315-span`) is included in the fragment elements as the
    value of the attribute :param:`id_attr` (e.g. `data-id` for HTML5).
    So this attribute allows to recognize the fragment elements of one
    virtual element.

    The tag name in the milestone tag name (e.g. `span` in `s315-span`)
    is taken as new tag name. The attributes in the starting milestone
    are repeated in every following fragment element except the ones
    mentioned in :param:`no_repeat_attrs`: These are included only in
    the first fragment element.

    .. important::
        - Non-self-closing tags will be ignored and omitted.
        - Self-closing tags not shaped as said milestone tags will
          throw an error or lead to an undefined result.
        - In the result, all void elements are represented as a
          starttag immediately followed by the belonging endtag.
          Self-closing tags are not used.

    .. important::
        Before the fragments are made, :param:`doc` may be changed.
        See the docstring of :class:`Itemizer`.

    .. testcode::
        import xmlhtml
        doc = "<s0_i/>This is italic <s1_b/>and<e0_i/> this is bold<e1_b/>."
        print(xmlhtml.fragmentize(doc, id_attr = ''))

    .. testoutput::
        <i>This is italic <b>and</b></i><b> this is bold</b>.
    '''
    parser = Itemizer()
    parser.parse(doc)
    items = deque()
    open_tags = deque()
    while parser.items:
        start, attrs, end, text, com, pi, decl, undecl = parser.items.popleft()
        if text:
            items.append(['', [], '', text, '', '', '', ''])
        elif start and end:
            ident, tag_name = start[1:].split('_', 1)
            if start[0] == 's':
                if id_attr:
                    attrs += [(id_attr, ident)]
                items.append([tag_name, attrs, '', '', '', '', '', ''])
                open_tags.append((
                        ident,
                        tag_name,
                        [ (k, v) for k, v in attrs if k not in no_repeat_attrs ]
                        ))
            else:
                temp = deque()
                while open_tags:
                    pre_ident, pre_tag_name, pre_attrs = open_tags.pop()
                    if ident == pre_ident:
                        items.append(['', [], tag_name, '', '', '', '', ''])
                        break
                    else:
                        items.append(['', [], pre_tag_name, '', '', '', '', ''])
                        temp.appendleft((pre_ident, pre_tag_name, pre_attrs))
                for ident, tag_name, attrs in temp:
                    items.append([tag_name, attrs, '', '', '', '', '', ''])
                    open_tags.append((ident, tag_name, attrs))
    return serialize(items)

def get_text(element: ET.Element, xpath: str = '.', namespaces = None) -> str:
    '''
    In :param:`element`, search with :param:`xpath` and return the text of
    the first matching element or the empty string. The default value of
    :param:`xpath` refers to :param:`element` itself.
    '''
    match = element.find(xpath, namespaces = namespaces)
    return '' if match is None else (match.text or '')

def indent(element: ET.Element, level: int = 0, indenter: str = '\t') -> None:
    '''
    Attribution notice: By Fredrik Lundh; minor changes by Stefan Müller.
    See http://effbot.org/zone/element-lib.htm#prettyprint.
    '''
    i = "\n" + level * indenter
    if len(element):
        if not element.text or not element.text.strip():
            element.text = i + indenter
        if not element.tail or not element.tail.strip():
            element.tail = i
        for element in element:
            indent(element, level + 1)
        if not element.tail or not element.tail.strip():
            element.tail = i
    else:
        if level and (not element.tail or not element.tail.strip()):
            element.tail = i

def itemize(
        doc: str,
        with_id: bool = False,
        balance: bool = True,
        ) -> '''Union[
        Deque[List[str, List[Tuple[str, str]], str, str, str, str, str, str]],
        Deque[List[str, List[Tuple[str, str]], str, str, str, str, str, str, int]]
        ]''':
    '''
    Parse the (x)HTML document :param:`doc` into a deque of items and
    return them. This is a convenience function for :class:`Itemizer`
    or, if :param:`with_id` is ``True``, for :class:`ItemizerID`.
    See the docstrings of these classes for further details.

    .. testcode::
        import xmlhtml

        doc = """
                <tr><td><p align="left">
                  paragraph&apos;s text<br/>
                  <a></a>paragraph&#x2019;s text</tr>
                </non-match>end."""

        for item in xmlhtml.itemize(doc):
            print(item)

    .. testoutput::
        ['', [], '', '\n                ', '', '', '', '']
        ['tr', [], '', '', '', '', '', '']
        ['td', [], '', '', '', '', '', '']
        ['p', [('align', 'left')], '', '', '', '', '', '']
        ['', [], '', "\n                  paragraph's text", '', '', '', '']
        ['br', [], 'br', '', '', '', '', '']
        ['', [], '', '\n                  ', '', '', '', '']
        ['a', [], '', '', '', '', '', '']
        ['', [], 'a', '', '', '', '', '']
        ['', [], '', 'paragraph’s text', '', '', '', '']
        ['', [], 'p', '', '', '', '', '']
        ['', [], 'td', '', '', '', '', '']
        ['', [], 'tr', '', '', '', '', '']
        ['', [], '', '\n                end.', '', '', '', '']
    '''
    if with_id:
        parser = ItemizerID()
    else:
        parser = Itemizer(balance)
    parser.parse(doc)
    return parser.items

def milestone(
        doc: str,
        ) -> '''Deque[List[
                           str,
                           List[Tuple[str, str]],
                           str,
                           str,
                           str,
                           str,
                           str,
                           str]]''':
    '''
    Convert the elements in :param:`doc` into milestone ranges.
    A milestone is made from every starttag and endtag as follows:

    - The milestone is a self-closing tag.
    - The tag name starts with ``'s'``, if it is a starting milestone.
    - The tag name starts with ``'e'``, if it is an ending milestone.
    - After this first letter, there is a number serving as the ID for
      the milestone range, i.e. connecting the opening and the closing
      milestone. (The number is incremented with every element.)
    - After the ID, there is a hyphen and then the old tag name of the
      starttag.
    - The attributes given in the old starttag are preserved both in the
      opening and in the closing milestone and ordered alphanumerically.
    - But if attributes in the same starttag share the same name (key),
      which is not well-formed XML or HTML anyway, only one of them is kept.
    - If the old element is a self-closing one already, the outcome is
      the same as in the case of a void element, i.e. the opening
      milestone is immediately followed by its closing counterpart.
      That is done so that every opening milestone has exactly one
      closing counterpart.

    The result is a deque as described in :class:`Itemizer` and can
    be serialized into a string by :func:`serialize`.

    .. important::
        - All tags and attribute names are inevitably converted to lower case!
        - An attribute without a value (a boolean attribute) is read with
          the value being ``None`` and is written with the value being ``''``.
        - Missing endtags are inserted at the last possible place, i.e.,
          either at the very end of the input or immediately before an endtag
          occurs whose opening counterpart occured before the starttag whose
          closing counterpart is missing.
        - Endtags not (or: no longer) matching a starttag are omitted.

    .. note::
        To justify the format: In a tag name of a milestone, the letter
        differentiating between an opening or a closing milestone comes first,
        because a tag name must not start with a digit. Then the ID precedes
        the old tag name in order to be easier to find (e.g. the old tag name
        may have contained an underscore).

    .. testcode::
        import xmlhtml
        doc = """<p dir="ltr">paragraph&apos;s <i>text</i></p>"""
        print(xmlhtml.serialize(xmlhtml.milestone(doc)))

    .. testoutput::
        <s0_p dir="ltr"/>paragraph's <s1_i/>text<e1_i/><e0_p dir="ltr"/>
    '''
    parser = Itemizer()
    parser.parse(doc)
    items = deque()
    open_tags = deque()
    i = 0
    while parser.items:
        start, attrs, end, text, com, pi, decl, undecl = parser.items.popleft()
        if start:
            if attrs:
                attrs = sorted(dict(attrs).items())
            tag = '{i}_{start}'.format(i = i, start = start)
            i += 1
            items.append(['s' + tag, attrs, '_', '', '', '', '', ''])
            if end:
                items.append(['e' + tag, attrs, '_', '', '', '', '', ''])
            else:
                open_tags.append(('e' + tag, attrs))
        elif end:
            tag, attrs = open_tags.pop()
            items.append([tag, attrs, '_', '', '', '', '', ''])
        elif text:
            items.append(['', [], '', text, '', '', '', ''])
    return items

def normalize(
        doc: str,
        replace_bracket_declaration: 'Callable[[str, dict], str]'
            = lambda decl, info: '',
        replace_comment:             'Callable[[str, dict], str]'
            = lambda comment, info: '',
        replace_declaration:         'Callable[[str, dict], str]'
            = lambda decl, info: '',
        replace_processing_instruct: 'Callable[[str, dict], str]'
            = lambda pi, info: '',
        replace_element: '''Callable[
                [str, List[Tuple[str, Optional[str]]], bool, dict],
                Optional[str]]'''
            = lambda tag, attrs, startend, info: None,
        replace_tag: '''Callable[
                [str, List[Tuple[str, Optional[str]]], bool, dict],
                 str, List[Tuple[str, Optional[str]]], bool]'''
            = lambda tag, attrs, startend, info:
                (tag, sorted(dict(attrs).items()), tag in HTML_VOIDS),
        replace_text: 'Callable[[str, dict], str]'
            = lambda text, info: text,
        consider_endtag: 'Optional[Callable[[str, str, dict]]]'
            = None,
        info: dict = {},
        ) -> str:
    '''
    Normalize the HTML document :param:`doc`:

    Tags (along with any attributes) and escaping is handled as in
    :func:`serialize` (see the docstring of this function).

    Further on, if the remaining keyword arguments are left with their
    default value:

    - The following items are deleted:
      Comments, declarations, bracket declarations, processing instructions.
    - All tags listed in :const:`HTML_VOIDS` are treated as follows:
      `<br>` and `<br></br>` are changed to `<br/>`.
    - Attributes are ordered alphanumerically.
    - If attributes of the same element share the same name (key), only
      one of them is kept.

    .. note::
        A root element is not added here. If you ensure that yourself,
        it is possible to parse the result with an XML parser.

    These and further manipulations can be specified by passing customized
    callables to this function. See the docstring of :meth:`Replacer.__init__`.

    .. important::
        - All tags and attribute names are inevitably converted to lower case!
        - An attribute without a value (a boolean attribute) is read with
          the value being ``None`` and is written with the value being ``''``.
        - Missing endtags are inserted at the last possible place, i.e.,
          either at the very end of the input or immediately before an endtag
          occurs whose opening counterpart occured before the starttag whose
          closing counterpart is missing.
        - Endtags not (or: no longer) matching a starttag are omitted.
    '''
    replacer = Replacer(
            replace_bracket_declaration = replace_bracket_declaration,
            replace_comment             = replace_comment,
            replace_declaration         = replace_declaration,
            replace_processing_instruct = replace_processing_instruct,
            replace_element             = replace_element,
            replace_tag                 = replace_tag,
            replace_text                = replace_text,
            consider_endtag             = consider_endtag,
            info                        = info,
            )
    replacer.parse(doc)
    return ''.join(replacer.results)

def serialize(
        items: '''Union[
        Deque[List[str, List[Tuple[str, str]], str, str, str, str, str, str]],
        Deque[List[str, List[Tuple[str, str]], str, str, str, str, str, str, int]]
        ]''',
        ) -> str:
    '''
    Serialize :param:`items` into a document with markup and escaping.

    :param:`items` is a deque as described in :class:`Itemizer` (with 8 items)
    or as described in :class:`ItemizerID` (with 9 items).

    - The following characters are escaped in all text nodes and in all
      attribute values:

      - `<` as `&lt;`.
      - `>` as `&gt;`.
      - `&` as `&amp;`.
      - `"` as `&quot;`.

    - All other characters are given unescaped.
    - All attribute values are delimited by `"`.
    - The value of an otherwise valueless attribute is the empty string.
    - Within tags and outside attribute values, whitespace is not used
      except from one normal space (Unicode 0x20) before any attribute:

      - `<tag>`
      - `<tag/>`
      - `</tag>`
      - `<tag key="value" key2="value">`
      - `<tag key="value" key2="value"/>`

    .. warning::
        - The XML declaration and XML processing instructions end with `?>`,
          whereas HTML processing instructions end with `>`. Therefore the
          end markup provided here is just `>`. If the target is XML, the
          given item itself must already end with `?`.
        - Some special parts of an XML or HTML document unfortunately do not
          allow an escaping. An item to become one of those parts must not
          contain characters making the right border of the part ambiguous:

          - A comment must not contain `-->`.
            If it does, an :class:`EscapeError` is raised.
          - A `<![...]>` part (i.e. CDATA) must not contain `]>`.
            If it does, an :class:`EscapeError` is raised.
          - A `<?...?>` part (i.e. an XML declaration or an XML processing
            instruction) must not contain `?>`.
            If it does, an :class:`EscapeError` is raised.
          - A `<?...> part (i.e. an HTML processing instruction) must not
            contain `>`.
            If it does, an :class:`EscapeError` is raised.
          - A `<!...>` part (i.e. a declaration) must not contain `>` that
            is not balanced by a `<` and not part of an attribute value.
            If it does, an :class:`EscapeError` is raised.
    '''
    return ''.join(serialize_items(items))

def serialize_items(
        items: '''Union[
        Deque[List[str, List[Tuple[str, str]], str, str, str, str, str, str]],
        Deque[List[str, List[Tuple[str, str]], str, str, str, str, str, str, int]]
        ]''',
        ) -> '''Deque[str]''':
    '''
    Serialize each item of :param:`items` into a string with markup and escaping.
    For details of the serialization, see :func:`serialize`.
    '''
    serialized_items = deque()
    while items:
        start, attrs, end, text, com, pi, decl, undecl, *_ = items.popleft()
        if start:
            attrs = (
                    ''.join(
                        ''' {key}="{value}"'''.format(
                            key = key, value = escape(value or ''))
                        for key, value in attrs )
                    ) if attrs else ''
            item = '<{start}{attrs}{slash}>'.format(
                    start = start, attrs = attrs, slash = "/" if end else "")
        elif end:
            item = '</{end}>'.format(end = end)
        elif text:
            item = escape(text, quote = False)
        elif com:
            if '-->' in com:
                raise CommentEscapeError(com)
            item = '<!--{com}-->'.format(com = com)
        elif pi:
            if pi[-1] == '?' and '?>' in pi:
                raise DeclarationInstructionEscapeError(pi)
            elif '>' in pi:
                raise ProcessingInstructionEscapeError(pi)
            item = '<?{pi}>'.format(pi = pi)
        elif decl:
            if '>' in decl:
                temp = re.sub(r'''("[^"]*"|'[^']*')''', '', decl)
                if temp.count('<') != temp.count('>'):
                    raise DeclarationEscapeError(decl)
            item = '<!{decl}>'.format(decl = decl)
        elif undecl:
            if ']>' in undecl:
                raise UnknownDeclarationEscapeError(undecl)
            item = '<![{undecl}]>'.format(undecl = undecl)
        else:
            item = ''
        serialized_items.append(item)
    return serialized_items

def serialize_tree(
        tree: '''
            Deque[Union[
                str,
                Container[str],
                List[str, Dict[str, str], Deque]
                ]]''',
        all_void: bool = True,
        voids: 'Set[str]' = HTML_VOIDS,
        check: bool = False,
        ) -> str:
    '''
    Serialize :param:`tree` to an XML or HTML-XML string. This is useful for
    the case that you build a data structure within a Python program and want
    to convert the structure into XML or HTML.

    :param tree: has the following structure *S*::

        A list or deque (maybe empty) of items of the following types:
            string: normal text content, will be escaped.
            Or (rarely)
            container of one string: This string will be inserted as-is and should
                be used for all kinds of declarations, instructions, comments.
                Escaping and delimiters must already be correct. Note:
                In XML, processing instructions and the documenttype declaration
                end with :xml:`?>`. In HTML, they end just with :html:`>`.
            Or
            list or tuple of three items: representing an element:
                A string: the tag name.
                A dictionary (maybe empty): attributes.
                A list or deque: inner content: has the structure *S* again.

    :param all_void: If ``True``, all empty elements are written as self-closing
        like :xml:`<tag/>`. Else, only the elements in :param:`voids` are.
    :param voids: See :param:`all_void`.
    :param check: If ``True``, check whether the result is well-formed XML.
        Issues may be:

        - :param:`tree` did not contain exactly one root element.
        - Tag names or attribute names did contain invalid characters,
          e.g. whitespace.
        - A string of the already escaped strings was not well-formed,
          e.g. a comment did not end with :xml:`-->`.

    .. testcode::
        import xmlhtml
        tree = [
                ('root', {}, [
                    'Siehe diesen ',
                    ('a', {'href': 'http://example.org'}, [
                        'Beispiellink'
                        ]),
                    '!',
                    ])
               ]
        print(serialize_tree(tree))

    .. testoutput::
        <root>Siehe diesen <a href="http://example.org">Beispiellink</a>!</root>
    '''
    doc = deque()
    while tree:
        item = tree.pop()
        if isinstance(item, str):
            doc.appendleft(escape(item, quote = False))
        elif len(item) == 1:
            doc.appendleft(item[0])
        else:
            tag, attrs, inner = item
            start = '<' + tag + ''.join(
                    ' ' + k + '="' + escape(v) + '"' for k, v in attrs.items() )
            if inner:
                doc.extendleft(('>', tag, '</'))
                tree.append((start + '>',))
                tree.extend(inner)
            else:
                if all_void or tag in voids:
                    doc.extendleft(('/>', start))
                else:
                    doc.extendleft(('>', tag, '></', start))
    doc = ''.join(doc)
    if check:
        ET.fromstring(doc)
    return doc

def strip_namespaces(element: ET.Element) -> None:
    for subelement in element.iter():
        # NB: The first subelement is the element itself.
        subelement.tag = subelement.tag.split('}')[-1]

def table2tags(
        table: 'Iterable[Iterable[str]]',
        table_attrs: 'Dict[str, str]' = {},
        first_row_is_header: bool = False,
        table_wrap: bool = True,
        rows_wrap: bool = False
        ) -> 'Generator[str]':
    '''
    Convert :param:`table` to an HTML-XML-table. Yield the table cell by cell
    for the ``'td'`` elements and tag by tag for the surrounding elements.

    :param table_attrs: If not empty, its key-value-pairs will be included as
        attributes in the `<table>` tag.
    :param first_row_is_header: If true, the cells of the first row are tagged
        with `<th>` instead of `<td>`, and the first row is enclosed by a
        `<thead>` tag.
    :param table_wrap: If true, the output table is wrapped, so that
        each row starts on a new line.
    :param rows_wrap: If true, the rows of the output table are wrapped,
        so that each cell starts on a new line.
    '''
    prerow = (
            '\n' if rows_wrap else
            '\n\t' if table_wrap else
            '')
    postrow = '\n' if rows_wrap else ''
    precell = '\n\t' if rows_wrap else ''
    table = iter(table)
    yield '<table{}>'.format(
            ''.join( ' {}="{}"'.format(key, escape(value))
                for key, value in table_attrs.items() ) if table_attrs else
            '')
    if first_row_is_header:
        yield '\n<thead>'
        yield prerow + '<tr>'
        for cell in next(table):
            yield '{}<th>{}</th>'.format(precell, escape(str(cell)))
        yield postrow + '</tr>'
        yield '\n</thead>'
    yield '\n<tbody>'
    for row in table:
        yield prerow + '<tr>'
        for cell in row:
            yield '{}<td>{}</td>'.format(precell, escape(str(cell)))
        yield postrow + '</tr>'
    yield '\n</tbody>'
    yield '\n</table>'

def table2tree(
        table: 'Iterable[Iterable[str]]',
        Dict: type = OrderedDict,
        leaveless: bool = False,
        ) -> 'Dict[str, Union[str, Dict]]':
    '''
    Convert :param:`table` to a tree-like dictionary of dictionaries.

    .. note::
        The conversion is done incrementally and iteratively.

    :param Dict: specifies the kind of dictionaries: Choose
        `collections.OrderedDict`, if the original order of rows is to be
        preserved in the tree, choose `dict` else.
    :param leaveless: speficies the kind of tree: Choose :bool:`False`,
        if a branch shall end in ``{last_column: {}}``, and :bool:`True`,
        if a branch shall end in ``{prelast_column: last_column}``.

    .. testcode::
        import xml.etree.ElementTree as ET
        from pprint import pprint
        import xmlhtml
        table = deque([
                deque(['a....', 'e....', 'f....', 'g....', 'h....']),
                deque(['a....', 'e....', 'f....', 'i....', 'j....']),
                deque(['a....', 'e....', 'k....', 'l....']),
                deque(['a....', 'b....', 'c....', 'd....']),
                ])
        pprint(xmlhtml.table2tree(d, Dict = dict))

    .. testoutput::
        {'a....': {'b....': {'c....': 'd....'},
                   'e....': {'f....': {'g....': 'h....', 'i....': 'j....'},
                             'k....': 'l....'}}}
    '''
    tree = Dict()
    if table:
        if leaveless:
            for row in table:
                subtree = tree
                for cell in row:
                    subtree = subtree.setdefault(cell, Dict())
        else:
            for row in table:
                subtree = tree
                while len(row) > 2:
                    subtree = subtree.setdefault(row.popleft(), Dict())
                subtree[row[-2]] = row[-1]
    return tree

def taggify(
        tag_name: str,
        attrs: 'Iterable[Tuple[str, Optional[str]]]' = [],
        end: bool = False,
        startend: bool = False,
        ) -> str:
    '''
    Convert the given :param:`tag_name` and :param:`attrs` to an
    (X)HTML tag string. :param:`attrs` may be empty.

    If :param:`tag_name` is empty, the empty string is returned.
    If :param:`end` is :bool:`True`, a closing tag is returned,
    if :param:`startend` is :bool:`True`, a self-closing tag is
    returned, else an opening tag.

    Tags (along with any attributes) and escaping is handled as in
    :func:`serialize` (see the docstring of this function).
    '''
    if tag_name:
        if end:
            return '</{tag_name}>'.format(tag_name = tag_name)
        attrs = (
                ''.join(
                    ''' {key}="{value}"'''.format(
                        key = key, value = escape(value or ''))
                    for key, value in attrs )
                ) if attrs else ''
        return '<{tag_name}{attrs}{slash}>'.format(
                    tag_name = tag_name,
                    attrs = attrs,
                    slash = "/" if startend else "")
    return ''

def tree2table(tree: 'Dict[str, Union[str, Dict]]') -> 'Deque[Deque[str]]':
    '''
    Convert a tree (:param:`tree`) to a table-like deque of deques.
    The tree is a dictionary of dictionaries. The dictionaries may
    be of the type `OrderedDict` in order to preserve an original
    order of the rows.

    .. note::
        The conversion is done incrementally and iteratively.

    .. testcode:
        import xmlhtml
        tree = {'a....': {'b....': {'c....': 'd....'},
                          'e....': {'f....': {'g....': 'h....',
                                              'i....': 'j....'},
                                    'k....': 'l....'}}}
        for row in xmlhtml.tree2table(tree):
            print(row)

    .. testoutput:
        deque(['a....', 'e....', 'f....', 'g....', 'h....'])
        deque(['a....', 'e....', 'f....', 'i....', 'j....'])
        deque(['a....', 'e....', 'k....', 'l....'])
        deque(['a....', 'b....', 'c....', 'd....'])
    '''
    table = deque()
    row = deque()
    trees = deque()
    level = 0
    trees.appendleft((level, tree))
    while trees:
        level = trees[0][0]
        key, value = trees[0][1].popitem()
        if not trees[0][1]:
            trees.popleft()
        row.append(key)
        if value and isinstance(value, dict):
            trees.appendleft((level + 1, value))
        else:
            if value:
                row.append(value)
            table.appendleft(row)
            row = deque()
            if trees:
                for i in range(trees[0][0]):
                    row.append(table[0][i])
    return table
