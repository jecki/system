% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
% lang_id = dao.default_lang_id
% page_id = request.path[1:]
% text_lang_id = kwargs.get('text_lang_id', 'en')
% is_default_page = bool(page_id == dao.default_page_id)
% query = request.query_string
% query = '?' + query if query else ''
% entry = kwargs['entry']
% cat_lang = entry['cat_lang']
% pagina = kwargs['pagina']
% scan_path = entry['scan_path']
% tran = entry['tran']
% work = entry['work']
% volume = ', '.join( entry[key] for key in ('place_name', 'lib_name', 'ms_shelfmark', 'print_publisher', 'print_date', 'work_title') if key in entry )
% img_scale = kwargs['img_scale']
% rtl = 'rtl' if cat_lang == 'Arabic' else ''
<header{{!' dir="rtl"' if rtl else ''}}>
	<h1 style="display:flex;flex-wrap:wrap;justify-content:space-between"><a href="/work/{{work['work_id']}}">{{'' if 'Anonymous' in work['person_name'] else work['person_name'] + ', '}}{{!dao.format_title(work['work_title'])}}</a><a class="key" href="/start">Start</a></h1>
	<div class="flexbase">
		<h2><a href="/{{entry['doc_type']}}/{{entry['doc_id']}}" rel="chapter">{{volume}}</a>&#xa0;· <strong style="display:inline-block;width:1.5em">{{!entry['pagina_styled']}}</strong></h2>
		<nav class="flexbase">
			<div class="droparea" tabindex="0">
				<a class="key arrowdown">Scale</a>
				<div class="droparea shadow" style="width:100%;">
				  % for new_img_scale in (0.4, 0.6, 0.8, 1, 1.2, 1.4, 1.6, 1.8, 2, 2.5, 3, 3.5, 4, 4.5, 5):
					% if new_img_scale == img_scale:
					<strong>{{round(100 * img_scale)}}</strong>
					% else:
					<a class="key" href="?img_scale={{new_img_scale}}">{{round(100 * new_img_scale)}}</a>
					% end
				  % end
				</div>
			</div>
			<div class="droparea" tabindex="0">
				<a class="key arrowdown">Select a page</a>
				<div class="droparea shadow" style="font-size:0.9em;max-width:60vw;width:400%;">
				  % for new_pagina, new_pagina_styled in entry['paginas']:
					% if new_pagina == pagina:
					<strong>{{!new_pagina_styled}}</strong>
					% else:
					<a class="key" href="/{{entry['doc_type']}}/{{entry['doc_id']}}/{{entry['part_id']}}/{{new_pagina}}{{query}}">{{!new_pagina_styled}}</a>
					% end
				  % end
				</div>
			</div>
			% include('repro_nav.tpl', entry = entry, pagina = pagina, rtl = rtl)
		</nav>
	</div>
</header>
<main class="flexstart flush"{{!' dir="rtl"' if rtl else ''}}>
  % if scan_path:
	<img id="image" class="shadow" src="/repros/{{scan_path}}" alt="... Loading: {{volume}} · {{entry['pagina_styled']}} ..." style="height:{{img_scale * 90}}vh"/>
  % end
  % if tran:
	<article id="transcription" class="card" dir="ltr"><!-- Because in the transcription, the direction must be set case-by-case. -->
		<nav class="flexbase"{{!' dir="rtl"' if rtl else ''}}>
			% include('repro_nav.tpl', entry = entry, pagina = pagina, rtl = rtl)
		  % if entry['fulltran_part']:
			<a class="key" href="/{{entry['doc_type']}}/{{entry['doc_id']}}/{{entry['part_id']}}/transcription/{{entry['fulltran_part']}}#{{pagina}}">Full&#xa0;text</a>
		  % end
		</nav>
		{{!tran}}
		<nav class="flexbase"{{!' dir="rtl"' if rtl else ''}}>
			% include('repro_nav.tpl', entry = entry, pagina = pagina, rtl = rtl)
		  % if entry['fulltran_part']:
			<a class="key" href="/{{entry['doc_type']}}/{{entry['doc_id']}}/{{entry['part_id']}}/transcription/{{entry['fulltran_part']}}#{{pagina}}">Full&#xa0;text</a>
		  % end
		</nav>
	</article>
  % end
</main>
<script>
$(document).ready(function() {
	highlight();
	$('#transcription').draggable();
	$('#image').draggable();
	document.getElementById('image').addEventListener('wheel', zoom);
})
</script>

% rebase('base_b.tpl', request = request, dao = dao, kwargs = kwargs)