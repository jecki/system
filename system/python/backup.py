# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2016 ff. (© http://badw.de)
'''
Infinite loop saving files at certain time intervals and
thinning them out after certain time intervals.
'''
import os
import subprocess
import time
from collections import deque
from datetime import datetime

import file_io

DATEFORMAT = '%Y-%m-%dT%H%M%S'

def run_backup(
        command_sans_filename: 'Iterable[str]',
        backupdirpath: str,
        seconds_between_backups: int = 3600, # 1 hour
        thin_out_seconds: 'Dict[int, int]' = {
            86400: 21600,
            1036800: 345600,
            2073600: 1036800,
            3110400: 2764800,
            4147200: 3110400,
            5184000: 5184000,
            10368000: 10368000,
            31104000: 31104000,
            },
        ) -> None:
    '''
    Do a backup with :param:`command_sans_filename`, i.e. a list of arguments
    which are executable by :func:``subprocess.call`` and make a backup,
    e.g. an SQL dump, and store it in a file in :param:`backupdirpath`.
    The basename of the file is a time stamp produced while the backup
    is taken. In this regard, it is important to note:

    .. important::
        At least one argument of :param:`command_sans_filename` must contain
        :str:`{}` instead of the **basename of the file**.
        Any :str:`{}` in any argument will be replaced by a time stamp for
        naming each backup. Do not use :str:`{}` except for this purpose.
        Do not use :str:`{}` more than once in each argument.

    An example commmand:
    ``['mysqldump', 'DB', '--user=YYY', '--password=ZZZ', '--result-file={}']``

    Do the backup every :param:`seconds_between_backups` in an endless loop.

    Thin out older backups according to :param:`thin_out_seconds`. Its keys
    and values are durations in seconds and best understood with the help
    of the given default, which means:

    - After 1 day, keep only backups with a distance of at least 6 hours.
    - After 12 days, keep only backups with a distance of at least 4 days.
    - After 24 days, keep only backups with a distance of at least 12 days.
    - After 36 days, keep only backups with a distance of at least 24 days.
    - After 48 days, keep only backups with a distance of at least 36 days.
    - After 60 days, keep only backups with a distance of at least 60 days.
    - After 120 days, keep only backups with a distance of at least 120 days.
    - After 360 days, keep only backups with a distance of at least 360 days.

    After each thin-out, yield the names of the backup files still saved.
    '''
    dateformat = DATEFORMAT
    while True:
        current_date = datetime.now()
        backupfilepath = file_io.pave(file_io.get_new_path(
                os.path.join(backupdirpath, current_date.strftime(dateformat))))
        backupdirpath, backupfilename = os.path.split(backupfilepath)
        os.chdir(backupdirpath)
        subprocess.call(
                [ arg.format(backupfilename) for arg in command_sans_filename ])
        for path in thin_out_files(
                backupdirpath,
                dateformat,
                thin_out_seconds,
                ):
            try:
                os.remove(path)
            except: # Sic.
                pass
        while True:
            time.sleep(1)
            if ((datetime.now() - current_date).total_seconds() >=
                    seconds_between_backups):
                break

def thin_out_files(
        dirpath: str,
        dateformat: str,
        thin_out_seconds: 'Dict[int, int]',
        ) -> 'Deque[str]':
    '''
    In :param:`dirpath`, parse the basename of every file as a date formatted
    according to :param:`dateformat`. (If this fails, skip the file.)
    Return all filepaths which are to be removed according to
    :param:`thin_out_seconds`. (For this parameter, see :func:`run_backup`.)
    The oldest backup is never returned.
    If there are less than two files, an empty sequence is returned.
    '''
    names = os.listdir(dirpath)
    for i in range(len(names) - 1, -1, -1):
        try:
            date = datetime.strptime(os.path.splitext(names[i])[0], dateformat)
        except:
            del names[i]
        else:
            names[i] = (date, names[i])
    if len(names) < 2:
        return []
    names.sort(reverse = True)
    # The oldest backup is never returned.
    # Its date is the starting point to measure the distances of backups.
    older = names.pop()[0]
    # The date of the newest backup is the fixed point to measure the ages.
    newest = names[0][0]
    to_be_removed_paths = deque()
    while names:
        date, name = names.pop()
        current_age = (newest - date).total_seconds()
        current_distance = (date - older).total_seconds()
        if all(
                (current_age <= age or current_distance >= distance)
                for age, distance in thin_out_seconds.items() ):
            older = date
        else:
            to_be_removed_paths.append(os.path.join(dirpath, name))
    return to_be_removed_paths
