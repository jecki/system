% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
<main>
<article class="flush_unhyph sheet sheetwide">
{{!kwargs['project']['project_description']}}
{{!kwargs['project']['project_corpus']}}
</article>
</main>\\
% rebase('base.tpl', request = request, dao = dao, kwargs = kwargs)