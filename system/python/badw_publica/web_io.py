# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
import os
from datetime import datetime
from html import unescape

import __init__
import bottle
from bottle import HTTPError, redirect, request, response, static_file, template
import file_io
from file_io import log_ip_medium

class App(bottle.Bottle):
    '''
    A bottle application object.
    See the docstring of :func:`bottleapp.get` in :path:`../../bottleapp.py`.
    '''
    def __init__(
            self,
            dao_class: type,
            dao_kwargs: 'Dict[str, Any]',
            https: bool,
            host: str,
            port: str,
            debug: bool,
            server: str,
            views_paths: 'Sequence[str]',
            cssjs_path: str,
            ):
        super().__init__()
        self.https = https
        self.host = host
        self.port = port
        self.debug = debug
        self.server = server
        self.cssjs_path = os.path.abspath(cssjs_path) + os.sep
        self.views_paths = [ os.path.abspath(p) + os.sep for p in views_paths ]
        bottle.TEMPLATE_PATH = self.views_paths
        self.log = file_io.Log(
                __file__,
                log_folder_path = '../../../../../seite_publikationen_zugriffe',
                log_file_name = \
                    datetime.now().isoformat().replace(':', '').split('.')[0] +
                    '--.log')
        self.dao = dao_class(**dao_kwargs)
        self.set_routes(self.dao)

    def log_access(self, request: 'bottle.LocalRequest', pub_id: str) -> None:
        log_ip_medium(
                self.log,
                request.remote_addr,
                '█' + request.environ.get(
                    'HTTP_USER_AGENT', '').split(None, 1)[0] + '█' + pub_id
                )

    def run(self) -> None:
        '''
        Run :param:`self` by calling :meth:`bottle.run`. Before or in this call,
        you may apply middleware, e.g. instead of mere ``app = self``, pass
        ``app = SessionMiddleware(self, self.dao.session_options)``.
        '''
        bottle.run(
                app = self,
                host = self.host,
                port = self.port,
                debug = self.debug,
                server = self.server(
                    self.host,
                    self.port,
                    **{
                        key:
                            (self.dao.paths.get(name, '') if self.https else '')
                        for key, name in (
                            ('certfile', 'ssl_cert_path'),
                            ('keyfile', 'ssl_key_path'),
                            ('chainfile', 'ssl_chain_path'),
                            )
                        }
                    )
                )

    def set_routes(self, dao) -> None:
        '''
        Define the request-handling functions and assign them to routes.
        '''
        @self.route('<path:path>/')
        def redirect_endslash(path):
            '''
            Redirect requests whose URL ends with `/`: Ignore the trailing `/`.
            '''
            redirect(path)

        @self.route('/')
        def redirect_short_url():
            '''
            Redirect requests which specify no page nor language.
            Assume the default page and the default language of the site.

            The redirected request will be dealt with by :func:`.return_page`.
            Further explanation is found in the docstring of that function.
            '''
            redirect('/{}/meta/{}'.format(
                    dao.default_lang_id, dao.default_page_id))

        @self.route('/cssjs/<path:path>')
        def return_cssjs(path):
            '''
            Return a CSS or JS file for client-side styling or processing.
            For static content files, see :func:`.return_static_content`.

            Example URLs are:

            - `/cssjs/jquery/jquery.js`
            - `/cssjs/normalize/normalize.css`

            The path of the file on the server must be the joint of
            :attr:`self.cssjs_path` and :param:`path`.
            Otherwise, the access is denied.
            '''
            return static_file(path, self.cssjs_path)

        @self.route('/{}/<path:path>'.format(dao.redirected_links_prefix))
        def redirect_redirected_links(path):
            '''
            Redirect certain dead links which are to be reanimated here.

            All of these links start with :attr:`dao.redirected_links_prefix`.
            The rest of the link is stored in :attr:`dao.redirected_links` and
            mapped to the new link. If the rest is not found there, the
            request is redirected to the default page.
            '''
            redirect(dao.redirected_links.get(path, '/'))

        @self.route('/<pub_id>')
        def redirect_short_permalink(pub_id):
            '''
            Redirect assuming the default language.

            The redirected request will be dealt with by :func:`.return_pub`.
            Further explanation is found in the docstring of that function.
            '''
            redirect('/{}/{}'.format(dao.default_lang_id, pub_id))

        @self.route('/<lang_id>/api/oai')
        def return_oai_filedata(lang_id):
            '''
            Return OAI data of the files containing publications.
            Response to the following types of queries:

            - ?verb=ListRecords&from=2017-10&until=2018-02
            - ?verb=GetRecord&identifier=123454321.pdf

            The date information after `from` or `until` must be given in
            the extended format of ISO 6801 with the granularity at least
            of a year and at most of a microsecond. The two dates will be
            understood as inclusive.
            '''
            verb = request.query.verb.lower()
            filename = request.query.identifier
            start = request.query.get('from', '')
            end = request.query.until
            if verb == 'getrecord':
                names = [(os.path.basename(filename),)]
            elif verb == 'listrecords':
                try:
                    names = dao.get_filenames_by_time(start, end)
                except: # Sic.
                    names = []
            doc = '<?xml version="1.0" encoding="UTF-8"?>'
            doc += '\n<collection xmlns="{}">'.format(dao.catalog.xmlns_default)
            for (name,) in names:
                try:
                    with open(
                            dao.temp_bibl_path + name + '.xml', 'r',
                            encoding = 'utf-8') as file:
                        doc += '\n' + file.read()
                except: # Sic.
                    continue
            doc += '</collection>'
            response.content_type = 'application/xml'
            return doc

        @self.route('/<lang_id>/cite/<pub_id>')
        def return_citation(lang_id, pub_id):
            '''
            Return a suggested citation.

            :param lang_id: See :func:`.return_page`.
            :param pub_id: contains the actual pub_id and, after a dot,
                the format of the suggested citation (e.g. `bib`, `ris`, `txt`).
            '''
            pub_id, cite_form = (pub_id.split('.', 1)
                    if '.' in pub_id else ('', ''))
            pub_entry = dao.get_publication_by_pub_id(pub_id)
            if cite_form and pub_entry:
                response.set_header('Content-Disposition', 'attachment')
                response.content_type = 'text/plain; charset=UTF8'
                return dao.get_citation(
                        pub_entry, cite_form, lang_id, unescaping = True)
            else: raise HTTPError(404, dao.glosses['httperror404'][lang_id])

        @self.route('/<lang_id>/file/<pub_id>/<path:path>')
        def return_static_file_of_publication(lang_id, pub_id, path):
            '''
            Return a static file from a subfolder named :param:`pub_id` and
            belonging to the publication with the eigenkennung :param:`pub_id`.

            Such a subfolder may e.g. contain a file “index.json” (containing
            data for an overview of images) and belonging images.

            :param lang_id: See :func:`.return_page`.
            :param path: the relative path to the static file, seen from said
                subfolder.
            '''
            match = dao.filename_re.search(pub_id)
            if match and match.group('end'):
                raise HTTPError(423, dao.glosses['httperror423'][lang_id])
            return static_file(
                    path,
                    dao.repros_path + pub_id,
                    download = bool(request.query.download))

        @self.route('/<lang_id>/meta/<page_id>')
        def return_page(lang_id, page_id):
            '''
            Return the page specified by the URL.

            If :param:`lang_id` does not denote a supported language,
            redirect to its default language.

            If :param:`page_id` does not denote a page of the site,
            it might be a pub_id, so redirect to `/<lang_id>/<page_id>`.
            The redirected request will be dealt with by :func:`.return_pub`.
            Further explanation is found in the docstring of that function.

            :param lang_id: As a rule, it is an id conforming to ISO 639 and
                http://tools.ietf.org/html/rfc5646, page 4 (‘shortest ISO 639
                code sometimes followed by extended language subtags’).
                It must not be str:`-` in order to exclude any confusion
                with the route of :func:`.return_static_code`.
            '''
            if lang_id not in dao.lang_ids:
                redirect('/{}/{}'.format(dao.default_lang_id, page_id))
            if page_id == 'ersatzindex':
                template_id, text, text_lang_id = dao.get_ersatzindex(lang_id)
            else:
                template_id, text, text_lang_id = dao.get_template_id_and_text(
                        lang_id = lang_id, page_id = page_id)
            if text:
                if text[:3] == b'\xef\xbb\xbf':
                    text = text[3:]
                return template(
                        template_id, request = request, dao = dao,
                        kwargs = {
                            'text_lang_id': text_lang_id,
                            'lang_id': lang_id,
                            'page_id': 'meta/' + page_id,
                            'text': text,
                            'ersatzlink': '/{}/meta/ersatzindex'.format(lang_id)
                                if template_id == 'index.tpl' else '',
                            })
            else:
                redirect('/{}/{}'.format(lang_id, page_id))

        @self.route('/<lang_id>/<pub_id>')
        def return_pub(lang_id, pub_id):
            '''
            For a certain publication, return either its permalink page
            with bibliographical information and the link(s) to its file(s)
            or one of these files, if there is no retention.

            If :param:`lang_id` does not denote a supported language,
            redirect to its default language. See further information
            in the docstring of :func:`.return_page`.

            For :param:`pub_id`:

            - Delete any match of :attr:`dao.del_from_id_re` from it.
              If that leads to a change, redirect to the new pub_id.
            - Look it up in :attr:`dao.id_id_map` and, if present, get
              the new pub_id from this mapping of old to new pub_ids
              and redirect to the new one.

            Now, if :param:`pub_id` is a filename (i.e.: contains a dot),
            return this file from the folder :attr:`dao.repros_path`.
            If it is not found, try to fetch the actual pub_id from the
            filename and redirect to the permalink page of this pub_id.
            Log this access.

            If not, return the permalink page of this pub_id. Log this access.
            '''
            if lang_id not in dao.lang_ids:
                redirect('/{}/{}'.format(dao.default_lang_id, pub_id))
            new_pub_id = dao.del_from_id_re.sub('', pub_id)
            if new_pub_id and new_pub_id != pub_id:
                redirect('/{}/{}'.format(lang_id, new_pub_id))
            if pub_id in dao.id_id_map:
                redirect('/{}/{}'.format(lang_id, dao.id_id_map[pub_id]))
            if '.' in pub_id:
                match = dao.filename_re.search(pub_id)
                if match:
                    retention = match.group('end') or ''
                    gestalt = (match.group('gestalt') or '').split('.')
                    if gestalt[-1] in {'htm', 'html'}:
                        template_id = gestalt[-2] if len(gestalt) > 1 else 'text'
                        template_id += '.tpl'
                        text, text_lang_id = dao.get_pub_text_html(pub_id)
                        if text[:3] == b'\xef\xbb\xbf':
                            text = text[3:]
                        response = template(
                                template_id, request = request, dao = dao,
                                kwargs = {
                                    'text_lang_id': text_lang_id,
                                    'lang_id': lang_id,
                                    'page_id': pub_id,
                                    'text': text,
                                    })
                    else:
                        response = static_file(
                                pub_id,
                                dao.repros_path,
                                download = bool(request.query.download))
                    if retention or isinstance(response, bottle.HTTPError):
                        new_pub_id = match.group('pub_id') or ''
                        if new_pub_id and new_pub_id != pub_id:
                            redirect('/{}/{}'.format(lang_id, new_pub_id))
                    else:
                        self.log_access(request, pub_id)
                        return response
            pub_entry = dao.get_publication_by_pub_id(pub_id)
            if pub_entry:
                self.log_access(request, pub_id)
                return template(
                        'publication', request = request, dao = dao,
                        kwargs = {
                            'text_lang_id': lang_id,
                            'lang_id': lang_id,
                            'page_id': pub_id,
                            'pub_id': pub_id,
                            'pub_entry': pub_entry,
                            })
            else:
                redirect('/{}/meta/404'.format(lang_id))

        @self.route('/<_>/<path:path>')
        def return_static_content(_, path):
            '''
            Return a static content file, e.g. an image file.

            Example URLs are:

            - `/de/icons/shortcut.ico`
            - `/de/acta/secreta/secretissima/1890-03-20.pdf?download=true`

            If the URL contains a query string with a key `download` and a
            non-empty value, the static file is served for download.

            The path of the file on the server must be the joint of
            :attr:`dao.path` and :param:`path`. Otherwise, the access is denied.

            :param _: is included in the route so that in a page with the URL
                `/de/start` a relative URL `icons/shortcut.ico` can be
                used, because then the absolute version of this relative URL is:
                `/de/icons/shortcut.ico`.
            '''
            return static_file(
                    path,
                    dao.path,
                    download = bool(request.query.download))
