# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
'''
Access to the database of the Bavarian Academy of Sciences.
'''
import getpass
import os
import re
import smtplib
import socket
import sqlite3
import sys
import time
import xml.etree.ElementTree as ET
from collections import deque
from email.mime.text import MIMEText
from functools import partial
from itertools import chain

import pymysql as mysql

import __init__
import file_io
import catalog_io
from badw_publica import dao_io
from compare import get_sortnumber
from context import DBCon
from structs import empty

class DB:
    def __init__(
            self,
            dbhost: str,
            dbport: int,
            dbname: str,
            dbuser: str,
            dbpassword: str,
            dbcharset: str = 'utf8mb4',
            catalog_url: str = 'http://bvbr.bib-bvb.de:5661/bvb01sru',
            catalog_kwargs: dict = {},
            catalog_pub_query_key: str = 'marcxml.idn',
            socket_defaulttimeout: float = 3600,
            role_mapping: 'Dict[str, Tuple[int, bool]]' = {
                'aut': (1, False),
                'ant': (6, False),
                'cmp': (6, False),
                'ctb': (5, True),
                'edt': (3, True),
                'ill': (6, False),
                'nrt': (6, False),
                'oth': (0, False),
                'rcp': (6, False),
                'red': (2, True),
                'trl': (6, False),
                'prf': (6, False),
                'itr': (6, False),
                'sng': (6, False),
                'voc': (6, False),
                'hnr': (6, False),
                },
            role_mapping_default: 'Tuple[Any, bool]' = (0, False),
            logged_path: str = __file__,
            ):
        '''
        Make an object for access to a specific database.
        Assign the necessary SQL commands to object attributes.

        :param dbhost: host of the database, i.e. the IP address of the
            database (or an alias).
        :param dbport: port of the database, e.g. ``3306``.
        :param dbname: name of the database within the database management
            system.
        :param dbuser: name of a user who has already been created with read
            and write permissions.
        :param dbpassword: password of this user.
        :param dbcharset: character set with which the database has been
            created.
        :param catalog_url: URL of the library catalog which contains (among
            others) the publications which have to be inserted in the database,
            too. This parameter is irrelevant if :meth:`import_from_catalog`
            is not used.
        :param catalog_kwargs: keyword parameters to instantiate an object
            for access to the catalog. They are the keyword parameters of
            :meth:`catalog_io.Catalog.__init__`. They are irrelevant if
            :meth:`import_from_catalog` is not used.
        :param catalog_pub_query_key: the query key to obtain an entry by
            the publication key of the library catalog. The default is the
            query key for an SRU query in MARC-XML, e.g. for the query term
            ``'marcxml.idn=BV042597690'``.
            See :meth:`catalog_io.Catalog.get_query_sru` for details.
        :param role_mapping: a mapping from every possibly occurring catalog
            role key (e.g. ``'aut'`` meaning 'author') to a pair consisting of

            - the corresponding database role key (e.g. ``1`` meaning 'author'),
            - a boolean indicating whether the corresponding database role text
              (e.g. ``'Verfasser'``) shall be included in the field in which
              all involved persons are named.

            If a catalog role key cannot be found in :param:`role_mapping`,
            nothing is inserted (so the default value is used) and a log entry
            is made.
        :param role_mapping_default: a default value to be used if the catalog
            role key cannot be found in :param:`role_mapping`.
        :param logged_path: See :param:`file_io.Log.__init__`. If empty, no
            log is written.
        :param socket_defaulttimeout: seconds after which an attempt at
            connecting with a host (catalog or database) is cancelled.
            During an import, this incident is written in the log,
            and the import is cancelled without raising an exception.
        '''
        socket.setdefaulttimeout(socket_defaulttimeout)
        if logged_path:
            self.log = file_io.Log(logged_path)
        else:
            self.log = empty
        self.connect = partial(mysql.connect,
                host        = dbhost,
                port        = dbport,
                user        = dbuser,
                passwd      = dbpassword,
                db          = dbname,
                charset     = dbcharset,
                cursorclass = mysql.cursors.DictCursor,
                )
        self.catalog_url          = catalog_url
        self.catalog_kwargs       = catalog_kwargs
        self.pub_query_key        = catalog_pub_query_key
        self.role_mapping         = role_mapping
        self.role_mapping_default = role_mapping_default

        self.genre_id_col           = 'badw_puf_id'
        self.pub_id_col             = 'badw_pub_id'
        self.pub_catid_col          = 'badw_pub_bvnr'
        self.pub_link_col           = 'badw_pub_digipublink'
        self.pub_status_col         = 'badw_pub_neu'
        self.series_id_col          = 'badw_rei_id'
        self.series_name_col        = 'badw_rei_reihentitel'
        self.series_description_col = 'badw_rei_reihentext'
        self.series_catid_alias_col = 'badw_bvz_ziel'

        self.sql_get_genre_id_by_genre = """
                select badw_puf_id
                from badw_list_publikationsform
                where badw_puf_name = %s
                """
        self.sql_get_person_by_person_catid = """
                select badw_per_id
                from badw_data_personen
                where badw_per_pnd = %s
                """
        self.sql_get_pub_ids_and_link = """
                select
                    badw_pub_id,
                    badw_pub_bvnr,
                    badw_pub_digipublink
                from badw_data_publikationen
                where badw_pub_digipublink != ''
                and badw_pub_deleted = 0
                """
        self.sql_get_pub_id_by_pub_catid = """
                select
                    badw_pub_id
                from badw_data_publikationen
                where badw_pub_bvnr = %s
                and badw_pub_deleted = 0
                """
        self.sql_get_pub_ids_and_status_from_db = """
                select
                    badw_pub_id,
                    badw_pub_bvnr,
                    badw_pub_neu
                from badw_data_publikationen
                where badw_pub_bvnr is not null
                and badw_pub_deleted = 0
                """
        self.sql_get_role_text = """
                select badw_pua_name
                from badw_list_publizistenart
                where badw_pua_id = %s
                """
        self.sql_get_series_by_series_catid = """
                select
                    badw_rei_id,
                    badw_rei_reihentitel,
                    badw_rei_reihentext
                from badw_data_reihen
                where badw_rei_nr = %s
                """
        self.sql_get_series_catid_alias = """
                select badw_bvz_ziel
                from badw_data_bvzuordnung
                where badw_bvz_quelle = %s
                """
        self.sql_insert_pub_catid_and_link = """
                insert into badw_data_publikationen
                    (badw_pub_bvnr, badw_pub_digipublink)
                values
                    (%s, %s)
                """
        self.sql_map_pub_genre_if_not_exists = """
                insert into badw_nm_publikationen_publikationsform
                    (badw_pub_id, badw_puf_id)
                select        %s,          %s
                from dual
                where not exists
                    (select * from badw_nm_publikationen_publikationsform
                     where badw_pub_id = %s
                        and badw_puf_id = %s)
                """
        self.sql_map_pub_person_if_not_exists = """
                insert into badw_nm_personen_publikationen
                    (badw_pub_id, badw_per_id, badw_pua_id)
                select        %s,          %s,          %s
                from dual
                where not exists
                    (select * from badw_nm_personen_publikationen
                     where badw_pub_id = %s
                        and badw_per_id = %s
                        and badw_pua_id = %s)
                """
        self.sql_map_pub_series_if_not_exists = """
                insert into badw_nm_publikationen_reihen
                    (badw_pub_id,
                     badw_rei_id,
                     badw_pub_bandnummer,
                     badw_pub_bandsort)
                select %s, %s, %s, %s
                from dual
                where not exists
                    (select * from badw_nm_publikationen_reihen
                     where badw_pub_id = %s
                        and badw_rei_id = %s)
                """
        self.sql_unmap_pub_genre = """
                delete from badw_nm_publikationen_publikationsform
                where badw_pub_id = %s
                """
        self.sql_unmap_pub_person = """
                delete from badw_nm_personen_publikationen
                where badw_pub_id = %s
                """
        self.sql_unmap_pub_series = """
                delete from badw_nm_publikationen_reihen
                where badw_pub_id = %s
                """
        self.sql_update_pub = """
                update badw_data_publikationen
                set
                    badw_pub_bandnr     = %s,
                    badw_pub_autoren    = %s,
                    badw_pub_jahr       = %s,
                    badw_pub_ort        = %s,
                    badw_pub_reihe      = %s,
                    badw_pub_reihetext  = %s,
                    badw_pub_titel      = %s,
                    badw_pub_untertitel = %s,
                    badw_pub_verlag     = %s,
                    badw_pub_seitenzahl = %s,
                    badw_pub_in_nummer  = %s,
                    badw_pub_neu        = 0
                where badw_pub_id       = %s
                """

    def check_and_add(
            self,
            config_path: str,
            is_adding: bool = False,
            permaloop: bool = False,
            ) -> None:
        '''
        Compare the entries of the database :param:`self` with the entries on
        the server that is specified at :param:`config_path`.

        .. important::
            If :param:`is_adding` is ``True``, an entry seemingly missing in
            the database is added and filled with the information available
            on the server.

        Inform a mail recipient (specified at :param:`config_path`) about the
        results and whether the database was changed. This information also
        contains a description of the performed checks (also specified at
        :param:`config_path`).

        :param permaloop: If ``True``, the program starts again after a run
            in an infinite loop. The waiting time between new starts is
            specified at :param:`config_path`.
        '''
        sys.stderr = self.log
        config_path = os.path.abspath(config_path)
        config = file_io.get_config(config_path)
        paths = file_io.get_abspaths(config, config_path)
        meta_path = paths['meta_path']
        catalog_link_prefix = config['catalog']['url'].rstrip('/') + '/'
        link_prefix = config['pub_link']['url'].rstrip('/') + '/de/'
        pre_pub_link_re = re.compile(config['pub_link']['pattern'])
        pub_link_re = re.compile(config['pub_link']['fullpattern'])
        mail_config = config['mail']
        if is_adding:
            catalog = catalog_io.Catalog(
                    self.catalog_url, **self.catalog_kwargs)
        refresh_pause = int(config['check_db_time']['pause_seconds'])
        while True:
            with DBCon(sqlite3.connect(meta_path)) as (connection, cur):
                cur.execute(dao_io.SQL_GET_PUBLICATIONS)
                pub_linkids = {
                        row[0]: list(map(str,
                            [row[2], row[4]] + # person, title
                            list(row[7:11]) + # place, year, series, serial
                            [row[1]] # pub_catid
                            ))
                        for row in cur.fetchall()
                        }
            db_pub_linkids = {}
            malformed_links = deque()
            duplicate_links = deque()
            try:
                with DBCon(self.connect()) as (connection, cur):
                    cur.execute(self.sql_get_pub_ids_and_link)
                    for row in cur.fetchall():
                        pub_id = str(row[self.pub_id_col])
                        pub_catid = str(row[self.pub_catid_col])
                        pub_link = str(row[self.pub_link_col])
                        if pre_pub_link_re.search(pub_link):
                            pub_link = pub_link.strip().rstrip('/')
                            try:
                                pub_linkid = pub_link_re.search(pub_link).group(
                                        'pub_id')
                            except (AttributeError, IndexError):
                                malformed_links.append((
                                        pub_link,
                                        catalog_link_prefix + pub_catid,
                                        pub_id,
                                        ))
                                continue
                            if pub_linkid in db_pub_linkids:
                                duplicate_links.append((
                                        pub_link,
                                        catalog_link_prefix + pub_catid,
                                        pub_id,
                                        ))
                            else:
                                db_pub_linkids[pub_linkid] = (
                                        pub_link, pub_catid, pub_id)
            except Exception as e:
                self.log.write_line(str(e))
                time.sleep(12)
                continue
            diverging_pub_catids = deque()
            missing_in_db_pub_linkids = deque()
            missing_in_db_but_matching_catid_pub_linkids = deque()
            for pub_linkid, row in pub_linkids.items():
                pub_catid = row.pop()
                result = [
                        link_prefix + pub_linkid,
                        catalog_link_prefix + pub_catid,
                        ]
                db_row = db_pub_linkids.get(pub_linkid)
                if db_row:
                    if pub_catid != db_row[1]:
                        result += [
                                catalog_link_prefix + db_row[1],
                                db_row[2], # pub_id
                                ] + row
                        diverging_pub_catids.append(result)
                else:
                    pub_id = ''
                    with DBCon(self.connect()) as (connection, cur):
                        cur.execute(
                                self.sql_get_pub_id_by_pub_catid, (pub_catid,))
                        db_row = cur.fetchone()
                        # The database must ensure that a catid is unique.
                        if db_row:
                            pub_id = db_row[self.pub_id_col]
                    if is_adding and not db_row:
                        # Even if `is_adding`, do not add if catid is found.
                        with DBCon(self.connect()) as (connection, cur):
                            cur.execute(
                                    self.sql_insert_pub_catid_and_link,
                                    (pub_catid, link_prefix + pub_linkid))
                            pub_id = cur.lastrowid
                            connection.commit()
                        term = self.pub_query_key + '=' + pub_catid
                        entry = catalog.get_entry(term)
                        if entry and catalog.get_pub_id(entry):
                            self.update_pub_and_relations(
                                    pub_id, entry, catalog)
                    result += [str(pub_id)] + row
                    if db_row:
                        missing_in_db_but_matching_catid_pub_linkids.append(
                                result)
                    else:
                        missing_in_db_pub_linkids.append(result)
            missing_in_pub_linkids = deque(
                    [db_row[0], catalog_link_prefix + db_row[1], db_row[2]]
                    for db_pub_linkid, db_row in db_pub_linkids.items()
                    if db_pub_linkid not in pub_linkids )
            text = '\n\n'.join((
                    mail_config['report_db_start'],
                    '1. ' + mail_config[
                        'missing_in_db_but_matching_catid_pub_linkids'],
                    '\n'.join( '\t'.join(row) for row in
                        missing_in_db_but_matching_catid_pub_linkids ) if
                        missing_in_db_but_matching_catid_pub_linkids else
                            mail_config['no_entry'],
                    '2. ' + mail_config['missing_in_db_pub_linkids'] + (
                            mail_config['hint_at_adding'] if is_adding else
                            mail_config['hint_at_no_adding']),
                    '\n'.join( '\t'.join(row) for row in
                        missing_in_db_pub_linkids ) if
                        missing_in_db_pub_linkids else mail_config['no_entry'],
                    '3. ' + mail_config['missing_in_pub_linkids'],
                    '\n'.join( '\t'.join(row) for row in
                        missing_in_pub_linkids ) if
                        missing_in_pub_linkids else mail_config['no_entry'],
                    '4. ' + mail_config['malformed_links'],
                    '\n'.join( '\t'.join(row) for row in
                        malformed_links ) if
                        malformed_links else mail_config['no_entry'],
                    '5. ' + mail_config['duplicate_links'],
                    '\n'.join( '\t'.join(row) for row in
                        duplicate_links ) if
                        duplicate_links else mail_config['no_entry'],
                    '6. ' + mail_config['diverging_pub_catids'],
                    '\n'.join( '\t'.join(row) for row in
                        diverging_pub_catids ) if
                        diverging_pub_catids else mail_config['no_entry'],
                    )) + '\n'
            message = MIMEText(text, _charset = 'utf-8')
            message['Subject'] = mail_config['report_db_start']
            message['From'] = mail_config['sender']
            recipients = ', '.join(mail_config['recipients'].split())
            message['To'] = recipients
            try:
                with smtplib.SMTP(
                        host = mail_config['smtp_host'],
                        port = int(mail_config['smtp_port']),
                        ) as server:
                    server.send_message(message)
                if not permaloop:
                    print('Der Befund wurde versandt an:\n' + recipients)
            except ConnectionRefusedError:
                self.log.write_lines(text)
                print('Der Befund konnte nicht versandt werden und wurde '\
                      'geschrieben in:\n' + self.log.path)
            if not permaloop:
                break
            end = time.time() + refresh_pause
            while time.time() < end:
                time.sleep(2)
                # This loop is done instead of e.g. time.sleep(refresh_pause)
                # in order to avoid blocking and enable cancelling with ctrl-c.

    def combine_titles(
            self,
            toptitle: str,
            subtitle: str,
            sectiontitle: str,
            punctuation_re: 'Pattern[str]' = re.compile(r'[.:!?]\W*$'),
            ) -> 'Tuple[str, str]':
        '''
        Combine the :param:`toptitle`, :param:`subtitle`, :param:`sectiontitle`,
        (obtained from the library catalog) to a toptitle and a subtitle.
        '''
        toptitle = toptitle.strip()
        subtitle = subtitle.strip()
        sectiontitle = sectiontitle.strip()
        if subtitle and sectiontitle:
            if subtitle:
                subtitle = subtitle[0].upper() + subtitle[1:]
            if sectiontitle:
                sectiontitle = sectiontitle[0].upper() + sectiontitle[1:]
            if subtitle and sectiontitle:
                if not punctuation_re.search(subtitle):
                    subtitle += '.'
                subtitle += ' '
        subtitle = subtitle + sectiontitle
        for old, new in (
                ('\x98', ''),
                ('\x9c', ''),
                (' ;', ';'),
                (' :', ':'),
                ):
            toptitle = toptitle.replace(old, new)
            subtitle = subtitle.replace(old, new)
        return toptitle, subtitle

    def get_persontext_map_pub_person(
            self,
            pub_id: str,
            persons: 'List[Tuple[str, str, str, str, str]]',
            ) -> str:
        '''
        Combine the information in :param:`persons` to a string serving as
        freetext information about the persons involved in the publication
        specified by :param:`pub_id` (the database ID).

        For each person, take a role information from :param:`persons`.
        (This information characterizes the person’s role regarding the
        publication.) The term expressing the role information originates
        from the library catalog. By this term, get the role id valid in
        the database, namely from :attr:`role_mapping` (given by
        :param:`__init__.role_mapping`). By this id, get the corresponding
        role text. If such text is found and if the :attr:`role_mapping`
        says ``True`` towards a display of this text, add it as a hint
        behind the person’s name in the freetext information to be returned.

        .. important::
            If the role term from the catalog is not found in
            :attr:`role_mapping`, ``0`` is assumed as the role id valid
            in the database and ``False`` is assumed as answer towards the
            question whether the role shall be displayed or not.

        Also for each person, search the given person id in the entries of
        the table of persons. If it is found, map the corresponding entry to
        the entry of the publication specified by :param:`pub_id`. In
        this mapping, save also the person’s role regarding the publication.

        .. important::
            All *old* mappings between the specified publication and a person
            are deleted.

        :param persons: as returned from
            :meth:`catalog_io.Catalog.get_personal_names_ids_roles_marc`.

            .. important::
                :param:`persons` from the catalog may contain duplicates that
                will be filtered out here.
        '''
        authors = []
        others = []
        with DBCon(self.connect()) as (connection, cur):
            cur.execute(self.sql_unmap_pub_person, pub_id)
            for name, name_enum, name_add, person_catid, role in persons:
                name = name.replace(' ', '\u202F')
                if name_enum:
                    name += '\u202F' + name_enum
                if name_add:
                    name += '\xa0({})'.format(name_add)
                if role in self.role_mapping:
                    role_id, be_shown = self.role_mapping[role]
                else:
                    role_id, be_shown = self.role_mapping_default
                    self.log.write_line('''
                            Catalog role key “{}” is not found in
                            :attr:`role_mapping` and occurs with the publication
                            of the database id “{}”.
                            '''.format(role, pub_id))
                if be_shown:
                    cur.execute(self.sql_get_role_text, role_id)
                    temp = cur.fetchone()
                    if temp:
                        _, role_text = temp.popitem()
                        name += '\xa0[{}]'.format(role_text)
                if role == 'aut':
                    if name not in authors:
                        authors.append(name)
                else:
                    if name not in others:
                        others.append(name)
                cur.execute(
                        self.sql_get_person_by_person_catid, person_catid)
                temp = cur.fetchone()
                if temp:
                    _, person_id = temp.popitem()
                    cur.execute(self.sql_map_pub_person_if_not_exists,
                            (pub_id, person_id, role_id,
                             pub_id, person_id, role_id))
                connection.commit()
        return'; '.join(authors + others)

    def get_pub_ids_from_db(
            self,
            import_all: bool = False) -> 'Generator[Tuple[int, str]]':
        '''
        From the publications table, yield the publication id of the database
        and the publication id of the catalog for all entries ...

        - which are not set to deleted
        - which have a value in the field of the catalog id
        - and, if :param:`import_all` is ``False``, which are set to be fresh,
          i.e. recently inserted or updated in the catalog.
        '''
        with DBCon(self.connect()) as (connection, cur):
            cur.execute(self.sql_get_pub_ids_and_status_from_db)
            entries = cur.fetchall()
        for entry in entries:
            if import_all or entry[self.pub_status_col]:
                yield (entry[self.pub_id_col], entry[self.pub_catid_col])

    def get_series_texts_map_pub_series(
            self,
            pub_id: str,
            series: 'Dict[str, Deque[Tuple[str, str]]]',
            ) -> 'Tuple[str, str, str]':
        '''
        Get name and description of any series from :param:`series` by looking
        them up in the table of series. Combine the names and the descriptions,
        each into one string. Do the same with serial numbers of the belonging
        publication, specified by :param:`pub_id` (the database ID).
        Return these three strings.

        .. important::
            Before the lookup in the table of series, the identifying key
            from the catalog is looked up in a list with series key aliases
            and replaced by an alias if an alias exists. This is done in
            order to reduce the variety of series set by the library.

        Map the publication to each series that is given in :param:`series`
        *and* found in the tables of series of the database. To each mapping,
        assign the belonging serial number. From this serial number, form a
        string apt at sorting and assign this sortstring, too.

        .. important::
            All *old* mappings between the specified publication and a series
            are deleted.

        :param series: The dictionary keys are not used at the moment (they
            identify the source of the values). The values are pairs of a
            series key, used in the catalog to identify the series, and the
            serial number of the publication within its series.
        :param pub_id: id of the belonging publication.
        '''
        series_names        = []
        series_descriptions = []
        serial_nums         = []
        with DBCon(self.connect()) as (connection, cur):
            cur.execute(self.sql_unmap_pub_series, pub_id)
            unfound_series_catids = []
            mappings_number = 0
            for series_catid, serial_num in chain.from_iterable(
                    series.values()):
                serial_nums.append(serial_num)
                cur.execute(self.sql_get_series_catid_alias, series_catid)
                series_catid = (cur.fetchone() or empty
                        )[self.series_catid_alias_col] or series_catid
                cur.execute(
                        self.sql_get_series_by_series_catid, series_catid)
                entry = cur.fetchone()
                if entry:
                    series_id = entry[self.series_id_col]
                    series_name = entry[self.series_name_col]
                    series_description = entry[self.series_description_col]
                    series_names.append(series_name)
                    if series_description:
                        series_descriptions.append(series_description)
                    serial_sortnum = get_sortnumber(serial_num)
                    self.sql_map_pub_series_if_not_exists
                    cur.execute(self.sql_map_pub_series_if_not_exists, (
                            pub_id, series_id, serial_num, serial_sortnum,
                            pub_id, series_id))
                    mappings_number += 1
                else:
                    unfound_series_catids.append(series_catid)
            for unfound_series_catid in unfound_series_catids:
                self.log.write_line('''
                        Catalog series key “{}” is not found in the table of
                        series and occurs with the publication of the database
                        id “{}”, which could be mapped to a series {} times.
                        '''.format(series_catid, pub_id, mappings_number))
            connection.commit()
        series_nametext    = '; '.join(series_names)
        series_description = '\n\n'.join(series_descriptions)
        serial_numtext     = '; '.join(serial_nums)
        return series_nametext, series_description, serial_numtext

    def import_from_catalog(self, import_all: bool = False) -> None:
        '''
        Import bibliographical information from the library catalog associated
        with this database (see :meth:`__init__` for its parameters) in order
        to write or overwrite values or relations.

        The affected tables are:

        - the table of publications,
        - the table of mappings between publications and persons,
        - the table of mappings between publications and series.

        .. note::
            No longer affected are:

            - the table of mappings between publications and genres,
            - the table of genres.

            In the table of genres, a genre not found there but given in the
            catalog was added except if it is the empty string.

        (Other tables are only read in order to obtain the information to be
        fed in the aforementioned tables.)

        .. important::
            - A publication that is treated gets all of its *old* values
              overwritten and gets all of its *old* relations deleted.

        A publication is treated if the following conditions are met in its
        corresponding entry in the tables of publications:

        - Its entry has a valid value in the field with the catalog id.
          A value is valid if the URL generated out of this value leads to an
          entry of the catalog. About the connection with the catalog see
          the docstring of :meth:`__init__`.
        - Its entry is not set to be deleted (according to the *deleted* field
          in the entry).
        - Its entry is set to be fresh (according to the *fresh* field in the
          entry) or :param:`import_all` is ``True``. (An explanation follows:)

        If :param:`import_all` is ``False`` (the default), the import is done
        only for the entries set as fresh. This can be done relatively often.

        If :param:`import_all` is ``True``, the import is done for all entries
        meeting the first two conditions. This task may be so time consuming
        and may produce such an excessive load on the library catalog that it
        cannot be done often, but rather e.g. once a month.

        After being refreshed, an entry is set to be not fresh.

        .. note::
            The import and certain incidents occuring during the import are
            logged. Notably:
            - If a connection with the catalog or the database cannot be made
              within :param:`self.__init__.socket_defaulttimeout` seconds,
              the import is cancelled without raising an exception.
            - If a series key from the catalog cannot be found in the table of
              series, this series key and the publication id are logged.

        .. note::
            If a catalog role key cannot be found in :param:`role_mapping`,
            nothing is inserted (so the default value is used) and a log entry
            is made.

        .. note::
            One probably want to have a GUI to do the following tasks manually:

            - enter a new publication: Insert a new record and write the
              catalog id in its field. The *deleted* field could have an
              automatic default value of ``0``, the *fresh* field could have an
              automatic default value of ``1``.
            - delete a publication: Set the *deleted* field to ``1``.
            - (after having corrected the entry in the library catalog) trigger
              a refresh in the next run: Set the *fresh* field to ``1``.
        '''
        sys.stderr = self.log
        self.log.write_line(
                'A new import is started for {} entries having a catalog id.'\
                .format('all' if import_all else 'the fresh'))
        catalog = catalog_io.Catalog(
                self.catalog_url, **self.catalog_kwargs)
        for pub_id, pub_catid in self.get_pub_ids_from_db(import_all):
            entry = catalog.get_entry(self.pub_query_key + '=' + pub_catid)
            if not entry or not catalog.get_pub_id(entry):
                self.log.write_line('''
                        “{}” is not found in {}. The catalog is down or lacks
                        this key.'''.format(pub_catid, self.catalog_url))
                continue
            self.update_pub_and_relations(pub_id, entry, catalog)
        self.log.write_line(
                'The import is finished. Starttime: ' + self.log.start,
                with_time = True)

    def map_pub_genre(
            self,
            pub_id: str,
            genres: 'Generator[Tuple[str, str, str]]'
            ) -> None:
        '''
        Map the publication :param:`pub_id` (the database ID) to each genre
        given in :param:`genres`.

        .. important::
            - All *old* mappings between the specified publication and a series
              are deleted (except if the given genre is the empty string).
            - In the table of genres: A genre not found there but given in the
              catalog *was* added except if it is the empty string.
              Now, it is simply skipped.

        :param series: The keys are not used at the moment (they identify the
            source of the values). The values are pairs of a series key, used
            in the catalog to identify the series, and the serial number of
            the publication within its series.
        :param pub_id: id of the belonging publication.
        '''
        with DBCon(self.connect()) as (connection, cur):
            old_mappings_are_deleted = False
            genres = set( genre for _, genre, _ in genres )
            for genre in genres:
                if genre:
                    if not old_mappings_are_deleted:
                        cur.execute(self.sql_unmap_pub_genre, pub_id)
                        connection.commit()
                        old_mappings_are_deleted = True
                    cur.execute(self.sql_get_genre_id_by_genre, genre)
                    genre_id = cur.fetchone()
                    if genre_id is None:
                        self.log.write_line('''
                                Genre “{}” is not found in the list of genres
                                and occurs with the publication of the database
                                id “{}”.'''.format(genre, pub_id))
                    else:
                        genre_id = genre_id.popitem()[1]
                        cur.execute(self.sql_map_pub_genre_if_not_exists,
                                (pub_id, genre_id,
                                 pub_id, genre_id))
                    connection.commit()

    def update_pub_and_relations(
            self,
            pub_id: str,
            entry: ET.Element,
            catalog: catalog_io.Catalog,
            ) -> None:
        '''
        For the publication specified by :param:`pub_id` (the database ID),
        update its fields and relations in:

        - the table of publications,
        - the table of mappings between publications and persons,
        - the table of mappings between publications and series.

        .. important::
            See also :meth:`import_from_catalog`.

        :param entry: the entry that is obtained from the library catalog
            :param:`catalog` and belongs to the publication :param:`pub_id`.
            The information extracted from this entry is combined with
            information found in the database to perform the update.
        :param catalog: the model of the catalog associated with the database.
            Here, its methods for extracting data from :param:`entry` are used.
        '''
        year           = catalog.get_year(entry)
        year           = (re.search(r'\d{3,4}', year) or empty).group()
        # Database requires either integer or Null:
        year           = None if year == '' else int(year)
        place          = catalog.get_place(entry)
        publisher      = catalog.get_publisher(entry)
        persons        = list(catalog.get_personal_names_ids_roles(entry))
        persontext     = self.get_persontext_map_pub_person(pub_id, persons)
        (
            toptitle,
            subtitle,
            sectiontitle
        )              = catalog.get_titles(entry)
        (
            title,
            subtitle
        )              = self.combine_titles(toptitle, subtitle, sectiontitle)
        series         = catalog.get_series_id_serial_num(entry)
        (
            series_nametext,
            series_description,
            serial_numtext,
        )              = self.get_series_texts_map_pub_series(pub_id, series)
        extent         = '; '.join(filter(None, (
                         catalog.get_extent(entry),
                         catalog.get_extent_other(entry),
                         catalog.get_extent_accompanying_material(entry),
                         )))
        isbns_issns   = ', '.join(catalog.get_isbns_issns(entry))
        # Obsolete: self.map_pub_genre(pub_id, catalog.get_genre(entry))
        with DBCon(self.connect()) as (connection, cur):
            cur.execute(self.sql_update_pub,
                    (
                    serial_numtext,
                    persontext,
                    year,
                    place,
                    series_nametext,
                    series_description,
                    title,
                    subtitle,
                    publisher,
                    extent,
                    isbns_issns,
                    pub_id,
                    ))
            connection.commit()
