# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
'''
Start :meth:`db_io.DB.import_from_catalog` (see the docstring of this method).
'''
import getpass
import __init__
from badw_reldb import db_io

# Initialize database. See the description in :meth:`db_io.DB.__init__`.
db = db_io.DB(
        dbhost     = '10.156.32.59',
        dbport     = 3306,
        dbname     = 'badw',
        dbuser     = input('Database Username: '),
        dbpassword = getpass.getpass('Password: '),
        )

print('Die Einfuhr frischer Einträge aus dem Bibliothekskatalog in die Datenbank hat begonnen. Ich arbeite daran.')
db.import_from_catalog(import_all = False)
print('Ich bin fertig.')
