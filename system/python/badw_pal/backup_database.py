# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
'''
Infinite loop backing up a database at certain time intervals.
See :func:`backup.run_backup`.
'''
import os

import __init__
import backup
import file_io

mainfolderpath = __file__ + '/../../../../../'
dbuser, dbpassword = file_io.get_keys(os.path.join(mainfolderpath, 'db'))
backup.run_backup(
        [
            'mysqldump',
            'pal',
            '--user=' + dbuser,
            '--password=' + dbpassword,
            '--result-file={}.sql',
            '--ignore-table=pal.auth_event',
            '--ignore-table=pal.ft',
        ],
        os.path.join(mainfolderpath, 'backup_db')
        )
