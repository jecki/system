# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2017 ff.
'''
Addition to :module:`bottle`, may just be imported in bottle,
e.g. as ``from bottleadd import CherootServer`` so that in
another module you can just have ``bottle.CherootServer``,
as if it were included in bottle itself.
'''
from bottle import ServerAdapter

class CherootServer(ServerAdapter):
    def run(self, handler):
        from cheroot import wsgi
        from cheroot.ssl import builtin
        self.options['bind_addr'] = (self.host, self.port)
        self.options['wsgi_app'] = handler
        certfile = self.options.pop('certfile', None)
        keyfile = self.options.pop('keyfile', None)
        chainfile = self.options.pop('chainfile', None)
        server = wsgi.Server(**self.options)
        if certfile and keyfile:
            server.ssl_adapter = builtin.BuiltinSSLAdapter(
                    certfile, keyfile, chainfile)
        try:
            server.start()
        finally:
            server.stop()
