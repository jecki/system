# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2013 ff.
import difflib
import heapq
import math
import re
import unicodedata
from collections import deque
from decimal import Decimal as Dec
from functools import partial
from operator import itemgetter
from unicodedata import name as unicodename

UNIMAP = {
        unicodedata.name(chr(i)): chr(i) for i in
            list(range(48, 58))  + # digits
            list(range(65, 91))  + # latin majuscules
            list(range(97, 123)) + # latin minuscules
            [32]
        }

# The following function is out of the alphabetical order,
# because it is used in a function definition beneath.
def even_out(
        term: str,
        change: 'Optional[Callable[[str], str]]' =
            lambda term: unicodedata.normalize('NFKD', term).casefold(),
        erase_re: 'Optional[Pattern[str]]' =
            re.compile('''<("[^"]*"|'[^']*'|[^>])*>'''),
        replaces: 'Tuple[Tuple[str, str], ...]' = (),
        unimap: 'Dict[str, str]' = UNIMAP,
        strip: bool = True,
        ) -> str:
    '''
    Get an evened out form of :param:`term`, i.e.:

    - Perform a preparing action on :param:`term`, specified by :param:`change`.
    - Delete certain patterns, specified by :param:`erase_re`.
    - Replace certain sequences by certain other sequences, both specified as
      replacement pairs in :param:`replaces`.
    - For any character in :param:`term`, take the first four words of its
      Unicode name and delete it, if the stripped name is not found in
      :param:`unimap`, or replace it with the character associated with the
      name found in :param:`unimap`.
    - Lastly, if :param:`strip` equals ``True``, apply :meth:`str.strip`.

    For each action applies: If the given parameter is empty or ``None``,
    the action will not be performed.
    '''
    if change:
        term = change(term)
    if erase_re:
        term = erase_re.sub('', term)
    for old, new in replaces:
        term = term.replace(old, new)
    if unimap:
        chars = ''
        for char in term:
            try:
                name = unicodename(char)
                if name in unimap:
                    char = unimap[name]
                else:
                    char = unimap.get(' '.join(name.split()[:4]), '')
            except ValueError:
                pass
            chars += char
        term = chars
    return term.strip() if strip else term

def even_out_fast(
        term: str,
        delete_re: 'Pattern[str]' = re.compile(r'\W+'),
        ):
    '''
    Get an evened-out form of :param:`term`, i.e.:

    - Normalize unicode variants towards the analytical form.
    - Make majuscules to minuscules.
    - Delete matches of :param:`delete_re`.
    '''
    return delete_re.sub('', unicodedata.normalize('NFKD', term).casefold())

def compare_lines(
        lines0: 'List[str]',
        lines1: 'List[str]',
        line0_for_alignment: 'Callable[[str, str], str]' =
            lambda line, other_line: line,
        line1_for_alignment: 'Callable[[str, str], str]' =
            lambda line, other_line: line,
        line0_for_comparison: 'Callable[[str, str], str]' =
            lambda line, other_line: line,
        line1_for_comparison: 'Callable[[str, str], str]' =
            lambda line, other_line: line,
        align_span: int = 10,
        sep: str = '\t', # delimiter of fields within a line
        marker: str = '██', # tag appended to a field found unequal
        line_from_lines0_flag: str = '○',
        line_from_lines1_flag: str = '●',
        window: int = 5,
        ) -> 'MutableSequence[Tuple[str, str]]':
    '''
    Compare :param:`lines0` and :param:`lines1`.

    Try to re-align them for the case of insertion or deletion of lines.
    Looking for matching lines:

    - do not go further than :param:`align_span` lines.
    - compare lines while levelling the one via :param:`line0_for_alignment`
      and the other via :param:`line1_for_alignment`.

    Compare the lines: while levelling the one via :param:`line0_for_comparison`
    and the other via :param:`line1_for_comparison`.

    While comparing: if :param:`sep` and :param:`marker` are given:

    - Use :param:`sep` to split the lines in segments.
    - Append :param:`marker` to every segment of the other line that is not
      equal to the corresponding segment of the one line.

    Return differing lines, first the one, then the other, as tuples of:

    - the :param:`line_from_lines0_flag` or the :param:`line_from_lines1_flag`,
      indicating whether the line stems from lines0 or lines1.
    - and the actual line.

    Return the differing lines with :param:`window` lines of pre-context and
    with the same number of lines as post-context. The number of contextual
    lines is smaller if there are no more lines (i.e. at the beginning or
    end of the sequence of lines).
    '''
    nth = 0
    while nth < len(lines1):
        if nth >= len(lines0):
            lines0.insert(nth, '')
        elif (
                line0_for_alignment(lines0[nth], lines1[nth]) !=
                line1_for_alignment(lines1[nth], lines0[nth])
                ):
            gap = 0
            for subnth in range(
                    min(nth + 1, len(lines1) - 1),
                    min(nth + align_span, len(lines1) - 1)
                    ):
                if (
                        line0_for_alignment(lines0[nth], lines1[subnth]) ==
                        line1_for_alignment(lines1[subnth], lines0[nth])
                        ):
                    for gap in range(subnth - nth):
                        lines0.insert(nth, '')
                    gap += 1
                    break
            if not gap:
                for subnth in range(
                        min(nth + 1, len(lines0) - 1),
                        min(nth + align_span, len(lines0) - 1)
                        ):
                    if (
                            line0_for_alignment(lines0[subnth], lines1[nth]) ==
                            line1_for_alignment(lines1[nth], lines0[subnth])
                            ):
                        for gap in range(subnth - nth):
                            lines1.insert(nth, '')
                        gap += 1
                        break
            nth += gap
        nth += 1
    for _ in range(len(lines0) - len(lines1)):
        lines1.insert(nth, '')
    for nth in range(len(lines1) - 1, - 1, - 1):
        is_unequal = False
        if (line0_for_comparison(lines0[nth], lines1[nth]) !=
            line1_for_comparison(lines1[nth], lines0[nth])):
            is_unequal = True
            if sep and marker:
                # We do not need to change lines0 here.
                fields0 = lines0[nth].split(sep)
                # But lines1 will be adorned with the ``unequal_field_flag``.
                lines1[nth] = lines1[nth].split(sep)
                gap = len(fields0) - len(lines1[nth])
                if gap < 0:
                    fields0 += ([''] * gap)
                elif gap > 0:
                    lines1[nth] += ([''] * gap)
                for subnth in range(len(fields0)):
                    if fields0[subnth] != lines1[nth][subnth]:
                        lines1[nth][subnth] += marker
                lines1[nth] = sep.join(lines1[nth])
        lines0[nth] = (line_from_lines0_flag, lines0[nth])
        if is_unequal:
            lines0.insert(nth + 1, (line_from_lines1_flag, lines1[nth]))
    above = 0
    results = deque()
    for nth, line in enumerate(lines0, start = -1):
        if line[0] == line_from_lines1_flag:
            if above < nth - window:
                results.append(('', ''))
            for subnth in range(max(above, nth - window),
                                min(nth + window + 2, len(lines0))):
                results.append(lines0[subnth])
            above = subnth + 1
    return results

def get_diacritics(
        span: range = range(int('0x10ffff', base = 16) + 1),
        ) -> 'Generator[str]':
    for i in span:
        try:
            char = chr(i)
            if 'COMBINING' in unicodename(char):
                yield char
        except:
            continue

def get_levenstein(
        seq1: 'Iterable[Any]',
        seq2: 'Iterable[Any]',
        delcost: float = 1,
        inscost: float = 1,
        transpcost: float = 1.5,
        suppcost: float = 1,
        suppcosts: 'Dict[Tuple[Any, Any], float' = {},
        ) -> float:
    '''
    Calculate the Levenstein distance between :param:`seq1` and :param:`seq2`.

    .. note:: The calculated value is an absolute measure; a relative measure
        can be calculated by ``value / max(len(seq1), len(seq2), 1)``.

    The calculation is done following Levenshtein, Damerau (considering
    transpositions) and Homer (performance optimisation: do not store the
    whole matrix, but at most two preceding lines, and make the leftmost
    column to the rightmost). Added here is the option to determine a
    different cost for different substitutions, e.g. ``{('u', 'v'): 0.2}``.
    '''
    preline = None
    line = list(range(1, len(seq2) + 1)) + [0]
    for l in range(len(seq1)):
        prepreline, preline, line = preline, line, [0] * len(seq2) + [l + 1]
        for c in range(len(seq2)):
            line[c] = min(preline[c] + delcost,
                    line[c - 1] + inscost,
                    preline[c - 1] + suppcosts.get(
                        (seq1[l], seq2[c]),
                        (seq1[l] != seq2[c]) * suppcost))
            if (transpcost != delcost + inscost
                    and l > 0 and c > 0 and seq1[l] == seq2[c - 1]
                    and seq1[l - 1] == seq2[c] and seq1[l] != seq2[c]):
                line[c] = min(line[c], prepreline[c - 2] + transpcost)
    return line[len(seq2) - 1]

def get_sortnumber(
        number: str,
        max_subnumber: int = 99999,
        strip_re: 'Optional[Pattern[str]]' = re.compile(r'(^\D+|\D+$)'),
        split_re: 'Optional[Pattern[str]]' = re.compile(r'\D+'),
        ) -> str:
    '''
    Convert :param:`number` to a string which will be correctly sorted, e.g.
    ``'2'`` before ``'10'``. With the default settings, e.g. ``'Bd. 12,3/8'``
    would be converted into ``'000120000300008'``

    :param number: a serial number, consisting of one or more subnumbers
        which will be filled with zeroes.
    :param max_subnumber: the highest integer possibly occurring as a
        subnumber. The default is ``99999``, i.e. a subnumber must not be
        higher than 99999.
    :param strip_re: Pattern describing what to strip from the string.
        If ``None``, nothing is stripped.
    :param split_re: Pattern describing how the subnumbers are delimited.
        If ``None``, no split is done.
    '''
    number = str(number)
    if strip_re:
        number = strip_re.sub(r'', number)
    if number:
        if split_re:
            numbers = split_re.split(number)
        else:
            numbers = [number]
        length  = len(str(max_subnumber))
        number  = ''.join( n.zfill(length) for n in numbers )
    return number

def get_sortstring(
        term: str,
        number_re: 'Pattern[str]' = re.compile(r'\d+([^\s\w\d]+(?=\d))?'),
        max_subnumber: int = 99999,
        strip_re: 'Optional[Pattern[str]]' = re.compile(r'(^\D+|\D+$)'),
        split_re: 'Optional[Pattern[str]]' = re.compile(r'\D+'),
        ) -> str:
    '''
    In :param:`term`, change every match of :param:`number_re` by applying
    :func:`get_sortnumber`. See this function for the remaining arguments.
    '''
    def change(match: 'Match[str]') -> str:
        return get_sortnumber(match.group(), max_subnumber, strip_re, split_re)
    return number_re.sub(change, term)

def guess_entry_solitary(
        term: str,
        register: 'Dict[str, Dict[float, Dict[str, float]]]',
        isjunk: 'Optional[Callable[[str], bool]]' = None,
        modify_term: 'Callable[[str], str]' = even_out,
        modify_variant: 'Callable[[str], str]' = even_out,
        maxlen: int = 1,
        cutoff: float = 0.4,
        ) -> 'List[Tuple[Dec, str]]':
    '''
    Guess, which entry_id(s) in :param:`register` belong(s) to :param:`term`.
    Return at most :param:`maxlen` suggestions.

    :param:`register` is a dictionary like::
        {variant1: {prior_of_variant1: {entry_id1: prior_of_entry1,
                                        entry_id2: prior_of_entry2,
                                        entry_id3: prior_of_entry3}},
         variant2: {prior_of_variant2: {entry_id1: prior_of_entry1,
                                        entry_id4: prior_of_entry4,
                                        entry_id5: prior_of_entry5}},
        }

    .. note::
        The `prior_of_variant...` is not used at the time being and may be any
        value.

    The suggestions are a list of tuples, each containing a probability and an
    entry_id. The probability is calculated (1) from the similarity ratio
    between :param:`term` and the most similar variant that is in
    param:`register` associated with the entry_id, and (2) from a prior
    probability (`prior_of_entry...` in the example above). This prior is, as a
    rule, the ratio ``variant_x_occurs / variant_x_is_annotated_with_entry_y``.

    :param isjunk: The same as ``isjunk`` in :class:`difflib.SequenceMatcher`.
    :param modify_term: A function to modify the term, before it is compared
        with a variant.
    :param modify_variant: A function to modify the variant, before it is
        compared with a term.
    :param maxlen: The maximum length of the suggestions to be returned.
    :param cutoff: A threshold between 0 and 1: a similarity score a candidate
        must surpass; if the candidate does not surpass, it is discarded.

    .. note::
        The calculation of the similarity is expensive, but is avoided, if
        the :param:`register` contains at least :param:`maxlen` keys, for
        which the following applies: The key, modified by
        :param:`modify_variant`, is equal to the term, modified by
        :param:`modify_term`. Thus, one can accelerate the run by tweaking
        the :param:`register`.

    .. note::
        The probability of the suggested entry_id is calculated by updating the
        associated prior of the entry. The calculation here, alas, is not the
        quite right thing. The right thing is to multiply the prior with the
        probability `P(term|entry)`: How probable is the term, if the entry is
        meant? Here, this probability is only roughly approximated by the
        similarity ratio between term and entry.
    '''
    def consider(
            variant: str,
            suggestions: 'List[Tuple[Dec, str]]',
            register: 'Dict[str, Tuple[str, Dec]]',
            maxlen: int,
            ratio: Dec
            ) -> 'List[Tuple[Dec, str]]':
        for entry_id, prior in register[variant].items():
            prob = Dec(prior) * ratio ** 3
            if len(suggestions) < maxlen:
                heapq.heappush(suggestions, (prob, entry_id))
            else:
                heapq.heappushpop(suggestions, (prob, entry_id))
        return suggestions

    register = { variant: tuple(prior_entry_id_prior.values())[0]
            for variant, prior_entry_id_prior in register.items() }
    variants = register.keys()
    term_mod = modify_term()
    suggestions = []
    for variant in variants:
        if modify_variant(variant) == term_mod:
            suggestions = consider(variant, suggestions, register, maxlen, 1)
            if len(suggestions) == maxlen:
                break
    if len(suggestions) < maxlen:
        matcher = difflib.SequenceMatcher(isjunk = isjunk, autojunk = False)
        matcher.set_seq2(term_mod)
        for variant in variants:
            variant_mod = modify_variant(variant)
            if not variant_mod == term_mod:
                matcher.set_seq1(variant_mod)
                if matcher.real_quick_ratio() > cutoff and \
                        matcher.quick_ratio() > cutoff:
                    ratio = Dec(matcher.ratio())
                    if ratio > cutoff:
                        suggestions = consider(
                                variant, suggestions, register, maxlen, ratio)
    total = sum( prob for prob, _ in suggestions )
    suggestions = [ (entry_id, Dec(prob) / total)
            for prob, entry_id in suggestions ]
    suggestions.sort(key = itemgetter(1, 0), reverse = True)
    return suggestions
