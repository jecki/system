# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2013 ff.
from collections import defaultdict
from collections import deque

# (Just for fun:)
class BTree:
    '''
    A binary tree. It contains a value, a left branch and a right branch.
    A branch is either a ``BTree`` again or ``None``.
    '''
    def __init__(
            self,
            value: 'Any',
            left: 'Optional[BTree]',
            right: 'Optional[BTree]',
            ) -> None:
        self.value = value
        self.left  = left
        self.right = right

    def print(self) -> None:
        level = 0
        stack = deque([(self, level)])
        while stack:
            subtree, level = stack.pop()
            print(level * '    ' + str(subtree.value))
            if subtree.left or subtree.right:
                level += 1
            for branch in (subtree.right, subtree.left):
                if branch:
                    stack.append((branch, level))

    def invert(self):
        '''
        Invert the binary tree, so that every pair of a left and a right
        branch is inverted: Left becomes right, right becomes left.
        Fair is foul and foul is fair.
        '''
        stack = deque([self])
        while stack:
            subtree = stack.pop()
            subtree.left, subtree.right = subtree.right, subtree.left
            for branch in (subtree.left, subtree.right):
                if branch:
                    stack.append(branch)

class DefaultkeyDict(dict):
    '''
    If a key ``k`` is looked up via ``[k]``, i.e. ``.__getitem__(k)``,
    and ``k`` is not in the dictionary, then a default key (specified on
    initialization) is looked up instead. If the default key is not in
    the dictionary, too, then a structure is returned that behaves as
    the string ``k`` but will also return ``k`` when indexed with a
    non-integer (as if it were a dictionary).

    The DefaultkeyDict dictionary is not changed by lookups.

    A use-case:
    ``glosses[word][lang_id]`` is to return a translation of ``word``
    in the language ``lang_id``.
    If ``glosses`` is a ``DefaultkeyDict`` of ``DefaultkeyDict``s and
    ``defaultkey`` specifies a default language, then:

    - If ``word`` is not found, ``word`` is returned regardless of ``lang_id``.
    - If ``lang_id`` is not found for a ``keyword``, the translation of the
      default language is returned.
    '''
    def __init__(self, defaultkey: 'Hashable', *args, **kwargs):
        '''
        :param defaultkey: The default key, which is:

        - used to be looked up instead of any key that is looked up
          but not found in the dictionary.
        - returned, if it is not in the dictionary.
        '''
        self.update(*args, **kwargs)
        self.defaultkey = defaultkey

    def __missing__(self, key: str):
        ersatzstring = DefaultkeyString(key)
        return self.get(self.defaultkey, ersatzstring)

class DefaultkeyString(str):
    def __getitem__(self, key):
        if isinstance(key, str):
            return self
        return super().__getitem__(key)

class Empty:
    '''
    Structure for returning an empty or null or ``False`` value.

    .. testcode::
        import re
        from structs import empty

        author = 'Ὅμηρος'
        entry = 'Year unknown.'
        year = (re.search(r'(?i)year\s*(\d+)', entry) or empty).group(1)
        sigil = author + (' ' + year if year else '')

        print(sigil)

    .. testoutput::
        Ὅμηρος
    '''
    def __bool__(*args, **kwargs):
        return False

    def __getitem__(*args, **kwargs):
        return None

    def group(*args, **kwargs):
        return ''

    def write(*args, **kwargs):
        return 0

empty = Empty()

class Void:
    '''
    Structure for returning itself except from returning
    ``False`` when tested for its boolean value.
    '''
    def __bool__(*args, **kwargs):
        return False

    def __getitem__(self, *args, **kwargs):
        return self

    def group(self, *args, **kwargs):
        return self

    def write(self, *args, **kwargs):
        return self

void = Void()

if __name__ == '__main__':
    tree = BTree(
            0,
            BTree(
                1,
                BTree(
                    2,
                    None,
                    None,
                    ),
                BTree(
                    3,
                    None,
                    None,
                    ),
                ),
            BTree(
                4,
                BTree(
                    5,
                    None,
                    None,
                    ),
                BTree(
                    6,
                    None,
                    None,
                    ),
                ),
            )
    tree.print()
    print('\nwill be inverted ...\n')
    tree.invert()
    tree.print()
