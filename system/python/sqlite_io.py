# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2017. (© http://badw.de)
import os
import sqlite3
from context import DBCon

def walk(
        db_path: str,
        scope: '''
            Dict[
                str,
                Tuple[
                    Set[str],
                    Set[str]
                ]
            ]''',
        change: 'Callable[[T], T]',
        ) -> None:
    '''
    Apply :param:`change` to fields of the database at :param:`db_path`;
    :param:`scope` determines, which columns are included:

    - Every key specifies a table.
    - The belonging value is a tuple consisting of two sets:

      - The primary key column(s) of this table.
      - The column(s) to which :param:`change` is to be applied. If empty, every
        column of the table is taken if not already given in the first set.

    .. note::
        You might prefer :func:`wander` for your task.

    .. important::
        - It will be checked if a table or column specified in :param:`scope`
          is known in the database; if not, the table or column is invalid and
          will be ignored.
        - A column in the second set that already has occurred in the first set
          is invalid and will be ignored.
        - If the first set does not specify a valid column, the belonging table
          is left out.
        - If the second set is neither empty nor specifies a valid column, the
          belonging table is left out.
    '''
    if not os.path.isfile(db_path):
        print('No file found at: ' + db_path)
        return
    with DBCon(sqlite3.connect(db_path)) as (connection, cur):
        cur.execute("select name from sqlite_master where type = 'table'")
        tables = set( r[0] for r in cur.fetchall() if r[0] in scope )
        for table in tables:
            print(table)
            cur.execute("pragma table_info({});".format(table))
            all_cols = { result[1] for result in cur.fetchall() }
            id_cols = [ c for c in scope[table][0] if c in all_cols ]
            val_cols = [
                    c for c in (scope[table][1] or all_cols)
                    if (c in all_cols and c not in id_cols) ]
            print('   ', id_cols, '\n   ', val_cols)
            if not id_cols: continue
            if not val_cols: continue
            id_cols_sql = " and ".join( "`{}` = ?".format(c) for c in id_cols )
            number = 0
            cur.execute("select `{}` from `{}`".format(
                    "`, `".join(id_cols + val_cols),
                    table))
            for row in cur.fetchall():
                ids = row[:len(id_cols)]
                vals = row[len(id_cols):]
                for val_col, val in zip(val_cols, vals):
                    new_val = change(val)
                    if new_val != val:
                        number += 1
                        cur.execute(
                                "update `{}` set `{}` = ? where {}".format(
                                    table, val_col, id_cols_sql),
                                ((new_val,) + ids))
            connection.commit()
            print('   ', number, 'field(s) changed.')
            print()

def wander(
        db_path: str,
        change: '''
            Callable[
                [str, Tuple[str], List[T]],
                Tuple[
                    List[T],
                    Dict[str, Any]]]''',
        ) -> None:
    '''
    Apply :param:`change` to every row in the database at :param:`db_path`.
    :param:`change` takes the following positional arguments:

    - the name of the current table,
    - a tuple of the column names of the current table and, in the same order,
    - a list of the values of the current row. These values may be changed in
      :param:`change`.

    :param:`change` returns the list -- possibly with new values -- or a list
    of the same length and order as the old list.

    If the returned list does not differ, the row is just lef alone.

    If it does differ, every field whose old value differs from the belonging
    new value is updated. The association between new value and belonging old
    value is derived from the order of the list.

    For this update, :param:`change` must also return a dictionary which maps
    column names to values and will be used in a where-clause (e.g. `id = 12`)
    in order to identify the very row to be updated with the new values.

    If the same or an equal list is returned, the current row is not updated.
    '''
    if not os.path.isfile(db_path):
        print('No file found at: ' + db_path)
        return
    with DBCon(sqlite3.connect(db_path)) as (connection, cur):
        cur.execute("select name from sqlite_master where type = 'table'")
        for (table,) in cur.fetchall():
            cur.execute("pragma table_info({});".format(table))
            cols = tuple( r[1] for r in cur.fetchall() )
            colset = frozenset(cols)
            number = 0
            cur.execute(
                    "select `{}` from `{}`".format("`, `".join(cols), table))
            for row in cur.fetchall():
                new_row, where = change(table, cols, list(row))
                new_row = tuple(new_row)
                if new_row == row:
                    continue
                where_cols, where_vals = zip(*where.items())
                where = " and ".join( "`{}` = ?".format(c)
                        for c in where_cols if c in colset )
                select = "select * from {} where {}".format(table, where)
                assert where
                assert len(cur.execute(select, where_vals).fetchall()) == 1
                assert len(row) == len(new_row)
                for col, val, new_val in zip(cols, row, new_row):
                    if val != new_val:
                        number += 1
                        cur.execute(
                                "update `{}` set `{}` = ? where {}".format(
                                    table, col, where),
                                (new_val,) + where_vals)
            connection.commit()
            print(number, 'field(s) changed in', table)
