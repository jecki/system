# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2017. (© http://badw.de)
'''See :func:`main`.'''
import xml.etree.ElementTree as ET
from collections import deque

import regex as re

import __init__
import file_io
import xmlhtml

sub = re.sub
finditer = re.finditer

def convert(
        doc: str,
        smallcaps_cases: 'List[str]',
        ) -> str:

    def tidy_space(match: 'Match[str]') -> str:
        term = match.group().strip()
        term = sub(r'\n+', ' ', term)
        term = sub(r'  +', ' ', term)
        return term

    doc = doc.strip()
    doc = sub(r'(?s)<p[^>]*>((?R)|.)*?</p>', tidy_space, doc)
    doc = doc.replace('<p', ' <p')
    doc = sub(r'\s+<(/?)tr>', '\n <\g<1>tr>', doc)

    for case in smallcaps_cases:
        case = re.escape(case)
        doc = re.sub(f'(?<!\\w){case}(?!\\w)', make_smallcaps, doc)

    doc = sub(r"([\s([{](<[^>]*>)*)'", '\g<1>‘', doc)
    doc = doc.replace("'", '’')
    doc = sub(
            r'([a-zäöüßáéíóúàèìòùâêîôû]{2,}(?:<[^>]*?>)*?)–((?:<[^>]*?>)*?\w{2,})',
            '\g<1>-\g<2>', doc)

    doc = sub(
            r'(?s)<table>(?P<pre>\s*<tr>\s*<th>\s*<p>)(?P<letter>[A-Z])',
            '<table data-letter="\g<letter>">\g<pre>\g<letter>',
            doc)

    doc = rf'''<!DOCTYPE html>
<html>
<head>
 <meta charset="utf-8"/>
 <title>ad fontes</title>
 <style>{CSS}
 </style>
</head>
<body>
{doc}
</body>
</html>
'''
    assert ET.fromstring(doc)
    return doc

def get_styles(attrs: 'Dict[str, str]') -> 'Dict[str, str]':
    styles = {}
    for style in attrs.get('style', '').split(';'):
        if ':' in style:
            key, value = style.split(':', 1)
            styles[key.strip()] = value.strip()
    return styles

def make_smallcaps(match: 'Match[str]') -> str:
    term = match.group().title()
    term = f'<span class="form_sigle">{term}</span>'
    return term

def normalize(doc: str) -> str:
    def consider_endtag(
            tag: str,
            tag_new: str,
            info: dict,
            ) -> None:
        """
        Change (not: re-assign) :param:`info` on :param:`tag` or
        :param:`tag_new`, i.e. on the endtag before or after its
        name is changed. That can be used to control the actions
        done in the other functions.

        If :param:`tag_new` is empty, the corresponding starttag
        has been deleted. The function is not called at all:

        - if there never was a corresponding starttag.
        - for endtags within elements that has been deleted.
        - for self-closing tags (use :func:`replace_tag` instead).
        """
        if tag == 'thead':
            info['in table head'] = False
        return None

    def replace_element(
            tag: str,
            attrs: 'List[Tuple[str, Optional[str]]]',
            startend: bool,
            info: dict,
            ) -> 'Optional[str]':
        """
        Process :param:`tag` and :param:`attrs`.
        A return value of ``None`` does not change the element.
        If a string is returned, the element is replaced by it
        without escaping; if the empty string is returned, the
        element is deleted.
        If the element is void, :param:`startend` is ``True``.
        """
        if tag in {'head', 'script', 'style'}:
            return ''
        return None

    def replace_tag(
            tag: str,
            attrs: 'List[Tuple[str, Optional[str]]]',
            startend: bool,
            info: dict,
            ) -> 'Tuple[str, List[Tuple[str, str]], bool]':
        """
        Process :param:`tag` and :param:`attrs`.
        If the tag is self-closing, :param:`startend` is ``True``.
        If the new tag shall be self-closing, set it to ``True``.
        """
        attrs = dict(attrs)
        align = attrs.get('align', '')
        colspan = attrs.get('colspan', '')

        styles = get_styles(attrs)
        classnames = set()
        for name in attrs.get('class', '').strip().split():
            classnames.add(name.lower())
        classnames &= cssclassnames

        if classnames:
            attrs = {'class': ' '.join(sorted(classnames))}
        else:
            attrs = {}

        if align:
            attrs['align'] = align
        if colspan:
            attrs['colspan'] = colspan
        if tag == 'thead':
            info['in table head'] = True
        if tag == 'td' and info.get('in table head'):
            tag = 'th'
        if tag not in {
                'p', 'i', 'sub', 'sup', 'table', 'tr', 'td', 'th',
                }:
            tag = ''
        if attrs and not tag:
            tag = 'span'
        return tag, sorted(attrs.items()), startend

    cssclassnames = set()
    for match in finditer(r'(?<!\w)\.(\w[^{};]*?)\{', CSS):
        name = match.group(1).strip().lower()
        cssclassnames.add(name)
    doc = xmlhtml.normalize(
            doc,
            consider_endtag = consider_endtag,
            replace_element = replace_element,
            replace_tag     = replace_tag,
            )
    return doc

def smallcaps_sortkey(item: str) -> 'Tuple[int, str]':
    return (1 / len(item), item)

def strip(terms: 'Sequence[str]') -> 'Generator[str]':
    for term in terms:
        yield term.strip()

def main(
        urpath: str,
        urencoding: str,
        smallcaps_path: str,
        ):
    with open(urpath, 'r', encoding = urencoding) as file:
        doc = file.read()
    with open(smallcaps_path, 'r', encoding = 'utf-8') as file:
        smallcaps_cases = list(strip(file.read().strip().splitlines()))
    doc = normalize(doc)
    smallcaps_cases = sorted(smallcaps_cases, key = smallcaps_sortkey)
    doc = convert(doc, smallcaps_cases)
    file_io.write_tell(urpath, doc, 'utf-8')

if __name__ == '__main__':
    CSS = r'''
            .ebene1normal {
                margin-top:3.6pt;
                margin-right:0cm;
                margin-bottom:3.6pt;
                margin-left:11.25pt;
                text-indent:-11.25pt;
            }
            .ebene2normal {
                margin-top:3.6pt;
                margin-right:0cm;
                margin-bottom:3.6pt;
                margin-left:11.25pt;
            }
            .ebene2klein {
                margin-top:3.6pt;
                margin-right:0cm;
                margin-bottom:3.6pt;
                margin-left:11.25pt;
                font-size:10.0pt;
            }
            .ebene3normal {
                margin-top:3.6pt;
                margin-right:0cm;
                margin-bottom:3.6pt;
                margin-left:26.25pt;
            }
            .ebene3klein {
                margin-top:3.6pt;
                margin-right:0cm;
                margin-bottom:3.6pt;
                margin-left:26.25pt;
                font-size:10.0pt;
            }
            .tabelle2normal {
                margin-top:3.6pt;
                margin-right:0cm;
                margin-bottom:3.6pt;
                margin-left:8.5pt;
            }
            .tabelle2klein {
                margin-top:3.6pt;
                margin-right:0cm;
                margin-bottom:3.6pt;
                margin-left:17.0pt;
                text-indent:-8.5pt;
                font-size:10.0pt;
            }
            .tabelle2cil {
                margin-top:3.6pt;
                margin-right:0cm;
                margin-bottom:3.6pt;
                margin-left:8.5pt;
                font-size:10.0pt;
            }
            .tabelle3klein {
                margin-top:3.6pt;
                margin-right:0cm;
                margin-bottom:3.6pt;
                margin-left:30.9pt;
                text-indent:-8.5pt;
                font-size:10.0pt;
            }
            .formsigle1 {
                font-variant:small-caps;
            }
            .formsperrung1 {
                letter-spacing:0.2em;
            }
            p {
                margin-top:3.6pt;
                margin-right:0;
                margin-bottom:3.6pt;
                margin-left:0;
                font-size:12.0pt;
            }
            /* sub and sup styles from github.com/necolas/normalize.css */
            sub,
            sup {
                font-size: 0.8em;
                line-height: 0;
                position: relative;
                vertical-align: baseline;
            }
            sub {
                bottom: -0.25em;
            }
            sup {
                top: -0.5em;
            }
            table {
                border-collapse:collapse;
            }
            td, th {
                background-color:#dae8ff;
                border:2px solid #ffffff;
                padding-left:5px;
                padding-right:5px;
                vertical-align:top;
            }
            th {
                background-color:#4466aa;
                color:#ffffff;
            }
            table table td, table table th {
                border:1px solid #ffffff;
                border-left:none;
                border-right:none;
            }'''
    main(
            urpath = r'T:\TLL\Index TLL.htm',
            urencoding = 'utf-16',
            smallcaps_path = r'T:\TLL\Kapitälchenersetzungen.txt',
            )
