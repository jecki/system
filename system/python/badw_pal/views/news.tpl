% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
<main>
<article class="sheet sheetwide">
	<h1>News and Announcements</h1>
  % for news_item in kwargs['news']:
	<h2><a href="/news/{{news_item['news_id']}}">{{news_item['news_headline']}}</a></h2>
	<p class="flush_unhyph"><time>{{news_item['news_posted']}}</time>.&#8193;{{!news_item['news_teaser']}} <a class="ref" href="/news/{{news_item['news_id']}}"></a></p>
  % end
</article>
</main>\\
% rebase('base.tpl', request = request, dao = dao, kwargs = kwargs)