# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
'''
Data access for the project `Ptolemaeus Arabus et Latinus`.
'''
import itertools
import mmap
import os
import platform
import sqlite3
import subprocess
import sys
import time
import unicodedata
import xml.etree.ElementTree as ET
from collections import defaultdict
from collections import deque
from collections import OrderedDict as odict
from decimal import Decimal as Dec
from functools import partial
from html import escape, unescape
from operator import itemgetter
from urllib.parse import quote, unquote

try:
    import regex as re
except ImportError:
    import re

try:
    import pymysql as mysql
    from pymysql import IntegrityError
except ImportError:
    pass

import __init__
import file_io
import sys_io
import xmlhtml
from compare import get_diacritics
from compare import get_sortnumber
from context import DBCon
from portal import dao_io

DIACRITICS = ''.join(get_diacritics())

KEEPING_FIRST_P_COLS = {
        'mspart_note',
        }

KEEPING_P_COLS = {
        'home_content',
        'job_description',
        'jobs_description',
        'mspart_quotation',
        'news_content',
        }

MULTIPLES_MAP = {
        'za': 'bis',
        'zb': 'ter',
        'zc': 'quater',
        'zd': 'quinqies',
        'ze': 'sexies',
        'zf': 'septies',
        'zg': 'octies',
        'zh': 'nonies',
        'zi': 'decies',
        }

NON_BREAK_SPACE_HYPHEN_COLS = {
        'pagina_range',
        }

PAGINA_RE = re.compile(r'\d+[rv]?')

RICH_TEXT_COLS = {
        'home_content',
        'job_description',
        'jobs_description',
        'ms_bibliography',
        'ms_bibl_special',
        'ms_codicology',
        'ms_content',
        'ms_datestring',
        'ms_note',
        'ms_origin',
        'ms_provenance',
        'mspart_glosses',
        'mspart_info',
        'mspart_note',
        'mspart_quotation',
        'news_content',
        'print_bibliography',
        'print_content',
        'print_exemplar',
        'print_last_page',
        'print_note',
        'print_title',
        'printpart_glosses',
        'printpart_info',
        'printpart_note',
        'printpart_quotation',
        'project_corpus',
        'project_description',
        'user_cv',
        'user_description',
        'user_publications',
        'work_bibliography',
        'work_content',
        'work_printhistory',
        'work_note',
        'work_origin',
        'work_quotation',
        }

SQL_GET_COMMENTARIES_OF_WORK = """
        select
            Works.id               as work_id,
            Works.f_siglum         as work_siglum,
            Works.f_standard_title as work_title,
            Persons.f_person_name  as person_name,
            Languages.f_lang       as lang_name,
            Languages.id           as lang_id
        from Works
        join Creators
            on Creators.f_work = Works.id
        join Persons
            on Persons.id = Creators.f_creator
        join Languages
            on Languages.id = Works.f_lang
        where Creators.f_creator_role = 1
        and Works.f_hidden = 'F'
        and Works.f_commentary_on = %s
        order by Works.f_code
        """
SQL_GET_GLOSSES = """
        SELECT
            T.id                                as tid,
            IFNULL(C.id, IFNULL(CC.id, CCC.id)) as cid,
            T.term                              as expression,
            L.id                                as lang_id,
            L.isocode                           as isocode,
            L.f_lang                            as lang,
            L.f_lang_sorter                     as lang_sorter,
            L.f_rtl                             as rtl,
            T.grammar                           as grammar,
            IFNULL(C.id, IFNULL(CC.id, CCC.id)) as concept_id,
            IFNULL(C.name, IFNULL(CC.name, CCC.name))
                                                as concept,
            TT.term                             as translated_from,
            TT.lang                             as source_lang,
            R.generic_work                      as generic_work,
            R.short_title                       as short_title,
            R.title                             as title,
            GROUP_CONCAT(DISTINCT TR.location ORDER BY TR.location SEPARATOR '; ')
                                                as occurrences
        FROM Gloss_Terms T
        LEFT JOIN Gloss_Terms TT ON T.translated_from=TT.id
        LEFT JOIN Gloss_Terms TTT ON TT.translated_from=TTT.id
        LEFT JOIN Gloss_Concepts C ON T.concept=C.id
        LEFT JOIN Gloss_Concepts CC ON TT.concept=CC.id
        LEFT JOIN Gloss_Concepts CCC ON TTT.concept=CCC.id
        LEFT JOIN Gloss_Termrefs TR ON T.id=TR.termid
        LEFT JOIN Gloss_Refs R ON TR.refid=R.id
        LEFT JOIN Languages L ON L.id=T.lang
        WHERE T.is_modern=0
        GROUP BY concept, T.term, R.title, R.short_title, T.lang, T.grammar, TT.term, T.id, TT.id, TTT.id
        ORDER BY concept, IFNULL(TTT.id,IFNULL(TT.id, T.id)), CASE T.lang WHEN 2 THEN 0 WHEN 1 THEN 1 WHEN 3 THEN 2 ELSE 3 END, T.term, TR.refid
        """ # Attribution notice: by François Charette in 2016 ff.; minuscule modifications by Stefan Müller in 2017 ff.
SQL_GET_GROUPS = """
        select
            id       as group_id,
            plural   as group_heading
        from auth_group
        where do_list = 'T'
        order by do_order
        """
SQL_GET_JOB = """
        select
            f_jobdesc    as job_description,
            f_jobposted  as job_posted,
            f_jobtitle   as job_title,
            f_jobarchive as job_archived
        from t_jobs
        where t_jobs.id = %s
        limit 1
        """
SQL_GET_JOB_VACANCIES = """
        select
            f_jobdesc   as job_description,
            f_jobposted as job_posted,
            f_jobtitle  as job_title
        from t_jobs
        where f_jobarchive = 'F'
        """
SQL_GET_JOBS = """
        select
            id           as job_id,
            f_jobdesc    as job_description,
            f_jobposted  as job_posted,
            f_jobtitle   as job_title,
            f_jobarchive as job_archived
        from t_jobs
        order by f_jobposted desc
        """
SQL_GET_JOBS_DESCRIPTION = """
        select f_jobs_desc as jobs_description
        from t_jobs_desc
        limit 1
        """
SQL_GET_MS = """
        select
            Manuscripts.id                      as ms_id,
            Manuscripts.f_available_source      as ms_avail_source,
            Manuscripts.f_bibliography          as ms_bibliography,
            Manuscripts.f_bibl_special          as ms_bibl_special,
            Manuscripts.f_cat_lang              as cat_lang,
            Manuscripts.f_codicology            as ms_codicology,
            Manuscripts.f_content               as ms_content,
            Manuscripts.f_date_string           as ms_datestring,
            Manuscripts.f_ms_info               as ms_info,
            Manuscripts.f_note                  as ms_note,
            Manuscripts.f_place_compiled_string as ms_origin,
            Manuscripts.f_provenance            as ms_provenance,
            Manuscripts.f_shelfmark             as ms_shelfmark,
            Manuscripts.modified_on             as ms_modified_on,
            Libraries.f_lib_name                as lib_name,
            Libraries.f_lib_abbrev              as lib_abbrev,
            Places.f_placename                  as place_name,
            auth_user.first_name                as user_firstname,
            auth_user.last_name                 as user_lastname
        from Manuscripts
        join Libraries
            on Libraries.id = Manuscripts.f_place_library
        join Places
            on Places.id = Libraries.f_place
        left join auth_user
            on auth_user.id = Manuscripts.f_compiler
        where Manuscripts.id = %s
        and Manuscripts.f_hidden = 'F'
        limit 1
        """
SQL_GET_MSPARTS_OF_MS = """
        select
            MS_Parts.id            as part_id,
            MS_Parts.f_note        as mspart_note,
            MS_Parts.f_range       as pagina_range,
            MS_Parts.f_glosses     as mspart_glosses,
            MS_Parts.f_quotation   as mspart_quotation,
            Works.id               as work_id,
            Works.f_siglum         as work_siglum,
            Works.f_standard_title as work_title,
            Persons.f_person_name  as person_name
        from MS_Parts
        join Manuscripts
            on Manuscripts.id = MS_Parts.f_part_of_ms
        join Works
            on Works.id = MS_Parts.f_part_of_work
        join Creators
            on Creators.f_work = Works.id
        join Persons
            on Persons.id = Creators.f_creator
        where Creators.f_creator_role = 1
        and Manuscripts.id = %s
        and Works.f_hidden = 'F'
        order by MS_Parts.f_order_no
        """
SQL_GET_MSPART_OF_WORK = """
        select
            MS_Parts.id                as part_id,
            MS_Parts.f_range           as pagina_range,
            MS_Parts.f_date_folio_info as mspart_info,
            Manuscripts.f_shelfmark    as ms_shelfmark,
            Libraries.f_lib_name       as lib_name,
            Libraries.f_lib_abbrev     as lib_abbrev,
            Works.id                   as work_id,
            Places.f_placename         as place_name
        from MS_Parts
        join Manuscripts
            on Manuscripts.id = MS_Parts.f_part_of_ms
        join Works
            on Works.id = MS_Parts.f_part_of_work
        join Creators
            on Creators.f_work = Works.id
        join Persons
            on Persons.id = Creators.f_creator
        join Libraries
            on Libraries.id = Manuscripts.f_place_library
        join Places
            on Places.id = Libraries.f_place
        where Creators.f_creator_role = 1
        and Works.id = %s
        and Manuscripts.id = %s
        and Manuscripts.f_hidden = 'F'
        order by
            Places.f_placename,
            Libraries.f_lib_abbrev,
            Manuscripts.f_shelfmark,
            MS_Parts.f_range
        limit 1
        """
SQL_GET_MSPARTS_OF_WORK = """
        select
            MS_Parts.id                as part_id,
            MS_Parts.f_range           as pagina_range,
            MS_Parts.f_date_folio_info as mspart_info,
            Manuscripts.id             as ms_id,
            Manuscripts.f_shelfmark    as ms_shelfmark,
            Manuscripts.f_hidden       as ms_hidden,
            Works.id                   as work_id,
            Libraries.f_lib_name       as lib_name,
            Libraries.f_lib_abbrev     as lib_abbrev,
            Places.f_placename         as place_name
        from MS_Parts
        join Manuscripts
            on Manuscripts.id = MS_Parts.f_part_of_ms
        join Works
            on Works.id = MS_Parts.f_part_of_work
        join Creators
            on Creators.f_work = Works.id
        join Persons
            on Persons.id = Creators.f_creator
        join Libraries
            on Libraries.id = Manuscripts.f_place_library
        join Places
            on Places.id = Libraries.f_place
        where Creators.f_creator_role = 1
        and Works.id = %s
        order by
            Places.f_placename,
            Libraries.f_lib_abbrev,
            Manuscripts.f_shelfmark,
            MS_Parts.f_range
        """
SQL_GET_MSS = """
        select distinct
            Manuscripts.id          as ms_id,
            Manuscripts.f_shelfmark as ms_shelfmark,
            Libraries.f_lib_name    as lib_name,
            Libraries.f_lib_abbrev  as lib_abbrev,
            Places.f_placename      as place_name,
            Works.id                as work_id,
            Works.f_siglum          as work_siglum,
            Works.f_standard_title  as work_title,
            Persons.f_person_name   as person_name,
            Languages.f_lang        as lang_name,
            Languages.id            as lang_id
        from Manuscripts
        join Libraries
            on Libraries.id = Manuscripts.f_place_library
        join Places
            on Places.id = Libraries.f_place
        join MS_Parts
            on MS_Parts.f_part_of_ms = Manuscripts.id
        join Works
            on Works.id = MS_Parts.f_part_of_work
        join Creators
            on Creators.f_work = Works.id
        join Persons
            on Persons.id = Creators.f_creator
        join Languages
            on Languages.id = Works.f_lang
        where Creators.f_creator_role = 1
        and Manuscripts.f_hidden = 'F'
        and Works.f_hidden = 'F'
        order by Libraries.f_lib_name, Manuscripts.f_shelfmark
        """
SQL_GET_MSS_PAGINA_RANGE = """
        select distinct
            Manuscripts.id          as ms_id,
            Manuscripts.f_shelfmark as ms_shelfmark,
            Libraries.f_lib_name    as lib_name,
            Libraries.f_lib_abbrev  as lib_abbrev,
            Places.f_placename      as place_name,
            MS_Parts.id             as part_id,
            MS_Parts.f_range        as pagina_range,
            Works.id                as work_id,
            Works.f_siglum          as work_siglum,
            Works.f_standard_title  as work_title,
            Persons.f_person_name   as person_name,
            Languages.f_lang        as lang_name,
            Languages.id            as lang_id
        from Manuscripts
        join Libraries
            on Libraries.id = Manuscripts.f_place_library
        join Places
            on Places.id = Libraries.f_place
        join MS_Parts
            on MS_Parts.f_part_of_ms = Manuscripts.id
        join Works
            on Works.id = MS_Parts.f_part_of_work
        join Creators
            on Creators.f_work = Works.id
        join Persons
            on Persons.id = Creators.f_creator
        join Languages
            on Languages.id = Works.f_lang
        where Creators.f_creator_role = 1
        and Manuscripts.f_hidden = 'F'
        and Works.f_hidden = 'F'
        """
SQL_GET_NEWS = """
        select
            id         as news_id,
            f_content  as news_content,
            f_headline as news_headline,
            f_posted   as news_posted
        from t_news
        order by f_posted desc
        """
SQL_GET_NEWS_ITEM = """
        select
            id         as news_id,
            f_content  as news_content,
            f_headline as news_headline,
            f_posted   as news_posted
        from t_news
        where id = %s
        limit 1
        """
SQL_GET_PRINT = """
        select
            Editions.id               as print_id,
            Editions.f_bibliography   as print_bibliography,
            Editions.f_content        as print_content,
            Editions.f_date_num_exact as print_date,
            Editions.f_exemplar       as print_exemplar,
            Editions.f_last_page      as print_last_page,
            Editions.f_note           as print_note,
            Editions.f_publisher      as print_publisher,
            Editions.f_title          as print_title,
            Places.f_placename        as place_name
        from Editions
        join Ed_Parts
            on Ed_Parts.f_part_of_edition = Editions.id
        join Places
            on Places.id = Editions.f_place_compiled
        where Editions.id = %s
        and Editions.f_hidden = 'F'
        limit 1
        """
SQL_GET_PRINTPART_OF_WORK = """
        select
            Ed_Parts.id               as part_id,
            Ed_Parts.f_range          as pagina_range,
            Editions.f_date_num_exact as print_date,
            Editions.f_publisher      as print_publisher,
            Works.id                  as work_id,
            Places.f_placename        as place_name
        from Ed_Parts
        join Editions
            on Editions.id = Ed_Parts.f_part_of_edition
        join Works
            on Works.id = Ed_Parts.f_part_of_work
        join Creators
            on Creators.f_work = Works.id
        join Persons
            on Persons.id = Creators.f_creator
        join Places
            on Places.id = Editions.f_place_compiled
        where Creators.f_creator_role = 1
        and Works.id = %s
        and Editions.id = %s
        and Editions.f_hidden = 'F'
        order by
            Places.f_placename,
            Editions.f_publisher,
            Editions.f_date_num_exact
        limit 1
        """
SQL_GET_PRINTPARTS_OF_PRINT = """
        select
            Ed_Parts.id            as part_id,
            Ed_Parts.f_note        as printpart_note,
            Ed_Parts.f_quotation   as printpart_quotation,
            Ed_Parts.f_range       as pagina_range,
            Works.id               as work_id,
            Works.f_siglum         as work_siglum,
            Works.f_standard_title as work_title,
            Persons.f_person_name  as person_name
        from Ed_Parts
        join Editions
            on Editions.id = Ed_Parts.f_part_of_edition
        join Works
            on Works.id = Ed_Parts.f_part_of_work
        join Creators
            on Creators.f_work = Works.id
        join Persons
            on Persons.id = Creators.f_creator
        where Creators.f_creator_role = 1
        and Editions.id = %s
        and Works.f_hidden = 'F'
        order by Ed_Parts.f_order_no
        """
SQL_GET_PRINTPARTS_OF_WORK = """
        select
            Ed_Parts.id                as part_id,
            Ed_Parts.f_date_folio_info as printpart_info,
            Ed_Parts.f_range           as pagina_range,
            Editions.id                as print_id,
            Editions.f_date_num_exact  as print_date,
            Editions.f_publisher       as print_publisher,
            Editions.f_hidden          as print_hidden,
            Works.id                   as work_id,
            Places.f_placename         as place_name
        from Ed_Parts
        join Editions
            on Editions.id = Ed_Parts.f_part_of_edition
        join Works
            on Works.id = Ed_Parts.f_part_of_work
        join Creators
            on Creators.f_work = Works.id
        join Persons
            on Persons.id = Creators.f_creator
        join Places
            on Places.id = Editions.f_place_compiled
        where Creators.f_creator_role = 1
        and Works.id = %s
        order by
            Places.f_placename,
            Editions.f_publisher,
            Editions.f_date_num_exact
        """
SQL_GET_PRINTS = """
        select distinct
            Editions.id               as print_id,
            Editions.f_date_num_exact as print_date,
            Editions.f_publisher      as print_publisher,
            Editions.f_title          as print_title,
            Places.f_placename        as place_name,
            Works.id                  as work_id,
            Works.f_siglum            as work_siglum,
            Works.f_standard_title    as work_title,
            Persons.f_person_name     as person_name,
            Languages.f_lang          as lang_name,
            Languages.id              as lang_id
        from Editions
        join Places
            on Places.id = Editions.f_place_compiled
        join Ed_Parts
            on Ed_Parts.f_part_of_edition = Editions.id
        join Works
            on Works.id = Ed_Parts.f_part_of_work
        join Creators
            on Creators.f_work = Works.id
        join Persons
            on Persons.id = Creators.f_creator
        join Languages
            on Languages.id = Works.f_lang
        where Creators.f_creator_role = 1
        and Editions.f_hidden = 'F'
        and Works.f_hidden = 'F'
        order by Places.f_placename, Editions.f_publisher
        """
SQL_GET_PRINTS_PAGINA_RANGE = """
        select distinct
            Editions.id               as print_id,
            Editions.f_date_num_exact as print_date,
            Editions.f_publisher      as print_publisher,
            Editions.f_title          as print_title,
            Places.f_placename        as place_name,
            Ed_Parts.id               as part_id,
            Ed_Parts.f_range          as pagina_range,
            Works.id                  as work_id,
            Works.f_siglum            as work_siglum,
            Works.f_standard_title    as work_title,
            Persons.f_person_name     as person_name,
            Languages.f_lang          as lang_name,
            Languages.id              as lang_id
        from Editions
        join Places
            on Places.id = Editions.f_place_compiled
        join Ed_Parts
            on Ed_Parts.f_part_of_edition = Editions.id
        join Works
            on Works.id = Ed_Parts.f_part_of_work
        join Creators
            on Creators.f_work = Works.id
        join Persons
            on Persons.id = Creators.f_creator
        join Languages
            on Languages.id = Works.f_lang
        where Creators.f_creator_role = 1
        and Editions.f_hidden = 'F'
        and Works.f_hidden = 'F'
        """
SQL_GET_PROJECT = """
        select
            f_desc as   project_description,
            f_corpus as project_corpus
        from t_project
        limit 1
        """
SQL_GET_STARTTEXT = """
        select
            f_home as home_content
        from t_home
        limit 1
        """
SQL_GET_TERMS = r"""
        select
            Gloss_Terms.id              as id,
            Gloss_Terms.lang            as lang_id,
            Gloss_Terms.term            as expression,
            Gloss_Terms.concept         as concept_id,
            Gloss_Terms.translated_from as from_id,
            Gloss_Terms.is_modern       as is_modern,
            Gloss_Terms.grammar         as grammar,
            Gloss_Terms.note            as note,
            Gloss_Concepts.name         as concept,
            Languages.f_lang            as lang,
            Languages.isocode           as lang_abbr,
            Languages.f_lang_sorter     as lang_sorter,
            Languages.f_rtl             as rtl
        from Gloss_Terms
        join Gloss_Concepts
            on Gloss_Concepts.id = Gloss_Terms.concept
        join Languages
            on Languages.id = Gloss_Terms.lang
        where Gloss_Terms.is_active = 'T'
        """
SQL_GET_TRANSLATIONS_OF_WORK = """
        select
            Works.id               as work_id,
            Works.f_siglum         as work_siglum,
            Works.f_standard_title as work_title,
            Persons.f_person_name  as person_name,
            Languages.f_lang       as lang_name,
            Languages.id           as lang_id
        from Works
        join Creators
            on Creators.f_work = Works.id
        join Persons
            on Persons.id = Creators.f_creator
        join Languages
            on Languages.id = Works.f_lang
        where Creators.f_creator_role = 1
        and Works.f_hidden = 'F'
        and Works.f_translation_of = %s
        order by Works.f_code
        """
SQL_GET_USER = """
        select
            auth_user.id          as user_id,
            auth_user.first_name  as user_firstname,
            auth_user.last_name   as user_lastname,
            auth_user.institution as user_institution,
            auth_user.title       as user_title,
            auth_user.deg         as user_degree,
            auth_user.cv          as user_cv,
            auth_user.cv_link     as user_cvlink,
            auth_user.description as user_description,
            auth_user.email       as user_email,
            auth_user.picture     as user_picturefile,
            auth_user.pubs        as user_publications,
            auth_group.role       as group_role
        from auth_user
        join auth_membership
            on auth_membership.user_id = auth_user.id
        join auth_group
            on auth_group.id = auth_membership.group_id
        where auth_group.do_list = 'T'
        and auth_user.id = %s
        """
SQL_GET_USERS = """
        select
            auth_user.id          as user_id,
            auth_user.first_name  as user_firstname,
            auth_user.last_name   as user_lastname,
            auth_user.institution as user_institution,
            auth_user.title       as user_title,
            auth_user.deg         as user_degree
        from auth_user
        join auth_membership
            on auth_membership.user_id = auth_user.id
        where auth_membership.group_id = %s
        and auth_user.last_name is not null
        and auth_user.last_name != ''
        order by
            auth_user.last_name,
            auth_user.first_name
        """
SQL_GET_WORK = """
        select
            Works.id                as work_id,
            Works.f_siglum          as work_siglum,
            Works.f_standard_title  as work_title,
            Works.f_translation_of  as work_translated_from,
            Works.f_commentary_on   as work_commentatum,
            Works.f_content         as work_content,
            Works.f_quotation       as work_quotation,
            Works.f_origin          as work_origin,
            Works.f_note            as work_note,
            Works.f_bibliography    as work_bibliography,
            Works.f_edition_history as work_printhistory,
            Works.modified_on       as work_modified_on,
            Persons.f_person_name   as person_name,
            Languages.f_lang        as lang_name,
            Languages.id            as lang_id,
            auth_user.first_name    as user_firstname,
            auth_user.last_name     as user_lastname
        from Works
        join Creators
            on Creators.f_work = Works.id
        join Persons
            on Persons.id = Creators.f_creator
        join Languages
            on Languages.id = Works.f_lang
        left join auth_user
            on auth_user.id = Works.f_compiler
        where Creators.f_creator_role = 1
        and Works.id = %s
        and Works.f_hidden = 'F'
        limit 1
        """
SQL_GET_WORK_OF_MSPART = """
        select
            Works.id                as work_id,
            Works.f_siglum          as work_siglum,
            Works.f_standard_title  as work_title,
            Works.f_translation_of  as work_translated_from,
            Works.f_commentary_on   as work_commentatum,
            Works.f_content         as work_content,
            Works.f_quotation       as work_quotation,
            Works.f_origin          as work_origin,
            Works.f_note            as work_note,
            Works.f_bibliography    as work_bibliography,
            Works.f_edition_history as work_printhistory,
            Works.modified_on       as work_modified_on,
            Persons.f_person_name   as person_name,
            Languages.f_lang        as lang_name,
            Languages.id            as lang_id,
            auth_user.first_name    as user_firstname,
            auth_user.last_name     as user_lastname
        from Works
        join Creators
            on Creators.f_work = Works.id
        join Persons
            on Persons.id = Creators.f_creator
        join Languages
            on Languages.id = Works.f_lang
        left join auth_user
            on auth_user.id = Works.f_compiler
        join MS_Parts
            on MS_Parts.f_part_of_work = Works.id
        where MS_Parts.id = %s
        and Works.f_hidden = 'F'
        limit 1
        """
SQL_GET_WORK_OF_PRINTPART = """
        select
            Works.id                as work_id,
            Works.f_siglum          as work_siglum,
            Works.f_standard_title  as work_title,
            Works.f_translation_of  as work_translated_from,
            Works.f_commentary_on   as work_commentatum,
            Works.f_content         as work_content,
            Works.f_quotation       as work_quotation,
            Works.f_origin          as work_origin,
            Works.f_note            as work_note,
            Works.f_bibliography    as work_bibliography,
            Works.f_edition_history as work_printhistory,
            Works.modified_on       as work_modified_on,
            Persons.f_person_name   as person_name,
            Languages.f_lang        as lang_name,
            Languages.id            as lang_id,
            auth_user.first_name    as user_firstname,
            auth_user.last_name     as user_lastname
        from Works
        join Creators
            on Creators.f_work = Works.id
        join Persons
            on Persons.id = Creators.f_creator
        join Languages
            on Languages.id = Works.f_lang
        left join auth_user
            on auth_user.id = Works.f_compiler
        join Ed_Parts
            on Ed_Parts.f_part_of_work = Works.id
        where Ed_Parts.id = %s
        and Works.f_hidden = 'F'
        limit 1
        """
SQL_GET_WORKS = """
        select
            Works.id               as work_id,
            Works.f_siglum         as work_siglum,
            Works.f_standard_title as work_title,
            Persons.f_person_name  as person_name,
            Languages.f_lang       as lang_name,
            Languages.id           as lang_id
        from Works
        join Creators
            on Creators.f_work = Works.id
        join Persons
            on Persons.id = Creators.f_creator
        join Languages
            on Languages.id = Works.f_lang
        where Creators.f_creator_role = 1
        and Works.f_hidden = 'F'
        """

SQLITE_CREATE_INDEX_DOC = """
        create index if not exists i1 on repros
        (doc_type, doc_id, part_id, pagina)
        """
SQLITE_CREATE_INDEX_WORK = """
        create index if not exists i2 on repros
        (part_id, pagina)
        """
SQLITE_CREATE_TABLE_REPROS = """
        create table if not exists repros (
            doc_type      TEXT default '', -- ms or print
            doc_id        TEXT default '', -- e.g. '123'
            part_id       TEXT default '', -- e.g. '4'
            pagina        TEXT default '', -- e.g. '235zar'
            pagina_sorted TEXT default '', -- e.g. '000000235zar'
            pagina_styled TEXT default '', -- e.g. '235<sup>bis</sup>r'
            fulltran_part TEXT default '', -- e.g. '7' (the part of the full transcript)
            scan_path     TEXT default '', -- e.g. 'Paris, BnF, xyz #123/Ptol. Alm. Ger. #4/235zar.jpg' or 'Paris, BnF, xyz #123/ZZ.jpg'
            tran_path     TEXT default '', -- e.g. 'Paris, BnF, xyz #123/Ptol. Alm. Ger. #4/transcription/pages/235zar.xml'
            UNIQUE(
                doc_type,
                part_id,
                pagina
                ) ON CONFLICT REPLACE
        )
        """
SQLITE_GET_TRAN_PATH = """
        select tran_path from repros
        where doc_type = ? and doc_id = ? and part_id = ? and tran_path != ''
        limit 1
        """
SQLITE_INSERT_OR_REPLACE_REPRO = """
        insert or replace into repros values (
            ?,
            ?,
            ?,
            ?,
            ?,
            ?,
            ?,
            ?,
            ?
        )
        """

class DB(dao_io.Folder):
    def __init__(
            self,
            config_path: str = __file__ + '/../../../../configuration.utf-8.ini',
            glosses_path: str = __file__ + '/../../../../glosses.xml',
            repros_path: str = __file__ + '/../../../../../repros',
            is_refreshing: bool = True,
            is_collecting_fulltext: bool = True,
            wal_mode: bool = True,
            ) -> None:
        '''
        The docstring of :class:`dao_io.Folder` is relevant.
        During initialization a new exchange database is made if none
        is present at the path that is specified by the configuration;
        and if :param:`is_refreshing` is ``True``, :func:`refresh_forever`
        is started (see the docstring of said function).

        :param config_path: Path to a file containing the configuration.
        :param glosses_path: Path to the file containing all placeholder
            keywords and their corresponding translations, which are inserted
            into the served page according to the currently chosen language.
        :param repros_path: Path to a folder named e.g. `repros`, which contains
            the facsimilia and transcriptions as follows. A note beforehand:

            - `□` means a folder, `▪` means a file.
            - For the names of the files and folders, the following applies:

              - Explanative additions are written in parentheseis
                and do not belong to the name.
              - Example parts are written in quotations marks. They are not
                used by the system, but improve human readability.
              - Mandatory name patterns are written in angular brackets.
              - Mandatory parts are written as-is.

            ::

            □ repros
            ├─ □ ms (contains the manuscripts)
            │  ├─ □ "Basel, UB, F.III.33" #<permanent ID of the MS>
            │  │  ├─ □ "..." #<permanent ID of the work>
            │  │  │  ├─ □ transcription
            │  │  │  │  ├─ □ agendum (contains the transcribers’ input file(s))
            │  │  │  │  ├─ □ intus (contains files embedded in the transcription)
            │  │  │  │  ├─ □ pages (contains the transcription paginated)
            │  │  │  │  │  ├─ ▪ <001r.xml>
            │  │  │  │  │  ├─ ▪ <001v.xml>
            │  │  │  │  │  ├─ ▪ <002r.xml>
            │  │  │  │  │  ⁞
            │  │  │  │  │
            │  │  │  │  ├─ ▪ <1.xml>
            │  │  │  │  ⁞
            │  │  │  │
            │  │  │  ├─ ▪ <001r.jpg>
            │  │  │  ├─ ▪ <001v.jpg>
            │  │  │  ├─ ▪ <002r.jpg>
            │  │  │  ⁞
            │  │  │
            │  │  ├─ □ "..." #<permanent ID of the work>
            │  │  ⁞
            │  │
            │  ├─ □ "Basel, UB, F.V.22" #<permanent ID of the MS>
            │  ⁞
            │
            └─ □ print (contains the prints)
               ├─ □ "Venice, Petrus Liechtenstein, 1515" #<permanent ID of the print>
               ⁞

        :param wal_mode: Only relevant, if :param:`is_refreshing` is ``True``.
            See the docstring of :func:`make_new_empty_db_if_absent`.
        '''
        # To use ET or xmlhtml in templates without import or parameter:
        self.ET = ET
        self.xmlhtml = xmlhtml

        self.multisite = False

        config_path = os.path.abspath(config_path)
        glosses_path = os.path.abspath(glosses_path)
        self.repros_path = os.path.abspath(repros_path)

        self.config = config = file_io.get_config(config_path)

        paths = file_io.get_abspaths(config, config_path)
        self.paths = paths
        self.path = paths['public_path'] + '/'
        self.meta_path = paths['meta_path']
        fulltext_index_path = file_io.pave(paths['fulltext_index_path'])
        db_access_path = paths['db_access_path']

        self.default_page_id = tuple(config['default_page_id'])[0]
        self.default_lang_id = 'en'
        self.default_lang_ids = {
                '': self.default_lang_id,
                'jordanus': 'en',
                } #### TODO: Into config!
        self.default_site_id = ''
        self.site_ids = ('',)

        self.glosses = self.read_glosses(glosses_path)

        self.remote_static_path = config['old']['remote_static_path']

        self.connect = get_connect(config, db_access_path)
        self.connect_jordanus = get_connect(
                config, db_access_path, db_name_key = 'name_jordanus')

        if is_refreshing:
            make_new_empty_db_if_absent(self.meta_path, wal_mode)
            self.process = subprocess.Popen(
                    [
                        sys.executable,
                        __file__,
                        str(os.getpid()),
                        config_path,
                        self.repros_path,
                    ])
        if is_collecting_fulltext:
            self.process_fulltext = subprocess.Popen(
                    [
                        sys.executable,
                        file_io.join(__file__, 'collect_fulltext.py'),
                        str(os.getpid()),
                        config_path,
                    ])

        self.make_temptable_jordanus()

    def add_sentence_jordanus(
            self,
            ms_id: 'Optional[int]',
            subsiglum: 'Optional[int]',
            field_id: int,
            term: str,
            ) -> int:
        '''
        In Jordanus, add a new sentence to the database: about the manuscript
        :param:`ms_id` and its part :param:`subsiglum`, which is ``0`` if the
        whole manuscript is meant. The predicate (or verb) of the sentence is
        :param:`field_id`, the predicative is the term :param:`term`.

        If the term is not already given in the database, it is added. Its ID
        is used in the sentence. Return the ID of the new sentence.

        If :param:`ms_id` is ``None``, a new manuscript is added. The ID will
        be the greatest manuscript ID so far incremented by ``1``. If, at the
        same time, :param:`subsiglum` is ``None``, too, the subsiglum will be
        set to ``0``.

        If only :param:`subsiglum` is none, the subsiglum will be the highest
        subsiglum so far incremented by ``1``.
        '''
        with DBCon(self.connect_jordanus()) as (connection, cur):
            while True:
                cur.execute(r'''
                        select term_id from terms where term = %s limit 1
                            LOCK IN SHARE MODE''', term)
                match = cur.fetchone()
                if match:
                    term_id = match['term_id']
                    break
                cur.execute(r'insert into terms (term) values (%s)', term)
            if ms_id is None:
                cur.execute(r'''
                        select @num := max(ms_id) from sentences FOR UPDATE;
                        insert into sentences
                            (ms_id, subsiglum, field_id, term_id) values (
                                @num + 1,
                                %s, %s, %s
                                );
                        ''', (subsiglum or 0, field_id, term_id))
            elif subsiglum is None:
                cur.execute(r'''
                        select @num := max(subsiglum) from sentences
                            where ms_id = %s FOR UPDATE;
                        insert into sentences
                            (ms_id, subsiglum, field_id, term_id) values (
                                %s,
                                @num + 1,
                                %s, %s
                                );
                        ''', (ms_id, ms_id, field_id, term_id))
            else:
                cur.execute(r'''
                        insert into sentences
                            (ms_id, subsiglum, field_id, term_id) values
                            (%s, %s, %s, %s);
                        ''', (ms_id, subsiglum, field_id, term_id))
            connection.commit()
            sentence_id = cur.lastrowid
        return sentence_id

    def change_sentence_term_jordanus(
            self,
            sentence_id: int,
            term: str,
            ) -> 'Optional[int]':
        '''
        In Jordanus, change the sentence with the ID :param:`sentence_id`. It
        is :param:`term` the new predicative (or verb) of the sentence.

        If the term is not already given in the database, it is added. Its ID
        is used in the sentence. Return the ID of the eventually used term.

        If the old term is no longer used, delete it. - This may leave a term
        behind that is no longer used in any sentence. Delete this term, too.

        If :param:`sentence_id` is unknown, return ``None``.
        '''
        term_id = None
        with DBCon(self.connect_jordanus()) as (connection, cur):
            # We check whether a sentence with sentence_id exists and, if so,
            # get its old term_id for deleting the term if it is no longer used.
            cur.execute(r'''
                    select term_id from sentences where sentence_id = %s
                    ''', (sentence_id,))
            match = cur.fetchone()
            if match:
                old_term_id = match['term_id']
                while True:
                    cur.execute(r'''
                            select term_id from terms where term = %s limit 1
                                LOCK IN SHARE MODE''', (term,))
                    match = cur.fetchone()
                    if match:
                        term_id = match['term_id']
                        break
                    cur.execute(r'''
                            insert into terms (term) values (%s)
                            ''', (term,))
                cur.execute(r'''
                        update sentences set term_id = %s where sentence_id = %s
                        ''', (term_id, sentence_id))
                connection.commit()
                try:
                    cur.execute(r'''
                            delete from terms where term_id = %s
                            ''', (old_term_id,))
                    connection.commit()
                except IntegrityError:
                    connection.rollback()
            return term_id

    def del_sentence_jordanus(self, sentence_id: int) -> None:
        '''
        In Jordanus, delete the sentence with the ID :param:`sentence_id`.

        This may leave a term or a field behind that is no longer used in
        any sentence. A dangling field should not be removed - a dangling
        term, however, is removed.
        '''
        with DBCon(self.connect_jordanus()) as (connection, cur):
            cur.execute(r'select term_id from sentences where sentence_id = %s',
                    (sentence_id,))
            sentence = cur.fetchone()
            if sentence is None:
                return
            cur.execute(r'delete from sentences where sentence_id = %s',
                    (sentence_id,))
            connection.commit()
            term_id = sentence.get('term_id', None)
            if term_id is None:
                return
            try:
                cur.execute(r'delete from terms where term_id = %s', (term_id,))
                connection.commit()
            except IntegrityError:
                connection.rollback()

    def format_title(self, term: str) -> str:
        term = term.split(' (by')[0]
        if ' (' in term:
            term, tail = term.split(' (', 1)
            term = '<cite>{}</cite> ({}'.format(term, tail)
        else:
            term = '<cite>{}</cite>'.format(term)
        return term

    def get_index(
            self,
            kind: str,
            ) -> 'Generator[Dict[str, str]]':
        '''
        Yield all database entities of a certain :param:`kind`.
        The format is apt for a datatable.
        '''
        with DBCon(self.connect()) as (connection, cur):
            if kind == 'glossary':
                cur.execute(SQL_GET_GLOSSES)
                for entry in cur.fetchall():
                    concept = entry['concept'] or ''
                    concept_id = entry['concept_id']
                    concept_sorter = concept.lower()
                    expression = entry['expression'] or ''
                    expression_sorter = expression.lower()
                    concept = '<a class="key" href="/glossary/{}/{}">{}</a>'\
                            .format(concept_id, quote(expression), concept)
                    expression = '<a class="key" href="/glossary/{}/{}">{}</a>'\
                            .format(concept_id, quote(expression), expression)
                    isocode = entry['isocode'] or ''
                    if entry['rtl']:
                        expression = '<div lang="{}" dir="rtl">{}</div>'.format(
                                isocode, expression)
                    else:
                        expression = '<div lang="{}">{}</div>'.format(
                                isocode, expression)
                    yield { str(rank): item for rank, item in enumerate((
                            {
                                '_': concept,
                                'sort': concept_sorter,
                            },
                            {
                                '_': expression,
                                'sort': expression_sorter,
                            },
                            {
                                '_': entry['lang'],
                                'sort': entry['lang_sorter'],
                            },
                            entry['translated_from'] or '',
                            entry['generic_work'] or '',
                            entry['short_title'] or '',
                            entry['occurrences'] or '',
                            )) }
            elif kind == 'mss':
                cur.execute(SQL_GET_MSS)
                for doc in cur.fetchall():
                    place = str(doc['place_name'])
                    place = (
                            ('' if place == '?' else place + ', ') +
                            (str(doc['lib_abbrev'] or doc['lib_name'])) +
                            '<span class="trim">, ' +
                            str(doc['ms_shelfmark']) + '</span>'
                            )
                    yield { str(rank): item for rank, item in enumerate((
                            {
                                '_': '<a href="ms/{}">{}</a>'.format(
                                    doc['ms_id'], place),
                                'sort': get_sortstring(place)
                            },
                            get_siglum(doc),
                            get_person_name(doc),
                            get_title(doc),
                            str(doc['lang_name']),
                            )) }
            elif kind == 'prints':
                cur.execute(SQL_GET_PRINTS)
                for doc in cur.fetchall():
                    title = doc['print_title']
                    if len(title) > 80:
                        title = title[:80] + '…'
                    title = '<a href="print/{}">{}</a>'.format(
                            doc['print_id'],
                            escape(title),
                            )
                    yield { str(rank): item for rank, item in enumerate((
                            ', '.join((
                                doc['place_name'],
                                doc['print_publisher'],
                                str(doc['print_date']))),
                            title,
                            get_siglum(doc),
                            get_person_name(doc),
                            get_title(doc),
                            str(doc['lang_name']),
                            )) }
            elif kind == 'scans':
                with DBCon(sqlite3.connect(self.meta_path)) as (_, subcur):
                    subcur.execute("""
                            select doc_type, doc_id, part_id, pagina,
                                min(pagina_sorted)
                            from repros group by doc_type, doc_id, part_id
                            """)
                    repros = subcur.fetchall()
                for (doc_type, doc_id, part_id, pagina, _) in repros:
                    if doc_type == 'ms':
                        cur.execute(SQL_GET_WORK_OF_MSPART, (part_id,))
                    else:
                        cur.execute(SQL_GET_WORK_OF_PRINTPART, (part_id,))
                    work = cur.fetchone()
                    location = self.get_location(doc_type, doc_id)
                    if work and location:
                        yield {
                                str(rank): item for rank, item in enumerate((
                                    get_siglum(work),
                                    get_person_name(work),
                                    get_title(work),
                                    {
                                        '_': '<a href="/{}/{}">{}</a>'\
                                            .format(doc_type, doc_id, location),
                                        'sort': get_sortstring(location),
                                    },
                                    '<a class="ref" href="/{}/{}/{}/{}">Scan</a>'\
                                        .format(doc_type, doc_id, part_id,
                                            pagina),
                                    work['lang_name'],
                                    )) }
            elif kind == 'transcriptions':
                with DBCon(sqlite3.connect(self.meta_path)) as (_, subcur):
                    subcur.execute("""
                            select doc_type, doc_id, part_id, min(fulltran_part)
                            from repros
                            where fulltran_part <> ''
                            group by doc_type, doc_id, part_id
                            """)
                    repros = subcur.fetchall()
                for (doc_type, doc_id, part_id, fulltran_part) in repros:
                    if doc_type == 'ms':
                        cur.execute(SQL_GET_WORK_OF_MSPART, (part_id,))
                    else:
                        cur.execute(SQL_GET_WORK_OF_PRINTPART, (part_id,))
                    work = cur.fetchone()
                    location = self.get_location(doc_type, doc_id)
                    if work and location:
                        yield {
                                str(rank): item for rank, item in enumerate((
                                    get_siglum(work),
                                    get_person_name(work),
                                    get_title(work),
                                    {
                                        '_': '<a href="/{}/{}">{}</a>'\
                                            .format(doc_type, doc_id, location),
                                        'sort': get_sortstring(location),
                                    },
                                    '<a class="ref" href="/{}/{}/{}/transcription/{}">Transcription</a>'\
                                        .format(doc_type, doc_id, part_id,
                                            fulltran_part),
                                    work['lang_name'],
                                    )) }
            elif kind == 'works':
                cur.execute(SQL_GET_WORKS)
                for work in cur.fetchall():
                    yield { str(rank): item for rank, item in enumerate((
                            get_siglum(work),
                            get_person_name(work),
                            get_title(work),
                            str(work['lang_name']),
                            )) }
            else:
                raise Exception('Unknown :param:`kind` of index: ' + str(kind))

    def get_index_jordanus(
            self,
            kind: str,
            pagina_re: 'Pattern[str]' = re.compile(r'^\s*\(.*?\)\s*'),
            ) -> 'Generator[Dict[str, str]]':
        '''
        Yield all database entities of a certain :param:`kind` from Jordanus.
        The format is apt for a datatable.
        '''
        if kind == 'mss':
            with DBCon(self.connect_jordanus()) as (connection, cur):
                cur.execute('select ms_id, location from temp')
                for entry in cur.fetchall():
                    ms_id = entry['ms_id']
                    term = entry['location']
                    term_evened = even_out_for_search(term)
                    yield { '0': {
                            '_': '<a href="ms/{0}">{1}'\
                                 '</a><div hidden="">{2}</div>'.format(
                                     ms_id,
                                     escape(term),
                                     term_evened),
                            'sort': term_evened,
                            }}
        else:
            fields, hypernum = {
                    'authors': ((57, 58), 2),
                    'titles': ((9, 10, 11), 3),
                    'incipits': ((65, 66), 5),
                    }.get(kind, ((), 0))
            if not fields:
                return
            with DBCon(self.connect_jordanus()) as (connection, cur):
                cur.execute('''
                        select distinct term
                        from sentences
                        join terms on terms.term_id = sentences.term_id
                        where sentences.field_id in {}
                        '''.format(fields))
                for entry in cur.fetchall():
                    term = entry['term']
                    if kind == 'incipits':
                        term_evened = even_out_for_search(
                                pagina_re.sub('', term))
                    else:
                        term_evened = even_out_for_search(term)
                    yield { '0': {
                            '_': '<a href="search_results?{0}={1}&exact=1">{2}'\
                                 '</a><div hidden="">{3}</div>'.format(
                                     hypernum,
                                     quote(term),
                                     escape(term),
                                     term_evened),
                            'sort': term_evened,
                            }}

    def get_jobs(self, job_id: str = '') -> 'List[Dict[str, str]]':
        '''
        If :param:`job_id` is empty, get all current and former vacancies.
        Else, get the vacancy with the id :param:`job_id`.
        In any case, return a list (consisting of one or more items).
        '''
        with DBCon(self.connect()) as (connection, cur):
            if job_id:
                cur.execute(SQL_GET_JOB, job_id)
            else:
                cur.execute(SQL_GET_JOBS)
            return [ self.modify_cols(item) for item in cur.fetchall() ]

    def get_jobs_information(self) -> 'Tuple[str, List[Dict[str, str]]]':
        '''
        Get a description of the current job positions in general and the
        description of any open jobs.
        '''
        with DBCon(self.connect()) as (connection, cur):
            cur.execute(SQL_GET_JOBS_DESCRIPTION)
            general = self.modify_cols(cur.fetchone() or {'': ''}
                    ).popitem()[1]
            cur.execute(SQL_GET_JOB_VACANCIES)
            vacancies = [
                    self.modify_cols(item) for item in cur.fetchall() ]
            return general, vacancies

    def get_langs_sorted(self) -> 'List[Dict[str, str]]':
        with DBCon(self.connect()) as (connection, cur):
            cur.execute(r"""
                    select
                        id     as lang_id,
                        f_lang as lang,
                        f_lang_sorter as lang_sorter
                    from Languages
                    where is_active = 'T'
                    order by f_lang_sorter
                    """)
            return cur.fetchall()

    def get_lemma(
            self,
            concept_id: 'Optional[int]',
            expression: 'Optional[str]',
            item_id: 'Optional[int]' = None,
            with_transl_froms: bool = False,
            with_transl_intos: bool = False,
            with_synonyms: bool = False,
            ) -> 'Dict[str, Union[str, Dict[str, str]]]':
        '''
        From the glossary, get information related both to the concept
        :param:`concept_id` and to the expression :param:`expression`.
        '''
        lemma = {}
        with DBCon(self.connect()) as (connection, cur):
            if item_id is None:
                cur.execute((SQL_GET_TERMS + """
                        and Gloss_Terms.concept = %s
                        and Gloss_Terms.term = %s
                        """), (concept_id, expression))
                items = cur.fetchall()
                if not items:
                    return {}
            else:
                cur.execute(
                        (SQL_GET_TERMS + "and Gloss_Terms.id = %s"), (item_id,))
                item = cur.fetchone()
                if item is None:
                    return {}
                concept_id = item['concept_id']
                expression = item['expression']
                items = [item]
            lemma['expression'] = expression
            lemma['concept_id'] = concept_id
            lemma['concept'] = items[0]['concept']
            lemma['lang'] = items[0]['lang']
            lemma['lang_id'] = items[0]['lang_id']
            lemma['lang_abbr'] = items[0]['lang_abbr']
            lemma['lang_sorter'] = items[0]['lang_sorter']
            lemma['rtl'] = items[0]['rtl']
            lemma['grammar'] = '. – '.join(sorted(set(filter(None,
                    ( (item['grammar'] or '').strip() for item in items )))))
            lemma['refs'] = self.get_refs(cur, items)
            if with_transl_froms:
                idents = set(filter(None,
                        ( item['from_id'] for item in items )))
                lemma['transl_froms'] = sorted(filter(None,
                        ( self.get_lemma(None, None, ident)
                          for ident in idents )),
                        key = lambda x: x['lang_sorter'])
            if with_transl_intos:
                idents = set()
                idents_modern = set()
                for item in items:
                    cur.execute("""
                            select id, is_modern from Gloss_Terms
                            where translated_from = %s
                            """, (item['id'],))
                    for row in cur.fetchall():
                        ident = row['id']
                        if ident is not None:
                            if row['is_modern'] == 0:
                                idents.add(ident)
                            else:
                                idents_modern.add(ident)
                lemma['transl_intos'] = sorted(filter(None,
                        ( self.get_lemma(None, None, ident)
                          for ident in idents )),
                        key = lambda x: x.get('lang_sorter', 0))
                lemma['transl_intos_modern'] = sorted(filter(None,
                        ( self.get_lemma(None, None, ident)
                          for ident in idents_modern )),
                        key = lambda x: x.get('lang_sorter', 0))
            if with_synonyms:
                cur.execute("""
                        select id from Gloss_Terms
                        where Gloss_Terms.concept = %s
                        and Gloss_Terms.term != %s
                        and Gloss_Terms.lang = %s
                        order by term
                        """, (concept_id, expression, lemma['lang_id']))
                idents = { row['id'] for row in cur.fetchall() }
                lemma['synonyms'] = sorted(filter(None,
                        ( self.get_lemma(None, None, ident)
                          for ident in idents )),
                        key = lambda x: x.get('expression', ''))
            return lemma

    def get_location(self, doc_type: str, doc_id: str) -> str:
        '''
        Get the location of :param:`doc_id` according to :param:`doc_type`:

        - for MSS: place of the library, abbreviation of the library, shelfmark
        - for prints: place of the publisher, publisher, year
        '''
        with DBCon(self.connect()) as (connection, cur):
            if doc_type == 'ms':
                cur.execute(SQL_GET_MS, (doc_id,))
                doc = cur.fetchone()
                return '{}, {}, {}'.format(
                        doc['place_name'],
                        doc['lib_abbrev'],
                        doc['ms_shelfmark'])\
                        if doc else ''
            else:
                cur.execute(SQL_GET_PRINT, (doc_id,))
                doc = cur.fetchone()
                return '{}, {}, {}'.format(
                        doc['place_name'],
                        doc['print_publisher'],
                        doc['print_date'])\
                        if doc else ''

    def get_ms_jordanus(self, ms_id) -> 'OrderedDict':
        '''
        Find all parts and pieces of information belonging to :param`ms_id`
        and return them ordered in a main part and ms parts.
        '''
        with DBCon(self.connect_jordanus()) as (connection, cur):
            cur.execute(r'''
                    select
                        subsiglum,
                        field_rank,
                        field_name,
                        field_hypernum,
                        term,
                        sentences.sentence_id as sentence_id,
                        sentences.field_id    as field_id
                    from sentences
                    join terms on
                        terms.term_id = sentences.term_id
                    join fields on
                        fields.field_id = sentences.field_id
                    where
                        sentences.ms_id = %s
                        and
                        fields.field_public > 0
                    order by
                        subsiglum,
                        field_rank,
                        field_name,
                        term
                    ''', ms_id)
            entry = odict()
            for subsiglum, fields in itertools.groupby(
                    cur.fetchall(),
                    itemgetter('subsiglum'),
                    ):
                entry[subsiglum] = odict()
                for field in fields:
                    key = field['field_id']
                    # There may be multiple values for one field and one mspart.
                    # The sentence_id, by the way, is unique.
                    if key not in entry[subsiglum]:
                        entry[subsiglum][key] = [
                                field['field_name'],
                                field['field_hypernum'],
                                odict()]
                    entry[subsiglum][key][2][field['sentence_id']] =\
                            field['term']
            return entry

    def get_news(
            self,
            fragment_tag_re: 'Pattern[str]' = re.compile(r'<[^>]*$'),
            ) -> 'List[Dict[str, str]]':
        '''Get a list of all news items.'''
        with DBCon(self.connect()) as (connection, cur):
            cur.execute(SQL_GET_NEWS)
            news = []
            for item in cur.fetchall():
                headline = item['news_headline'].lstrip()
                if headline.startswith('*'):
                    item['news_headline'] = headline[1:]
                teaser = item['news_content']
                if len(teaser) > 500:
                    teaser = teaser[:500] + '…'
                    teaser = fragment_tag_re.sub('', teaser)
                    teaser = xmlhtml.normalize(teaser)
                item['news_teaser'] = teaser
                news.append(item)
        return news

    def get_news_item(self, news_id: str) -> 'Dict[str, str]':
        '''Get the news item :param:`news_id`.'''
        with DBCon(self.connect()) as (connection, cur):
            cur.execute(SQL_GET_NEWS_ITEM, news_id)
            item = cur.fetchone()
            headline = item['news_headline'].lstrip()
            if headline.startswith('*'):
                item['news_headline'] = headline[1:]
            return self.modify_cols(item)

    def get_num_of_mss(self) -> int:
        '''
        Get the number of manuscripts.
        (This meets a specific requirement for the data table of MSS.)
        '''
        with DBCon(self.connect()) as (connection, cur):
            cur.execute("""
                    select count(1) as num from Manuscripts where f_hidden = 'F'
                    """)
            return cur.fetchone().get('num', 0)

    def get_project(self) -> 'Dict[str, str]':
        '''Get the outline and description of the project.'''
        with DBCon(self.connect()) as (connection, cur):
            cur.execute(SQL_GET_PROJECT)
            return self.modify_cols(cur.fetchone() or {})

    def get_refs(self, cur, items) -> str:
        '''
        With :param:`cur` in the pertinent database, get all references
        assigned to :param:`items`. The items are term-concept-translation
        entries. Combine the references to a string representation.
        '''
        cur.execute("""
                select
                    Gloss_Termrefs.location as token_location,
                    Gloss_Refs.type         as ref_type,
                    Gloss_Refs.author       as ref_author,
                    Gloss_Refs.title        as ref_title,
                    Gloss_Refs.year         as ref_year,
                    Gloss_Refs.generic_work as ref_generic_work,
                    Gloss_Refs.short_title  as ref_short_title
                from Gloss_Termrefs
                join Gloss_Refs
                    on Gloss_Refs.id = Gloss_Termrefs.refid
                where
                    Gloss_Termrefs.termid in ({})
                order by Gloss_Refs.short_title, Gloss_Termrefs.location
                """.format(', '.join( str(item['id']) for item in items )))
        refs = defaultdict(list)
        for ref in cur.fetchall():
            refs[ref['ref_short_title']].append(ref['token_location'])
        return '. – '.join(sorted(
                title + ': ' + '; '.join(sorted(location))
                for title, location in refs.items() ))

    def get_repro_entry(
            self,
            doc_type: str,
            doc_id: str,
            part_id: str,
            pagina: str,
            ) -> 'Dict[str, Union[str, bytes, int]]':
        '''
        For the given type, IDs and pagina, get the information needed for
        the page which shows the corresponding facsimile or transcription
        (or both). Get this information from the temporary database.
        '''
        entry = {}
        with DBCon(self.connect()) as (connection, cur):
            if doc_type == 'ms':
                cur.execute(SQL_GET_MS, (doc_id,))
            else:
                cur.execute(SQL_GET_PRINT, (doc_id,))
            catalog_doc = cur.fetchone()
            if doc_type == 'ms':
                cur.execute(SQL_GET_WORK_OF_MSPART, (part_id,))
            else:
                cur.execute(SQL_GET_WORK_OF_PRINTPART, (part_id,))
            work = cur.fetchone()
        if not (catalog_doc and work):
            return {}
        with DBCon(sqlite3.connect(self.meta_path)) as (connection, cur):
            cur.execute("""
                    select * from repros
                    where doc_type = ? and doc_id = ? and part_id = ?
                    order by pagina_sorted
                    """, (doc_type, doc_id, part_id))
            repros = cur.fetchall()
        if not repros:
            return {}
        for rank, repro in enumerate(repros):
            if pagina == repro[3]:
                break
        else:
            return {}
        repro = repros[rank]
        tran = b''
        if repro[8]:
            tran_path = '/'.join((self.repros_path, doc_type, repro[8]))
            try:
                with open(tran_path, 'rb') as file:
                    tran = file.read()
            except OSError:
                pass # Sic.
        entry['cat_lang'] = catalog_doc.get('cat_lang', '')
        entry['tran'] = tran
        entry['fulltran_part'] = repro[6]
        entry['scan_path'] = quote('/'.join((doc_type, repro[7])))
        if doc_type == 'ms':
            entry['place_name'] = catalog_doc['place_name']
            entry['lib_name'] = (
                    catalog_doc['lib_abbrev'] or
                    catalog_doc['lib_name'])
            entry['ms_shelfmark'] = catalog_doc['ms_shelfmark']
        else:
            entry['place_name'] = catalog_doc['place_name']
            entry['print_publisher'] = catalog_doc['print_publisher']
            entry['print_date'] = str(catalog_doc['print_date'])
        entry['work'] = work
        entry['doc_type'] = doc_type
        entry['doc_id'] = doc_id
        entry['part_id'] = part_id
        entry['pagina_styled'] = repro[5]
        entry['paginas'] = [ (pagina, pagina_styled)
                for (_, _, _, pagina, _, pagina_styled, *_) in repros ]
        entry['prev'] = repros[rank - 1][3] if rank > 0 else ''
        entry['next'] = repros[rank + 1][3] if rank + 1 < len(repros) else ''
        return entry

    def get_repro_tran_path(
            self,
            doc_type: str,
            doc_id: str,
            part_id: str,
            tailpath: str = '',
            ) -> str:
        '''
        Get the relpath within :attr:`.repros_path` that corresponds to
        :param:`doc_type`, :param:`doc_id` and :param:`part_id`.
        '''
        with DBCon(sqlite3.connect(self.meta_path)) as (connection, cur):
            cur.execute("""
                    select tran_path from repros
                    where doc_type = ? and doc_id = ? and part_id = ?
                    limit 1
                    """, (doc_type, doc_id, part_id))
            tran = cur.fetchone()
        if tran:
            return '/'.join((doc_type, tran[0], '../..', tailpath))
        else:
            return ''

    def get_searchfields_jordanus(self):
        '''
        Get the searchfields (hyperfields) for the search form of Jordanus.
        '''
        with DBCon(self.connect_jordanus()) as (connection, cur):
            cur.execute(r'''
                    select distinct
                        field_public,
                        field_hypernum,
                        field_hypername,
                        field_hypername_de
                    from fields
                    where field_public > 1
                    order by field_public DESC, field_rank ASC
                    ''')
            fieldgroups = [ list(group) for key, group in itertools.groupby(
                    cur.fetchall(),
                    lambda x: x['field_public'],
                    ) ]
            fieldgroups.append(None)
            return fieldgroups[:2]

    def get_starttext(self) -> 'Tuple[str, List[Dict[str, str]]]':
        '''
        Get the text of the start page as stored in the database
        and the most recent news or announcements.
        '''
        with DBCon(self.connect()) as (connection, cur):
            cur.execute(SQL_GET_STARTTEXT)
            entry = cur.fetchone()
        text = self.modify_cols(entry)['home_content'] if entry else ''
        return text

    def get_team(self) -> 'List[Dict[str, Union[str, List[Dict[str, str]]]]]':
        '''
        Get the description of the team as a list of groups,
        which are role categories, and sublists of group members.
        '''
        with DBCon(self.connect()) as (connection, cur):
            cur.execute(SQL_GET_GROUPS)
            groups = cur.fetchall() or []
            for i, group in enumerate(groups):
                cur.execute(SQL_GET_USERS, group['group_id'])
                groups[i]['users'] = cur.fetchall() or []
        return groups

    def get_template_id_and_entry(
            self,
            doc_type: str,
            doc_id: str,
            ) -> 'Tuple[str, Dict[str, Union[str, int]]]':
        '''
        For :param:`doc_type` (e.g. ``'ms'``) and :param:`doc_id`
        (e.g. ``'12'``), get the corresponding template id (e.g. ``'ms.tpl'``)
        and the whole entry (e.g. the dictionary describing the manuscript
        with the id 12).
        '''
        template_id = ''
        entry = {}
        with DBCon(self.connect()) as (connection, cur):
            if doc_type == 'print':
                template_id = 'print.tpl'
                cur.execute(SQL_GET_PRINT, doc_id)
                entry = cur.fetchone() or {}
                if entry:
                    self.modify_cols(entry)
                    cur.execute(SQL_GET_PRINTPARTS_OF_PRINT, doc_id)
                    entry['printparts'] = tuple(
                            self.modify_cols(row, doc_type, doc_id)
                            for row in cur.fetchall() )
            elif doc_type == 'ms':
                template_id = 'ms.tpl'
                cur.execute(SQL_GET_MS, doc_id)
                entry = cur.fetchone() or {}
                if entry:
                    self.modify_cols(entry)
                    cur.execute(SQL_GET_MSPARTS_OF_MS, doc_id)
                    entry['msparts'] = tuple(
                            self.modify_cols(row, doc_type, doc_id)
                            for row in cur.fetchall() )
            elif doc_type == 'work':
                template_id = 'work.tpl'
                cur.execute(SQL_GET_WORK, doc_id)
                entry = cur.fetchone() or {}
                if entry:
                    self.modify_cols(entry)
                    cur.execute(SQL_GET_PRINTPARTS_OF_WORK, doc_id)
                    printparts = [
                            self.modify_cols(row, 'print', row['print_id'])
                            for row in cur.fetchall() ]
                    printparts.sort(key = lambda e: (
                            get_sortstring(e['place_name']),
                            get_sortstring(e['print_publisher']),
                            get_sortstring(e['print_date']),
                            get_pagina_sorted(e['pagina_range']),
                            ))
                    entry['printparts'] = printparts
                    cur.execute(SQL_GET_MSPARTS_OF_WORK, doc_id)
                    msparts = [
                            self.modify_cols(row, 'ms', row['ms_id'])
                            for row in cur.fetchall() ]
                    msparts.sort(key = lambda e: (
                            get_sortstring(e['place_name']),
                            get_sortstring(e['lib_abbrev']),
                            get_sortstring(e['ms_shelfmark']),
                            get_pagina_sorted(e['pagina_range']),
                            ))
                    entry['msparts'] = msparts
                    for key, sql in (
                            ('translations', SQL_GET_TRANSLATIONS_OF_WORK),
                            ('commentaries', SQL_GET_COMMENTARIES_OF_WORK),
                            ):
                        cur.execute(sql, doc_id)
                        rows_per_lang = defaultdict(list)
                        for row in cur.fetchall():
                            rows_per_lang[row['lang_id']].append(
                                    self.modify_cols(row))
                        entry[key] = rows_per_lang
        return template_id, entry

    def get_tran(
            self,
            doc_type: str,
            doc_id: str,
            part_id: str,
            page_id: str,
            ) -> 'Tuple[bytes, str]':
        '''
        Get the page :param:`page_id` of the full transcription of
        the document part :param:`part_id` of the print or manuscript
        (decided by :param:`doc_type`) with :param:`doc_id`.
        '''
        with DBCon(sqlite3.connect(self.meta_path)) as (connection, cur):
            cur.execute(SQLITE_GET_TRAN_PATH, (doc_type, doc_id, part_id))
            tran = cur.fetchone()
        if not tran:
            return b''
        path = os.path.abspath('/'.join(
                (self.repros_path, doc_type, tran[0], '../..', page_id + '.xml')
                ))
        try:
            with open(path, 'rb') as file:
                return file.read()
        except OSError:
            return b''

    def get_user(self, user_id: str) -> 'Dict[str, str]':
        '''Get the team member :param:`user_id`.'''
        with DBCon(self.connect()) as (connection, cur):
            cur.execute(SQL_GET_USER, user_id)
            return self.modify_cols(cur.fetchone() or {})

    def make_temptable_jordanus(self):
        '''
        Make a temporary table for ms metadata in jordanus in order to
        hold these data available precomputed. They are to be shown in
        results list, which are also sorted by them.

        Do nothing, if there already is a table named `temp`.
        '''
        with DBCon(self.connect_jordanus()) as (connection, cur):
            cur.execute(r'show tables')
            if any( row.popitem()[1] == 'temp' for row in cur.fetchall() ):
                return
            print('I create a temporary table in the Jordanus database and '\
                  'insert many precomputed values (for faster queries) ...')
            cur.execute(r'''
                    drop table if exists temp;
                    create table temp
                    (
                    ms_id INT,
                    location TEXT,
                    persons TEXT,
                    titles TEXT,
                    incipits TEXT,
                    PRIMARY KEY (ms_id)
                    );
                    create index i_mss_temp on temp (ms_id);
                    ''')
            connection.commit()
            cur.execute(r'select distinct ms_id from sentences')
            rows = cur.fetchall()
            for row in rows:
                ms_id = row['ms_id']
                temps = [ms_id]
                for delimiter, field_ids in (
                        (', ', '(4, 5, 6)'),        # City, Library, Shelfmark
                        ('; ', '(32, 57, 58, 59)'), # Persons
                        ('; ', '(9, 10, 11)'),      # Titles
                        ('; ', '(65, 66)'),         # Incipits
                        ):
                    cur.execute(r'''
                            select distinct term
                            from terms
                            join sentences on sentences.term_id = terms.term_id
                            join fields on fields.field_id = sentences.field_id
                            where sentences.ms_id = %s
                            and sentences.field_id in {}
                            order by field_rank
                            '''.format(field_ids), ms_id)
                    temps.append(
                            delimiter.join( r['term'] for r in cur.fetchall() ))
                cur.execute('''
                        insert into temp values (%s, %s, %s, %s, %s)
                        ''', temps)
            connection.commit()

    def modify_cols(
            self,
            entry: 'Dict[str, Union[str, int, None]]]',
            doc_type: str = '',
            doc_id: str = '',
            p_tags_re: 'Pattern[str]' =
                re.compile(r'''(?s)</?p\b(?:[^"'>]*?|".*?"|'.*?')*?>'''),
            ) -> 'Dict[str, Union[str, int]]]':
        '''
        In :param:`entry`, ensure that every value is a string.

        If the key is listed in ``RICH_TEXT_COLS``, carry out modifications,
        e.g. remove unwanted tags.

        If a key `pagina_range` is found in the entry, check in the temporary
        database whether there is a belonging facsimile or transcription or both
        and accordingly add zero, one or two links to the entry. Such a link
        leads to a page presenting the facsimile or transcription or both.
        '''
        replacer = xmlhtml.Replacer(
                replace_element = replace_element,
                replace_tag = replace_tag,
                )
        for key in entry:
            text = str(entry[key] or '')
            ##################### TEMP:
            text = text.replace('⸻', '—')
            ##################### :TEMP
            if key in RICH_TEXT_COLS:
                replacer.parse(text)
                text = ''.join(replacer.results).strip()
                replacer.results = deque()
                if key in KEEPING_FIRST_P_COLS:
                    text = p_tags_re.sub('', text, count = 2)
                elif key not in KEEPING_P_COLS:
                    text = p_tags_re.sub('', text)
                text = text.replace('<ul>', '<ul class="list">')
                text = text.replace('<ol>', '<ol class="list">')
                text = text.replace('\u2005', ' ')
            if key in NON_BREAK_SPACE_HYPHEN_COLS:
                text = text.replace(' ', '\u202f').replace('-', '\u2011')
            entry[key] = text
        if entry.get('pagina_range', '').strip():
            part_id = entry.get('part_id', '')
            with DBCon(sqlite3.connect(self.meta_path)) as (connection, cur):
                cur.execute("""
                        select pagina, scan_path, tran_path from repros
                        where doc_type = ? and doc_id = ? and part_id = ?
                        order by pagina_sorted
                        limit 1
                        """,
                        (doc_type, doc_id, part_id))
                result = cur.fetchone()
            pagina, scan_path, tran_path = result if result else ('', '', '')
            link = '/{}/{}/{}/{}'.format(doc_type, doc_id, part_id, pagina)
            entry['scan_link'] = link if scan_path else ''
            entry['tran_link'] = link if tran_path else ''
        return entry

    def search_fulltext(
            self,
            term: str,
            no_case: bool = True,
            regex: bool = False,
            single_words: bool = False,
            status_ids: 'Sequence[str]' = [],
            site_ids: 'Sequence[str]' = [],
            at_least_one_letter_re: 'Pattern[str]' = re.compile(r'(?<!\\)\w'),
            flags_re: 'Pattern[str]' = re.compile(r'\(\?[aiLmsux]+?\)'),
            ) -> 'Tuple[str, str, str, AbstractSet[str]]':
        '''
        Search :param:`term` in the fulltext index.
        Yield (as a generator) tuples containing:

        - a status flag, either `source` or `non-source`.
        - the URL to the text passage containing the match.
        - the text passage containing the match.
        - a set of the matching substrings within the passage.
          This may be useful, when a search is done with a regular
          expression and the matching substrings shall be marked
          e.g. in an HTML output.

        .. important::
            At the moment, only the first 500 results are returned.
            This limitation is set to avoid performance issues both
            on the host computer and even more in e.g. the browser
            processing the results.

        .. todo::
            Remove the above mentioned limitation with the help of a
            solution via ajax and paging (or infinite scroll?).

        :param no_case: If true, upper and lower case are not distinguished.
        :param regex: If true, the :param:`term` is used as a regular
            expression.
        :param status_ids: a possibly empty list of status flags to restrict
            the search to records starting with one of these flags. If empty,
            no restriction takes place.
        :param site_ids: a possibly empty list of site ids to restrict
            the search to records whose URL points to any site having one
            of these site ids. If empty, no restriction takes place. The
            site_id is the name of the folder containing the site.
            To be left at the default value if :attr:`.multisite` is ``False``.
        '''
        term = unicodedata.normalize('NFKD', term)
        if not at_least_one_letter_re.search(term):
            term = r'\Z.'
            regex = True
        if single_words:
            term = r'(\b{}\b)'.format(
                   r'\b)|(\b'.join(map(
                   r'\b.*?\b'.join, permutations(
                       term.split() if regex else
                       map(re.escape, term.split())))))
        elif regex:
            term = flags_re.sub('', ' '.join(term.split()))
            term = ''.join( '({})'.format(c)
                    if ord(c) > 127 else c for c in term )
        else:
            term = re.escape(' '.join(term.split()))
        if no_case:
            term += '(?i)'
        try:
            term_re = re.compile(term, re.ASCII)
        except:
            term = r'\Z.'
            term_re = re.compile(term, re.ASCII)
        fullterm = r'\n{}//{}[^/\n]*//.*?({}).*'.format(
                r'(?:{})'.format('|'.join( s for s in status_ids ))
                        if len(status_ids) > 1 else
                    status_ids[0] if status_ids else
                    r'[^/\n]*',
                '' if not self.multisite else
                    r'(?:{})/'.format('|'.join( s for s in site_ids ))
                        if len(site_ids) > 1 else
                    site_ids[0] + '/' if site_ids else
                    r'[^/\n]*/',
                term
                )
        fullterm_b = fullterm.encode(encoding = 'utf-8', errors = 'ignore')
        for counter, match in enumerate(re.finditer(fullterm_b, self.fulltext)):
            status, url, text = match.group().decode().split('//', 2)
            matches = set( m.group() for m in term_re.finditer(text) )
            yield status, url, text, matches
            if counter > 500:
                yield '', '', 'Break: too many results.', {''}
                break

    def search_jordanus(self, query):
        '''
        Search the data of Jordanus applying :param:`query`, which are
        query parameters sent from the website.
        '''
        searches = []
        for hypernum in query:
            term = unquote(getattr(query, hypernum))
            try:
                term = term.strip(' %_')
                if term:
                    term = '%' + term + '%'
                    hypernum = 0 if hypernum == 'any_field' else Dec(hypernum)
                    searches.append((hypernum, term))
            except:
                pass
        if searches:
            with DBCon(self.connect_jordanus()) as (connection, cur):
                rows = deque()
                ms_ids = deque()
                for hypernum, term in searches:
                    if hypernum:
                        cur.execute(r'''
                                select field_id from fields
                                where field_public > 1 and field_hypernum = %s
                                ''', hypernum)
                    else: # The term might be in the search field for any field.
                        cur.execute(r'''
                                select field_id from fields
                                where field_public > 1
                                ''')
                    field_ids = ', '.join(
                            str(row['field_id']) for row in cur.fetchall() )
                    if field_ids:
                        comparator = 'like' if ('_' in term or '%' in term)\
                                else '='
                        cur.execute(r'''
                                select
                                    sentences.ms_id as ms_id,
                                    subsiglum,
                                    sentences.field_id as field_id,
                                    field_name,
                                    term,
                                    location,
                                    persons,
                                    titles,
                                    incipits
                                from sentences
                                join temp on
                                    temp.ms_id = sentences.ms_id
                                join fields on
                                    fields.field_id = sentences.field_id
                                join terms on
                                    terms.term_id = sentences.term_id
                                where
                                    sentences.field_id in ({})
                                    and
                                    terms.term {} %s
                                order by location, fields.field_rank
                                '''.format(field_ids, comparator), term)
                        ms_ids.append(set())
                        for row in cur.fetchall():
                            rows.append(row)
                            ms_ids[-1].add(row['ms_id'])
                if rows:
                    ms_ids_intersection = set.intersection(*ms_ids)
                    for row in rows:
                        ms_id = row['ms_id']
                        if ms_id in ms_ids_intersection:
                            yield (
                                    ms_id,
                                    row['subsiglum'],
                                    row['field_id'],
                                    row['field_name'],
                                    row['term'],
                                    row['location'],
                                    row['persons'],
                                    row['titles'],
                                    row['incipits'],
                                    )

def delete_entries_without_files(
        repros_path: str,
        meta_path: str,
        ) -> None:
    '''
    Called by :func:`refresh_forever`; see the docstring of this function.
    '''
    with DBCon(sqlite3.connect(meta_path)) as (connection, cur):
        cur.execute(
                "select rowid, doc_type, scan_path, tran_path from repros")
        for (row_id, doc_type, scan_path, tran_path) in cur.fetchall():
            # Prevent sqlite error `parameters are of unsupported type`:
            row_id = str(row_id)
            folder = '/'.join((repros_path, doc_type))
            scan_path_exists = os.path.isfile('/'.join((folder, scan_path)))\
                    if scan_path else False
            tran_path_exists = os.path.isfile('/'.join((folder, tran_path)))\
                    if tran_path else False
            if not scan_path_exists and not tran_path_exists:
                cur.execute("delete from repros where rowid = ?", (row_id,))
            elif scan_path and not scan_path_exists:
                cur.execute("""
                        update repros
                        set scan_path = ''
                        where rowid = ?""", (row_id,))
            elif tran_path and not tran_path_exists:
                cur.execute("""
                        update repros
                        set tran_path = '', fulltran_part = ''
                        where rowid = ?""", (row_id,))
            connection.commit()
        try:
            cur.execute("vacuum repros")
            connection.commit()
        except:
            pass # Sic.

def even_out_for_search(
        term: str,
        unify_re: 'Pattern[str]' = re.compile(r'[\s\W]+'),
        erase_re: 'Pattern[str]' = re.compile(r'[^\w ]'),
        normalize: 'Callable' = unicodedata.normalize,
        get_name: 'Callable' = unicodedata.name,
        ) -> str:
    '''
    Get an evened out form of :param:`term`. This form may be used as
    a hidden search string. It is prefixed with a special character (█)
    to separate it from the visible string so that a search does not
    find a string running from the visible into the hidden string.
    '''
    term = erase_re.sub('', unify_re.sub(' ', term))
    term = normalize('NFKD', term.strip())
    chars = '█'
    for char in term:
        if not get_name(char).startswith('COMBINING'):
            chars += char
    return chars

def get_person_name(
        entry: 'Dict[str, Optional[str]]',
        field: str = 'person_name',
        ) -> 'Dict[str, str]':
    '''
    Get the :param:`field` of the entry :param:`entry` and convert it to
    the term as it is used in the JSON for the datatable representation.
    '''
    name = entry[field]
    name_evened = even_out_for_search(name)
    return {
            '_': name + '<div hidden="">' + name_evened + '</div>',
            'sort': name_evened,
            }

def get_connect(
        config: 'configparser.ConfigParser',
        db_access_path: str,
        db_name_key: str = 'name',
        ) -> partial:
    '''
    Get an object that, when called, establishes a connection to a database
    with the specifications given in :param:`config`.

    :param db_access_path: file storing a hash of user name and password
        that will be used to access the database.
    :param db_name_key: key term on which the db section of the configuration
        returns the database name. This parameter can be used to dynamically
        select one certain database out of several.
    '''
    dbuser, dbpassword = file_io.get_keys(db_access_path)
    return partial(
            mysql.connect,
            host        = config['db']['host'],
            port        = int(config['db']['port']),
            user        = dbuser,
            passwd      = dbpassword,
            db          = config['db'][db_name_key],
            charset     = config['db']['charset'],
            unix_socket = config['db']['unix_socket'] if sys.platform == 'linux'
                          else None,
            cursorclass = mysql.cursors.DictCursor,
            )

def get_pagina(filename: str) -> str:
    return filename.rsplit('.', 1)[0].strip().lstrip('0')

def get_pagina_sorted(
        pagina: str,
        parts_pattern: 'Pattern[str]' = re.compile(r'([-\d()IVXLC]+)(.*)'),
        ) -> str:
    match = parts_pattern.search(pagina)
    if match:
        pagina, tail = match.group(1, 2)
    else:
        tail = ''
    return pagina.zfill(9) + tail

def get_pagina_styled(
        pagina: str,
        sup_pattern: 'Pattern[str]' = re.compile(r'({})'.format(
            '|'.join(
                sorted(MULTIPLES_MAP.values(), key = len, reverse = True)
                ))),
        ) -> str:
    for old, new in sorted(MULTIPLES_MAP.items(), key = len, reverse = True):
        if old in pagina:
            pagina = pagina.replace(old, new)
            break
    return sup_pattern.sub('<sup>\g<1></sup>', pagina)

def get_siglum(entry: 'Dict[str, Optional[str]]') -> 'Dict[str, str]':
    '''
    Get the siglum field of the entry :param:`entry` and convert it to the
    field as it is used in the JSON for the datatable representation.
    '''
    siglum = str(entry['work_siglum'])
    siglum_char, \
    siglum_num = siglum.split('.', 1) if '.' in siglum else ('', siglum)
    return {
            '_': siglum,
            'sort': siglum_char + get_sortnumber(siglum_num),
            }

def get_sortstring(
        term: str,
        normalize: 'Callable[[str], str]' = unicodedata.normalize,
        delete_re: 'Pattern[str]'         =
            re.compile(r'([^\w\s]|['+DIACRITICS+'])'),
        whitespace_re: 'Pattern[str]'     = re.compile(r'\s+'),
        number_re: 'Pattern[str]'         = re.compile(r'\d+'),
        ) -> str:
    '''
    Get a comparison-friendly form of :param:`term`, i.e.:

    - Normalize unicode variants towards the analytical form.
    - Make majuscules to minuscules.
    - Delete matches of :param:`delete_re`.
    - Turn matches of :param:`whitespace_re` into one blank each.
    - Replace matches of :param:`number_re` with a zero-filled
      string of length 9.
    - Strip leading and trailing whitespace.
    '''
    return number_re.sub(
            lambda m: m.group().zfill(9),
            whitespace_re.sub(
                ' ',
                delete_re.sub(
                    '',
                    normalize('NFKD', term).casefold()))).strip()

def get_title(entry: 'Dict[str, Optional[str]]') -> 'Dict[str, str]':
    '''
    Get the title field of the entry :param:`entry` and convert it to the
    field as it is used in the JSON for the datatable representation.
    '''
    title = str(entry['work_title']).split(' (by')[0]
    title_evened = even_out_for_search(title)
    return {
            '_': '<a href="work/{}">{}</a>'.format(entry['work_id'], title)
                 + '<div hidden="">' + title_evened + '</div>',
            'sort': title_evened,
            }

def make_new_empty_db_if_absent(
        meta_path: str,
        wal_mode: bool,
        ) -> None:
    '''
    Make a new emtpy database at :param:`meta_path`, if none is
    present there or if the one present there is not readable.

    .. important::
        In the latter case, :param:`meta_path` is **replaced**
        by the new empty database.

    :param wal_mode: tells whether the WAL mode (i.e. write ahead logging)
        is used while operating on the SQLite database.

    .. important::
        WAL mode may be impossible e.g. on network drives.
        But non-WAL mode is not usable for production,
        because any write process blocks all read processes
        long enough to make the response fail.
    '''
    def build_db(wal_mode):
        with DBCon(sqlite3.connect(meta_path)) as (connection, cur):
            if wal_mode:
                cur.execute('PRAGMA journal_mode=WAL')
                print('The SQLite db is built in WAL mode.')
            cur.execute('PRAGMA encoding = "UTF-8"')
            cur.execute(SQLITE_CREATE_TABLE_REPROS)
            cur.execute(SQLITE_CREATE_INDEX_DOC)
            cur.execute(SQLITE_CREATE_INDEX_WORK)
            connection.commit()
    try:
        build_db(wal_mode)
    except sqlite3.DatabaseError:
        os.remove(meta_path)
        build_db(wal_mode)
        print(
                'A DatabaseError occurred while building the SQLite db.'\
                ' Maybe the file at {} was not well-formed or the WAL mode'\
                ' ({}) did not match. The file was removed and built anew.'
                .format(meta_path, wal_mode))

def refresh_forever(
        pid: int,
        config_path: str,
        repros_path: str,
        ) -> None:
    '''
    Refresh the database storing the metadata for the table of publications and
    for the pages dedicated to one publication each in a loop as follows:

    - Exit, when no process with :param`pid` is running anymore.
      This should be the process ID of the process having started this function.
    - Delete any entry for which neither a facsimile nor a transcription exists.
    - Insert or replace entries in the database based on the files and folders
      found in :param:`repros_path`. For the structure of this folder, see the
      docstring of :meth:`DB.__init__`.
    - Start anew.

    Any occurring errors or issues are written into a :class:`file_io.Log`
    instance.

    :param config_path: absolute path to a file containing the configuration,
        e.g. the path to the very database being refreshed in this function.
    '''
    log = file_io.Log(__file__)
    sys.stderr = log
    config = file_io.get_config(config_path)
    repros_path = os.path.abspath(repros_path)
    paths = file_io.get_abspaths(config, config_path)
    meta_path = paths['meta_path']
    while True:
        for doc_type in ('ms', 'print'):
            if not sys_io.pid_exists(pid):
                sys.exit(0)
            doc_type_path = '/'.join((repros_path, doc_type))
            for doc_name in os.listdir(doc_type_path):
                delete_entries_without_files(repros_path, meta_path)
                doc_id = doc_name.split('#')[-1].strip()
                if not doc_id:
                    continue
                doc_path = '/'.join((doc_type_path, doc_name))
                repros = defaultdict(dict)
                for path in file_io.get_paths(doc_path):
                    if os.sep != '/':
                        path = path.replace(os.sep, '/')
                    tail = path.rsplit('/' + doc_type + '/', 1)[-1].strip('/')
                    pieces = tail.split('/')
                    part_id = pieces[1].split('#')[-1].strip() \
                            if len(pieces) > 2 and '#' in pieces[1] else ''
                    pagina = get_pagina(pieces[-1])
                    if re.search(r'(?i)\.(jpg|png|tiff?)$', pieces[-1]) and (
                            (len(pieces) == 2 and not part_id) or
                            (len(pieces) == 3 and part_id)
                            ):
                        repros[(doc_type, doc_id, part_id, pagina)].update({
                                'scan_path': tail})
                    elif (len(pieces) == 5 and
                            pieces[2] == 'transcription' and
                            pieces[3] == 'pages'):
                        fulltran_part, pagina = pagina.split('#')
                        repros[(doc_type, doc_id, part_id, pagina)].update({
                                'fulltran_part': fulltran_part,
                                'tran_path': tail})
                repros = [ (
                        doc_type,
                        doc_id,
                        part_id,
                        pagina,
                        get_pagina_sorted(pagina),
                        get_pagina_styled(pagina),
                        values.get('fulltran_part', ''),
                        values.get('scan_path', ''),
                        values.get('tran_path', ''),
                        ) for ((
                            doc_type,
                            doc_id,
                            part_id,
                            pagina,
                            ),
                            values)
                        in repros.items() ]
                with DBCon(sqlite3.connect(meta_path)) as (connection, cur):
                    cur.executemany(SQLITE_INSERT_OR_REPLACE_REPRO, repros)
                    connection.commit()
                time.sleep(0.1)

def replace_element(
        tag: str,
        attrs: 'List[Tuple[str, str]]',
        startend: bool,
        info: dict,
        ) -> 'Optional[str]':
    '''
    Function for a :class:`xmlhtml.Replacer` instance, used to clean up
    the HTML of rich text field entries.
    '''
    for key, value in attrs:
        if key == 'style':
            value = value.replace(' ', '')
            if 'color:red' in value or 'color:#ff0000' in value:
                return ''
    return None

def replace_tag(
        tag: str,
        attrs: 'List[Tuple[str, str]]',
        startend: bool,
        info: dict,
        ) -> 'Tuple[str, List[Tuple[str, str]], bool]':
    '''
    Function for a :class:`xmlhtml.Replacer` instance, used to clean up
    the HTML of rich text field entries.
    '''
    attrs_new = []
    for key, value in attrs:
        value_sans = value.replace(' ', '')
        if key not in (
                'style',
                'align',
                'lang',
                'class',
                'cellpadding',
                'cellspacing',
                ):
            attrs_new.append((key, value))
        elif key == 'style':
            if 'small-caps' in value_sans:
                attrs_new.append((key, value))
            elif 'list-style-type:upper-alpha' in value_sans:
                attrs_new.append((key, 'list-style-type:upper-alpha'))
            elif 'underline' in value_sans:
                tag = 'u'
            elif 'line-through' in value_sans:
                tag = 's'
    attrs = attrs_new
    if tag == 'strong':
        tag = 'b'
    if tag in ('ul', 'ol'):
        attrs.append(('class', 'list'))
    tag = tag if (attrs or tag not in ('div', 'span')) else ''
    startend = startend or tag in xmlhtml.HTML_VOIDS
    return tag, attrs, startend

if __name__ == '__main__':
    if len(sys.argv) > 3:
        pid = int(sys.argv[1])
        config_path = sys.argv[2]
        repros_path = sys.argv[3]
    else:
        # For testing.
        pid = os.getpid()
        config_path = os.path.abspath(__file__ + '/../../../../../../6.1.7 Projekte und Arbeitsplanung/Ptolemaeus/seite_ptolemaeus/configuration.utf-8.ini')
        repros_path = r'\\nas.ads.mwn.de\badw\pal\7. Website\repros/'
        make_new_empty_db_if_absent(
                os.path.abspath(__file__ + '/../../../../../../6.1.7 Projekte und Arbeitsplanung/Ptolemaeus/seite_ptolemaeus/index.sqlite'),
                False,
                )
    refresh_forever(pid, config_path, repros_path)
