% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2013 ff.
% # Template for displaying search results; gets filled into the base template.
% lang_id = kwargs.get('lang_id', '')
% query = request.query
% cut_off = 80
<script src="/cssjs/jquery/jquery.dataTables.min.js"></script>
<script src="/cssjs/jquery/dataTables.responsive.min.js"></script>
<script>
	$(document).ready(
		function() {
			var config = {
			"dom": "filptp",
			"deferRender": true,
			"lengthMenu": [ 60, 80, 100, 150, 200, 250, 500 ],
			"language": {
				"search": "Filter:"
				},
			"order": [[0, "asc"]],
			"paging": true,
			"pageLength": 100
			};
			var table = $("#index").DataTable(config);
		}
	);
</script>
<main class="non-source">
<details class="card" role="search"><summary>Adjust search</summary>
	<p style="margin-left:0.5em">
		<b><dfn>_</dfn></b> in a search term is understood as <q>zero or one unknown character</q>. </br>
		<b><dfn>%</dfn></b> is understood as <q>zero or more unknown characters</q>.
	</p>
	<form action="search_results" method="get">
		<div class="table">
			<div>
				<label for="any_field"><b>Any field</b>:</label>
				<input value="{{query.get('any_field', '')}}" aria-label="{{dao.glosses['search_term'][lang_id]}}" autofocus="" name="any_field" pattern=".*\w.*" title="{{dao.glosses['at_least_one_letter'][lang_id]}}" type="search"/>
			</div>
		  % mainfields, subfields = dao.get_searchfields_jordanus()
		  % for row in mainfields:
			<div>
				<label for="{{row['field_hypernum']}}">{{row['field_hypername']}}:</label>
				<input value="{{query.get(str(row['field_hypernum']), '')}}" aria-label="{{dao.glosses['search_term'][lang_id]}}" name="{{row['field_hypernum']}}" pattern=".*\w.*" title="{{dao.glosses['at_least_one_letter'][lang_id]}}" type="search"/>
			</div>
		  % end
		</div>
		  % if subfields:
			% vals = [ query.get(str(row['field_hypernum']), '') for row in subfields ]
			<details class="index table"{{' open=""' if any(vals) else ''}}><summary>Further fields:</summary>
			% for val, row in zip(vals, subfields):
				<div>
					<label for="{{row['field_hypernum']}}">{{row['field_hypername']}}:</label>
					<input value="{{val}}" aria-label="{{dao.glosses['search_term'][lang_id]}}" name="{{row['field_hypernum']}}" pattern=".*\w.*" title="{{dao.glosses['at_least_one_letter'][lang_id]}}" type="search"/>
				</div>
			% end
			</details>
		  % end
		<p><button type="submit">Search&#8239;►</button></p>
	</form>
</details>
<article class="index">
	<div id="index_wrapper" class="dataTables_wrapper no-footer">
		<table class="cell-border order-column dataTable no-footer" id="index" aria-describedby="index_info" role="grid" style="width: 100%;" width="100%">
			<thead>
				<tr role="row">
					<th>Matching MS</th>
					<th>Matching Field</th>
					<th>Matching Field Content</th>
					<th>Add.&#8239;info: Persons</th>
					<th>Add.&#8239;info: Titles</th>
					<th>Add.&#8239;info: Incipits</th>
				</tr>
			</thead>
			<tbody>
			% for ms_id, subsiglum, field_id, field_name, term, location, persons, titles, incipits in dao.search_jordanus(query):
				<tr class="odd" role="row">
					<td>{{location}} <a class="ref" href="/jordanus/ms/{{ms_id}}#{{subsiglum}}_{{field_id}}">MS</a></td>
					<td>{{field_name.replace(' ', ' ')}}</td>
					<td class="mark">{{term}}</td>
				  % if len(persons) > cut_off:
					<td class="abbr">{{persons[:cut_off]}}<span class="cut_off">{{persons[min(cut_off, len(persons)):]}}</span></td>
				  % else:
					<td>{{persons}}</td>
				  % end
				  % if len(titles) > cut_off:
					<td class="abbr">{{titles[:cut_off]}}<span class="cut_off">{{titles[min(cut_off, len(titles)):]}}</span></td>
				  % else:
					<td>{{titles}}</td>
				  % end
				  % if len(incipits) > cut_off:
					<td class="abbr">{{incipits[:cut_off]}}<span class="cut_off">{{incipits[min(cut_off, len(incipits)):]}}</span></td>
				  % else:
					<td>{{incipits}}</td>
				  % end
				</tr>
			% end
			</tbody>
		</table>
	  % if 'ms_id' not in locals():
		<p><b>Sorry, no results found.</b></p>
	  % end
	</div>
</article>
</main>
% rebase('base_jordanus.tpl', request = request, dao = dao, kwargs = kwargs)
