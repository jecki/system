# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2009 ff.
import itertools
import os
import re
import statistics
import sys
import unicodedata
import webbrowser
from collections import Counter
from collections import defaultdict
from collections import deque
from collections.abc import Iterable
from functools import partial
from operator import itemgetter

import chart
import compare
import file_io
from parse import get_passage

s = re.search
sub = re.sub

# Adjektivadverbien aus dem Korpus.
# Lemmata mit und ohne `aller- sind zusammengefasst unter der Form ohne `aller-.
# Steigerungsformen ohne eigenen Lemmaansatz sind im Positiv zusammengefasst.
ADJADVS = (
        'abe-ge-schèiden-lîche', 'all-be-garwe', 'all-be-sunter', 'all-gare', 'all-ge-lîche', 'all-ge-mèin-lîche', 'all-ge-mèine', 'all-ge-rihte',
        'all-ge-riht#en', 'all-gâh#es', 'all-lîche', 'all-mèhtig-lîche', 'all-mèistige', 'all-rëhte', 'all-so-lîche', 'all-voll#en', 'all-vèste',
        'all-wâr', 'all-ëbenst', 'allig-lîche', 'alteres-èine', 'ander-halb#en', 'ane-dæhtig-lîche', 'ane-ge-sihtig-lîche', 'ane-sihtig-lîche',
        'ang-lîche', 'ange', 'ang#en', 'angest-lîche', 'ant-læz-lîche', 'ante', 'arme-lîche', 'armig-lîche', 'bald-lîche',
        'balde', 'baldig-lîche', 'barm-hërzen-lîche', 'barm-hërzig-lîche', 'bazz', 'be-dwungen-lîche', 'be-dâht-lîche', 'be-dæhtig-lîche',
        'be-hènte', 'be-hèntig-lîche', 'be-rèite', 'be-schèiden-lîche', 'be-sihtig-lîche', 'be-sunter', 'be-sunter#en', 'be-sunter-lîche',
        'be-zèichen-lîche', 'be-zîte', 'bil-lîche', 'bitter-lîche', 'blicke-lîche', 'bloède-lîche', 'bloèdig-lîche', 'bloèz-lîche',
        'blîde', 'blôz', 'blûg-lîche', 'blüèjente', 'bore-lange', 'boès-lîche', 'bruoder-lîche', 'bèr-lîche',
        'bèrme-lîche', 'bèzzer', 'bèzzeste', 'bèzziste', 'de-bazz', 'dicke', 'dickeste', 'dige-lîche',
        'diutisch#en', 'diutische', 'diè-muot-lîche', 'diè-müètig-lîche', 'dièb-lîche', 'diènest-lîche', 'drâhe', 'drâte',
        'drî-forme-lîche', 'drî-valtig-lîche', 'drô-lîche', 'dultig-lîche', 'dultig-lîcheste', 'durh-liuhtig-lîche', 'durh-nèhte', 'durh-nèhte-lîche',
        'durh-nèhtig-lîche', 'dwërh#es', 'dëgen-lîche', 'en-all-vèrte', 'er-barmig-lîche', 'er-bolgen-lîche', 'er-bèrmig-lîche', 'er-komen-lîche',
        'er-lèdig-lîche', 'er-schrocken-lîche', 'fîn', 'ganz', 'ganz-lîche', 'gare', 'gare-lîche', 'garwe',
        'gast-lîche', 'ge-dult-lîche', 'ge-dultig-lîche', 'ge-dultige', 'ge-dîhtig-lîche', 'ge-hug-lîche', 'ge-hëlfe-lîche', 'ge-luste-lîche',
        'ge-lustige', 'ge-lëgen-lîche', 'ge-lîch-lîche', 'ge-lîche', 'ge-lîch#es', 'ge-lîcheste', 'ge-mach', 'ge-mahel-lîche',
        'ge-mèch-lîche', 'ge-mèin-lîche', 'ge-mèine', 'ge-mèinig-lîche', 'ge-mèit#en', 'ge-nuoge', 'ge-næde-lîche', 'ge-nædig-lîche',
        'ge-nædige', 'ge-nèndig-lîche', 'ge-nôte', 'ge-rade', 'ge-rihte', 'ge-rihtig-lîche', 'ge-rihtige', 'ge-ring-lîche',
        'ge-ruoch-lîche', 'ge-ruowig-lîche', 'ge-rèite', 'ge-rèit#en', 'ge-rëch-lîche', 'ge-rëhte', 'ge-rëhtig-lîche', 'ge-satz-lîche', 'ge-siht-lîche',
        'ge-sihtig-lîche', 'ge-sprëch-lîche', 'ge-sunter', 'ge-swinde', 'ge-swâs-lîche', 'ge-swâse', 'ge-sèllig-lîche', 'ge-tougen',
        'ge-triuwe-lîche', 'ge-triuwe-lîcheste', 'ge-türste-lîche', 'ge-türstig-lîche', 'ge-vuog-lîche', 'ge-vuoge', 'ge-vær-lîche', 'ge-vèllig-lîche',
        'ge-walt-lîche', 'ge-waltig-lîche', 'ge-willig-lîche', 'ge-wis', 'ge-wiss-lîche', 'ge-wisse', 'ge-wizzen-lîche', 'ge-wone-lîche',
        'ge-wâre', 'ge-wær-lîche', 'ge-wëgen-lîche', 'ge-wër-lîche', 'ge-zogen-lîche', 'girdig-lîche', 'gire-lîche', 'gote-lîche',
        'grimme', 'grimme-lîche', 'grimmig-lîche', 'griuwe-lîche', 'grobe-lîche', 'groèzig-lîche', 'grème-lîche', 'grôz-lîche',
        'grôze', 'guot-lîche', 'guote', 'gâh-lîche', 'gâhe', 'gâhens', 'gâh#es', 'gæhe-lîche',
        'gæheling#en', 'gæh#es', 'gèist-lîche', 'gërne', 'gërneste', 'halb#en', 'halbe', 'halb#es', 'halt', 'halter',
        'hant-vèste-lîche', 'harte', 'harwe', 'hinter', 'hiure', 'hove-lîche', 'hoèheste', 'huor-lîche',
        'hurte-lîche', 'hurtig-lîche', 'hæling#en', 'hèftig-lîche', 'hèidenische', 'hèiftig-lîche', 'hèilig-hafte', 'hèilig-lîche',
        'hèilige', 'hèime-lîche', 'hèiter-lîche', 'hèitere', 'hèize', 'hèrte-lîche', 'hèrtig-lîche', 'hêbrêische', 'hêbrêisch#en',
        'hêr-lîche', 'hêrren-lîche', 'hëlfe-lîche', 'hërzen-lîche', 'hërzen-wole', 'hërzig-lîche', 'hôh-vèrtig-lîche', 'hôhe',
        'hôhe-lîche', 'hövisch-lîche', 'in-dwërh', 'inne-lîche', 'inne-wèntig-lîche', 'inne-wèntige', 'inner', 'inner-lîche',
        'innig-lîche', 'iè-ge-nôte', 'iè-lange', 'jungeste', 'jâmer-lîche', 'jâr-lang', 'jær-ge-lîche', 'jær-ge-lîch#es',
        'jær-lîche', 'jær-lîch#es', 'jærig-lîche', 'kampf-lîche', 'kind-lîche', 'kiusch-lîche', 'kiusche', 'klage-lîche',
        'kluoge', 'klâr-lîche', 'klèin-lîche', 'klèine', 'klôster-lîche', 'kone-lîche', 'koste-lîche', 'kostig-lîche',
        'kouf-lîche', 'krank-lîche', 'kristen-lîche', 'krièchische', 'krèftig-lîche', 'kumber-lîche', 'kund-lîche', 'kurg',
        'kurz-lîche', 'kurze', 'kèiser-lîche', 'kèrk-lîche', 'kûme', 'kûm#en', 'kümftig-lîche', 'kümftige', 'küning-lîche',
        'künstig-lîche', 'kür-lîche', 'küèn-lîche', 'lachen-lîche', 'lang-lîche', 'lang-sam', 'lange', 'lang#es',
        'laster-lîche', 'latînische', 'lazze', 'lidig-lîche', 'lind-lîche', 'linde', 'listig-lîche', 'lièb-lîche',
        'lièb-lîcheste', 'lièbe', 'lièhte', 'lobe-lîche', 'lobe-sam', 'lustig-lîche', 'læz-lîche', 'lèdig-lîche',
        'lèid-lîche', 'lèide', 'lèider', 'lèidig-lîche', 'lèngeste', 'lèzzeste', 'lëben-lîche', 'lîb-lîche', 'lîchname-lîche',
        'lîhte', 'lîhte-lîche', 'lîhteste', 'lîhtig-lîche', 'lîs-lîche', 'lîse', 'lûte', 'lûter',
        'lûter-lîche', 'lügen-lîche', 'lützel', 'maged-lîche', 'manig-valt', 'manig-valt-lîche', 'manig-valtig-lîche', 'mann-lîche',
        'marter-lîche', 'michel', 'michel#es', 'milich-lîche', 'milt-lîche', 'miltig-lîche', 'min', 'minnen-lîche',
        'minner', 'minneste', 'minnig-lîche', 'mittel', 'mitteste', 'mord-lîche', 'morgen-lîche', 'muoter-lîche',
        'muoz-lîche', 'muozig-lîche', 'mære', 'mæz-lîche', 'mæzig-lîche', 'mèht-lîche', 'mèhtige', 'mèin-tætig-lîche',
        'mèin#es', 'mèist-tèilig', 'mèiste', 'mèister-lîche', 'mèistige', 'mènnisch-lîche', 'mêre', 'müge-lîche',
        'müè-lîche', 'müèzig-lîche', 'nacken', 'name-lîche', 'natûr-lîche', 'nidere', 'nidereste', 'niuw#en',
        'niuwe', 'niuwe-lîche', 'niuweling#en', 'niuw#enes', 'niuw#es', 'nièt-lîche', 'nou', 'nâh',
        'nâh#ene', 'nâhe', 'næhe', 'næheste', 'nèhte-ge-lîch#es', 'nîd-lîche', 'nôte', 'nütze-lîche',
        'nützig-lîche', 'obe-wèntige', 'obereste', 'offen-bâr-lîche', 'offen-bâre', 'offen-lîche', 'offene', 'orden-lîche',
        'oèdig-lîche', 'pfaff-lîche', 'prîs-lîche', 'pîn-lîche', 'pînig-lîche', 'ring-lîche', 'ringe', 'rische-lîche',
        'riuwe-lîche', 'riuwig-lîche', 'roub-lîche', 'râwig-lîche', 'rède-lîche', 'rèin-lîche', 'rèine', 'rèinig-lîche',
        'rëht-lîche', 'rëhte', 'rëht#en', 'rîche', 'rîche-lîche', 'rîtære-lîche', 'same-lîche', 'samene', 'sament-haftige',
        'sament-lîche', 'samft-muot-lîche', 'samfte', 'schade-lîche', 'schallig-lîche', 'schame-lîche', 'schant-lîche', 'scharpf-lîche',
        'scharpfe', 'schière', 'schièreste', 'schëlle', 'schîne', 'schône', 'siben-valt', 'siben-valt-lîche',
        'sicher', 'sicher-lîche', 'sider', 'siht-lîche', 'sinne-lôs', 'sinnig-lîche', 'sitig-lîche', 'siune', 'sliunig-lîche',
        'sliunige', 'slêwig-lîche', 'slëhte', 'slëht#es', 'smæhe-lîche', 'snëll-lîche', 'snëlle', 'snëllig-lîche',
        'snôr-lîche', 'spott-lîche', 'spuotige', 'spâte', 'spæte', 'spèr-lîche', 'stark', 'starke',
        'stil-lîche', 'stille', 'still#en', 'stillig-lîche', 'stolz', 'stolz-lîche', 'strack#es', 'strange', 'strènge-lîche',
        'strît-lîche', 'strîtig-lîche', 'stumpf', 'sturm-lîche', 'sturm#es', 'stæte', 'stæte-lîche', 'stæt#es',
        'stætig-lîche', 'stèrk-lîche', 'sume-lîche', 'sun-lîche', 'sunder', 'sunder#en', 'sunter', 'sunter-bar',
        'sunter-bâre', 'sunter-lîche', 'sunteringe', 'sunterling#en', 'sunters', 'suoze', 'swache', 'swinde',
        'swindig-lîche', 'swâre', 'swær-lîche', 'sælig-lîche', 'sèine', 'sèmft-müètig-lîche', 'sèmftig-lîche', 'sène-lîche',
        'sêre', 'sêreste', 'sêrig-lîche', 'sëltene', 'sîd', 'sûber-lîche', 'sûbere', 'sûmesèlige',
        'sûre', 'sünte-lîche', 'süntig-lîche', 'süèze-lîche', 'süèzig-lîche', 'tage-lang', 'tage-lîche', 'tage-lîch#es',
        'tigere', 'tiure', 'tiuvel-lîche', 'tièfe', 'tièf#en','tobe-lîche', 'touge', 'tougen', 'tougen-lîche',
        'tratz-lîche', 'triuwe-lîche', 'trâge', 'træg-lîche', 'trôst-lîche', 'trûrig-lîche', 'trügen-lîche', 'tugen-lîche',
        'tugend-lîche', 'tumb-lîche', 'turst-lîche', 'tèile-ge-lîche', 'tôd-lîche', 'tôr-lîche', 'tûsent-valt', 'türstig-lîche',
        'un-angest-lîche', 'un-barm-hërzig-lîche', 'un-barme-lîche', 'un-be-grîfen-lîche', 'un-be-quâme', 'un-be-schèiden-lîche', 'un-be-wëge-lîche', 'un-bil-lîche',
        'un-er-barmig-lîche', 'un-er-komen-lîche', 'un-er-schrocken-lîche', 'un-er-været', 'un-ge-bèitete', 'un-ge-bür-lîche', 'un-ge-hinteret', 'un-ge-hiur-lîche',
        'un-ge-hiure', 'un-ge-holfen', 'un-ge-hôr-same-lîche', 'un-ge-limpf-lîche', 'un-ge-lîche', 'un-ge-mache', 'un-ge-mèch-lîche', 'un-ge-næde-lîche',
        'un-ge-nædig-lîche', 'un-ge-sparet', 'un-ge-swâse', 'un-ge-sèllig-lîche', 'un-ge-triuwe-lîche', 'un-ge-vuog-lîche', 'un-ge-vuoge', 'un-ge-wëgen',
        'un-ge-wër-lîche', 'un-ge-zogen-lîche', 'un-ge-zogene', 'un-guot-lîche', 'un-hinter-lîche', 'un-hôhe', 'un-kiusch-lîche', 'un-klage-lîche',
        'un-küstig-lîche', 'un-lange', 'un-lang#en', 'un-lang#es', 'un-lougen-lîche', 'un-lust-lîche', 'un-lîde-lîche', 'un-mann-lîche', 'un-mâz#en',
        'un-mâz-lîche', 'un-mâze', 'un-mæzig-lîche', 'un-mènnisch-lîche', 'un-müèdente', 'un-müèzig-lîche', 'un-nâhe', 'un-nâh#en', 'un-nütze-lîche',
        'un-orden-lîche', 'un-ruoch-lîche', 'un-rède-lîche', 'un-rèine', 'un-rëht-lîche', 'un-rëhte', 'un-rëhtig-lîche', 'un-samfte',
        'un-schame-lîche', 'un-schuldig-lîche', 'un-schône', 'un-sliunig-lîche', 'un-sprëchen-lîche', 'un-spèr-lîche', 'un-suoze', 'un-sèmftig-lîche',
        'un-tiure', 'un-tougen', 'un-triuwe-lîche', 'un-tôd-lîche', 'un-ver-borgen', 'un-ver-brochen-lîche', 'un-ver-drozzen-lîche', 'un-ver-gëbene',
        'un-ver-gëben#es', 'un-ver-holene', 'un-ver-mitte-lîche', 'un-ver-schèiden-lîche', 'un-ver-tougen-lîche', 'un-ver-zage-lîche', 'un-voll-kumen-lîche', 'un-vride-lîche',
        'un-vroè-lîche', 'un-vërre', 'un-wirdig-lîche', 'un-wizzent-lîche', 'un-wizzente', 'un-wær-lîche', 'un-wërd-lîche', 'un-wërde',
        'un-wërdig-lîche', 'un-wîs-lîche', 'un-zale-lîche', 'un-zer-brochen', 'un-zime-lîche', 'un-zorne-lîche', 'un-zît-lîche', 'un-èigen-lîche',
        'un-ènte-lîche', 'un-ê-lîche', 'un-ëbene', 'unter', 'uob-lîche', 'valsch', 'valsch-lîche', 'vaste',
        'vater-lîche', 'ver-borgen-lîche', 'ver-borgene', 'ver-dult-lîche', 'ver-dultig-lîche', 'ver-gëbene', 'ver-gëben#es', 'ver-holen-lîche',
        'ver-holene', 'ver-mëzzen-lîche', 'ver-numft-lîche', 'ver-numftig-lîche', 'ver-stolene', 'ver-stènt-lîche', 'ver-sûme-lîche', 'ver-tragente',
        'ver-wænen-lîche', 'vile', 'vile-lîhte', 'vièr-valt', 'vlèisch-lîche', 'vlêhe-lîche', 'vlîz-lîche', 'vlîzig-lîche',
        'vlîzige', 'vol', 'voll#en', 'voll-komen', 'voll-komen-lîche', 'voll-lîche', 'volle', 'voll#es',
        'vollig-lîche', 'vollige', 'vorder-lîche', 'vordere', 'vordereste', 'vore-lang', 'vorht-sam-lîche', 'vorhte-lîche',
        'vra-bald-lîche', 'vram-spuotig-lîche', 'vrast-munt-lîche', 'vrast-munte', 'vride-lîche', 'vrisch-lîche', 'vrische', 'vriunt-lîche',
        'vroè-lîche', 'vrume-lîche', 'vruo', 'vrèis-lîche', 'vrèisige', 'vrèmede', 'vrèmedig-lîche', 'vrèvel-lîche',
        'vrëche', 'vrî', 'vrî-lîche', 'vrô', 'vröuwede-lîche', 'vrümig-lîche', 'vrüèje', 'vârente',
        'vær-lîche', 'vèste', 'vèste-lîche', 'vèsten-lîche', 'vèstig-lîche', 'vërne', 'vërr#en', 'vërre',
        'vërr#enes', 'vërreste', 'vîent-lîche', 'vûl-lîche', 'vürder-lîche', 'vürder-mâl', 'vüre-sihtig-lîche', 'vüre-trëff-lîche',
        'warme', 'wasse', 'wil-lîche', 'wilde', 'wilde-lîche', 'willent#es', 'willig-lîche',
        'willige', 'winter-lange', 'wirde-lîche', 'wirdig-lîche', 'wirke-lîche', 'wirs', 'wirser', 'wirt-lîche',
        'wiss-lîche', 'wizzent-lîche', 'woche-ge-lîche', 'woche-lîche', 'wole', 'wole-ge-muote', 'wunsch-lîche', 'wunter-balde',
        'wunter-lîche', 'wunter-schière', 'wunter-sêre', 'wunter-vol', 'wâr-hafte', 'wâre', 'wæhe', 'wær-lîche',
        'wæt-lîche', 'wèich-lîche', 'wèiden-lîche', 'wèiger-lîche', 'wèntig-lîche', 'wère-lîche', 'wètte-vast', 'wênige',
        'wëche-lîch#es', 'wëhsel-lîche', 'wërd-lîche', 'wërde', 'wërdig-lîche', 'wërelt-lîche', 'wërt', 'wësent-lîche',
        'wîb-lîche', 'wîg-lîche', 'wîs-lîche', 'wîte', 'wünne-bære', 'wünne-lust-lîche', 'wünnig-lîche', 'zage-lîche',
        'zart-lîche', 'zarte', 'zime-lîche', 'zièr-lîche', 'zogen-lîche', 'zorn-lîche', 'zornig-lîche', 'zuht-lîche',
        'zwi-spilde', 'zwi-valt-lîche', 'zwi-valte', 'zwi-valtig-lîche', 'zwîvel-lîche', 'zèichen-lîche', 'zëhen-zig-valtige', 'zît-lîche',
        'zühtig-lîche', 'èdel-lîche', 'èges-lîche', 'èigen-lîche', 'èin-ge-nôte', 'èin-hëllig-lîche', 'èin-lîche', 'èin-muot-lîche',
        'èin-müètig-lîche', 'èin-trèhte-lîche', 'èin-valtig#en', 'èin-valtig-lîche', 'èin-valtige', 'èine', 'èinig', 'èinig-lîche',
        'èinzeht#en', 'èinzel#en', 'èinzel-lîche', 'èinzeling#en', 'èl-lèntig-lîche', 'èllent-hafte', 'èmz-lîche', 'èmzig-lîche',
        'èmzige', 'èmzigeste', 'ènte-hafte', 'ènte-lîche', 'èntig-lîche', 'èrbe-lîche', 'êr', 'êr-bære-lîche',
        'êr-bærig-lîche', 'êr-lîche', 'êr-same', 'êreste', 'êwe-liche', 'êwe-lîche', 'êwig-lîche', 'êwige',
        'ëben-ge-lîche', 'ëben-rîche', 'ëben-êwig-lîche', 'ëbene', 'ërnest-hafte', 'ërnest-lîche', 'ëten-vile', 'ëtes-wiè-vile',
        'îlente', 'îlige', 'îtel', 'ôd-müète-lîche', 'ôd-müètig-lîche', 'ûf-bazz', 'ûf-ge-riht#es', 'ûz-ge-schèiden-lîche',
        'ûz-wèntige', 'ûzer-lîche', 'übel-lîche', 'übele', 'über-lang', 'über-lustig-lîche', 'über-lût', 'über-vlüzzig-lîche',
        'über-vol', 'üppig-lîche', 'üppige',
        )

ADJADVS2 = tuple( a.replace('#', '') for a in ADJADVS )

# Subset of those ADJADVS which are similar to comparatives:
NEAR_COMPS = ('inner', 'unter', 'nidere', 'hinter', 'vordere')

ADJNEUTRA_RE = re.compile(
        r'^(guot|rëht|èigen|arg|bunt|diut\(i\)sch|hol|hèil|hèrmîn|hërze\(n\)-lièb|kalt|klâr|latîn|lièb|lèid|lützel|michel|'\
        r'nazz|quât|sunder|sûr|un-guot|un-rëht|ver-lor\(e\)n|warm|wâr|wênig|êr\(e\)st|übel) @n(?!\w)')

CHARS_NORMAL = 'abcdefghijklmnopqrstuvwxyzæ'
'''
All characters after unicodedata.normalize('NFD', text) and
text.lower(), w/o diacritics, nasal bar, r-abbreviation and
r-signifying superposition.
'''

CHARS_NORMAL_CONS = 'bcdfghklpqtxmnrs$zv'
'''
All consonant characters after unicodedata.normalize('NFD', text) and
text.lower(), w/o nasal bar and r-abbreviation,
w/o diacritics and r-signifying superposition.
'v' arbitrarily considered as consonant; 'j' and 'w' left out.
'''

DIACRITICS = (
        ("\\'", '̍'),
        ('\\´', '́'),
        ('\\:', '̈'),
        ('\\^', '̂'),
        ('\\o', 'ͦ'),
        )

JA_A_STEMS = [('bære', 'bâr'), ('gèile', 'gèil(e)'), ('gèrwe', 'garw'), ('gæhe', 'gâh'), ('hazze', 'hazz'), ('hèfte', 'haft'), ('hèrte', 'hart'),
              ('hèzze', 'hazz'), ('krènke', 'krank'), ('künde', 'kund'), ('lènge', 'lang'), ('lîbe', 'lîb'), ('mèine', 'mèin(e)'), ('noète', 'nôt'),
              ('rihte', 'rëht'), ('schiter(e)', 'schëter'), ('schèrpfe', 'scharpf'), ('spitze', 'spitz'), ('strènge', 'strang'), ('swære', 'swâr'),
              ('swènke', 'swank'), ('sèmfte', 'samft'), ('sèrpfe', 'sarpf'), ('træge', 'trâg'), ('vrüète', 'vruot'), ('vèste', 'vast'),
              ('vüège', 'vuog'), ('vüèze', 'vuoz'), ('wènke', 'wank'), ('wèsse', 'wass(e)'), ('èrge', 'arg')]

SUBJUNCTIONS = ('all-sam(e)', 'all-sô', 'all-sô~lange', 'all-sô~schière', 'all-èin(e)', 'alle~diè~wîle', 'alle~diè~wîle~dazz', 'be-dazz',
                'biz', 'biz~dazz', 'bî-daz', 'danne', 'danne~dazz', 'dar(e)', 'dazz', 'diè~wîle', 'diè~wîle~dazz', 'diè~wîle~unte',
                'durh~dazz', 'dâr', 'dô', 'ne-wære', 'niuwan', 'nobe', 'nû', 'obe', 'sam(e)', 'same-sô', 'sider', 'sint', 'sint~dazz',
                'sus', 'swanne', 'swanne~dazz', 'swiè', 'sîd', 'sît~dazz', 'sô', 'sô~dazz', 'umbe~dazz', 'unter-diu', 'unz(e)',
                'unz(e)~dazz', 'unzen(t)', 'vür(e)~dazz', 'wan', 'wan~dazz', 'wiè~dazz', 'âne', 'âne~dazz', 'êr', 'ê~dazz', 'ëht', 'ûf~dazz')

TABLE = (
        [[('1050-1150', 'obd.')]],
        [[('1150-1200', 'alem.')], [('1150-1200', 'alem.-bair.')], [('1150-1200', 'bair.')], [('1150-1200', 'hess.-thür.-omd.')], [('1150-1200', 'rhfrk.-hess.')], [('1150-1200', 'mfrk.')]],
        [[('1200-1250', 'obd.')], [('1200-1250', 'alem.')], [('1200-1250', 'alem.-bair.')], [('1200-1250', 'bair.')], [('1200-1250', 'hess.-thür.-omd.')], [('1200-1250', 'rhfrk.-hess.')], [('1200-1250', 'mfrk.')]],
        [[('1250-1300', 'alem.')], [('1250-1300', 'alem.-bair.')], [('1250-1300', 'bair.')], [('1250-1300', 'hess.-thür.-omd.')], [('1250-1300', 'rhfrk.-hess.')], [('1250-1300', 'mfrk.')]],
        [[('1300-1350', 'alem.')], [('1300-1350', 'alem.-bair.')], [('1300-1350', 'bair.')], [('1300-1350', 'ofrk.')], [('1300-1350', 'hess.-thür.-omd.')], [('1300-1350', 'rhfrk.-hess.')], [('1300-1350', 'mfrk.')]],
        )

TABLE_WMD = (
        [[('1050-1150', 'obd.')]],
        [[('1150-1200', 'alem.')], [('1150-1200', 'alem.-bair.')], [('1150-1200', 'bair.')], [('1150-1200', 'hess.-thür.-omd.')], [('1150-1200', 'wmd.')]],
        [[('1200-1250', 'obd.')], [('1200-1250', 'alem.')], [('1200-1250', 'alem.-bair.')], [('1200-1250', 'bair.')], [('1200-1250', 'hess.-thür.-omd.')], [('1200-1250', 'wmd.')]],
        [[('1250-1300', 'alem.')], [('1250-1300', 'alem.-bair.')], [('1250-1300', 'bair.')], [('1250-1300', 'hess.-thür.-omd.')], [('1250-1300', 'rhfrk.-hess.')], [('1250-1300', 'mfrk.')]],
        [[('1300-1350', 'alem.')], [('1300-1350', 'alem.-bair.')], [('1300-1350', 'bair.')], [('1300-1350', 'ofrk.')], [('1300-1350', 'hess.-thür.-omd.')], [('1300-1350', 'rhfrk.-hess.')], [('1300-1350', 'mfrk.')]],
        )

TABLE_EARLY = (
        [['Will']],
        [['WNot']],
        [['BaGB', 'HuH', 'Ezzo', 'Mem', 'Meri', 'RPaul']],
        [['PrZü', 'Muri', 'LEnt', 'Scop'], ['Spec', 'WMEv', 'Mess'], ['Phys', 'Wind', 'Kchr', 'HLit'], ['PrFr', 'Aegi'], ['TrPs', 'ArnM'], ['RBib']],
        [['Iw', 'Nib', 'Parz', 'Tris'], ['TrHL', 'Luci', 'Flor', 'TriF'], ['ZwBR', 'Hoff'], ['PrMi', 'PrPa', 'Mar', 'Hchz'], ['PrMK', 'GRud', 'AlxS'], ['VatG'], ['RhMl', 'RhTun']],
##        [['PrSch', 'SwSp', 'UFreib1', 'RWchr', 'RWh'], ['DvATr', 'StBA', 'UAugsb1', 'Wins'], ['Bart', 'BKön', 'Diet', 'Lieht'], ['JMar', 'MüRB', 'AthP'], ['PrMF', 'SalH', 'Himlf'], ['Brig', 'PLilie', 'UKöln1', 'KuG', 'VLilie']],
##        [['NikP', 'UFreib2', 'Mart', 'Rapp'], ['Baum', 'UAugsb2', 'Hartw', 'Türh'], ['ObEv', 'Rupr', 'ULands', 'MMag'], ['BeEv', 'MBeh', 'UJena', 'HTri', 'LuKr', 'Pass'], ['Hleb', 'OxBR', 'UMainz', 'Elis', 'Erlös', 'PrRei'], ['BuMi', 'Taul', 'UKöln2', 'Göll', 'Yol']],
        )

TEXT_INFOS = [
        {"Gattung": "P", "Kürzel": "BaGB", "Lateinabhängigkeit": "frei", "Name": "Bamberger Glaube und Beichte", "Raum": "obd.", "Stufe": "²11–¹12", "geistlich|weltlich": "geistlich", "Unterraum": "obd."},
        {"Gattung": "P", "Kürzel": "HuH", "Lateinabhängigkeit": "frei", "Name": "Himmel und Hölle", "Raum": "obd.", "Stufe": "²11–¹12", "geistlich|weltlich": "geistlich", "Unterraum": "obd."},
        {"Gattung": "P", "Kürzel": "Will", "Lateinabhängigkeit": "frei", "Name": "Williram von Ebersberg: Hohelied-Paraphrase und \xadkommentar", "Raum": "obd.", "Stufe": "²11–¹12", "geistlich|weltlich": "geistlich", "Unterraum": "obd."},
        {"Gattung": "P", "Kürzel": "WNot", "Lateinabhängigkeit": "frei", "Name": "Wiener Notker", "Raum": "obd.", "Stufe": "²11–¹12", "geistlich|weltlich": "geistlich", "Unterraum": "obd."},
        {"Gattung": "V", "Kürzel": "Ezzo", "Lateinabhängigkeit": "", "Name": "Ezzos Gesang", "Raum": "obd.", "Stufe": "²11–¹12", "geistlich|weltlich": "geistlich", "Unterraum": "obd."},
        {"Gattung": "V", "Kürzel": "Mem", "Lateinabhängigkeit": "", "Name": "Memento Mori", "Raum": "obd.", "Stufe": "²11–¹12", "geistlich|weltlich": "geistlich", "Unterraum": "obd."},
        {"Gattung": "V", "Kürzel": "Meri", "Lateinabhängigkeit": "", "Name": "Merigarto", "Raum": "obd.", "Stufe": "²11–¹12", "geistlich|weltlich": "weltlich", "Unterraum": "obd."},
        {"Gattung": "V", "Kürzel": "RPaul", "Lateinabhängigkeit": "", "Name": "Rheinauer Paulus", "Raum": "obd.", "Stufe": "²11–¹12", "geistlich|weltlich": "geistlich", "Unterraum": "obd."},
        {"Gattung": "P", "Kürzel": "Phys", "Lateinabhängigkeit": "frei", "Name": "Physiologus", "Raum": "bair.", "Stufe": "²12", "geistlich|weltlich": "", "Unterraum": "bair."},
        {"Gattung": "P", "Kürzel": "Wind", "Lateinabhängigkeit": "engstens", "Name": "Windberger Psalter", "Raum": "bair.", "Stufe": "²12", "geistlich|weltlich": "geistlich", "Unterraum": "bair."},
        {"Gattung": "V", "Kürzel": "HLit", "Lateinabhängigkeit": "", "Name": "Heinrichs Litanei", "Raum": "bair.", "Stufe": "²12", "geistlich|weltlich": "geistlich", "Unterraum": "bair."},
        {"Gattung": "V", "Kürzel": "Kchr", "Lateinabhängigkeit": "", "Name": "Kaiserchronik", "Raum": "bair.", "Stufe": "²12", "geistlich|weltlich": "weltlich", "Unterraum": "bair."},
        {"Gattung": "P", "Kürzel": "Spec", "Lateinabhängigkeit": "frei", "Name": "Speculum ecclesiae", "Raum": "alem.-bair.", "Stufe": "²12", "geistlich|weltlich": "geistlich", "Unterraum": "alem.-bair."},
        {"Gattung": "P", "Kürzel": "WMEv", "Lateinabhängigkeit": "frei", "Name": "Wien-Münchener Evangelien (Bruchstücke)", "Raum": "alem.-bair.", "Stufe": "²12", "geistlich|weltlich": "geistlich", "Unterraum": "alem.-bair."},
        {"Gattung": "V", "Kürzel": "Mess", "Lateinabhängigkeit": "", "Name": "Deutung der Messgebräuche", "Raum": "alem.-bair.", "Stufe": "²12", "geistlich|weltlich": "geistlich", "Unterraum": "alem.-bair."},
        {"Gattung": "P", "Kürzel": "Muri", "Lateinabhängigkeit": "frei", "Name": "Gebete und Benediktionen aus Muri", "Raum": "alem.", "Stufe": "²12", "geistlich|weltlich": "geistlich", "Unterraum": "alem."},
        {"Gattung": "P", "Kürzel": "PrZü", "Lateinabhängigkeit": "frei", "Name": "Züricher Predigten", "Raum": "alem.", "Stufe": "²12", "geistlich|weltlich": "geistlich", "Unterraum": "alem."},
        {"Gattung": "V", "Kürzel": "LEnt", "Lateinabhängigkeit": "frei", "Name": "Linzer Entechrist", "Raum": "alem.", "Stufe": "²12", "geistlich|weltlich": "geistlich", "Unterraum": "alem."},
        {"Gattung": "V", "Kürzel": "Scop", "Lateinabhängigkeit": "", "Name": "Scoph von dem lône", "Raum": "alem.", "Stufe": "²12", "geistlich|weltlich": "geistlich", "Unterraum": "alem."},
        {"Gattung": "P", "Kürzel": "TrPs", "Lateinabhängigkeit": "engstens", "Name": "Trierer Psalmen", "Raum": "wmd.", "Stufe": "²12", "geistlich|weltlich": "geistlich", "Unterraum": "rhfrk.-hess."},
        {"Gattung": "V", "Kürzel": "ArnM", "Lateinabhängigkeit": "", "Name": "Arnsteiner Marienlied", "Raum": "wmd.", "Stufe": "²12", "geistlich|weltlich": "geistlich", "Unterraum": "rhfrk.-hess."},
        {"Gattung": "V", "Kürzel": "RBib", "Lateinabhängigkeit": "", "Name": "Mittelfränkische Reimbibel", "Raum": "wmd.", "Stufe": "²12", "geistlich|weltlich": "geistlich", "Unterraum": "mfrk."},
        {"Gattung": "P", "Kürzel": "PrFr", "Lateinabhängigkeit": "frei", "Name": "Frankfurter Predigtfragmente", "Raum": "omd.", "Stufe": "²12", "geistlich|weltlich": "geistlich", "Unterraum": "omd."},
        {"Gattung": "V", "Kürzel": "Aegi", "Lateinabhängigkeit": "", "Name": "Trierer Aegidius", "Raum": "omd.", "Stufe": "²12", "geistlich|weltlich": "geistlich", "Unterraum": "omd."},
        {"Gattung": "V", "Kürzel": "Iw", "Lateinabhängigkeit": "", "Name": "Hartmann: Iwein (B)", "Raum": "obd.", "Stufe": "¹13", "geistlich|weltlich": "weltlich", "Unterraum": "obd."},
        {"Gattung": "V", "Kürzel": "Nib", "Lateinabhängigkeit": "", "Name": "Nibelungenlied", "Raum": "obd.", "Stufe": "¹13", "geistlich|weltlich": "weltlich", "Unterraum": "obd."},
        {"Gattung": "V", "Kürzel": "Parz", "Lateinabhängigkeit": "", "Name": "Wolfram von Eschenbach: Parzival (D)", "Raum": "obd.", "Stufe": "¹13", "geistlich|weltlich": "weltlich", "Unterraum": "obd."},
        {"Gattung": "V", "Kürzel": "Tris", "Lateinabhängigkeit": "", "Name": "Gottfried von Straßburg: Tristan", "Raum": "obd.", "Stufe": "¹13", "geistlich|weltlich": "weltlich", "Unterraum": "obd."},
        {"Gattung": "P", "Kürzel": "PrMi", "Lateinabhängigkeit": "", "Name": "Millstätter Predigtsammlung", "Raum": "bair.", "Stufe": "¹13", "geistlich|weltlich": "geistlich", "Unterraum": "bair."},
        {"Gattung": "P", "Kürzel": "PrPa", "Lateinabhängigkeit": "", "Name": "St. Pauler Predigten", "Raum": "bair.", "Stufe": "¹13", "geistlich|weltlich": "geistlich", "Unterraum": "bair."},
        {"Gattung": "V", "Kürzel": "Hchz", "Lateinabhängigkeit": "", "Name": "Die Hochzeit", "Raum": "bair.", "Stufe": "¹13", "geistlich|weltlich": "geistlich", "Unterraum": "bair."},
        {"Gattung": "V", "Kürzel": "Mar", "Lateinabhängigkeit": "", "Name": "Priester Wernher: Marienleben (D)", "Raum": "bair.", "Stufe": "¹13", "geistlich|weltlich": "geistlich", "Unterraum": "bair."},
        {"Gattung": "P", "Kürzel": "Hoff", "Lateinabhängigkeit": "", "Name": "Hoffmannsche Predigten", "Raum": "alem.-bair.", "Stufe": "¹13", "geistlich|weltlich": "geistlich", "Unterraum": "alem.-bair."},
        {"Gattung": "P", "Kürzel": "ZwBR", "Lateinabhängigkeit": "engstens", "Name": "Zwifaltener Benediktinerregel", "Raum": "alem.-bair.", "Stufe": "¹13", "Untergattung": "geistlich", "Unterraum": "alem.-bair."},
        {"Gattung": "P", "Kürzel": "Luci", "Lateinabhängigkeit": "frei", "Name": "Lucidarius (Göttinger Fragment G)", "Raum": "alem.", "Stufe": "¹13", "geistlich|weltlich": "weltlich", "Unterraum": "alem."},
        {"Gattung": "P", "Kürzel": "TrHL", "Lateinabhängigkeit": "", "Name": "Trudperter Hohelied", "Raum": "alem.", "Stufe": "¹13", "geistlich|weltlich": "geistlich", "Unterraum": "alem."},
        {"Gattung": "V", "Kürzel": "Flor", "Lateinabhängigkeit": "", "Name": "Frauenfelder Flore (Bruchstücke)", "Raum": "alem.", "Stufe": "¹13", "geistlich|weltlich": "weltlich", "Unterraum": "alem."},
        {"Gattung": "V", "Kürzel": "TriF", "Lateinabhängigkeit": "", "Name": "Gottfried von Straßburg: Tristan (Fragmente f, l und Augsburg)", "Raum": "alem.", "Stufe": "¹13", "geistlich|weltlich": "weltlich", "Unterraum": "alem."},
        {"Gattung": "P", "Kürzel": "VatG", "Lateinabhängigkeit": "", "Name": "Vatikanische Gebete", "Raum": "wmd.", "Stufe": "¹13", "geistlich|weltlich": "geistlich", "Unterraum": "rhfrk.-hess."},
        {"Gattung": "V", "Kürzel": "RhMl", "Lateinabhängigkeit": "", "Name": "Rheinisches Marienlob", "Raum": "wmd.", "Stufe": "¹13", "geistlich|weltlich": "geistlich", "Unterraum": "mfrk."},
        {"Gattung": "V", "Kürzel": "RhTun", "Lateinabhängigkeit": "", "Name": "Rheinischer Tundalus (mittelfränkisch)", "Raum": "wmd.", "Stufe": "¹13", "geistlich|weltlich": "geistlich", "Unterraum": "mfrk."},
        {"Gattung": "P", "Kürzel": "PrMK", "Lateinabhängigkeit": "frei", "Name": "Mitteldeutsche Predigten (Fragment K)", "Raum": "omd.", "Stufe": "¹13", "geistlich|weltlich": "geistlich", "Unterraum": "omd."},
        {"Gattung": "V", "Kürzel": "AlxS", "Lateinabhängigkeit": "", "Name": "Straßburger Alexander", "Raum": "omd.", "Stufe": "¹13", "geistlich|weltlich": "weltlich", "Unterraum": "omd."},
        {"Gattung": "V", "Kürzel": "GRud", "Lateinabhängigkeit": "", "Name": "Graf Rudolf", "Raum": "omd.", "Stufe": "¹13", "geistlich|weltlich": "weltlich", "Unterraum": "omd."},
        {"Gattung": "P", "Kürzel": "Bart", "Lateinabhängigkeit": "frei", "Name": "Bartholomäus", "Raum": "bair.", "Stufe": "²13", "geistlich|weltlich": "weltlich", "Unterraum": "bair."},
        {"Gattung": "P", "Kürzel": "BKön", "Lateinabhängigkeit": "", "Name": "Buch der Könige alter und neuer Ee", "Raum": "bair.", "Stufe": "²13", "geistlich|weltlich": "", "Unterraum": "bair."},
        {"Gattung": "V", "Kürzel": "Diet", "Lateinabhängigkeit": "", "Name": "Dietrichs Flucht", "Raum": "bair.", "Stufe": "²13", "geistlich|weltlich": "weltlich", "Unterraum": "bair."},
        {"Gattung": "V", "Kürzel": "Lieht", "Lateinabhängigkeit": "", "Name": "Ulrich von Lichtenstein: Frauendienst (M)", "Raum": "bair.", "Stufe": "²13", "geistlich|weltlich": "weltlich", "Unterraum": "bair."},
        {"Gattung": "P", "Kürzel": "DvATr", "Lateinabhängigkeit": "", "Name": "David von Augsburg: Traktate", "Raum": "alem.-bair.", "Stufe": "²13", "geistlich|weltlich": "geistlich", "Unterraum": "alem.-bair."},
        {"Gattung": "P", "Kürzel": "StBA", "Lateinabhängigkeit": "", "Name": "Augsburger Stadtbuch", "Raum": "alem.-bair.", "Stufe": "²13", "geistlich|weltlich": "weltlich", "Unterraum": "alem.-bair."},
        {"Gattung": "U", "Kürzel": "UAugsb1", "Lateinabhängigkeit": "", "Name": "Augsburger Urkunden", "Raum": "alem.-bair.", "Stufe": "²13", "geistlich|weltlich": "weltlich", "Unterraum": "alem.-bair."},
        {"Gattung": "V", "Kürzel": "Wins", "Lateinabhängigkeit": "", "Name": "Winsbeke und Winsbekin", "Raum": "alem.-bair.", "Stufe": "²13", "geistlich|weltlich": "weltlich", "Unterraum": "alem.-bair."},
        {"Gattung": "P", "Kürzel": "PrSch", "Lateinabhängigkeit": "frei", "Name": "Schwarzwälder Predigten (Gr)", "Raum": "alem.", "Stufe": "²13", "geistlich|weltlich": "geistlich", "Unterraum": "alem.-bair.?"},
        {"Gattung": "P", "Kürzel": "SwSp", "Lateinabhängigkeit": "", "Name": "Schwabenspiegel", "Raum": "alem.", "Stufe": "²13", "geistlich|weltlich": "weltlich", "Unterraum": "alem."},
        {"Gattung": "U", "Kürzel": "UFreib1", "Lateinabhängigkeit": "", "Name": "Freiburger Urkunden", "Raum": "alem.", "Stufe": "²13", "geistlich|weltlich": "weltlich", "Unterraum": "alem."},
        {"Gattung": "V", "Kürzel": "RWchr", "Lateinabhängigkeit": "", "Name": "Rudolf von Ems: Weltchronik", "Raum": "alem.", "Stufe": "²13", "geistlich|weltlich": "weltlich", "Unterraum": "alem."},
        {"Gattung": "V", "Kürzel": "RWh", "Lateinabhängigkeit": "", "Name": "Rudolf von Ems: Wilhelm von Orlens", "Raum": "alem.", "Stufe": "²13", "geistlich|weltlich": "weltlich", "Unterraum": "alem."},
        {"Gattung": "P", "Kürzel": "Brig", "Lateinabhängigkeit": "", "Name": "Amtleutebuch St. Brigiden", "Raum": "mfrk.", "Stufe": "²13", "geistlich|weltlich": "weltlich", "Unterraum": "mfrk."},
        {"Gattung": "P", "Kürzel": "PLilie", "Lateinabhängigkeit": "", "Name": "Die Lilie (Prosateil)", "Raum": "mfrk.", "Stufe": "²13", "geistlich|weltlich": "geistlich", "Unterraum": "mfrk."},
        {"Gattung": "U", "Kürzel": "UKöln1", "Lateinabhängigkeit": "", "Name": "Gottfried Hagen: Kölner Urkunden", "Raum": "mfrk.", "Stufe": "²13", "geistlich|weltlich": "weltlich", "Unterraum": "mfrk."},
        {"Gattung": "V", "Kürzel": "KuG", "Lateinabhängigkeit": "", "Name": "Karl und Galie", "Raum": "mfrk.", "Stufe": "²13", "geistlich|weltlich": "weltlich", "Unterraum": "mfrk."},
        {"Gattung": "V", "Kürzel": "VLilie", "Lateinabhängigkeit": "", "Name": "Die Lilie (Versteil)", "Raum": "mfrk.", "Stufe": "²13", "geistlich|weltlich": "geistlich", "Unterraum": "mfrk."},
        {"Gattung": "P", "Kürzel": "PrMF", "Lateinabhängigkeit": "frei", "Name": "Mitteldeutsche Predigten (F (Frankfurt), G (Grieshaber: Freiburg), H1 (Nürnberg))", "Raum": "rhfrk.-hess.", "Stufe": "²13", "geistlich|weltlich": "geistlich", "Unterraum": "rhfrk.-hess."},
        {"Gattung": "P", "Kürzel": "SalH", "Lateinabhängigkeit": "", "Name": "Salomonis hûs", "Raum": "rhfrk.-hess.", "Stufe": "²13", "geistlich|weltlich": "geistlich", "Unterraum": "rhfrk.-hess."},
        {"Gattung": "V", "Kürzel": "Himlf", "Lateinabhängigkeit": "", "Name": "Marien Himmelfahrt", "Raum": "rhfrk.-hess.", "Stufe": "²13", "geistlich|weltlich": "geistlich", "Unterraum": "rhfrk.-hess."},
        {"Gattung": "P", "Kürzel": "JMar", "Lateinabhängigkeit": "", "Name": "Jenaer Martyrologium", "Raum": "omd.", "Stufe": "²13", "geistlich|weltlich": "geistlich", "Unterraum": "omd."},
        {"Gattung": "P", "Kürzel": "MüRB", "Lateinabhängigkeit": "", "Name": "Mühlhäuser Rechtsbuch", "Raum": "omd.", "Stufe": "²13", "geistlich|weltlich": "weltlich", "Unterraum": "omd."},
        {"Gattung": "V", "Kürzel": "AthP", "Lateinabhängigkeit": "", "Name": "Athis und Prophilias", "Raum": "omd.", "Stufe": "²13", "geistlich|weltlich": "weltlich", "Unterraum": "rhfrk.-hess.?"},
        {"Gattung": "P", "Kürzel": "ObEv", "Lateinabhängigkeit": "frei", "Name": "Oberaltaicher Evangelistar", "Raum": "bair.", "Stufe": "¹14", "geistlich|weltlich": "geistlich", "Unterraum": "bair."},
        {"Gattung": "P", "Kürzel": "Rupr", "Lateinabhängigkeit": "", "Name": "Ruprecht von Freising: Rechtsbuch", "Raum": "bair.", "Stufe": "¹14", "geistlich|weltlich": "weltlich", "Unterraum": "bair."},
        {"Gattung": "U", "Kürzel": "ULands", "Lateinabhängigkeit": "", "Name": "Landshuter Urkunden", "Raum": "bair.", "Stufe": "¹14", "geistlich|weltlich": "weltlich", "Unterraum": "bair."},
        {"Gattung": "V", "Kürzel": "MMag", "Lateinabhängigkeit": "frei", "Name": "Maria Magdalena (Verslegende)", "Raum": "bair.", "Stufe": "¹14", "geistlich|weltlich": "geistlich", "Unterraum": "bair."},
        {"Gattung": "P", "Kürzel": "Baum", "Lateinabhängigkeit": "", "Name": "Baumgarten geistlicher Herzen (L)", "Raum": "alem.-bair.", "Stufe": "¹14", "geistlich|weltlich": "geistlich", "Unterraum": "alem.-bair."},
        {"Gattung": "U", "Kürzel": "UAugsb2", "Lateinabhängigkeit": "", "Name": "Augsburger Urkunden", "Raum": "alem.-bair.", "Stufe": "¹14", "geistlich|weltlich": "weltlich", "Unterraum": "alem.-bair."},
        {"Gattung": "V", "Kürzel": "Hartw", "Lateinabhängigkeit": "", "Name": "Hartwig von dem Hage: Margaretenlegende; Tagzeiten", "Raum": "alem.-bair.", "Stufe": "¹14", "geistlich|weltlich": "geistlich", "Unterraum": "alem.-bair."},
        {"Gattung": "V", "Kürzel": "Türh", "Lateinabhängigkeit": "", "Name": "Ulrich von Türheim: Rennewart", "Raum": "alem.-bair.", "Stufe": "¹14", "geistlich|weltlich": "weltlich", "Unterraum": "alem.-bair."},
        {"Gattung": "P", "Kürzel": "NikP", "Lateinabhängigkeit": "", "Name": "Nikolaus von Straßburg: Predigten (C)", "Raum": "alem.", "Stufe": "¹14", "geistlich|weltlich": "geistlich", "Unterraum": "alem."},
        {"Gattung": "U", "Kürzel": "UFreib2", "Lateinabhängigkeit": "", "Name": "Freiburger Urkunden", "Raum": "alem.", "Stufe": "¹14", "geistlich|weltlich": "weltlich", "Unterraum": "alem."},
        {"Gattung": "V", "Kürzel": "Mart", "Lateinabhängigkeit": "", "Name": "Hugo von Langenstein: Martina", "Raum": "alem.", "Stufe": "¹14", "geistlich|weltlich": "geistlich", "Unterraum": "alem."},
        {"Gattung": "V", "Kürzel": "Rapp", "Lateinabhängigkeit": "", "Name": "Rappoltsteiner Parzival", "Raum": "alem.", "Stufe": "¹14", "geistlich|weltlich": "weltlich", "Unterraum": "alem."},
        {"Gattung": "P", "Kürzel": "BuMi", "Lateinabhängigkeit": "", "Name": "Buch der Minne / Rede von den 15 Graden", "Raum": "mfrk.", "Stufe": "¹14", "geistlich|weltlich": "geistlich", "Unterraum": "mfrk."},
        {"Gattung": "P", "Kürzel": "Taul", "Lateinabhängigkeit": "", "Name": "Tauler: Predigten", "Raum": "mfrk.", "Stufe": "¹14", "geistlich|weltlich": "geistlich", "Unterraum": "mfrk."},
        {"Gattung": "U", "Kürzel": "UKöln2", "Lateinabhängigkeit": "", "Name": "Kölner Urkunden", "Raum": "mfrk.", "Stufe": "¹14", "geistlich|weltlich": "weltlich", "Unterraum": "mfrk."},
        {"Gattung": "V", "Kürzel": "Göll", "Lateinabhängigkeit": "", "Name": "Schlacht bei Göllheim / Böhmenschlacht / Minnehof", "Raum": "mfrk.", "Stufe": "¹14", "geistlich|weltlich": "weltlich", "Unterraum": "mfrk."},
        {"Gattung": "V", "Kürzel": "Yol", "Lateinabhängigkeit": "", "Name": "Iolande von Vianden", "Raum": "mfrk.", "Stufe": "¹14", "geistlich|weltlich": "", "Unterraum": "mfrk."},
        {"Gattung": "P", "Kürzel": "Hleb", "Lateinabhängigkeit": "", "Name": "Heiligenleben", "Raum": "rhfrk.-hess.", "Stufe": "¹14", "geistlich|weltlich": "geistlich", "Unterraum": "rhfrk.-hess."},
        {"Gattung": "P", "Kürzel": "OxBR", "Lateinabhängigkeit": "eng", "Name": "Oxforder Benediktinerregel", "Raum": "rhfrk.-hess.", "Stufe": "¹14", "Untergattung": "geistlich", "Unterraum": "rhfrk.-hess."},
        {"Gattung": "U", "Kürzel": "UMainz", "Lateinabhängigkeit": "", "Name": "Mainzer Urkunden", "Raum": "rhfrk.-hess.", "Stufe": "¹14", "geistlich|weltlich": "weltlich", "Unterraum": "rhfrk.-hess."},
        {"Gattung": "V", "Kürzel": "Elis", "Lateinabhängigkeit": "", "Name": "Leben der heiligen Elisabeth", "Raum": "rhfrk.-hess.", "Stufe": "¹14", "geistlich|weltlich": "", "Unterraum": "rhfrk.-hess."},
        {"Gattung": "V", "Kürzel": "Erlös", "Lateinabhängigkeit": "", "Name": "Die Erlösung", "Raum": "rhfrk.-hess.", "Stufe": "¹14", "geistlich|weltlich": "geistlich", "Unterraum": "rhfrk.-hess."},
        {"Gattung": "V", "Kürzel": "PrRei", "Lateinabhängigkeit": "", "Name": "Hessische Reimpredigten", "Raum": "rhfrk.-hess.", "Stufe": "¹14", "geistlich|weltlich": "geistlich", "Unterraum": "rhfrk.-hess."},
        {"Gattung": "P", "Kürzel": "BeEv", "Lateinabhängigkeit": "eng", "Name": "Berliner Evangelistar", "Raum": "omd.", "Stufe": "¹14", "geistlich|weltlich": "geistlich", "Unterraum": "omd."},
        {"Gattung": "P", "Kürzel": "MBeh", "Lateinabhängigkeit": "frei", "Name": "Evangelienbuch des Matthias von Beheim", "Raum": "omd.", "Stufe": "¹14", "geistlich|weltlich": "geistlich", "Unterraum": "omd."},
        {"Gattung": "U", "Kürzel": "UJena", "Lateinabhängigkeit": "", "Name": "Jena-Weidaer Urkunden", "Raum": "omd.", "Stufe": "¹14", "geistlich|weltlich": "weltlich", "Unterraum": "omd."},
        {"Gattung": "V", "Kürzel": "HTri", "Lateinabhängigkeit": "", "Name": "Heinrich von Freiberg: Tristan", "Raum": "omd.", "Stufe": "¹14", "geistlich|weltlich": "weltlich", "Unterraum": "omd."},
        {"Gattung": "V", "Kürzel": "LuKr", "Lateinabhängigkeit": "", "Name": "Landgraf Ludwigs Kreuzfahrt", "Raum": "omd.", "Stufe": "¹14", "geistlich|weltlich": "", "Unterraum": "omd."},
        {"Gattung": "V", "Kürzel": "Pass", "Lateinabhängigkeit": "", "Name": "Passional", "Raum": "omd.", "Stufe": "¹14", "geistlich|weltlich": "geistlich", "Unterraum": "omd."},
        {"Gattung": "P", "Kürzel": "GnaÜ", "Lateinabhängigkeit": "", "Name": "Christine Ebner: Von der Gnaden Überlast", "Raum": "ofrk.", "Stufe": "¹14", "geistlich|weltlich": "geistlich", "Unterraum": "ofrk."},
        {"Gattung": "P", "Kürzel": "SBNü", "Lateinabhängigkeit": "", "Name": "Nürnberger Satzungsbuch (I/A)", "Raum": "ofrk.", "Stufe": "¹14", "geistlich|weltlich": "weltlich", "Unterraum": "ofrk."},
        {"Gattung": "P", "Kürzel": "WüPo", "Lateinabhängigkeit": "", "Name": "Würzburger Polizeisätze", "Raum": "ofrk.", "Stufe": "¹14", "geistlich|weltlich": "weltlich", "Unterraum": "ofrk."},
        {"Gattung": "U", "Kürzel": "UNürnb", "Lateinabhängigkeit": "", "Name": "Nürnberger Urkunden", "Raum": "ofrk.", "Stufe": "¹14", "geistlich|weltlich": "weltlich", "Unterraum": "ofrk."},
        {"Gattung": "V", "Kürzel": "Lupo", "Lateinabhängigkeit": "", "Name": "Lupold Hornburg: Reden", "Raum": "ofrk.", "Stufe": "¹14", "geistlich|weltlich": "weltlich", "Unterraum": "ofrk."},
        {"Gattung": "V", "Kürzel": "Renn", "Lateinabhängigkeit": "", "Name": "Hugo von Trimberg: Renner (E)", "Raum": "ofrk.", "Stufe": "¹14", "geistlich|weltlich": "weltlich", "Unterraum": "ofrk."},
        ]

def add_to_variant_register(inpath, outpath, size, results):
    print(inpath)
    unimap = { unicodedata.name(chr(i)): chr(i) for i in range(97, 123) }
    with open(inpath, 'r', encoding = 'cp850') as infile:
        for line in infile:
            line = line.split('\t')
            try:
                variant = line[5][1:]
                pivot = line[0]
            except IndexError:
                continue
            if '[!]' in pivot or '[!!]' in pivot:
                continue
            variant = re.sub(r'^(?P<x>u|v)n\\-$', '\g<x>nd', variant)
            for old, new in (
                    ('$', 's'),
                    ('æ', 'e'),
                    ('ä', 'e'),
                    ("'", 'er'),
                    ('\\-', 'n'),
                    ('%%', 'r'),
                    ('%2', 'ur'),
                    ('%9', 'us'),
                    ('9', 'con'),
                    ('3', 'et'),
                    ('&', 'et'),
                    ('4', 'rum'),
                    ):
                variant = variant.replace(old, new)
            variant = re.sub(r'(\\.|\*.|\*\[.*?\*\]|^\*f.*)', '', variant)
            variant = ''.join( unimap.get(
                    ' '.join(unicodedata.name(char).split()[:4]), '')
                    for char in unicodedata.normalize('NFKD', variant)
                    .lower() )
            results[variant][pivot] += 1
    return size, results

def check_text_formally(inpath, outpath, size, results, inencoding = 'cp850'):
    def nemen(results, ainpfad, zaile, felfälder = [], marke = '____'):
        for zal in felfälder:
            zaile[zal] += marke
        zaile = '\t'.join(zaile)
        zaile = ainpfad + '\t' + zaile
        results.append(zaile)

    def reen(rexe):
        return '(' + '|'.join(rexe) + ')'

    lem = 0
    lem_st = 1
    art = 2
    morph = 3
    norm = 4
    beleg = 5
    stäle = 6
    status = 7
    anm = 8
    graph = 9

    # re:
    zaihen_wort = ['[-()A-Za-z~ÉÈâäæèêëîôöûü^]+?']
    zaihen_norm = ['[-()A-Za-z~ÉÈâäæèêëîôöûüé<>\[\]!]+?']
    subst = ['ON', 'PN', 'BN', 'FlN', '([mfn]|mf|mn|fn|mfn)(<\(u\)>|<u>)?']
    pronsubst = ['pron_subst']
    verb = ['anv', 'stv(1a|1b|2a|2b|2c|2d|3a|3b|4|5|5b|6|6b|7|7b)', 'stv7/swv', 'swv(-a)?', 'v']
    adj = ['adj( comp| sup)?']
    adjpart = ['adj_part( comp| sup)?']
    part = [ i + '( pG| pV)' for i in verb ]
    inf = [ i + '( i)?' for i in verb ]
    prondet = ['art', 'num', 'pron(_poss|_subst| comp)?']
    indec = ['adv/konj', 'adv(_int|_part|_rel| comp| sup)?', 'interj', 'konj', 'partikel', 'präp']
    wortarten = subst + pronsubst + verb + adj + adjpart + prondet + indec
    wortarten_stäle = wortarten + part + inf
    adjdetcas = ['(N|A|G|D|I|Ab|V)+?[SP]+?[mfn]*?w?']

    with open(inpath, 'r', encoding = inencoding) as band:
        for zaile in band:
            if zaile.startswith('@H'):
                break
        for zaile in band:
            zaile = zaile.rstrip('\n')
            if zaile == '':
                continue
            fäldzal = None
            felfälder = []
            zaile = zaile.split('\t')
            if not re.match(r'[!*]*?│.*?│$', zaile[graph]):
                felfälder.append(graph)
            if not re.match(r':(/R|/[`¨#^~\\KE]\[.*?\])*?$', zaile[anm]):
                felfälder.append(anm)
            if not re.match(r'°(\*|Ausg\.|Hs\.)?$', zaile[status]):
                felfälder.append(status)
            if not re.match(r'_(I|II|III|IV|V)·(0|1|2|3|4[ab]?|5|6)·[PUV]_[A-Za-zëäöüÄÖÜ*]+?-'
                            r'[A-Za-z0-9/]+?[rv]?[a-z*]?(,[0-9]+[a-z*]?)?'
                            r'(\[[^\]]+\])?'
                            r'(\{[^}]+\})?'
                            r'\.[0-9][0-9]@?$', zaile[stäle]):
                felfälder.append(stäle)
            if not re.match(r'\\', zaile[beleg]):
                felfälder.append(beleg)
            if '\t'.join(zaile).startswith('`[!!] @[!!]\t¨[!!]\t#[!!]\t^\t~[!!]\t'):
                if felfälder:
                    nemen(results, ainpfad, zaile, felfälder)
                continue
            if '\t'.join(zaile).startswith('`[!!] @[!!]\t¨[!!]*\t#[!!]\t^\t~[!!]\t'):
                if felfälder:
                    nemen(results, ainpfad, zaile, felfälder)
                continue
            if re.match(r'`\[!\] @\[!\]\t¨.*?\t#.*?\t\^.*?\t~.*?\t', '\t'.join(zaile)):
                if felfälder:
                    nemen(results, ainpfad, zaile, felfälder)
                continue
            if re.match(r'`([AEIOU]\\[`^:])?' + reen(zaihen_wort) + r'\* @' + reen(wortarten)
                        + r'\t¨.*?\t#.*?\t\^.*?\t~.*?\t', '\t'.join(zaile)):
                if felfälder:
                    nemen(results, ainpfad, zaile, felfälder)
                continue
            if not re.match(r'~' + reen(zaihen_norm), zaile[norm]):
                felfälder.append(norm)
            if not re.match(r'`([AEIOU]\\[`^:])?' + reen(zaihen_wort) +
                    r' @' + reen(wortarten) + r'(/' + reen(wortarten) + ')?' + '$', zaile[lem]):
                felfälder.append(lem)
            if not re.match(r'¨([AEIOU]\\[`^:])?' + reen(zaihen_wort) +
                            '\*?((/|/\.?\+)?' + reen(zaihen_wort) + '\*?(\.\+|\+)?)*?$', zaile[lem_st]):
                felfälder.append(lem_st)
            if not re.match(r'#' + reen(wortarten_stäle) +
                    r'(/' + reen(wortarten_stäle) + ')?' + '$', zaile[art]):
                felfälder.append(art)
            art_inh = zaile[art][1:].split('/')
            morph_inh = zaile[morph][1:].split('/')
            if len(art_inh) == len(morph_inh):
                art_morph = zip(art_inh, morph_inh)
            elif len(art_inh) > len(morph_inh):
                if ' ' in art_inh[-1] and not ' ' in art_inh[0]:
                    art_zusaz = ' ' + art_inh[-1].split(' ')[1]
                    for i in range(len(art_inh)):
                        if not art_inh[i].endswith(art_zusaz):
                            art_inh[i] += art_zusaz
                art_morph = zip(art_inh, morph_inh * len(art_inh))
            elif len(art_inh) < len(morph_inh):
                art_morph = zip(art_inh * len(morph_inh), morph_inh)
            morphfeler = False
            for art_inh, morph_inh in art_morph:
                if re.match(reen(indec) + '$', art_inh):
                    if morph_inh != '':
                        morphfeler = True
                elif re.match(reen(verb) + '$', art_inh):
                    if not re.match(r'[123][SP][GV](\(?[BIK]\)?)?$', morph_inh):
                        morphfeler = True
                elif re.match(reen(inf) + '$', art_inh):
                    if not re.match(r'(-|[NAGD]S)$', morph_inh):
                        morphfeler = True
                elif re.match(reen(subst + pronsubst) + '$', art_inh):
                    if not re.match(r'(N|A|G|D|I|Ab|V)+?[SP]+?$', morph_inh):
                        morphfeler = True
                elif re.match(reen(adj + adjpart + part) + '$', art_inh):
                    if not re.match(r'(\?|-|0"?|[0?]?' + reen(adjdetcas) + '[°!"%&=]*?)$', morph_inh):
                        morphfeler = True
                    if not (' ' in zaile[beleg] or morph_inh.startswith('?') or morph_inh == '0'):
                        felfälder.append(beleg)
                elif re.match(reen(prondet) + '$', art_inh):
                    if zaile[lem_st][1:] in ['ich', 'dû', 'wir', 'ir']:
                        if not re.match(r'[NAGD]+?[SP]+?[?()®¿]*?$', morph_inh):
                            morphfeler = True
                    elif zaile[lem_st][1:] == 'ër':
                        if not re.match(reen(adjdetcas) + r'[?()®¿+#]*?$', morph_inh):
                            morphfeler = True
                    elif zaile[lem_st][1:] == 'sich':
                        if not re.match(r'[DA]+?$', morph_inh):
                            morphfeler = True
                    elif zaile[lem_st][1:] == 'èin-ander':
                        if not re.match(r'(0|0?' + reen(adjdetcas) + ')$', morph_inh):
                            morphfeler = True
                    elif zaile[lem_st][1:] == 'dër' and art_inh == 'pron':
                        if not re.match(reen(adjdetcas) + r'\+?$', morph_inh):
                            morphfeler = True
                        #if not (' ' in zaile[beleg] or morph_inh.startswith('?') or morph_inh.startswith('0')):
                        #    morphfeler = True
                    else:
                        if not re.match(r'(\?|-|0"?|[0?]?' + reen(adjdetcas) + '[°!"%&=]*?)$', morph_inh):
                            morphfeler = True
                        #if not (' ' in zaile[beleg] or morph_inh.startswith('?') or morph_inh.startswith('0')):
                        #    morphfeler = True
            if morphfeler == True:
                felfälder.append(morph)
            if not len(zaile) > 8:
                felfälder = list(range(len(zaile)))
            if felfälder:
                nemen(results, ainpfad, zaile, felfälder)
    return size, results

def collect_diffs(
        inpaths: 'Sequence[str]',
        outpaths: 'Sequence[str]',
        size: int,
        results: 'MutableSequence[Tuple[str, str]]',
        ) -> 'Tuple[int, MutableSequence[Tuple[str, str]]]':
    for path in inpaths:
        print(path)
    print()
    with open(inpaths[0], 'r', encoding = 'cp437') as infile:
        lines0 = infile.read().split('\n@H')[-1].strip().splitlines()
    with open(inpaths[1], 'r', encoding = 'cp437') as infile:
        lines1 = infile.read().split('\n@H')[-1].strip().splitlines()
    diffs = compare.compare_lines(
            lines0,
            lines1,
            level_line,
            level_line,
            level_line,
            level_line,
            )
    if diffs:
        results.append('\n▀▀▀▀ ○ {} ● {}'.format(inpaths[0], inpaths[1]))
        results.append(diffs)
    return size, results

def convert_to_grammar_lem(
        term: str,
        del_paren: bool = True,
        del_hyphen: bool = True,
        ) -> str:
    for old, new in (
            ('uo', 'ue'),
            ('üè', 'üe'),
            ('iè', 'ie'),
            ('æ', 'ǟ'),
            ('œ', 'ȫ'),
            ('oè', 'ȫ'),
            ('è', 'ė'),
            ('ë', 'ę'),
            ('â', 'ā'),
            ('ê', 'ē'),
            ('î', 'ī'),
            ('ô', 'ō'),
            ('û', 'ū'),
            ('tz', 'ƶƶ'),
            ('z', 'ȥ'), # muss nachgebessert werden, vielleicht auch ƶ.
            ('iu', 'ǖ'), # muss nachgebessert werden, vielleicht auch iu.
            ):
        term = term.replace(old, new)
    if del_paren:
        term = term.replace('(', '').replace(')', '')
    if del_hyphen:
        term = term.replace('-', '')
    return term

def delete_punctuation(term):
    term = term.replace('(=)', '').replace('=', '')
    term = re.sub(
            '''(?<!\\\\)%?\\[?\\[?<?\\(?([,;.:!"/?]|\\*[12])'''
            '''([,;.:!"/?*0123456789'<>()\\\\%]|\\[|\\])*(?P<x>\\|?)$''',
            '\g<x>', term)
    return term

def disambiguate_segmentation(line: str) -> str:
    line = line.split('\t')
    if len(line) > field_nth and line[field_nth].count(' ') == 2:
        instance = line[field_nth].split(' ')
        if ( not set(instance[1]) & set(CHARS_NORMAL_CONS)
             and set(instance[2]) & set(CHARS_NORMAL_CONS) ):
            line[field_nth] = instance[0] + ' ' + instance[1] + instance[2]
    return '\t'.join(line)

def even_out(ms_form, del_whitespace = True, del_all_diacritics = True):
    '''
    :param del_all_diacritics: If true, delete also consonant and vowel letters
        which are superscribed or written in ligature, but not the Nasalstrich
        nor the r-abbreviation.
    '''
    if del_whitespace:
        ms_form = sub(r'\s', '', ms_form)
    ms_form = compare.even_out(
            ms_form,
            erase_re = re.compile(r'\\[^-]') if del_all_diacritics else None,
            replaces = (
                ('æ', 'a' if del_all_diacritics else 'ae'),
                ('œ', 'o' if del_all_diacritics else 'oe'),
                ('$', 's'),
                ('\\-', 'N'),
                ("'", 'R'),
                ('\\', ''),
                ('/', ''),
                ),
            )
    ms_form = re.sub('[0-9]', '', ms_form)
    return ms_form

def even_out_a_ja_lemma(lemma: str) -> str:
    lemma = lemma.split('-')[-1]
    lemma = lemma.rstrip('(e)')
    lemma = re.sub(r'(?P<a>.)(?P=a)+', '\g<a>', lemma)
    for old, new in (
            ('üè', 'uo'),
            ('öu', 'ou'),
            ('oè', 'ô'),
            ('iu', 'û'),
            ('iè', 'û'), # because `iu` may correspond to `iè`, too.
            ('ö', 'o'),
            ('ü', 'u'),
            ('æ', 'â'),
            ('ä', 'a'),
            ('(?<!i)è(?!i)', 'a'),
            ('i', 'ë'),
            ):
        lemma = re.sub(old, new, lemma)
    for old, new in (
            ('pf', 'f'),
            ('tz', 'z'),
            ('ck', 'ch'),
            ('ch', 'h'),
            ('pp', 'p'),
            ('tt', 't'),
            ('ck', 'k'),
            ('p', 'b'),
            ('t', 'd'),
            ('k', 'g'),
            ):
        lemma = re.sub(r'{}$'.format(old), new, lemma)
    return lemma

def filter_lines(infile, include_if_all, exclude_if_any) -> str:
    if (include_if_all or exclude_if_any):
        text = infile.readlines()
        text = [ line for line in text if all(
                pattern.search(line) for pattern in include_if_all ) ]
        text = [ line for line in text if not any(
                pattern.search(line) for pattern in exclude_if_any ) ]
        return ''.join(text)
    else:
        return infile.read()

def get_reference(
        record: str,
        periods_mapping: 'Sequence[Tuple[str, str]]' = (
            ('I',   '1050-1150'),
            ('II',  '1150-1200'),
            ('III', '1200-1250'),
            ('IV',  '1250-1300'),
            ('V',   '1300-1350'),
            ),
        regions_mapping: 'Sequence[Tuple[str, str]]' = (
            ('0',  'obd.'),
            ('1',  'bair.'),
            ('2',  'alem.-bair.'),
            ('3',  'alem.'),
            ('4',  'wmd.'),
            ('4a', 'mfrk.'),
            ('4b', 'rhfrk.-hess.'),
            ('5',  'hess.-thür.-omd.'),
            ('6',  'ofrk.'),
            ),
        differ: 'Dict[str, str]' = {
            'TrPs':  'rhfrk.-hess.',
            'VatG':  'rhfrk.-hess.',
            'ArnM':  'rhfrk.-hess.',
            'RBib':  'mfrk.',
            'RhMl':  'mfrk.',
            'RhTun': 'mfrk.',
            },
        ) -> 'Tuple[str, str, str, str, str, str, str]':
    '''
    Extract a reference from :param:`record`, which is a corpus record or
    a part of a corpus record and contains the reference of this record.

    Split the reference in the following items:

    1. period,
    2. region,
    3. shorttitle,
    4. genre,
    5. line in manuscript,
    6. line in edition,
    7. position of the form in the line.

    Change the period and the region to more self-explanatory values.

    Change the shorttitle in order to match the conventions for the
    new MHG grammar.

    Return the items.

    :param differ: maps shorttitles to regions. If the shorttitle of the
        reference is one of the keys, the region of the reference is
        replaced by the associated region. Can be cancelled by calling
        the function with the parameter ``differ = {}``.
    '''
    ref = record.split('\t_')[1] if '\t_' in record else record.lstrip('_ \t')
    ref = ref.split('\t')[0]
    period, region, genre, shorttitle, line, position = \
            s(r'(.*?)·(.*?)·(.*?)_(.*?)-(.*?)\.(.*)', ref).groups()
    period = dict(periods_mapping)[period]
    region = dict(regions_mapping)[region]
    if genre == 'U' or shorttitle == 'Lilie':
        shorttitle = genre + shorttitle
        additions = {'1250-1300': '1', '1300-1350': '2'}
        if shorttitle in ('UAugsb', 'UFreib', 'UKöln') and period in additions:
            shorttitle += additions[period]
    if shorttitle in ('BaGBH', 'EzMem'):
        shorttitle = {
                'G': 'BaGB',
                'H': 'HuH',
                'E': 'Ezzo',
                'M': 'Mem'}[line[0]]
        line = line[1:]
    if shorttitle in differ:
        region = differ[shorttitle]
    # If ``genre == 'V'``, the manuscript line may be given in braces and the
    # edition line is given without braces. Else, it is the other way round.
    line = line.rstrip('}').split('{') + ['']
    if genre == 'V':
        edition_line = line[0]
        ms_line = line[1]
    else:
        edition_line = line[1]
        ms_line = line[0]
    return period, region, shorttitle, genre, ms_line, edition_line, position

def has_umlaut(grapho: str) -> bool:
    '''`iu` cannot be tested.'''
    grapho = re.split(r'(?<!\\)-', grapho, 1)[-1]
    return bool(
            s(r"│[^=]*['eéèêiíìîæ][^=]*\=[^│]*(è(?!i)|ä|ö|ü|æ|oè)[^│]*│",
            grapho))

def ingest(
        filepath: str = file_io.join(__file__, '../../mhd/Vergleich.txt'),
        folderpath: str = file_io.join(__file__, '../../mhd/P'),
        oteil_re: 'Pattern[str]' = re.compile(r'\t_[^\t]+'),
        ) -> None:
    def process(inpath, outpath, size, results):
        try:
            with open(inpath, 'r', encoding = 'cp437') as file:
                text = file.read()
        except UnicodeDecodeError:
            print('UnicodeDecodeError:', inpath)
            return size, results
        is_changed = False
        lines = text.strip().splitlines()
        for num, line in enumerate(lines):
            match = oteil_re.search(line)
            if match:
                oteil = match.group()
                new_line = inserenda.pop(oteil, None)
                if new_line is not None and new_line != line:
                    is_changed = True
                    results.append(line)
                    results.append(new_line)
                    results.append('')
                    lines[num] = new_line
        if is_changed:
            new_text = '\n'.join(lines)
            size += file_io.write(outpath, text, outencoding = 'cp437')
        return size, results
    with open(filepath, 'r', encoding = 'utf-8') as file:
        text = file.read()
    lines = [ line.strip() for line in text.strip().splitlines() ]
    lines.reverse()
    inserenda = {}
    is_next = False
    for line in lines:
        if line.startswith('●'):
            is_next = True
        elif line.startswith('○') and len(line) > 1 and is_next == True:
            is_next = False
            try:
                oteil = oteil_re.search(line).group()
            except Exception as e:
                print(line)
                raise e
            inserenda[oteil] = line.lstrip('○\t')
    shared_outpath, size, results = file_io.run_over_files(
            shared_inpath = folderpath,
            include_in_inpaths_re = r'',
            exclude_from_inpaths_re = r'',
            ext = None,
            size = 0,
            results = deque(),
            function = process,
            )
    file_io.tell(shared_outpath, size, results)
    if results:
        outpath = file_io.get_new_path(shared_outpath, '.txt')
        text = '\n'.join(results)
        size = file_io.write(outpath, text, 'utf-8')
        file_io.tell(outpath, size)
        if size > 0:
            webbrowser.open(outpath)
    if inserenda:
        for line in inserenda.values():
            print(line)
    else:
        print('\nNullum inserendum superest.')

def level_line(line: str, other_line: str) -> str:
    line = line.split('\t')[:6]
    if len(line) > 4:
        for o, n in (
                ('é', 'e'),
                ('<<', '<'),
                ('>>', '>'),
                ):
            line[4] = line[4].replace(o, n)
    return '\t'.join(line)

def make_vertical_text(
        inpath, outpath, size, results,
        inencoding = 'cp850', outencoding = 'cp850'):
    print(inpath)
    with open(inpath, 'r', encoding = inencoding) as infile:
        a = infile.read().strip().replace('\n', '\n ')
    a = sub('\\|(?P<x>\\S)', '| |\\g<x>', a)
    a = sub('(?P<x>[^*]){(?P<y>[^}]*?)}', '\\g<x>\\f {\\g<y>} ', a)
    while re.search(' {[^}]*? [^}]*?}', a):
        a = sub(' {(?P<x>[^}]*?) (?P<y>[^}]*?)}', ' {\\g<x>\\g<y>}', a)
    while re.search('  ', a):
        a = a.replace('  ', ' ')
    a = sub('\\|{(?P<x>[^}]*?)} ', '{\\g<x>} |', a)
    a = a.replace('||', '|')
    a = a.split(' ')
    kopf = []
    anf = 0
    while not '@H' in a[anf]:
        kopf += [a[anf]]
        anf += 1
    while not '-' in a[anf]:
        kopf += [a[anf]]
        anf += 1
    kopf = ' '.join(kopf).replace('\n ', '\n').replace('| |', '|').replace('\f', '')
    a = a[anf:]
    kl = 0 # Klammerendestelle
    zs = '' # Zeilenschluss
    lkl = '' # Lateinklammer
    rkl = '' # Rötungsklammer
    skl = '' # Schweifklammer
    aus = '' # Aussternung
    ordn = '' # Ordnungsteil
    kekl = '' # K- oder E-Klammer
    for i in range(len(a) - 1, - 1, - 1):
        if a[i] == '' or re.match('^\\s*$', a[i]):
            if '\n' in a[i]:
                try:
                    a[i - 1] += '\n'
                except IndexError:
                    pass
            del a[i]
        elif '\t' in a[i] or a[i][0] == '{' or \
             '+L' in a[i] or '@L' in a[i] or \
             '+R' in a[i] or '@R' in a[i]:
            if '\n' in a[i]:
                try:
                    a[i - 1] += '\n'
                except IndexError:
                    pass
        elif kl == 0 and ('@K' in a[i] or '@E' in a[i]):
            kl = 1
        elif len(a[i]) > 1 and (a[i][0:2] == '+K' or a[i][0:2] == '+E'):
            kl = 0
            ii = 1
            while not '@K' in a[i + ii] and not '@E' in a[i + ii]:
                ii += 1
            kekl = ' '.join(a[i:i + ii + 1]).replace(' @K', '').replace(' @E', '')
            del a[i:i + ii + 1]
            a.insert(i, kekl)
            i -= 1
        else:
            try:
                ii = 1
                while 1:
                    while '\t' in a[i + ii] or a[i + ii][0] == '{' or \
                          '+L' in a[i + ii] or '@L' in a[i + ii] or \
                          '+R' in a[i + ii] or '@R' in a[i + ii]:
                        ii += 1
                    if a[i + ii][0:2] == '+K' or a[i + ii][0:2] == '+E':
                        if not '\v' in a[i]:
                            a[i] += '\v'
                        while a[i + ii][0:2] == '+K' or a[i + ii][0:2] == '+E':
                            a[i] += '/' + a[i + ii][1] + '[' + a[i + ii][3:] + ']'
                            del a[i + ii]
                    else:
                        break
            except IndexError:
                pass
            if kl == 0 and (search('=[>\\]\\)]*\\n?$', a[i]) \
                          or re.search('#\\n?$', a[i]) \
                          or re.search('\\f\\n?$', a[i])):
                ii = 1
                try:
                    while '\t' in a[i + ii] or a[i + ii][0] == '{' or \
                          '+L' in a[i + ii] or '@L' in a[i + ii] or \
                          '+R' in a[i + ii] or '@R' in a[i + ii] or \
                          '+E' in a[i + ii] or '+K' in a[i + ii]:
                        ii += 1
                    a[i] += a[i + ii]
                    del a[i + ii]
                except IndexError:
                    pass
                a[i] = a[i].replace('\f', '')
            elif kl == 0 and '=|' in a[i]:
                ii = 1
                try:
                    while '\t' in a[i + ii] or a[i + ii][0] == '{' or \
                          '+L' in a[i + ii] or '@L' in a[i + ii] or \
                          '+R' in a[i + ii] or '@R' in a[i + ii] or \
                          '+E' in a[i + ii] or '+K' in a[i + ii]:
                        ii += 1
                    a[i + ii] = '|' + a[i + ii]
                except IndexError:
                    pass
    skl = ''
    for i in range(len(a)):
        if '\t' in a[i]:
            ordn = sub('\\s', '', a[i])
            a[i] = ''
            if ordn[-1] == '*':
                aus = '*'
                ordn = ordn[:-1]
            else:
                aus = ''
        elif not a[i] == '' and a[i][0] == '{':
            skl = sub('\\s', '', a[i])
            a[i] = ''
        elif a[i][0:2] == '+L':
            lkl = '*f'
            a[i] = ''
        elif a[i][0:2] == '@L':
            lkl = ''
            a[i] = ''
        elif a[i][0:2] == '+R':
            rkl = '/R'
            a[i] = ''
        elif a[i][0:2] == '@R':
            rkl = ''
            a[i] = ''
        elif not a[i] == '':
            if not '\n' in a[i]:
                zs = ''
            else:
                zs = '@'
                a[i] = a[i].replace('\n', '')
            if '\v' in a[i]:
                kekl = a[i].split('\v')[1]
                a[i] = a[i].split('\v')[0]
            else:
                kekl = ''
            a[i] = a[i].replace('a_e', 'æ')
            a[i] = '¨\t\t\t\t\\' + lkl + a[i] + '\t_' \
                   + ordn + skl + '.01' + zs + '\t' + aus + '\t' + kekl + rkl + '\t.\n'
            a[i] = a[i].replace('\\*f|', '\\|*f').replace('*f*f', '*f').replace('||', '|')
    a = kopf + ''.join(a)
    size += file_io.write(outpath, a, outencoding)
    return size, results

def make_continuous_text(
        inpath, outpath, size, results,
        inencoding = 'cp850', outencoding = 'cp850'):
    print(inpath)
    with open(inpath, 'r', encoding = inencoding) as infile:
        a = infile.read().strip().split('\n')
    an = 0
    aus = len(a)
    while not '@H' in a[an]:
        an += 1
    while not '¨' in a[an]:
        an += 1
    for i in range(aus - 1, an - 1, - 1):
        a[i] = a[i].split('\t')

        if a[i][6][-2:] == '01':
            ot = '\n' + a[i][6][1:-3] + '\t '
        else:
            ot = ''
        a[i] = ot + a[i][5][1:].replace(' ', '')
    a = ' '.join(a).replace(' \n', '\n').replace('| |', '|')
    size += file_io.write(outpath, a, outencoding)
    return size, results

def parse_forms(
        inpath, outpath, size, results: 'MutableSet[str]',
        inencoding = 'cp850', outencoding = 'cp850'
        ) -> 'Tuple[int, MutableSet[str]]':
    def glidern_stam_ändung(ur, urwort, cons_ext_flex, cons_in_flex):
        def forwascen(a):
            a = re.sub(r'\*\[.*?\*\]', '', a)
            for i in [
                    ["'", "r"],
                    ['%%', 'r'],
                    ['%2', 'ur'],
                    ['%9', 'us'],
                    ['3', 'et'],
                    ['4', 'rum'],
                    ['9', 'con'],
                    ['$', 's'],
                    ['\\-', 'n'],
                    ]:
                a = a.replace(i[0], i[1])
            a = re.sub(r'\\.', '', a)
            return a
        def scwäle_mal_minus_ains(i):
            return [i[0], i[1] * -1]
        def wascen(a):
            a = unicodedata.normalize('NFD', a)
            a = a.lower()
            a = ''.join([ i for i in a if i in LIT_NORM or i in '-_' ])
            for alt, noi in [
                    ['a_e', 'æ'],
                    ]:
                a = a.replace(alt, noi)
            for alt, noi in [
                    ['_', ''],
                    ['th', 't'],
                    ['t-h', 't-'],
                    ['ph', 'p'],
                    ['pf', 'p'],
                    ['ph', 'p'],
                    ['pf', 'p'],
                    ['b', 'p'],
                    ['d', 't'],
                    ['g', 'k'],
                    ['q', 'ku'],
                    ['h', 'k'],
                    ['z', 's'],
                    ['cs', 's'],
                    ['sc', 's'],
                    ['c', 'k'],
                    ['m', 'n'],
                    ['f', 'u'],
                    ['v', 'u'],
                    ['w', 'u'],
                    ['o', 'u'],
                    ['y', 'i'],
                    ['j', 'i'],
                    ['ai', 'i'],
                    ['ei', 'i'],
                    ['æ', 'a'],
                    ['e', 'a'],
                    ['-', ''],
                    ]:
                a = a.replace(alt, noi)
            merling = re.compile('(?P<a>.)(?P=a)')
            while re.search(merling, a):
                a = re.sub(merling, '\g<a>', a)
            return a
        baizaihen = re.search(get_punctuation_re(), ur)
        if baizaihen:
            urbeleg = ur[:baizaihen.start()]
            urbaizaihen = ur[baizaihen.start():]
        else:
            urbeleg = ur
            urbaizaihen = ''
        wort = urwort.lower()
        wort = re.sub(r'\([^)]*?\)', '', wort)
        wort = re.sub(r'e$', '', wort)
        wort = wascen(wort)
        aufwände = []
        scwäle = len(urbeleg)
        wa = compare.get_levenstein
        while scwäle > - 1:
            ändung = urbeleg[scwäle:]
            if ist_ändung(ändung, cons_ext_flex, cons_in_flex):
                stam = urbeleg[:scwäle].lower()
                stam = wascen(forwascen(stam))
                wandelaufwand = wa(wort, stam, tauscen = False) / max([len(wort), len(stam)])
                aufwände.append([wandelaufwand, scwäle])
            else:
                break
            scwäle -= 1
        aufwände.sort(key = scwäle_mal_minus_ains)
        scwäle = aufwände[0][1]
        urstam = urbeleg[:scwäle]
        urändung = urbeleg[scwäle:]
        unterhang = re.search(r'([*([{<\\%]|\\&)+$', urstam)
        if unterhang:
            scwäle = unterhang.start()
            unterhang = urstam[scwäle:]
            urstam = urstam[:scwäle]
            urändung = unterhang + urändung
        if urstam.endswith('e') and all([
                urwort.lower()[-1] not in 'êè',
                re.search('[' + VOC_NORM + '].*?[' + LIT_NORM + '].*?' + 'e', urstam),
                ]):
            urstam = urstam[:-1]
            urändung = 'e' + urändung
        for urstam_alt, urändung_alt, urstam_noi, urändung_noi in [
                ['choffi', '$', 'choff', 'i$'],
                ['geloy', 'u\\oe\\-', 'geloyu\\o', 'e\\-'],
                ]:
            if urstam == urstam_alt:
                urstam = urstam_noi
            if urändung == urändung_alt:
                urändung = urändung_noi
        return urstam, urändung, urbaizaihen
    def ist_adj(a):
        if type(a) == list:
            a = '\t'.join(a)
        if re.search(r'\t#(adj|[^\t]+? p)', a):
            return True
        return False
    def ist_ändung(ändung, cons_ext_flex, cons_in_flex):
        ändung = ändung.lower()
        if (
            not re.search('[' + cons_ext_flex + ']', ändung)
            and not 'w' in ändung and not 'uu' in ändung
            and not re.search('[' + cons_in_flex.replace('r', '') + '].*?[' + VOC_NORM +
                          '].*?[' + cons_in_flex + ']', ändung)
    ##        and not re.search('(?P<a>[aiou]' + cons_in_flex + ']).*?(?P=a)', ändung)
    ##        and not re.search('[uv].*?[aeouvæ]', ändung)
            ):
            return True
        return False
    def ist_det(a):
        if type(a) == list:
            a = '\t'.join(a)
        if re.search('`(?!dû |ër |ich |ir |sich |wir |.*?wëder ).*?\\t'
                     '#(?:pron|art)(?!_subst).*?\\t', a):
            return True
        return False
    def ist_entlent_name_unbestimt(a):
        if isinstance(a, list):
            a = '\t'.join(a)
        if (re.search('\\t¨.*?\\*\\t#', a) or
            re.search('`[ABCDEFGHIJKLMNOPQRSTUVWXYZÄÖÜÁÉÍÓÚÅÆ].*?\\t¨', a) or
            re.search('`\\[.*? @', a)):
            return True
        return False
    def ist_interrogatiuum_indirectum(wort):
        if re.search('`sw.*? @pron.*?\\t¨', wort):
            return True
        return False
    def ist_particulum_verbi(wort):
        if re.search('\\t\\\\.*?\\*1.*?\\t_', wort):
            return True
        return False
    def ist_relatiuum(wort):
        if re.search('\\t\\^.*?\\+.*?\\t~', wort):
            return True
        return False
    def ist_stam(stam, wort, cons_ext_flex):
        stam = ainebnen(stam)
        wort = ainebnen(wort)
        stamände = wort.rstrip('e')[-1]
        if stamände in cons_ext_flex:
            stamände = '[' + cons_ext_flex + ']'
        elif stamände == 'r':
            stamände = "[r']"
        elif stamände == 'w':
            stamände = '(?:w|uu|vv)'
        elif stamände in 'm':
            stamände = '(?:m|\\\\-)'
        elif stamände in 'n':
            stamände = '(?:n|\\\\-)'
        stamände += '$'
        if (re.search(stamände, stam) and
            (re.search(wort, stam) or
             re.search('[' + VOC_NORM + ']', stam))):
            return True
        return False
    def ist_subiunctio(wort):
        if any([ re.search('¨' + i + '\\t#konj', wort) for i in SUBIUNCTIONES ]):
            return True
        return False
    def ist_substantiuum(wort):
        if re.search('\\t#[mfn][^u]', wort):
            return True
        return False
    def ist_uerbum(wort):
        if re.search('\\t#(?:s[tw]v|anv)', wort):
            return True
        return False
    def ist_uerbum_finitum(wort):
        if re.search('\\t\\^[123]+[SP]', wort):
            return True
        return False
    def ist_uerbum_infinitum_praedicatiuum(wort):
        if re.search('\\t#(?:s[tw]v|anv).*?\\t\\^-', wort):
            return True
        return False
    print(inpath)
    ist_wortart = ist_substantiuum
    cons_ext_flex = 'bcdfghklpqtx'
    cons_in_flex = 'mnrsz'
    a = []
    with open(inpath, 'r', encoding = inencoding) as band:
        for zaile in band:
            a.append(zaile)
            if '@H' in zaile:
                break
        for zaile in band:
            if ('¨' in zaile
                    and ist_wortart(zaile)
                    and not ist_entlent_name_unbestimt(zaile)):
                zaile = zaile.split('\t')
                beleg = zaile[5][1:]
                wort = zaile[1][1:]
                stam, ändung, baizaihen = glidern_stam_ändung(beleg, wort,
                        cons_ext_flex, cons_in_flex)
                results.add(
                        wort + '\t' + stam.replace('"', '') + '\t' + ändung
                        )
                zaile[5] = '\\' + stam + '\t' + ändung + '\t§' + baizaihen
                zaile = '\t'.join(zaile)
                a.append(zaile)
            else:
                a.append(zaile)
        size += file_io.write(outpath, ''.join(a), outencoding)
    return size, results

def put_any_sentence_in_one_line(text):
    text = text.replace('\r', '')
    text = re.sub(r'@H\s*', '@H\r', text)
    text = re.sub(r'(?P<a>\\.*?(,,|\([;.:!?]\)).*?\t_.*)\n', '\g<a>\r', text)
    text = text.replace('\n', '\t').replace('\r', '\n')
    return text

def search(
        inpath: str,
        outpath: str,
        size: int,
        results: 'Deque[Tuple[Tuple[str, str, str, str, str, str, str], str]]',
        inencoding: str = 'cp850',
        searches: 'Tuple[Tuple[str, Pattern[str]], ...]' =
            tuple( (label, re.compile(pattern)) for label, pattern in [
                ('', r"(?m)^`(.*?) @(.*?)\t¨.*?\t#(.*?)\t\^(.*?)\t.*?\t\\(.*?)\t(_.*?)\t.*"),
                ] ),
        sections_before: int = 0,
        sections_after: int = 0,
        include_if_all: 'Tuple[Pattern[str], ...]' = tuple(map(re.compile, (
##            r'\t#(?:adj|[^ \t]+ p[VG])',       # only adj, part
##            r'\t#adj(?!_part)',                # only adj
##            r'\t_.*?·[PU]_',                   # only P and U genre
##            r'\t_.*?·V_',                      # only V genre
##            r'\t#adv(?!/|_)',                  # only adv
            ))),
        exclude_if_any: 'Tuple[Pattern[str], ...]' = tuple(map(re.compile, (
            r"\t\\[^\t]*?(?: [^\t]*?(?:\\-|(?<!\\)[a-zA-ZëäöüÄÖÜ'$æÆ])[^\t[]*\]|\][^-\w'$]*\t_)", # w/o square brackets in ending
            r'\t#.N',                          # w/o names
            r'\*\t#',                          # w/o foreign-language forms
            r'(?m)^`\[!',                      # w/o not annotated
            r'\t\^.*?/.*?\t~',                 # w/o ambiguous forms
            r'\t\^\?',                         # w/o abbreviations
            r'\t\\[^\t]+\|[^\t]*\t_',          # w/o | at the end
##            r'\t\\.*? .*?(?<!\\)\\-.*?\t_',    # w/o nasal bar
##            r"\t\\.*? .*?(?<!\\)'.*?\t_",      # w/o r-abbreviation
##            r'\t\\.*? .*? .*?\t_',             # w/o second space
##            r'\t(¨.*?e\)?\t#|\t#[^ \t]+ pG)',  # w/o stems in -e or (-e)
##            r'\t¨.*?w\)?\t#',                  # w/o stems in -w or (-w)
##            r'`sancte @adj',                   # w/o `sancte
##            r'`vrôn\(e\) @adj',                # w/o `vrôn(e)
##            r' comp\t\^',                      # w/o comparatives
##            r' sup\t\^',                       # w/o superlatives
##            r'`s[æè]lig @adj',                 # w/o `sælig, `sèlig
##            r'`lëbentig @adj',                 # w/o `lëbentig
##            r'\t\^.*?".*?\t~',                 # w/o " cases
##            r'\t\^.*?%.*?\t~',                 # w/o % cases
##            r'\t\^.*?&.*?\t~',                 # w/o & cases
##            r'\t\^.*?\!.*?\t~',                # w/o ! cases
##            r'\t\^.*?°.*?\t~',                 # w/o ° cases
##            r'\t\^0',                          # w/o 0 cases
##            r'\t\^.*?\=.*?\t~',                # w/o = cases
##            r'\t\^-',                          # w/o - cases
##            r'\t_[^_]+?_(Wind|TrPs|ZwBR)-',    # w/o Wind, TrPs, ZwBR
            ))),
        ) -> '''Tuple[
                int,
                Deque[Tuple[Tuple[str, str, str, str, str, str, str], str]]]''':
    print(inpath)
    with open(inpath, 'r', encoding = inencoding) as infile:
        text = filter_lines(infile, include_if_all, exclude_if_any)
    for label, pattern in searches:
        for match in pattern.finditer(text):

            lem = match.group(1)
            urpos = match.group(2)
            pos = match.group(3)
            mor = match.group(4)
            tok = match.group(5)
            ref = match.group(6)
            reference = get_reference(match.group())

            key = (label,) + reference
            passage = get_passage(text, match, sections_before, sections_after)
            results.append((key, passage))
    return size, results

# A modified copy for throwaway searches:
def search(
        inpath: str,
        outpath: str,
        size: int,
        results: 'Deque[Tuple[Tuple[str, str, str, str, str, str, str], str]]',
        inencoding: str = 'cp850',
        searches: 'Tuple[Tuple[str, Pattern[str]], ...]' =
            tuple( (label, re.compile(pattern)) for label, pattern in [
                ('', r"""(?m)
^`dër @art.*?\t#.*?\t\^[NA]+P(?!n).*?\t~.*?\t\\.*?\t_.*?\t.*
(?:^`.*? @.*?\t#.*?\t\^(?:\t|[0G].*?\t)~.*?\t\\.*?\t_.*?\t.*
)*?^`(.*?) @(adj)\t¨.*?\t#(.*?)\t\^([NA]+P(?!n).*?)\t~.*?\t\\(.*?)\t(_.*?)\t.*
"""),
                ] ),
        sections_before: int = 0,
        sections_after: int = 0,
        include_if_all: 'Tuple[Pattern[str], ...]' = tuple(map(re.compile, (
            ))),
        exclude_if_any: 'Tuple[Pattern[str], ...]' = tuple(map(re.compile, (
            ))),
        ) -> '''Tuple[
                int,
                Deque[Tuple[Tuple[str, str, str, str, str, str, str], str]]]''':
    print(inpath)
    with open(inpath, 'r', encoding = inencoding) as infile:
        text = filter_lines(infile, include_if_all, exclude_if_any)
    for label, pattern in searches:
        for match in pattern.finditer(text):

            lem = match.group(1)
            urpos = match.group(2)
            pos = match.group(3)
            mor = match.group(4)
            tok = match.group(5)
            ref = match.group(6)
            reference = get_reference(match.group())

            if not (not s(r'[^\t]+\|[^\t]*', tok)): continue
            if not (not s(r"^[^\t]*?(?: [^\t]*?(?:\\-|(?<!\\)[a-zA-ZëäöüÄÖÜ'$æÆ])[^\t[]*\]|\][^-\w'$]*$)", tok)): continue
            if not ('%' not in mor): continue
            if not ('=' not in mor): continue
            if not ('"' not in mor): continue
            if not ('&' not in mor): continue
            if not ('°' not in mor): continue
            if not (not s(r'[ew]\)?$', lem)): continue
            if not ('comp' not in pos): continue
            if not ('sup' not in pos): continue
            if not (('w' in mor and '!' not in mor) or ('w' not in mor and '!' in mor)): continue

            if '!' in mor:
                label = 0
            else:
                label = 1

            key = (label,) + reference
            passage = get_passage(text, match, sections_before, sections_after)
            results.append((key, passage))
    return size, results

def search(
        inpath: str,
        outpath: str,
        size: int,
        results: 'Deque[Tuple[Tuple[str, str, str, str, str, str, str], str]]',
        inencoding: str = 'cp850',
        searches: 'Tuple[Tuple[str, Pattern[str]], ...]' =
            tuple( (label, re.compile(pattern)) for label, pattern in [
                ('', r"""(?m)^`(.*?) @(.*?)\t¨.*?\t#(.*?)\t\^(.*?)\t~.*?\t\\(.*?)\t(_.*?)\t.*"""),
                ] ),
        sections_before: int = 0,
        sections_after: int = 0,
        include_if_all: 'Tuple[Pattern[str], ...]' = tuple(map(re.compile, (
            r'\t#(?:adj|[^ \t]+ p[VG])',       # only adj, part
##            r'\t#adj(?!_part)',                # only adj
##            r'\t_.*?·[PU]_',                   # only P and U genre
##            r'\t_.*?·V_',                      # only V genre
##            r'\t#adv(?!/|_)',                  # only adv
            ))),
        exclude_if_any: 'Tuple[Pattern[str], ...]' = tuple(map(re.compile, (
            r"\t\\[^\t]*?(?: [^\t]*?(?:\\-|(?<!\\)[a-zA-ZëäöüÄÖÜ'$æÆ])[^\t[]*\]|\][^-\w'$]*\t_)", # w/o square brackets in ending
            r'\t#.N',                          # w/o names
            r'\*\t#',                          # w/o foreign-language forms
            r'(?m)^`\[!',                      # w/o not annotated
            r'\t\^.*?/.*?\t~',                 # w/o ambiguous forms
            r'\t\^\?',                         # w/o abbreviations
            r'\t\\[^\t]+\|[^\t]*\t_',          # w/o | at the end
##            r'\t\\.*? .*?(?<!\\)\\-.*?\t_',    # w/o nasal bar
##            r"\t\\.*? .*?(?<!\\)'.*?\t_",      # w/o r-abbreviation
##            r'\t\\.*? .*? .*?\t_',             # w/o second space
##            r'\t(¨.*?e\)?\t#|\t#[^ \t]+ pG)',  # w/o stems in -e or (-e)
##            r'\t¨.*?w\)?\t#',                  # w/o stems in -w or (-w)
            r'`sancte @adj',                   # w/o `sancte
            r'`vrôn\(e\) @adj',                # w/o `vrôn(e)
##            r' comp\t\^',                      # w/o comparatives
##            r' sup\t\^',                       # w/o superlatives
##            r'`s[æè]lig @adj',                 # w/o `sælig, `sèlig
##            r'`lëbentig @adj',                 # w/o `lëbentig
##            r'\t\^.*?".*?\t~',                 # w/o " cases
##            r'\t\^.*?%.*?\t~',                 # w/o % cases
##            r'\t\^.*?&.*?\t~',                 # w/o & cases
##            r'\t\^.*?\!.*?\t~',                # w/o ! cases
            r'\t\^.*?°.*?\t~',                 # w/o ° cases
##            r'\t\^0',                          # w/o 0 cases
            r'\t\^.*?\=.*?\t~',                # w/o = cases
            r'\t\^-',                          # w/o - cases
            r'\t_[^_]+?_(Wind|TrPs|ZwBR)-',    # w/o Wind, TrPs, ZwBR
            ))),
        ) -> '''Tuple[
                int,
                Deque[Tuple[Tuple[str, str, str, str, str, str, str], str]]]''':
    print(inpath)
    with open(inpath, 'r', encoding = inencoding) as infile:
        text = filter_lines(infile, include_if_all, exclude_if_any)
    for label, pattern in searches:
        for match in pattern.finditer(text):

            lem = match.group(1)
            urpos = match.group(2)
            pos = match.group(3)
            mor = match.group(4)
            tok = match.group(5)
            ref = match.group(6)
            reference = get_reference(match.group())

            if not (
##                    s(r'[NA]+P(?!n)(?!.*[w])', mor) #e
##                    or
##                    s(r'[NA]+Pn(?!.*w)', mor) #e / iu
##                    or
##                    s(r'(NS|ASn).*?w', mor) #e (/ -)
##                    or
##                    s(r'NSf(?!.*w)', mor) #e / iu / -
##                    or
##                    s(r'ASf(?!.*w)', mor) #e
##                    or
##                    s(r'ASf.*?w', mor) #e / en
##                    or
                    s(r'(DP|ASm(?!.*n))', mor) #en
                    or
                    s(r'([NAG]P|DS|GS).*?w', mor) #en
##                    or
##                    s(r'DS[mn](?!.*w)', mor) #en / em(e)
##                    or
##                    s(r'NSm(?!.*w)', mor) #er / -
##                    or
##                    s(r'([GD]Sf|GP)(?!.*w)', mor) #er / ere
##                    or
##                    s(r'GS[mn](?!.*w)', mor) #es/z
##                    or
##                    s(r'[NA]Sn(?!.*w)', mor) #ez/s / -
                    ): continue

#            if not ('pG' not in pos): continue
#            if not ('comp' not in pos and 'sup' not in pos): continue
            if 'pV' in pos:
#                if not (s(r'st.*?pV', pos)): continue
                if (s(r'st.*?pV', pos)): continue
            else:
##                if not (s(r'(?i)[aâeêèéiîoôuû][bcdfghjklmnpqrstvwxyz]+\(?[e]\)?n[()e]*$', lem)): continue
##                if not (s(r'(?i)[aâeêèéiîoôuû][bcdfghjklmnpqrstvwxyz]+\(?[aâeêèéiîoôuû]+\)?n[()e]*$', lem)): continue
##                if not (s(r'(?i)n[()e]*$', lem)): continue
                if (s(r'(?i)n[()e]*$', lem) and not 'pG' in pos and not 'comp' in pos and not 'sup' in pos): continue

            if '0' in mor:
#                if even_out(tok).lower().endswith('n'):
                if True:
                    label = '0'
                else:
                    continue
            else:
                label = '1'

            key = (label,) + reference
            passage = get_passage(text, match, sections_before, sections_after)
            results.append((key, passage))
    return size, results

def search(
        inpath: str,
        outpath: str,
        size: int,
        results: 'Deque[Tuple[Tuple[str, str, str, str, str, str, str], str]]',
        inencoding: str = 'cp850',
        searches: 'Tuple[Tuple[str, Pattern[str]], ...]' =
            tuple( (label, re.compile(pattern)) for label, pattern in [
                ('', r"""(?m)^`(.*?) @(.*?)\t¨.*?#(.*?)\t\^(.*?)\t~.*?\t\\(.*?)\t(_.*?)\t.*"""),
                ] ),
        sections_before: int = 0,
        sections_after: int = 0,
        include_if_all: 'Tuple[Pattern[str], ...]' = tuple(map(re.compile, (
            r'\t#(?:adj|[^ \t]+ p[VG])',       # only adj, part
##            r'\t#adj(?!_part)',                # only adj
##            r'\t_.*?·[PU]_',                   # only P and U genre
##            r'\t_.*?·V_',                      # only V genre
##            r'\t#adv(?!/|_)',                  # only adv
            ))),
        exclude_if_any: 'Tuple[Pattern[str], ...]' = tuple(map(re.compile, (
            r"\t\\[^\t]*?(?: [^\t]*?(?:\\-|(?<!\\)[a-zA-ZëäöüÄÖÜ'$æÆ])[^\t[]*\]|\][^-\w'$]*\t_)", # w/o square brackets in ending
            r'\t#.N',                          # w/o names
            r'\*\t#',                          # w/o foreign-language forms
            r'(?m)^`\[!',                      # w/o not annotated
            r'\t\^.*?/.*?\t~',                 # w/o ambiguous forms
            r'\t\^\?',                         # w/o abbreviations
            r'\t\\[^\t]+\|[^\t]*\t_',          # w/o | at the end
##            r'\t\\.*? .*?(?<!\\)\\-.*?\t_',    # w/o nasal bar
##            r"\t\\.*? .*?(?<!\\)'.*?\t_",      # w/o r-abbreviation
##            r'\t\\.*? .*? .*?\t_',             # w/o second space
##            r'\t(¨.*?e\)?\t#|\t#[^ \t]+ pG)',  # w/o stems in -e or (-e)
##            r'\t¨.*?w\)?\t#',                  # w/o stems in -w or (-w)
            r'`sancte @adj',                   # w/o `sancte
            r'`vrôn\(e\) @adj',                # w/o `vrôn(e)
##            r' comp\t\^',                      # w/o comparatives
##            r' sup\t\^',                       # w/o superlatives
##            r'`s[æè]lig @adj',                 # w/o `sælig, `sèlig
##            r'`lëbentig @adj',                 # w/o `lëbentig
##            r'\t\^.*?".*?\t~',                 # w/o " cases
##            r'\t\^.*?%.*?\t~',                 # w/o % cases
##            r'\t\^.*?&.*?\t~',                 # w/o & cases
##            r'\t\^.*?\!.*?\t~',                # w/o ! cases
            r'\t\^.*?°.*?\t~',                 # w/o ° cases
##            r'\t\^0',                          # w/o 0 cases
            r'\t\^.*?\=.*?\t~',                # w/o = cases
            r'\t\^-',                          # w/o - cases
            r'\t_[^_]+?_(Wind|TrPs|ZwBR)-',    # w/o Wind, TrPs, ZwBR
            ))),
        ) -> '''Tuple[
                int,
                Deque[Tuple[Tuple[str, str, str, str, str, str, str], str]]]''':
    print(inpath)
    with open(inpath, 'r', encoding = inencoding) as infile:
        text = filter_lines(infile, include_if_all, exclude_if_any)
    for label, pattern in searches:
        for match in pattern.finditer(text):

            lem = match.group(1)
            urpos = match.group(2)
            pos = match.group(3)
            mor = match.group(4)
            tok = match.group(5)
            ref = match.group(6)
            reference = get_reference(match.group())

            if not (
##                    s(r'[NA]+P(?!n)(?!.*[w])', mor) #e
##                    or
##                    s(r'[NA]+Pn(?!.*w)', mor) #e / iu
##                    or
##                    s(r'(NS|ASn).*?w', mor) #e (/ -)
##                    or
##                    s(r'NSf(?!.*w)', mor) #e / iu / -
##                    or
##                    s(r'ASf(?!.*w)', mor) #e
##                    or
##                    s(r'ASf.*?w', mor) #e / en
##                    or
##                    s(r'(DP|ASm(?!.*n))', mor) #en
##                    or
##                    s(r'([NAG]P|DS|GS).*?w', mor) #en
##                    or
##                    s(r'DS[mn](?!.*w)', mor) #en / em(e)
##                    or
##                    s(r'NSm(?!.*w)', mor) #er / -
##                    or
                    s(r'([GD]Sf|GP)(?!.*w)', mor) #er / ere
##                    or
##                    s(r'GS[mn](?!.*w)', mor) #es/z
##                    or
##                    s(r'[NA]Sn(?!.*w)', mor) #ez/s / -
                    ): continue

            if not ('pG' not in pos and 'pV' not in pos): continue
            if not ('sup' not in pos): continue
##            if not ('comp' in pos or s(r'(?i)[aâeêèéiîoôuû][bcdfghjklmnpqrstvwxyz]+\(?[e]\)?r[()e]*$', lem)): continue
##            if not ('comp' in pos or s(r'(?i)[aâeêèéiîoôuû][bcdfghjklmnpqrstvwxyz]+\(?[aâeêèéiîoôuû]+\)?r[()e]*$', lem)): continue
            if not ('comp' in pos or s(r'(?i)r[()e]*$', lem)): continue
#            if (s(r'(?i)r[()e]*$', lem) or 'comp' in pos): continue

            if '0' in mor:
                if even_out(tok).lower().endswith('r'):
#                if True:
                    label = '0'
                else:
                    continue
            else:
                if not ('r' in even_out(tok, False).lower().split(' ')[-1]): continue
                label = '1'

            key = (label,) + reference
            passage = get_passage(text, match, sections_before, sections_after)
            results.append((key, passage))
    return size, results

def search(
        inpath: str,
        outpath: str,
        size: int,
        results: 'Deque[Tuple[Tuple[str, str, str, str, str, str, str], str]]',
        inencoding: str = 'cp850',
        searches: 'Tuple[Tuple[str, Pattern[str]], ...]' =
            tuple( (label, re.compile(pattern)) for label, pattern in [
                ('', r"""(?m)^`(.*?) @(.*?)\t¨.*?#(.*?)\t\^(.*?)\t~.*?\t\\(.*?)\t(_.*?)\t.*"""),
                ] ),
        sections_before: int = 0,
        sections_after: int = 0,
        include_if_all: 'Tuple[Pattern[str], ...]' = tuple(map(re.compile, (
            r'\t#(?:adj|[^ \t]+ p[VG])',       # only adj, part
##            r'\t#adj(?!_part)',                # only adj
##            r'\t_.*?·[PU]_',                   # only P and U genre
##            r'\t_.*?·V_',                      # only V genre
##            r'\t#adv(?!/|_)',                  # only adv
            ))),
        exclude_if_any: 'Tuple[Pattern[str], ...]' = tuple(map(re.compile, (
            r"\t\\[^\t]*?(?: [^\t]*?(?:\\-|(?<!\\)[a-zA-ZëäöüÄÖÜ'$æÆ])[^\t[]*\]|\][^-\w'$]*\t_)", # w/o square brackets in ending
            r'\t#.N',                          # w/o names
            r'\*\t#',                          # w/o foreign-language forms
            r'(?m)^`\[!',                      # w/o not annotated
            r'\t\^.*?/.*?\t~',                 # w/o ambiguous forms
            r'\t\^\?',                         # w/o abbreviations
            r'\t\\[^\t]+\|[^\t]*\t_',          # w/o | at the end
##            r'\t\\.*? .*?(?<!\\)\\-.*?\t_',    # w/o nasal bar
##            r"\t\\.*? .*?(?<!\\)'.*?\t_",      # w/o r-abbreviation
##            r'\t\\.*? .*? .*?\t_',             # w/o second space
##            r'\t(¨.*?e\)?\t#|\t#[^ \t]+ pG)',  # w/o stems in -e or (-e)
##            r'\t¨.*?w\)?\t#',                  # w/o stems in -w or (-w)
            r'`sancte @adj',                   # w/o `sancte
            r'`vrôn\(e\) @adj',                # w/o `vrôn(e)
##            r' comp\t\^',                      # w/o comparatives
##            r' sup\t\^',                       # w/o superlatives
##            r'`s[æè]lig @adj',                 # w/o `sælig, `sèlig
##            r'`lëbentig @adj',                 # w/o `lëbentig
##            r'\t\^.*?".*?\t~',                 # w/o " cases
##            r'\t\^.*?%.*?\t~',                 # w/o % cases
##            r'\t\^.*?&.*?\t~',                 # w/o & cases
##            r'\t\^.*?\!.*?\t~',                # w/o ! cases
            r'\t\^.*?°.*?\t~',                 # w/o ° cases
##            r'\t\^0',                          # w/o 0 cases
            r'\t\^.*?\=.*?\t~',                # w/o = cases
            r'\t\^-',                          # w/o - cases
            r'\t_[^_]+?_(Wind|TrPs|ZwBR)-',    # w/o Wind, TrPs, ZwBR
            ))),
        ) -> '''Tuple[
                int,
                Deque[Tuple[Tuple[str, str, str, str, str, str, str], str]]]''':
    print(inpath)
    with open(inpath, 'r', encoding = inencoding) as infile:
        text = filter_lines(infile, include_if_all, exclude_if_any)
    for label, pattern in searches:
        for match in pattern.finditer(text):

            lem = match.group(1)
            urpos = match.group(2)
            pos = match.group(3)
            mor = match.group(4)
            tok = match.group(5)
            ref = match.group(6)
            reference = get_reference(match.group())

            if not (
##                    s(r'[NA]+P(?!n)(?!.*[w])', mor) #e
##                    or
##                    s(r'[NA]+Pn(?!.*w)', mor) #e / iu
##                    or
##                    s(r'(NS|ASn).*?w', mor) #e (/ -)
##                    or
##                    s(r'NSf(?!.*w)', mor) #e / iu / -
##                    or
##                    s(r'ASf(?!.*w)', mor) #e
##                    or
##                    s(r'ASf.*?w', mor) #e / en
##                    or
##                    s(r'(DP|ASm(?!.*n))', mor) #en
##                    or
##                    s(r'([NAG]P|DS|GS).*?w', mor) #en
##                    or
##                    s(r'DS[mn](?!.*w)', mor) #en / em(e)
##                    or
##                    s(r'NSm(?!.*w)', mor) #er / -
##                    or
##                    s(r'([GD]Sf|GP)(?!.*w)', mor) #er / ere
##                    or
##                    s(r'GS[mn](?!.*w)', mor) #es/z
##                    or
                    s(r'[NA]Sn(?!.*w)', mor) #ez/s / -
                    ): continue

            if not ('pG' not in pos and 'pV' not in pos): continue
            if not ('comp' not in pos and 'sup' not in pos): continue
            if not ('mêr(e)' != lem and 'ge-nuog(e)' != lem): continue
##            if not ('comp' in pos or s(r'(?i)[aâeêèéiîoôuû][bcdfghjklmnpqrstvwxyz]+\(?[e]\)?r[()e]*$', lem)): continue
##            if not ('comp' in pos or s(r'(?i)[aâeêèéiîoôuû][bcdfghjklmnpqrstvwxyz]+\(?[aâeêèéiîoôuû]+\)?r[()e]*$', lem)): continue
            if not (s(r'(?i)[sz][()e]*$', lem)): continue
#            if (s(r'(?i)[sz][()e]*$', lem)): continue

            if '0' in mor:
#                if even_out(tok)[-1] in 'sz':
                if True:
                    label = '0'
                else:
                    continue
            else:
#                if not (set('sz') & set(even_out(tok, False).split(' ')[-1])): print(tok); continue
                label = '1'

            key = (label,) + reference
            passage = get_passage(text, match, sections_before, sections_after)
            results.append((key, passage))
    return size, results

def search(
        inpath: str,
        outpath: str,
        size: int,
        results: 'Deque[Tuple[Tuple[str, str, str, str, str, str, str], str]]',
        inencoding: str = 'cp850',
        searches: 'Tuple[Tuple[str, Pattern[str]], ...]' =
            tuple( (label, re.compile(pattern)) for label, pattern in [
                ('', r"""(?m)^`(.*?) @(.*?)\t¨.*?#(.*?)\t\^(.*?)\t~.*?\t\\(.*?)\t(_.*?)\t.*"""),
                ] ),
        sections_before: int = 0,
        sections_after: int = 0,
        include_if_all: 'Tuple[Pattern[str], ...]' = tuple(map(re.compile, (
            r'\t#(?:adj|[^ \t]+ p[VG])',       # only adj, part
##            r'\t#adj(?!_part)',                # only adj
##            r'\t_.*?·[PU]_',                   # only P and U genre
##            r'\t_.*?·V_',                      # only V genre
##            r'\t#adv(?!/|_)',                  # only adv
            ))),
        exclude_if_any: 'Tuple[Pattern[str], ...]' = tuple(map(re.compile, (
            r"\t\\[^\t]*?(?: [^\t]*?(?:\\-|(?<!\\)[a-zA-ZëäöüÄÖÜ'$æÆ])[^\t[]*\]|\][^-\w'$]*\t_)", # w/o square brackets in ending
            r'\t#.N',                          # w/o names
            r'\*\t#',                          # w/o foreign-language forms
            r'(?m)^`\[!',                      # w/o not annotated
            r'\t\^.*?/.*?\t~',                 # w/o ambiguous forms
            r'\t\^\?',                         # w/o abbreviations
            r'\t\\[^\t]+\|[^\t]*\t_',          # w/o | at the end
##            r'\t\\.*? .*?(?<!\\)\\-.*?\t_',    # w/o nasal bar
##            r"\t\\.*? .*?(?<!\\)'.*?\t_",      # w/o r-abbreviation
##            r'\t\\.*? .*? .*?\t_',             # w/o second space
##            r'\t(¨.*?e\)?\t#|\t#[^ \t]+ pG)',  # w/o stems in -e or (-e)
##            r'\t¨.*?w\)?\t#',                  # w/o stems in -w or (-w)
            r'`sancte @adj',                   # w/o `sancte
            r'`vrôn\(e\) @adj',                # w/o `vrôn(e)
##            r' comp\t\^',                      # w/o comparatives
##            r' sup\t\^',                       # w/o superlatives
##            r'`s[æè]lig @adj',                 # w/o `sælig, `sèlig
##            r'`lëbentig @adj',                 # w/o `lëbentig
##            r'\t\^.*?".*?\t~',                 # w/o " cases
##            r'\t\^.*?%.*?\t~',                 # w/o % cases
##            r'\t\^.*?&.*?\t~',                 # w/o & cases
            r'\t\^.*?\!.*?\t~',                # w/o ! cases
            r'\t\^.*?°.*?\t~',                 # w/o ° cases
##            r'\t\^0',                          # w/o 0 cases
            r'\t\^.*?\=.*?\t~',                # w/o = cases
            r'\t\^-',                          # w/o - cases
            r'\t_[^_]+?_(Wind|TrPs|ZwBR)-',    # w/o Wind, TrPs, ZwBR
            ))),
        ) -> '''Tuple[
                int,
                Deque[Tuple[Tuple[str, str, str, str, str, str, str], str]]]''':
    print(inpath)
    with open(inpath, 'r', encoding = inencoding) as infile:
        text = filter_lines(infile, include_if_all, exclude_if_any)
    for label, pattern in searches:
        for match in pattern.finditer(text):

            lem = match.group(1)
            urpos = match.group(2)
            pos = match.group(3)
            mor = match.group(4)
            tok = match.group(5)
            ref = match.group(6)
            reference = get_reference(match.group())

            if not (
                    s(r'[NA]+P(?!n)(?!.*[w])', mor) #e
##                    or
##                    s(r'[NA]+Pn(?!.*w)', mor) #e / iu
                    or
                    s(r'(NS|ASn).*?w', mor) #e (/ -)
##                    or
##                    s(r'NSf(?!.*w)', mor) #e / iu / -
                    or
                    s(r'ASf(?!.*w)', mor) #e
##                    or
##                    s(r'ASf.*?w', mor) #e / en
##                    or
##                    s(r'(DP|ASm(?!.*n))', mor) #en
##                    or
##                    s(r'([NAG]P|DS|GS).*?w', mor) #en
##                    or
##                    s(r'DS[mn](?!.*w)', mor) #en / em(e)
##                    or
##                    s(r'NSm(?!.*w)', mor) #er / -
##                    or
##                    s(r'([GD]Sf|GP)(?!.*w)', mor) #er / ere
##                    or
##                    s(r'GS[mn](?!.*w)', mor) #es/z
##                    or
##                    s(r'[NA]Sn(?!.*w)', mor) #ez/s / -
                    ): continue

#            if not ('pG' not in pos): continue
#            if not ('comp' not in pos and 'sup' not in pos): continue
            if 'pV' in pos:
#                if not (s(r'st.*?pV', pos)): continue
                if (s(r'st.*?pV', pos)): continue
            else:
##                if not (s(r'(?i)[aâeêèéiîoôuû][bcdfghjklmnpqrstvwxyz]+\(?[e]\)?n[()e]*$', lem)): continue
##                if not (s(r'(?i)[aâeêèéiîoôuû][bcdfghjklmnpqrstvwxyz]+\(?[aâeêèéiîoôuû]+\)?n[()e]*$', lem)): continue
##                if not (s(r'(?i)n[()e]*$', lem)): continue
                if (s(r'(?i)n[()e]*$', lem) and not 'pG' in pos and not 'comp' in pos and not 'sup' in pos): continue

            if '0' in mor:
                tok_even = even_out(tok).lower()
                if ' ' in tok_even:
                    print(tok, pos, mor)
                label = '\n'.join(('_'.join(reference[:2]), lem + '\t' + pos, '0'))
            else:
                label = '\n'.join(('_'.join(reference[:2]), lem + '\t' + pos, '1'))

            key = (label,) + reference
            passage = get_passage(text, match, sections_before, sections_after)
            results.append((key, passage))
    return size, results

def search(
        inpath: str,
        outpath: str,
        size: int,
        results: 'Deque[Tuple[Tuple[str, str, str, str, str, str, str], str]]',
        inencoding: str = 'cp850',
        searches: 'Tuple[Tuple[str, Pattern[str]], ...]' =
            tuple( (label, re.compile(pattern)) for label, pattern in [
                ('', r"""(?m)^`(.*?) @(.*?)\t¨.*?\t#(.*?)\t\^(.*?)\t~.*?\t\\(.*?)\t(_.*?)\t.*"""),
                ] ),
        sections_before: int = 0,
        sections_after: int = 0,
        include_if_all: 'Tuple[Pattern[str], ...]' = tuple(map(re.compile, (
            r'\t#(?:adj|[^ \t]+ p[VG])',       # only adj, part
##            r'\t#adj(?!_part)',                # only adj
##            r'\t_.*?·[PU]_',                   # only P and U genre
##            r'\t_.*?·V_',                      # only V genre
##            r'\t#adv(?!/|_)',                  # only adv
            ))),
        exclude_if_any: 'Tuple[Pattern[str], ...]' = tuple(map(re.compile, (
            r"\t\\[^\t]*?(?: [^\t]*?(?:\\-|(?<!\\)[a-zA-ZëäöüÄÖÜ'$æÆ])[^\t[]*\]|\][^-\w'$]*\t_)", # w/o square brackets in ending
            r'\t#.N',                          # w/o names
            r'\*\t#',                          # w/o foreign-language forms
            r'(?m)^`\[!',                      # w/o not annotated
            r'\t\^.*?/.*?\t~',                 # w/o ambiguous forms
            r'\t\^\?',                         # w/o abbreviations
            r'\t\\[^\t]+\|[^\t]*\t_',          # w/o | at the end
##            r'\t\\.*? .*?(?<!\\)\\-.*?\t_',    # w/o nasal bar
##            r"\t\\.*? .*?(?<!\\)'.*?\t_",      # w/o r-abbreviation
##            r'\t\\.*? .*? .*?\t_',             # w/o second space
##            r'\t(¨.*?e\)?\t#|\t#[^ \t]+ pG)',  # w/o stems in -e or (-e)
##            r'\t¨.*?w\)?\t#',                  # w/o stems in -w or (-w)
            r'`sancte @adj',                   # w/o `sancte
            r'`vrôn\(e\) @adj',                # w/o `vrôn(e)
##            r' comp\t\^',                      # w/o comparatives
##            r' sup\t\^',                       # w/o superlatives
##            r'`s[æè]lig @adj',                 # w/o `sælig, `sèlig
##            r'`lëbentig @adj',                 # w/o `lëbentig
##            r'\t\^.*?".*?\t~',                 # w/o " cases
            r'\t\^.*?%.*?\t~',                 # w/o % cases
##            r'\t\^.*?&.*?\t~',                 # w/o & cases
            r'\t\^.*?\!.*?\t~',                # w/o ! cases
            r'\t\^.*?°.*?\t~',                 # w/o ° cases
##            r'\t\^0',                          # w/o 0 cases
            r'\t\^.*?\=.*?\t~',                # w/o = cases
            r'\t\^-',                          # w/o - cases
            r'\t_[^_]+?_(Wind|TrPs|ZwBR)-',    # w/o Wind, TrPs, ZwBR
            ))),
        ) -> '''Tuple[
                int,
                Deque[Tuple[Tuple[str, str, str, str, str, str, str], str]]]''':
    print(inpath)
    with open(inpath, 'r', encoding = inencoding) as infile:
        text = filter_lines(infile, include_if_all, exclude_if_any)
    for label, pattern in searches:
        for match in pattern.finditer(text):

            lem = match.group(1)
            urpos = match.group(2)
            pos = match.group(3)
            mor = match.group(4)
            tok = match.group(5)
            ref = match.group(6)
            reference = get_reference(match.group())

            if not (
##                    '-' in mor or '=' in mor
##                    or
                    s(r'[NA]+P(?!n)(?!.*[w])', mor) #e
##                    or
##                    s(r'[NA]+Pn(?!.*w)', mor) #e / iu
                    or
                    s(r'(NS|ASn).*?w', mor) #e (/ -)
##                    or
##                    s(r'NSf(?!.*w)', mor) #e / iu / -
                    or
                    s(r'ASf(?!.*w)', mor) #e
##                    or
##                    s(r'ASf.*?w', mor) #e / en
##                    or
##                    s(r'(DP|ASm(?!.*n))', mor) #en
##                    or
##                    s(r'([NAG]P|DS|GS).*?w', mor) #en
##                    or
##                    s(r'DS[mn](?!.*w)', mor) #en / em(e)
##                    or
##                    s(r'NSm(?!.*w)', mor) #er / -
##                    or
##                    s(r'([GD]Sf|GP)(?!.*w)', mor) #er / ere
##                    or
##                    s(r'GS[mn](?!.*w)', mor) #es/z
##                    or
##                    s(r'[NA]Sn(?!.*w)', mor) #ez/s / -
                    ): continue

            lem = lem.rstrip('(e)')
            if 'comp' in pos and 'comp' not in urpos:
                lem += 'er'
            elif 'sup' in pos and 'sup' not in urpos:
                lem += 'est'
            elif 'pV' in pos:
                if 'sw' in pos:
                    lem = lem[:-1] + 't'
            elif 'pG' in pos:
                lem += 't'

            if not ('ASfn' not in mor): continue
            if not (' ' not in tok or not s(r'[ei][uvw]', even_out(tok.split(' ', 1)[1]))): continue
#            if not (s(r'[bcdfghkpqstvxz]$', lem)): continue
#            if not (s(r'([âêîôûæ]|oè|iu|iè|üè|uo|èi|ou|öu)[lmnr]$', lem)): continue
            if not (s(r'[bcdfghkpqstvxz][aeiouäëöüè][lmnr]$', lem)): continue
#            if not (s(r'[lmnr]$', lem)): continue

            if '0' in mor:
                label = '0' #'\t'.join(('0', lem, pos))
            else:
                label = '1' #'\t'.join(('1', lem, pos))

            key = (label,) + reference
            passage = get_passage(text, match, sections_before, sections_after)
            results.append((key, passage))
    return size, results

def search(
        inpath: str,
        outpath: str,
        size: int,
        results: 'Deque[Tuple[Tuple[str, str, str, str, str, str, str], str]]',
        inencoding: str = 'cp850',
        searches: 'Tuple[Tuple[str, Pattern[str]], ...]' =
            tuple( (label, re.compile(pattern)) for label, pattern in [
                ('', r"""(?m)^`(.*?) @(adj.*?)\t¨.*?\t#(.*?)\t\^(.*?)\t~.*?\t\\(.*?)\t(_.*?)\t.*"""),
                ] ),
        sections_before: int = 3,
        sections_after: int = 4,
        include_if_all: 'Tuple[Pattern[str], ...]' = tuple(map(re.compile, (
##            r'\t#(?:adj|[^ \t]+ p[VG])',       # only adj, part
##            r'\t#adj(?!_part)',                # only adj
##            r'\t_.*?·[PU]_',                   # only P and U genre
##            r'\t_.*?·V_',                      # only V genre
##            r'\t#adv(?!/|_)',                  # only adv
            ))),
        exclude_if_any: 'Tuple[Pattern[str], ...]' = tuple(map(re.compile, (
##            r"\t\\[^\t]*?(?: [^\t]*?(?:\\-|(?<!\\)[a-zA-ZëäöüÄÖÜ'$æÆ])[^\t[]*\]|\][^-\w'$]*\t_)", # w/o square brackets in ending
##            r'\t#.N',                          # w/o names
##            r'\*\t#',                          # w/o foreign-language forms
##            r'(?m)^`\[!',                      # w/o not annotated
##            r'\t\^.*?/.*?\t~',                 # w/o ambiguous forms
##            r'\t\^\?',                         # w/o abbreviations
##            r'\t\\[^\t]+\|[^\t]*\t_',          # w/o | at the end
##            r'\t\\.*? .*?(?<!\\)\\-.*?\t_',    # w/o nasal bar
##            r"\t\\.*? .*?(?<!\\)'.*?\t_",      # w/o r-abbreviation
##            r'\t\\.*? .*? .*?\t_',             # w/o second space
##            r'\t(¨.*?e\)?\t#|\t#[^ \t]+ pG)',  # w/o stems in -e or (-e)
##            r'\t¨.*?w\)?\t#',                  # w/o stems in -w or (-w)
##            r'`sancte @adj',                   # w/o `sancte
##            r'`vrôn\(e\) @adj',                # w/o `vrôn(e)
##            r' comp\t\^',                      # w/o comparatives
##            r' sup\t\^',                       # w/o superlatives
##            r'`s[æè]lig @adj',                 # w/o `sælig, `sèlig
##            r'`lëbentig @adj',                 # w/o `lëbentig
##            r'\t\^.*?".*?\t~',                 # w/o " cases
##            r'\t\^.*?%.*?\t~',                 # w/o % cases
##            r'\t\^.*?&.*?\t~',                 # w/o & cases
##            r'\t\^.*?\!.*?\t~',                # w/o ! cases
##            r'\t\^.*?°.*?\t~',                 # w/o ° cases
##            r'\t\^0',                          # w/o 0 cases
##            r'\t\^.*?\=.*?\t~',                # w/o = cases
##            r'\t\^-',                          # w/o - cases
##            r'\t_[^_]+?_(Wind|TrPs|ZwBR)-',    # w/o Wind, TrPs, ZwBR
            ))),
        ) -> '''Tuple[
                int,
                Deque[Tuple[Tuple[str, str, str, str, str, str, str], str]]]''':
    print(inpath)
    with open(inpath, 'r', encoding = inencoding) as infile:
        text = filter_lines(infile, include_if_all, exclude_if_any)
    for label, pattern in searches:
        for match in pattern.finditer(text):

            lem = match.group(1)
            urpos = match.group(2)
            pos = match.group(3)
            mor = match.group(4)
            tok = match.group(5)
            ref = match.group(6)
            reference = get_reference(match.group())

            if not (all( i in mor for i in 'w!' )): continue
            if not (all( i not in mor for i in '"%&' )): continue
            if not ('sup' in pos): continue
##            if not ('P' in mor): continue
##            if not (lem == 'vrî'): continue
            if not (not s(r'e\)?$', lem)): continue

            key = (label,) + reference
            passage = get_passage(text, match, sections_before, sections_after)
            results.append((key, passage))
    return size, results

def search(
        inpath: str,
        outpath: str,
        size: int,
        results: 'Deque[Tuple[Tuple[str, str, str, str, str, str, str], str]]',
        inencoding: str = 'cp850',
        searches: 'Tuple[Tuple[str, Pattern[str]], ...]' =
            tuple( (label, re.compile(pattern)) for label, pattern in [
                ('', r"""(?m)^`(mann) @(m.*?)\t¨.*?\t#(.*?)\t\^(.*?)\t~.*?\t\\(.*?)\t(_.*?)\t.*"""),
                ] ),
        sections_before: int = 0,
        sections_after: int = 0,
        include_if_all: 'Tuple[Pattern[str], ...]' = tuple(map(re.compile, (
##            r'\t#(?:adj|[^ \t]+ p[VG])',       # only adj, part
##            r'\t#adj(?!_part)',                # only adj
##            r'\t_.*?·[PU]_',                   # only P and U genre
##            r'\t_.*?·V_',                      # only V genre
##            r'\t#adv(?!/|_)',                  # only adv
            ))),
        exclude_if_any: 'Tuple[Pattern[str], ...]' = tuple(map(re.compile, (
##            r"\t\\[^\t]*?(?: [^\t]*?(?:\\-|(?<!\\)[a-zA-ZëäöüÄÖÜ'$æÆ])[^\t[]*\]|\][^-\w'$]*\t_)", # w/o square brackets in ending
##            r'\t#.N',                          # w/o names
##            r'\*\t#',                          # w/o foreign-language forms
##            r'(?m)^`\[!',                      # w/o not annotated
##            r'\t\^.*?/.*?\t~',                 # w/o ambiguous forms
##            r'\t\^\?',                         # w/o abbreviations
##            r'\t\\[^\t]+\|[^\t]*\t_',          # w/o | at the end
##            r'\t\\.*? .*?(?<!\\)\\-.*?\t_',    # w/o nasal bar
##            r"\t\\.*? .*?(?<!\\)'.*?\t_",      # w/o r-abbreviation
##            r'\t\\.*? .*? .*?\t_',             # w/o second space
##            r'\t(¨.*?e\)?\t#|\t#[^ \t]+ pG)',  # w/o stems in -e or (-e)
##            r'\t¨.*?w\)?\t#',                  # w/o stems in -w or (-w)
##            r'`sancte @adj',                   # w/o `sancte
##            r'`vrôn\(e\) @adj',                # w/o `vrôn(e)
##            r' comp\t\^',                      # w/o comparatives
##            r' sup\t\^',                       # w/o superlatives
##            r'`s[æè]lig @adj',                 # w/o `sælig, `sèlig
##            r'`lëbentig @adj',                 # w/o `lëbentig
##            r'\t\^.*?".*?\t~',                 # w/o " cases
##            r'\t\^.*?%.*?\t~',                 # w/o % cases
##            r'\t\^.*?&.*?\t~',                 # w/o & cases
##            r'\t\^.*?\!.*?\t~',                # w/o ! cases
##            r'\t\^.*?°.*?\t~',                 # w/o ° cases
##            r'\t\^0',                          # w/o 0 cases
##            r'\t\^.*?\=.*?\t~',                # w/o = cases
##            r'\t\^-',                          # w/o - cases
##            r'\t_[^_]+?_(Wind|TrPs|ZwBR)-',    # w/o Wind, TrPs, ZwBR
            ))),
        ) -> '''Tuple[
                int,
                Deque[Tuple[Tuple[str, str, str, str, str, str, str], str]]]''':
    print(inpath)
    with open(inpath, 'r', encoding = inencoding) as infile:
        text = filter_lines(infile, include_if_all, exclude_if_any)
    for label, pattern in searches:
        for match in pattern.finditer(text):

            lem = match.group(1)
            urpos = match.group(2)
            pos = match.group(3)
            mor = match.group(4)
            tok = match.group(5)
            ref = match.group(6)
            reference = get_reference(match.group())

            if s(r'[NAD]S', mor):
                continue
            if s(r'[aeiou].*?[-n].*?[aeiou]', unicodedata.normalize('NFKD', tok)):
                label = '1'
            else:
                label = '0'

            key = (label,) + reference
            passage = get_passage(text, match, sections_before, sections_after)
            results.append((key, passage))
    return size, results

def search_over_texts():
    shared_outpath, size, results = file_io.run_over_files(
        shared_inpath = __file__ + '/../../../mhd/pp/p',
        include_in_inpaths_re = r'',
        exclude_from_inpaths_re = r'(?i)ind-?list',
        function = search,
        results = deque(),
        )
    file_io.tell(shared_outpath, size, results)
    if results:

##        a = Counter( r[0][0] for r in results )
##        results = sorted(( (y, x) for x, y in a.items() ), reverse = True)

##        rs = [ r[1].split('\t')[0] + r[1].split('\t')[2] for r in results if '0' in r[0][0] ]
##        rs = Counter(rs)
##        rs = sorted(( (v, k) for k, v in rs.items() ), reverse = True)
##        results = rs

##        rs = deque( r[0][0].split('\t') for r in results )
##        ratios = defaultdict(list)
##        for label, lem, pos in rs:
##            ratios[(lem, pos)].append(int(label))
##        ratios = sorted(( (ratio, total, lem, pos)
##                for (lem, pos), (ratio, total)
##                in { (lem, pos): ((labels.count(0) / len(labels)), len(labels)) for (lem, pos), labels in ratios.items() }.items()
##                if total > 9
##                ), reverse = True)
##        ratios = [ '\t'.join(map(str, row)) for row in ratios ]
##        for r in ratios:
##            print(r)

        text = file_io.stringify(results)
        file_io.tell(*file_io.write_new(shared_outpath, text, 'utf-8', '.csv'))
        text = chart.get_heatmaps(deque( label for label, _ in results ),
                TABLE,
                'mhg_table_grammar.tpl',
                matchgetter = itemgetter(1, 2),
                format_config = {'spread_marker': '', 'too_few_limit': 4},
                )
        path, size = file_io.write_new(shared_outpath, text, 'utf-8', '.html')
        file_io.tell(path, size)
        webbrowser.open(path)

def process(inpath, outpath, size, results):
    try:
        with open(inpath, 'r', encoding = 'cp437') as file:
            text = file.read()
    except UnicodeDecodeError:
        #print('UnicodeDecodeError:', inpath)
        return size, results

    text, number = re.subn(
            r'''(@|#)_partikel''',
            r'''\g<1>partikel''',
            text)
    if number :
        print(inpath)
        size += file_io.write(outpath, text, outencoding = 'cp437')

##    for n, match in enumerate(re.finditer(
##                r'''(?mi)^.*?compare_lines.*''',
##                text)):
##        if n == 0:
##            print()
##            print('═' * len(inpath))
##            print(inpath)
##            print('─' * len(inpath))
##        print(match.group())

    return size, results

def main():
    shared_outpath, size, results = file_io.run_over_files(
            shared_inpath
                = file_io.join(__file__, '../../mhd/P-E'),
            include_in_inpaths_re = r'',
            exclude_from_inpaths_re = r'',
            ext = None,
            size = 0,
            function = process,
            )
    file_io.tell(shared_outpath, size, results)
    if results:
        outpath = file_io.get_new_path(shared_outpath, '.csv')
        size = file_io.write(
                outpath,
                file_io.stringify(sorted(results.items())),
                'utf-16',
                )
        file_io.tell(outpath, size)
        if size > 0:
            webbrowser.open(outpath)

if __name__ == '__main__':
##    path = file_io.join(__file__, '../../mhd/P-E')
##    for name in file_io.get_paths_sorted(path):
##        print(name)
##    exit(0)
##    main()
##    exit(0)
##    compare_two(
##            file_io.join(__file__, '../../mhd/P_teil'),
##            file_io.join(__file__, '../../mhd/P-D_teil'),
##            )
##    exit(0)
    if len(sys.argv) > 1:
        globals()[sys.argv[1]]()
    else:
        search_over_texts()
