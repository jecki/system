// Licensed under http://www.apache.org/licenses/LICENSE-2.0
// Attribution notice: by Stefan Müller in 2013 ff.
function addAttrs(row, data, dataIndex) {
	// For every row element, set attributes (e.g. `id`) provided by a non-XML/HTML data source.
	var rowAttrs = data.rowAttrs;
	if (rowAttrs != null) {
		for (var key in rowAttrs) {
			row.setAttribute(key, rowAttrs[key]);
		}
	}
};
function filterClear() {
	// Reset all filters and redraw the table
	$('#index').DataTable().search('').columns().search('').draw();
};
function filterColumn(i) {
	// Fulltext search in a specific column
	$('#index').DataTable().column(i).search(
		$('#column' + i + '_term').val(),
		$('#column' + i + '_regex').prop('checked'),
		$('#column' + i + '_smart').prop('checked'),
		$('#column' + i + '_caseInsen').prop('checked')
	).draw();
};
function filterGlobal() {
	// Fulltext search in all columns
	$('#index').DataTable().search(
		$('#global_term').val(),
		$('#global_regex').prop('checked'),
		$('#global_smart').prop('checked'),
		$('#global_caseInsen').prop('checked')
	).draw();
};
function highlight() {
	$('.idlight').hover(wrapTextNodes, unwrapTextNodes);
	// Provide what would be `.parentlight ChildOf *` in CSS:
	$('.parentlight').hover(
		function(){
			$(this).parent().addClass('mark');
		},
		function(){
			$(this).parent().removeClass('mark');
		}
	);
	$('.prevlight').hover(
		function(){
			$(this).prev().addClass('mark');
		},
		function(){
			$(this).prev().removeClass('mark');
		}
	);
};
function rowImg(e, path, colspan) {
	var old = document.getElementById('image_row');
	if (old) {old.outerHTML = ''};
	if (path) {
		$(e.target).parent().parent().before(
			'<tr id="image_row"><td colspan="'+colspan+'"><a class="key" href="'+path+'"><img id="image" alt="'+path+'" src="'+path+'"/></a></td></tr>'
		);
	};
};
function unwrapTextNodes() {
	var parent = $(this).parent().find('.mark').contents().unwrap();
};
function wrapTextNodes() {
	var id = $(this).attr('data-off');
	var node = this;
	while (true) {
		var node = node.previousSibling;
		console.log(node);
		if (node === null) {
			return;
		} else if (node.nodeType === 3) {
			$(node).wrap('<span class="mark">');
			// Hereafter, the wrapping is the sibling, thus:
			node = node.parentElement;
		} else if (node.nodeType === 1) {
			if (node.getAttribute('data-on') === id) {return;}
			$(node).wrap('<span class="mark">');
			node = node.parentElement;
		}
	};
};
function zoom(e) {
	$(this).css(
		'height',
		Math.max(
			128,
			parseInt($(this).css('height')) - ((e.deltaY > 0 ? 1 : -1) * 20)));
	e.preventDefault();
};
