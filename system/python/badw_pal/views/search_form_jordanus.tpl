% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2013 ff.
% # Template for a fulltext search form; gets filled into the base template.
% lang_id = kwargs.get('lang_id', '')
<main>
<article class="card non-source" role="search">
	<p style="margin-left:0.5em">
		<b><dfn>_</dfn></b> in a search term is understood as <q>zero or one unknown character</q>. </br>
		<b><dfn>%</dfn></b> is understood as <q>zero or more unknown characters</q>.
	</p>
	<form action="search_results" method="get">
		<div class="table">
			<div>
				<label for="any_field"><b>Any field</b>:</label>
				<input aria-label="{{dao.glosses['search_term'][lang_id]}}" autofocus="" name="any_field" pattern=".*\w.*" title="{{dao.glosses['at_least_one_letter'][lang_id]}}" type="search"/>
			</div>
		  % mainfields, subfields = dao.get_searchfields_jordanus()
		  % for row in mainfields:
			<div>
				<label for="{{row['field_hypernum']}}">{{row['field_hypername']}}:</label>
				<input aria-label="{{dao.glosses['search_term'][lang_id]}}" name="{{row['field_hypernum']}}" pattern=".*\w.*" title="{{dao.glosses['at_least_one_letter'][lang_id]}}" type="search"/>
			</div>
		  % end
		</div>
		  % if subfields:
			<details class="index table"><summary>Further fields:</summary>
			% for row in subfields:
				<div>
					<label for="{{row['field_hypernum']}}">{{row['field_hypername']}}:</label>
					<input aria-label="{{dao.glosses['search_term'][lang_id]}}" name="{{row['field_hypernum']}}" pattern=".*\w.*" title="{{dao.glosses['at_least_one_letter'][lang_id]}}" type="search"/>
				</div>
			% end
			</details>
		  % end
		<p><button type="submit">{{dao.glosses['search'][lang_id]}}&#8239;►</button></p>
	</form>
</article>
</main>
% rebase('base_jordanus.tpl', request = request, dao = dao, kwargs = kwargs)
