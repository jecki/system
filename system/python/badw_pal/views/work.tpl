% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
% entry = kwargs['entry']
% title = dao.format_title(entry['work_title'])
% modified_on = entry['work_modified_on'].split()[0]
% modified_on_shown = '.'.join(reversed(modified_on.split('-')))
<main>
<article class="sheet sheetwide">
	<p>Work {{entry['work_siglum']}}</p>
	<h1>{{entry['person_name']}}<br/>{{!title}}</h1>
  % if entry['work_quotation']:
	% paragraphs = entry['work_quotation'].split('\n')
	<p class="flush_unhyph"><b class="label">Text</b>&#8193;{{!paragraphs[0]}}</p>
	% for paragraph in paragraphs[1:]:
	<p class="flush_unhyph"><b class="label" style="color:#ffffff;">Text</b>&#8193;{{!paragraph}}</p>
	% end
  % end
  % if entry['work_content']:
	<p class="flush_unhyph"><b class="label">Content</b>&#8193;{{!entry['work_content']}}</p>
  % end
  % if entry['work_origin']:
	<p class="flush_unhyph"><b class="label">Origin</b>&#8193;{{!entry['work_origin']}}</p>
  % end
  % if entry['work_note']:
	% paragraphs = entry['work_note'].split('\n')
	% if len(paragraphs) == 1:
	<p class="flush_unhyph"><b class="label">Note</b>&#8193;{{!paragraphs[0]}}</p>
	% else:
	  % for rank, paragraph in enumerate(paragraphs, start = 1):
	<p class="flush_unhyph"><b class="label">Note&#160;{{rank}}</b>&#8193;{{!paragraph}}</p>
	  % end
	% end
  % end
  % if entry['work_bibliography']:
	<p class="flush_unhyph"><b class="label">Bibl.</b>&#8193;{{!entry['work_bibliography']}}</p>
  % end
  % if entry['work_printhistory']:
	<p class="flush_unhyph"><b class="label">Ed.</b>&#8193;{{!entry['work_printhistory']}}</p>
  % end
	<table class="parts with_subentries">
	  % if entry['printparts']:
		<tr>
			<th scope="row"><p>EDS</p></th>
			<td>
			  % for printpart in entry['printparts']:
			  %     scan_link = printpart.get('scan_link', '')
			  %     tran_link = printpart.get('tran_link', '')
				<p>
				% if printpart['print_hidden'] == 'F':
					<a href="/print/{{printpart['print_id']}}">{{printpart['place_name']}}, {{printpart['print_publisher']}}{{printpart['printpart_info']}}</a>&#8193;
				  % if scan_link and tran_link:
					<a class="key key_dark" href="{{scan_link}}">Scans|Text</a>
				  % elif scan_link:
					<a class="key key_dark" href="{{scan_link}}">Scans</a>
				  % elif tran_link:
					<a class="key key_dark" href="{{tran_link}}">Text</a>
				  % end
				% end
				</p>
			  % end
			</td>
		</tr>
	  % end
	  % if entry['msparts']:
		<tr>
			<th scope="row"><p>MSS</p></th>
			<td>
			  % for mspart in entry['msparts']:
			  %     scan_link = mspart.get('scan_link', '')
			  %     tran_link = mspart.get('tran_link', '')
			  %     place_name = mspart['place_name'].lstrip('?')
			  %     place_name = place_name + ', ' if place_name else ''
				<p>
				% if mspart['ms_hidden'] == 'F':
					<a href="/ms/{{mspart['ms_id']}}">{{place_name}}<abbr title="{{mspart['lib_name']}}">{{mspart['lib_abbrev'] or mspart['lib_name']}}</abbr>, {{mspart['ms_shelfmark']}}{{mspart['mspart_info'] or ', ' + mspart['pagina_range']}}</a>&#8193;
				  % if scan_link and tran_link:
					<a class="key key_dark" href="{{scan_link}}">Scans|Text</a>
				  % elif scan_link:
					<a class="key key_dark" href="{{scan_link}}">Scans</a>
				  % elif tran_link:
					<a class="key key_dark" href="{{tran_link}}">Text</a>
				  % end
				% else:
					{{place_name}}<abbr title="{{mspart['lib_name']}}">{{mspart['lib_abbrev'] or mspart['lib_name']}}</abbr>, {{mspart['ms_shelfmark']}}{{mspart['mspart_info'] or ', ' + mspart['pagina_range']}}
				% end
				</p>
			  % end
			</td>
		</tr>
	  % end
	</table>
  % for lang in dao.get_langs_sorted():
  %     for label in ('translations', 'commentaries'):
  %         coworks = entry.get(label, {}).get(lang['lang_id'], [])
  %         if coworks:
	<h2>{{lang['lang']}} {{label}}</h2>
	<ul class="with_subentries">
  %             for cowork in coworks:
		<li><p><a href="/work/{{cowork['work_id']}}">{{'' if 'Anonymous' in cowork['person_name'] else cowork['person_name'] + ', '}}{{!dao.format_title(cowork['work_title'])}} ({{cowork['work_siglum']}})</a></p></li>
  %             end
	</ul>
  %         end
  %     end
  % end
	<aside class="citebox">
		<p>How to cite this entry?</p>
		<p>{{entry['user_firstname']}} {{entry['user_lastname']}}, ‘{{entry['person_name']}}, {{!title}}’ (update: <time datetime="{{modified_on}}">{{modified_on_shown}}</time>), <cite>Ptolemaeus Arabus et Latinus. Works</cite>, URL&#8239;=&#8239;http://ptolemaeus.badw.de/work/{{entry['work_id']}}.</p>
	</aside>
</article>
</main>\\
% rebase('base.tpl', request = request, dao = dao, kwargs = kwargs)