% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2013 ff.
% # Template for a fulltext search form; gets filled into the base template.
% # In the example edition: http://localhost:8080/portal/search_fulltext.
% lang_id = kwargs.get('lang_id', '')
<main>
<article class="card non-source" role="search">
	<h1>{{dao.glosses['search_fulltext'][lang_id]}}</h1>
	<form action="search_results" method="get">
		<p><input aria-label="{{dao.glosses['search_term'][lang_id]}}" autofocus="" name="term" pattern=".*\w.*" required="" title="{{dao.glosses['at_least_one_letter'][lang_id]}}" type="search"/></p>
		<p><button type="submit">{{dao.glosses['search'][lang_id]}}&#8239;►</button></p>
		<fieldset>
			<legend>{{dao.glosses['settings'][lang_id]}}</legend>
			<label><input checked="" name="no_case" type="checkbox"/> {{dao.glosses['no_case'][lang_id]}}</label> <br/>
			<label><input name="single_words" type="checkbox"/> {{dao.glosses['whole_words'][lang_id]}} {{dao.glosses['no_order'][lang_id]}}</label> <br/>
			<label><input name="regex" type="checkbox"/> {{dao.glosses['regex'][lang_id]}} (<a href="/{{lang_id}}/{{dao.default_site_id}}/user_guide#regex">{{dao.glosses['click_for_more_info'][lang_id]}}</a>)</label>
		</fieldset>
		<fieldset>
			<legend>{{dao.glosses['domain'][lang_id]}}</legend>
			<label><input checked="" name="status_id" type="checkbox" value="source"/> {{dao.glosses['source'][lang_id]}}</label> <br/>
			<label><input checked="" name="status_id" type="checkbox" value="non-source"/> {{dao.glosses['introduction'][lang_id]}} / {{dao.glosses['commentary'][lang_id]}}</label>
		</fieldset>
	  % if dao.multisite:
		<fieldset>
			<legend>{{dao.glosses['publication'][lang_id]}}</legend>
		  % for dao_site_id in dao.site_ids:
		    % if not dao_site_id == dao.default_site_id:
			<label><input{{!' checked=""' if dao_site_id == request.query.base_site_id else ''}} name="site_id" type="checkbox" value="{{dao_site_id}}"/> {{!dao.glosses[dao_site_id][dao.default_lang_ids[dao_site_id]].replace('\n', ' ')}}</label> <br/>
			% end
		  % end
		</fieldset>
	  % end
	</form>
</article>
</main>
% rebase('base.tpl', request = request, dao = dao, kwargs = kwargs)
