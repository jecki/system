% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
<main>
<article class="sheet sheetwide">
%#<small class="note">
%#	<a class="card key" href="news">News</a>
%#	<a class="card key" href="jobs">Jobs</a>
%#	<section class="card" style="width:96%">
%#		Latest News:
%#		<ul class="list">
%#		  % for news_item in []: # kwargs['news']:
%#			<li><i><time>{{news_item['news_posted']}}</time></i>. {{news_item['news_headline']}} <a class="key" href="news/{{news_item['news_id']}}">more&#xa0;…</a></li>
%#		  % end
%#		</ul>
%#	</section>
%#</small>
<section class="flush_unhyph">
{{!kwargs['text']}}
</section>
</article>
</main>\\
% rebase('base.tpl', request = request, dao = dao, kwargs = kwargs)