# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
import json
import os
from datetime import datetime

import __init__
import bottle
from bottle import HTTPError, redirect, request, response, static_file, template
import file_io
from file_io import log_ip_medium

class App(bottle.Bottle):
    '''
    A bottle application object.
    See the docstring of :func:`bottleapp.get` in :path:`../../bottleapp.py`.
    '''
    def __init__(
            self,
            dao_class: type,
            dao_kwargs: 'Dict[str, Any]',
            https: bool,
            host: str,
            port: str,
            debug: bool,
            server: str,
            views_paths: 'Sequence[str]',
            cssjs_path: str,
            ):
        super().__init__()
        self.https = https
        self.host = host
        self.port = port
        self.debug = debug
        self.server = server
        self.cssjs_path = os.path.abspath(cssjs_path) + os.sep
        self.views_paths = [ os.path.abspath(p) + os.sep for p in views_paths ]
        bottle.TEMPLATE_PATH = self.views_paths
        self.log = file_io.Log(
                __file__,
                log_folder_path = '../../../../../seite_kdih_zugriffe',
                log_file_name = \
                    datetime.now().isoformat().replace(':', '').split('.')[0] +
                    '--.log')
        self.dao = dao_class(**dao_kwargs)
        self.set_routes(self.dao)

    def log_access(self, request: 'bottle.LocalRequest', pub_id: str) -> None:
        log_ip_medium(
                self.log,
                request.remote_addr,
                '█' + request.environ.get(
                    'HTTP_USER_AGENT', '').split(None, 1)[0] + '█' + pub_id
                )

    def run(self) -> None:
        '''
        Run :param:`self` by calling :meth:`bottle.run`. Before or in this call,
        you may apply middleware, e.g. instead of mere ``app = self``, pass
        ``app = SessionMiddleware(self, self.dao.session_options)``.
        '''
        bottle.run(
                app = self,
                host = self.host,
                port = self.port,
                debug = self.debug,
                server = self.server(
                    self.host,
                    self.port,
                    **{
                        key:
                            (self.dao.paths.get(name, '') if self.https else '')
                        for key, name in (
                            ('certfile', 'ssl_cert_path'),
                            ('keyfile', 'ssl_key_path'),
                            ('chainfile', 'ssl_chain_path'),
                            )
                        }
                    )
                )

    def set_routes(self, dao) -> None:
        '''
        Define the request-handling functions and assign them to routes.
        '''
        @self.route('<path:path>/')
        def redirect_endslash(path):
            '''
            Redirect requests whose URL ends with `/`: Ignore the trailing `/`.
            '''
            redirect(path)

        @self.route('/kdih/<path:path>')
        def redirect_legacy(path):
            redirect('/datenbank/' + path)

        @self.route('/datenbank')
        def redirect_short_url():
            '''
            Redirect requests which specify no page.
            Assume the default page of the site.

            The redirected request will be dealt with by :func:`.return_page`.
            Further explanation is found in the docstring of that function.
            '''
            redirect('/datenbank/' + dao.default_page_id)

        @self.route('/datenbank/cssjs/<path:path>')
        def return_cssjs(path):
            '''
            Return a CSS or JS file for client-side styling or processing.
            For static content files, see :func:`.return_static_content`.

            Example URLs are:

            - `/datenbank/cssjs/jquery/jquery.js`
            - `/datenbank/cssjs/normalize/normalize.css`

            The path of the file on the server must be the joint of
            :attr:`self.cssjs_path` and :param:`path`.
            Otherwise, the access is denied.
            '''
            return static_file(path, self.cssjs_path)

        @self.route('/datenbank/<kind:re:(druck|handschrift|stoffgruppe|untergruppe)>/<stoff>')
        @self.route('/datenbank/<kind:re:(druck|handschrift|stoffgruppe|untergruppe)>/<stoff>/<unter>')
        @self.route('/datenbank/<kind:re:(druck|handschrift|stoffgruppe|untergruppe)>/<stoff>/<unter>/<ein>')
        @self.route('/datenbank/<kind:re:(druck|handschrift|stoffgruppe|untergruppe)>/<stoff>/<unter>/<ein>')
        def return_entry(kind, stoff, unter = None, ein = None):
            entry, text_lang_id = dao.get_entry(kind, stoff, unter, ein)
            if entry:
                return template(
                        kind + '.tpl', request = request, dao = dao,
                        kwargs = {
                            'text_lang_id': 'de',
                            'lang_id': dao.default_lang_id,
                            'page_id': '/'.join(
                                filter(None, (kind, stoff, unter, ein))),
                            'entry': entry,
                            'prepath': '/datenbank/',
                            })
            else: redirect('/datenbank/404')

        @self.route('/datenbank/indices/<kind>.json')
        def return_index(kind):
            '''
            Return JSON for a Datatable of e.g. MSS (or other entities,
            specified by :param:`kind`).
            '''
            return json.dumps(
                    {'data': tuple(dao.get_index(kind))},
                    separators = (',', ':'))

        @self.route('/datenbank/<page_id>')
        def return_page(page_id):
            '''
            Return the page specified by the URL.

            If :param:`page_id` does not denote a page of the site,
            raise the HTTP error 404 (`Not found`).
            '''
            template_id, text, text_lang_id = dao.get_template_id_and_text(
                    lang_id = dao.default_lang_id, page_id = page_id)
            if text:
                if text[:3] == b'\xef\xbb\xbf':
                    text = text[3:]
                return template(
                        template_id, request = request, dao = dao,
                        kwargs = {
                            'text_lang_id': text_lang_id,
                            'lang_id': dao.default_lang_id,
                            'page_id': page_id,
                            'text': text,
                            'prepath': '/datenbank/',
                            })
            else: redirect('/datenbank/404')

        @self.route('/datenbank/<path:path>')
        def return_static_content(path):
            '''
            Return a static content file, e.g. an image file.

            Example URLs are:

            - `/datenbank/icons/shortcut.ico`
            - `/datenbank/acta/secretissima/1890-03-20.pdf?download=true`

            If the URL contains a query string with a key `download` and a
            non-empty value, the static file is served for download.

            The path of the file on the server must be the joint of
            :attr:`dao.path` and :param:`path`. Otherwise, the access is denied.

            :param _: is included in the route so that in a page with the URL
                `/de/start` a relative URL `icons/shortcut.ico` can be
                used, because then the absolute version of this relative URL is:
                `/de/icons/shortcut.ico`.
            '''
            return static_file(
                    path,
                    dao.path,
                    download = bool(request.query.download))
