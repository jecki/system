import re
import traceback

from collections import deque
from urllib.parse import urlparse
from urllib.request import Request as Rq
from urllib.request import urlopen

from dao_io import get_connect

import __init__
import xmlhtml
from context import DBCon

def collect(
        scheme: str,
        netloc: str,
        path: str,
        addresses: 'Deque[Tuple[str, str, str]]',
        seen_addresses: 'Dict[Tuple[str, str], str]',
        headers: 'Dict[str, str]' = {'User-Agent': 'bot'},
        path_catalog_re = re.compile(
            r'^/((works|manuscript|prints)|(work|ms|print)/\d+)$'),
        path_transcriptions_re = re.compile(
            r'^/(transcriptions|(ms|print)/\d+/\d+/transcription/\d+)$'),
        url_norm_re = re.compile(r'href="(.*?)"'),
        url_absolutize_re = re.compile(r'/[^/]*/../'),
        ) -> 'Generator[Dict[str, str]]':
    '''
    - Read the URL from :param:`scheme`, :param:`netloc`, :param:`path`
      with :param:`headers` in the request. The scheme and netloc must
      be given; the path must either be empty or start with a slash.
    - Decode the response according to UTF-8. On any :class:`UnicodeError`
      during decoding, set the placeholder character.
    - Notify the realm of the content: catalog or glossary or transcriptions
      or nothing (the empty string).
    - Collect addresses having the same netloc as :param:`url`.
      Discard any query or fragment. Absolutize relative URLs.
      Add the addresses to :param:`addresses`. But leave out:

      - addresses in :param:`seen_addresses`, but do not distinguish between
        “http” and “https”.
      ############
      - Ohne Jordanus.
      - Ohne Beiseiten? Ohne welche Beiseiten?
      - Ohne Repro-Seiten.
      - Ohne Nicht-HTML-Seiten.
      - Aus Ersatztabelle nur URLs lesen, keinen Reintext aufnehmen!

    - Reintext auslesen.

      - Einschränken auf main-Element.
      - Jede Randnote aus Satz herausziehen und ganz ans Ende einzeln setzen.
      - Stückelung.

        - bis h-Element mit ID oder Note mit ID.
        - h-Element mit ID bis … s.o.
        - Note mit ID bis … s.o.

      - Für jedes Stück:

        - Inline-Tags ersatzlos streichen.
        - Sonstige durch Leerzeichen ersetzen.
        - HTML-Zeichenumschreibungen entschlüsseln.
        - .strip()
        - Jede Leerraumfolge zu je einem Leerzeichen mindern.
        - Unicodenormung
        - Einschübe löschen.

          - In Transkriptionen: /11r/ u.a.

    - Drei Lesarten erstellen?

      - Urtext.
      - .lower()
      - .lower & Diakritika löschen.
    '''
    parentpath = path.rsplit('/', 1)[0] + '/'
    if path == '/glossary' or path[:10] == '/glossary/':
        realm = 'glossary'
    elif path_catalog_re.match(path):
        realm = 'catalog'
    elif path_transcriptions_re.match(path):
        realm = 'transcriptions'
    else:
        realm = ''
    url = scheme + '://' + netloc + path
    text = urlopen(Rq(url, headers = headers)).read().decode(errors = 'replace')
    text = '<body' + text.split('<body', 1)[1]
    for match in url_norm_re.finditer(text):
        new_scheme, new_netloc, new_path, *_ = urlparse(match.group(1))
        new_netloc = new_netloc or netloc
        new_path = new_path.rstrip('/')
        if new_path and new_path[0] != '/':
            new_path = parentpath + new_path
        n = 1
        while n:
            new_path, n = url_absolutize_re.subn('/', new_path)
        if netloc != new_netloc:
            continue
        old_scheme = seen_addresses.get((new_netloc, new_path), '')
        if new_scheme == old_scheme or \
                {new_scheme, old_scheme} == {'http', 'https'}:
            continue
        ####################
        print(addresses)
        print(seen_addresses)
        exit(0)
        addresses.add((new_scheme, new_netloc, new_path))
        seen_addresses[(new_netloc, new_path)] = new_scheme
    ############
    ############
        yield (url, realm, text)

def iter_pages(start_url: str) -> 'Generator[Dict[str, str]]':
    log = file_io.Log(__file__)
    sys.stderr = log
    scheme, netloc, path, *_ = urlparse(start_url)
    path = path.rstrip('/')
    assert scheme
    assert netloc
    assert path.startswith('/') or not path
    start_address = (scheme, netloc, path)
    addresses = deque([start_address])        # updated in :func:`collect`.
    seen_addresses = {(netloc, path): scheme} # updated in :func:`collect`.
    while addresses:
        address = addresses.popleft()
        try:
            yield from collect(*address, addresses, seen_addresses)
        except Exception as e: # Sic.
            log.write('Exception in collecting', str(address) + ':', e)

def main(
        pid: int,
        config_path: str,
        repros_path: str,
        ) -> None:
    '''
    Make or refresh a fulltext table of the database for searching. Do it in an
    endless loop as follows:

    - Exit, when no process with :param`pid` is running anymore.
      This should be the process ID of the process having started this function.
    - ####################
    - Normalize the HTML to HTML-XML and remove some parts:

      - Leave out the elements head, style, script.
      - Leave out elements having an attribute role with the value “search”.
      - Leave out the tag html.

    - #####################
    - Start anew.

    Any occurring errors or issues are written into a :class:`file_io.Log`
    instance.

    :param config_path: absolute path to a file containing the configuration,
        e.g. the path to the very database being refreshed in this function.
    '''
    log = file_io.Log(__file__)
    sys.stderr = log
    config = file_io.get_config(config_path)
    repros_path = os.path.abspath(repros_path)
    paths = file_io.get_abspaths(config, config_path)
    start_url = config['urls']['start_url']
    connect = get_connect(config, paths['db_access_path'])
    with DBCon(connect()) as (connection, cur):
        # realm in tabelle einbeziehen: damit kann Suche eingeschränkt werden. Auch für Jordanus praktisch.
                r"""
                create table if not exists ft
                (
                ft_id        INT AUTO_INCREMENT,
                ft_url       VARCHAR(250), -- with fragment, if apt.
                ft_urtext    TEXT,         -- (LONGTEXT is not possible with fulltext index.) NFKDed?
                ft_grundtext TEXT,         -- (NFKD without diacritics and without non-word-characters)
                PRIMARY KEY (ft_id),
                CONSTRAINT c_ft_url_unique UNIQUE (ft_url)
                )
                ENGINE = MyISAM -- because on the server, an older version of MariaDB is installed.
                ;
                create unique index i_ft_url on ft (ft_url);
                create fulltext index i_ft_ur on ft (ft_urtext);
                create fulltext index i_ft_urgrund on ft (ft_urtext, ft_grundtext);
                """
        connection.commit()
    for url, realm, body in iter_pages(start_url):

def replace_element(
        tag: str,
        attrs: 'List[Tuple[str, Optional[str]]]',
        startend: bool,
        info: dict,
        ) -> 'Optional[str]':
    """
    Process :param:`tag` and :param:`attrs`.
    A return value of ``None`` does not change the element.
    If a string is returned, the element is replaced by it
    without escaping; if the empty string is returned, the
    element is deleted.
    If the element is void, :param:`startend` is ``True``.
    """
    attrs = dict(attrs)
    if tag in {'head', 'style', 'script'} or attrs.get('role', '') == 'search':
        return ''
    return None

def replace_tag(
        tag: str,
        attrs: 'List[Tuple[str, Optional[str]]]',
        startend: bool,
        info: dict,
        ) -> 'Tuple[str, List[Tuple[str, str]], bool]':
    """
    Process :param:`tag` and :param:`attrs`.
    If the tag is self-closing, :param:`startend` is ``True``.
    If the new tag shall be self-closing, set it to ``True``.
    """
    if tag == 'html':
        return '', attrs, startend
    return tag, attrs, startend

if __name__ == '__main__':
    if len(sys.argv) > 2:
        pid = int(sys.argv[1])
        config_path = sys.argv[2]
    else:
        # For testing.
        pid = os.getpid()
        config_path = os.path.abspath(__file__ + '/../../../../../../6.1.7 Projekte und Arbeitsplanung/Ptolemaeus/seite_ptolemaeus/configuration.utf-8.ini')
    main(pid, config_path, repros_path)
