% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2013 ff.
% # Template for HTML-XML-text which gets filled into the base template without
% # any internal modification. In the example edition:
% # http://localhost:8080/de/example/introduction.
<main>
{{!kwargs['text']}}
</main>\\
% rebase(kwargs.get('base', 'base.tpl'), request = request, dao = dao, kwargs = kwargs)