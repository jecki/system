# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2016 ff. (© http://badw.de)
import os
from functools import partial

from PIL import Image

import file_io

def convert(
        inpath: str, outpath: str, size: int, results: 'Any',
        convargs: 'Optional[Dict[str, Union[str, int, Tuple[float]]]]' = None,
        ) -> 'Tuple[int, Any]':
    '''
    Convert the image at :param:`inpath` to the format given by the file
    extension of :param:`outpath`. Save the image at :param:`outpath`.

    If :param:`convargs` is not ``None``: before saving, convert it with
    the key-value pairs of this argument. These pairs must accord to the
    keyword arguments of the following function:
    http://pillow.readthedocs.io/en/latest/reference/Image.html#PIL.Image.Image.convert
    E.g. ``convert(inpath, outpath, size, results, convargs = {'mode': 'RGB'}``.

    Add the size of this image to :param:`size`, which is an accumulator
    of the image sizes. Return both this size and :param:`results`. Both
    are kept for compatibility with :func:`file_io.run_over_files`.
    '''
    print(inpath)
    img = Image.open(inpath)
    if not convargs is None:
        img = img.convert(**convargs)
    img.save(file_io.pave(outpath))
    size += os.path.getsize(outpath)
    return size, results

def crop(
        inpath: str, outpath: str, size: int, results: 'Any',
        left: int = 1,
        right: int = 1,
        top: int = 1,
        bottom: int = 1,
        ) -> 'Tuple[int, Any]':
    '''
    Crop the image at :param:`inpath`.

    Add the size of this image to :param:`size`, which is an accumulator
    of the image sizes. Return both this size and :param:`results`. Both
    are kept for compatibility with :func:`file_io.run_over_files`.

    :param top: width (in pixels) of the top margin to be cut off.
    :param bottom: width (in pixels) of the bottom margin to be cut off.
    :param left: width (in pixels) of the left margin to be cut off.
    :param right: width (in pixels) of the right margin to be cut off.
    '''
    print(inpath)
    img = Image.open(inpath)
    width, height = img.size
    img = img.crop((left, top, width - right, height - bottom))
    img.save(file_io.pave(outpath))
    size += os.path.getsize(outpath)
    return size, results

def extract_from_PDF(
        inpath: str, outpath: str, size: int, results: 'Any',
        output_format: str = 'png',
        resolution: int = 300,
        ) -> 'Tuple[int, Any]':
    '''
    Extract the images from the PDF at :param:`inpath`, and save them in
    the format :param:`output_format`.

    Add the size of this image to :param:`size`, which is an accumulator
    of the image sizes. Return both this size and :param:`results`. Both
    are kept for compatibility with :func:`file_io.run_over_files`.

    :param resolution: Resolution (in dpi) used for reading the images.
    '''
    from wand.image import Image as WImage
    print(inpath)
    with WImage(filename = inpath, resolution = resolution) as original:
        with original.convert(output_format) as converted:
            outpath = file_io.pave(outpath)
            converted.save(filename = outpath)
            print('\t', outpath)
    size = -1
    return size, results

def reduce(
        inpath: str, outpath: str, size: int, results: 'Any',
        maxwidth: int = 0,
        maxheight: int = 0,
        maxlength: int = 1000,
        quality: int = 100,
        ) -> 'Tuple[int, Any]':
    '''
    Open the image at :param:`inpath`, and look up its width and height.

    If :param:`maxwidth` is greater than zero and less than the width of
    the image, reduce the width to :param:`maxwidth` and the length with
    the same ratio.

    After that, if :param:`maxheight` is greater than zero and less than
    the current height, reduce correspondingly.

    If both parameters, however, equal zero and if :param:`maxlength` is
    greater than zero and less than the longest axis, be it the width or
    the height, reduce the longer axis to :param:`maxlength`, and reduce
    the other side with the same ratio.

    Save the image (reduced or not) at :param:`outpath` with the quality
    of :param:`quality` (a value between 0 and 100).

    Add the size of this image to :param:`size`, which is an accumulator
    of the image sizes. Return both this size and :param:`results`. Both
    are kept for compatibility with :func:`file_io.run_over_files`.
    '''
    print(inpath)
    img = Image.open(inpath)
    width_old, height_old = img.size
    width = width_old
    height = height_old
    if maxwidth == maxheight == 0:
        maxlength_old = max((width_old, height_old))
        if maxlength_old > maxlength > 0:
            ratio = maxlength / maxlength_old
            width = round(ratio * width_old)
            height = round(ratio * height_old)
    else:
        if maxwidth > 0:
            if width_old > maxwidth:
                width = maxwidth
                height = round(height_old * maxwidth / width_old)
        if maxheight > 0:
            if height_old > maxheight:
                height = maxheight
                width = round(width_old * maxheight / height_old)
    if width != width_old or height != height_old:
        img = img.resize((width, height), resample = Image.ANTIALIAS)
    img.save(file_io.pave(outpath), quality = quality)
    size += os.path.getsize(outpath)
    return size, results
