# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
'''
Data access for the publication server of the Bavarian Academy of Sciences.
'''
import os
import xml.etree.ElementTree as ET

import __init__
import catalog_io
import file_io
import xmlhtml
from portal import dao_io

class Folder(dao_io.Folder):
    def __init__(
            self,
            config_path: str = __file__ + '/../../../../configuration.utf-8.ini',
            glosses_path: str = __file__ + '/../../../../glosses.xml',
            wal_mode: bool = True,
            is_refreshing: bool = True,
            ) -> None:
        '''
        The docstring of :class:`dao_io.Folder` is relevant.

        :param config_path: path to a file containing the configuration.
        :param glosses_path: path to the file containing all placeholder
            keywords and their corresponding translations, which are inserted
            into the served page according to the currently chosen language.
        '''
        # To use ET or xmlhtml in templates without import or parameter:
        self.ET = ET
        self.xmlhtml = xmlhtml

        self.multisite = False

        config_path = os.path.abspath(config_path)
        glosses_path = os.path.abspath(glosses_path)

        self.config = config = file_io.get_config(config_path)

        self.perma_url = config['perma_url']

        paths = file_io.get_abspaths(config, config_path)
        self.paths = paths
        self.path = paths['public_path'] + os.sep

        self.lang_ids = tuple(config['lang_ids'])
        self.default_page_id = tuple(config['default_page_id'])[0]
        self.default_lang_id = tuple(config['default_lang_id'])[0]
        if self.default_lang_id not in self.lang_ids:
            self.lang_ids += (self.default_lang_id,)
        # For compatibility with multisite instances:
        self.default_lang_ids = {'': self.default_lang_id}
        self.default_site_id = ''
        self.site_ids = ('',)

        self.glosses = self.read_glosses(glosses_path)

    def get_entry_by_lem_id(self, lem_id: str) -> str:
        with open(self.path + 'lemmata/' + lem_id, 'r', -1, 'utf-8') as file:
            return file.read()
