# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2017. (© http://badw.de)
'''See :func:`main`.'''
import xml.etree.ElementTree as ET
from collections import deque

try:
    import regex as re
except ImportError:
    import re

import __init__
import file_io
import xmlhtml

sub = re.sub

def convert(
        doc: str,
        smallcaps_cases: 'List[str]',
        endform: str,
        ) -> str:
    O = '[^<]*'
    I = '[^>]*'

    doc = sub(r'(?s)^.*?<table', '<table data-letter="a"', doc)

    doc = doc.rsplit('</table>', 1)[0] + '</table>'

    doc = sub(r'[\n ]+', ' ', doc)

    for case in smallcaps_cases:
        case = re.escape(case)
        doc = re.sub(f'(?<!\\w){case}(?!\\w)', make_smallcaps, doc)

    doc = sub(r'\s*\w(\xa0\w)+', make_spacing, doc)

    doc = sub(r'\s+', ' ', doc)

    doc = sub(r' █', '█', doc)
    doc = sub(r'█[█ ]+', '█', doc)
    # Remove tab-surrogate after and before <p>, if there is no doc between.
    doc = sub(rf'(<p{I}>(<[^p]{I}>)*)█', '\g<1>', doc)
    doc = sub(rf'█((<[^p]{I}>)*</p>)', '\g<1>', doc)

    doc = tidy(doc)

    doc = sub(r'(?s)<p\b.*?</p>', make_tds, doc)

    doc = sub(
            r'(?s)((?:<tr data-new-row="">.*?</tr> ?)+)',
            '<table data-new-table="">\g<1></table>',
            doc)
    doc = doc.replace(' data-new-row=""', '')

    doc = tidy(doc)

    doc = sub(
            rf'(?P<pre> *<tr{I}><td{I}>{O}#(?P<letter>.)#)',
            '</table>\n\n<table data-letter="\g<letter>">\n\g<pre>',
            doc)

    root = ET.fromstring(f'<root>{doc}</root>')
##    for row in root.findall('./table/tr'):
##        first = row.find('./td[1]')
##        if first is None:
##            print(xmlhtml.elem2string(row))
##            root.remove(row)
##        else:
##            first.set('style', 'display:none')
    heading = root.find('./table/tr')
    for cell in heading.findall('./td'):
        cell.tag = 'th'
    doc = xmlhtml.elem2string(root, inner = True).strip()
    for old, new in (
            ('</th>', '</th>\n '),
            ('</td>', '</td>\n '),
            ('</tr>', '</tr>\n'),
            ):
        doc = doc.replace(old, new)

    return doc

def make_smallcaps(match: 'Match[str]') -> str:
    term = match.group().title()
    term = f'<span class="form_sigle">{term}</span>'
    return term

def make_spacing(match: 'Match[str]') -> str:
    term = match.group()
    term = term.replace('\xa0', '')
    term = f'<span class="form_sperrung">{term}</span>'
    return term

def make_tds(match: 'Match[str]') -> str:
    term = match.group()
    if '█' not in term:
        return term
    open_tags = deque([('td', [])])
    items = deque([['td', [], '', '', '', '', '', '']])
    for start, attrs, end, text, com, pi, decl, undecl in xmlhtml.itemize(term):
        if start:
            open_tags.append((start, attrs))
            items.append([start, attrs, end, text, com, pi, decl, undecl])
        elif end:
            last, _ = open_tags.pop()
            if end != last:
                raise Exception(f'{end} != {last}')
            items.append([start, attrs, end, text, com, pi, decl, undecl])
        elif text:
            if '█' not in text:
                items.append([start, attrs, end, text, com, pi, decl, undecl])
            else:
                texts = text.split('█')
                items.append(['', [], '', texts[0], '', '', '', ''])
                for text in texts[1:]:
                    for tag, _ in reversed(open_tags):
                        items.append(['', [], tag, '', '', '', '', ''])
                    for tag, attrs in open_tags:
                        items.append([tag, attrs, '', '', '', '', '', ''])
                    items.append(['', [], '', text, '', '', '', ''])
    for tag, _ in reversed(open_tags):
        items.append(['', [], tag, '', '', '', '', ''])
    term = xmlhtml.serialize(items)
    term = f'<tr data-new-row="">{term}</tr>'
    return term

def replace_element(
        tag: str,
        attrs: 'List[Tuple[str, Optional[str]]]',
        startend: bool,
        info: dict,
        ) -> 'Optional[str]':
    """
    Process :param:`tag` and :param:`attrs`.
    A return value of ``None`` does not change the element.
    If a string is returned, the element is replaced by it
    without escaping; if the empty string is returned, the
    element is deleted.
    If the element is void, :param:`startend` is ``True``.
    """
    if tag in {'head', 'script', 'style'}:
        return ''
    return None

def replace_tag(
        tag: str,
        attrs: 'List[Tuple[str, Optional[str]]]',
        startend: bool,
        info: dict,
        ) -> 'Tuple[str, List[Tuple[str, str]], bool]':
    """
    Process :param:`tag` and :param:`attrs`.
    If the tag is self-closing, :param:`startend` is ``True``.
    If the new tag shall be self-closing, set it to ``True``.
    """
    css = dict(attrs).get('style', '').strip(';')
    if css:
        rules = [ tuple(strip(rule.split(':'))) for rule in css.split(';') ]
        rules = dict(rules)
        if rules.get('top', '').strip().startswith('-'):
            if tag == 'span':
                tag = 'sup'
            else:
                raise Exception(f'{tag}, {attrs}')

    styloids = dict(attrs).get('class', '').split()
    styles = (
            'ebene1normal',
            'ebene1klein',
            'ebene2normal',
            'ebene2klein',
            'ebene3normal',
            'ebene3klein',
            )
    for style in styles:
        if any( styloid.startswith(style) for styloid in styloids ):
            break
    else:
        style = ''

    if style:
        tag = 'p'
        attrs = [('class', style)]
    elif tag in {'i', 'sup', 'table', 'tr', 'td'}:
        attrs = []
    else:
        tag = ''

    return tag, attrs, startend

def smallcaps_sortkey(item: str) -> 'Tuple[int, str]':
    return (1 / len(item), item)

def strip(terms: 'Sequence[str]') -> 'Generator[str]':
    for term in terms:
        yield term.strip()

def tidy(doc: str) -> str:
    doc = sub(
            r'<(?P<tag>p|i|sup|)\b[^>]*>(?P<space> ?)</(?P=tag)>',
            '\g<space>',
            doc)
    doc = sub(r'\s+', ' ', doc)
    doc = sub(r' (?P<tag></(table|tr|th|td|p)>)', '\g<tag>', doc)
    doc = sub(r'(?P<tag></(table|tr|th|td|p)>) ', '\g<tag>', doc)
    doc = sub(r'(?P<tag><(table|tr|th|td|p)[^/>]*>) ', '\g<tag>', doc)
    return doc

def main(
        urpath: str,
        urencoding: str,
        smallcaps_path: str,
        endform: str,
        ):
    with open(urpath, 'r', encoding = urencoding) as file:
        doc = file.read()
    with open(smallcaps_path, 'r', encoding = 'utf-8') as file:
        smallcaps_cases = list(strip(file.read().strip().splitlines()))
    smallcaps_cases = sorted(smallcaps_cases, key = smallcaps_sortkey)
    doc = xmlhtml.normalize(
            doc,
            replace_element             = replace_element,
            replace_tag                 = replace_tag,
            )
    doc = convert(doc, smallcaps_cases, endform)
    file_io.write_tell(urpath, doc, 'utf-8')

if __name__ == '__main__':
    main(
            urpath = r'Z:\Ablage2013\6 IT\6.1 Digitalisierung\6.1.7 Projekte und Arbeitsplanung\TLL\Index TLL\Index TLL_IT.htm',
            urencoding = 'cp1252',
            smallcaps_path = r'Z:\Ablage2013\6 IT\6.1 Digitalisierung\6.1.7 Projekte und Arbeitsplanung\TLL\Index TLL\Kapitälchenersetzungen.txt',
            endform = 'html',
            )
