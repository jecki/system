% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
% entry = kwargs['entry']
<main>
<article class="sheet sheetwide">
	<h1>{{entry['place_name']}}, {{entry['print_publisher']}}, {{entry['print_date']}}</h1>
	<p class="flush_unhyph"><b>Title page</b>: {{!entry['print_title']}}{{!'&#8193;<b>Last page:</b> ' + entry['print_last_page'] if entry['print_last_page'] else ''}}</p>
	<p class="flush_unhyph">{{!entry['print_content']}}</p>
  % if entry['print_exemplar']:
	<p class="flush_unhyph"><b class="label">Exemplar</b>&#8193;{{!entry['print_exemplar']}}</p>
  % end
  % if entry['print_note']:
	% paragraphs = entry['print_note'].split('\n')
	% if len(paragraphs) == 1:
	<p class="flush_unhyph"><b class="label">Note</b>&#8193;{{!entry['print_note']}}</p>
	% else:
	  % for rank, paragraph in enumerate(paragraphs, start = 1):
	<p class="flush_unhyph"><b class="label">Note&#160;{{rank}}</b>&#8193;{{!paragraph}}</p>
	  % end
	% end
  % end
	<p class="flush_unhyph"><b class="label">Lit.</b>&#8193;{{!entry['print_bibliography']}}</p>
	<table class="parts">
	  % for printpart in entry['printparts']:
	  %     scan_link = printpart.get('scan_link', '')
	  %     tran_link = printpart.get('tran_link', '')
		<tr id="{{printpart['part_id']}}" role="row">
			<th scope="row">
				<p title="part id: {{printpart['part_id']}}">{{printpart['pagina_range']}}</p>
			  % if scan_link and tran_link:
				<p><a class="key key_dark" href="{{scan_link}}">Scans|Text</a></p>
			  % elif scan_link:
				<p><a class="key key_dark" href="{{scan_link}}">Scans</a></p>
			  % elif tran_link:
				<p><a class="key key_dark" href="{{tran_link}}">Text</a></p>
			  % end
			</th>
			<td>
				<p class="flush_unhyph">{{!printpart['printpart_quotation']}}</p>
				<p class="flush_unhyph">=&#8239;<a href="/work/{{printpart['work_id']}}">{{'' if 'Anonymous' in printpart['person_name'] else printpart['person_name'] + ', '}}{{!dao.format_title(printpart['work_title'])}} ({{printpart['work_siglum']}})</a>{{!printpart['printpart_note']}}</p>
			</td>
		</tr>
	  % end
	</table>
  % end
</article>
</main>\\
% rebase('base.tpl', request = request, dao = dao, kwargs = kwargs)