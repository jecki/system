# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
'''
Data access for the publication server of the Bavarian Academy of Sciences.
'''
import json
import os
import re
import sqlite3
import subprocess
import sys
import tempfile
import time
import xml.etree.ElementTree as ET
from datetime import datetime, timezone
from html import unescape
from operator import itemgetter
from urllib.parse import quote, unquote

import __init__
import catalog_io
import file_io
import parse
import sys_io
import xmlhtml
from compare import get_sortnumber
from context import DBCon
from portal import dao_io

child = ET.SubElement

SQL_CREATE_INDEX_TIMESTAMP = """
        create index if not exists i_timestamp on files (timestamp)
        """
SQL_CREATE_INDEX_pub_id = """
        create index if not exists i_pub_id on files (pub_id)
        """
SQL_CREATE_TABLE_FILES = """
        create table if not exists files (
            pub_id    REFERENCES publications(pub_id) ON DELETE CASCADE,
            name      TEXT UNIQUE,
            timestamp REAL
        )
        """
SQL_CREATE_TABLE_PUBLICATIONS = """
        create table if not exists publications (
            pub_id              TEXT PRIMARY KEY,
            pub_catid           TEXT,
            personstring        TEXT,
            linked_personstring TEXT,
            title               TEXT,
            sorttitle           TEXT,
            place               TEXT,
            publisher           TEXT,
            year                TEXT,
            seriesstring        TEXT,
            serial_num          TEXT
        )
        """
SQL_DELETE_FILE_BY_NAME = """
        delete from files where name = ?
        """
SQL_DELETE_PUBLICATION_BY_PUB_ID = """
        delete from publications where pub_id = ?
        """
SQL_GET_PUB_IDS = """
        select pub_id from publications
        """
SQL_GET_PUBLICATION_BY_PUB_ID = """
        select * from publications where pub_id = ?
        """
SQL_GET_PUBLICATIONS = """
        select * from publications
        order by year DESC, seriesstring ASC
        """
SQL_GET_FILES_BY_PUB_ID = """
        select name from files where pub_id = ?
        order by name
       """
SQL_GET_FILENAMES = """
        select name from files
        """
SQL_GET_FILENAMES_BY_TIMESTAMP_END = """
        select name from files
        where timestamp <= ?
        order by timestamp
       """
SQL_GET_FILENAMES_BY_TIMESTAMP_START_END = """
        select name from files
        where timestamp >= ? and timestamp <= ?
        order by timestamp
       """
SQL_GET_FILENAMES_BY_TIMESTAMP_START = """
        select name from files
        where timestamp >= ?
        order by timestamp
       """
SQL_INSERT_OR_REPLACE_PUBLICATION = """
        insert or replace into publications values (
            ?,
            ?,
            ?,
            ?,
            ?,
            ?,
            ?,
            ?,
            ?,
            ?,
            ?
        )
        """
SQL_INSERT_OR_IGNORE_FILE = """
        insert or ignore into files
        values (
            ?,
            ?,
            ?
            )
        """

class DB(dao_io.Folder):
    def __init__(
            self,
            config_path: str =
                file_io.join(__file__, '../../../configuration.utf-8.ini'),
            glosses_path: str =
                file_io.join(__file__, '../../../glosses.xml'),
            is_refreshing: bool = True,
            wal_mode: bool = True,
            ) -> None:
        '''
        The docstring of :class:`dao_io.Folder` is relevant.
        If :param:`is_refreshing` is ``True``, the initialization makes a new
        exchange database if none is present at the path that is specified in
        the configuration, and starts :func:`refresh_forever`.

        :param config_path: path to a file containing the configuration.
        :param glosses_path: path to the file containing all placeholder
            keywords and their corresponding translations, which are inserted
            into the served page according to the currently chosen language.
        :param wal_mode: Only relevant, if :param:`is_refreshing` is ``True``.
            See the docstring of :func:`make_new_empty_db_if_absent`.
        '''
        # To use ET or xmlhtml in templates without import or parameter:
        self.ET = ET
        self.xmlhtml = xmlhtml

        self.multisite = False

        config_path = os.path.abspath(config_path)
        glosses_path = os.path.abspath(glosses_path)

        self.config = config = file_io.get_config(config_path)

        self.perma_url = config['pub_link']['url'].rstrip('/')
        self.publisher_name = config['publisher']['name']
        self.publisher_place = config['publisher']['place']

        paths = file_io.get_abspaths(config, config_path)
        self.paths = paths
        self.path = paths['public_path'] + '/'
        self.json_path = paths['json_path']
        self.meta_path = paths['meta_path']
        self.repros_path = paths['repros_path'] + '/'
        self.temp_bibl_path = paths['temp_bibl_path'] + '/'

        self.filename_re = re.compile(config['filename_re']['pattern'])
        self.end_re = re.compile(config['filename_re']['remove_end'])

        self.id_id_map = config['id_id_map']
        self.del_from_id_re = re.compile(config['del_from_id_re']['pattern'])

        self.redirected_links = {}
        for ident, link in config['redirected_links'].items():
            prefix, link = link.split('/', 1)
            self.redirected_links[link] = ident
        self.redirected_links_prefix = prefix

        self.lang_ids = tuple(config['lang_ids'])
        self.default_page_id = tuple(config['default_page_id'])[0]
        self.default_lang_id = tuple(config['default_lang_id'])[0]
        if self.default_lang_id not in self.lang_ids:
            self.lang_ids += (self.default_lang_id,)
        # For compatibility with multisite instances:
        self.default_lang_ids = {'': self.default_lang_id}
        self.default_site_id = ''
        self.site_ids = ('',)

        self.glosses = self.read_glosses(glosses_path)

        self.roles = config['roles']
        self.series_titles = config['series_titles']
        self.id_map = config['id_map']

        self.catalog = catalog_io.Catalog(config['catalog']['query_url'])
        self.query_key = config['catalog']['query_key']
        self.catid_template = config['catalog']['catid_template']
        self.refresh_pause = int(config['refresh_time']['pause_seconds'])
        self.refresh_cycle = int(config['refresh_time']['cycle_length'])

        if is_refreshing:
            make_new_empty_db_if_absent(self.meta_path, wal_mode)
            self.process = subprocess.Popen(
                    [
                        sys.executable,
                        __file__,
                        str(os.getpid()),
                        config_path,
                    ],
                    )

    def get_citation(
            self,
            pub_entry: 'Tuple',
            cite_form: str,
            lang_id: str = '',
            unescaping: bool = False,
            tag_re: 'Pattern[str]' = re.compile(r'<.*?>'),
            ) -> bytes:
        '''
        From the information in :param:`pub_entry`, get a citation in the format
        :param:`cite_form`.

        :param lang_id: identifier of the language used in the context, in which
            the citation will be embedded, used to customize expressions with
            the help of ``self.glosses``. If not found in ``self.lang_ids``,
            the expression of ``self.default_lang_id`` will be selected.
        :param unescaping: whether the resulting citation is to be left escaped,
            so that it can be embedded in a webpage, or is to be unescaped,
            so that it can be delivered as download.
        :param tag_re: regular expression for removing all tags.
        '''
        (
            pub_id,
            pub_catid,
            personstring,
            linked_personstring,
            title,
            sorttitle,
            place,
            publisher,
            year,
            seriesstring,
            serial_num,
        ) = (
                # Replace em- and en-spaces with normal spaces.
                str(entry).replace(' ', ' ').replace(' ', ' ')
                for entry in pub_entry
            )
        infix = '/{}/'.format(lang_id) if lang_id else '/'
        personstring = tag_re.sub('', personstring)
        if cite_form not in ('html_table', 'txt', 'ris', 'bib'):
            cite_form = 'txt'
        if cite_form == 'html_table':
            unescaping = False
            links = []
            with DBCon(sqlite3.connect(self.meta_path)) as (connection, cur):
                cur.execute(SQL_GET_FILES_BY_PUB_ID, (pub_id,))
                files = cur.fetchall()
            for (name,) in files:
                try:
                    size = os.path.getsize(self.repros_path + name)
                except OSError:
                     # The file is inaccessible at the moment.
                    continue
                match = self.filename_re.search(name)
                if not match:
                    continue
                end_of_retention = match.group('end') or ''
                lic = match.group('lic') or 'BY'
                part = match.group('part') or ''
                if part:
                    part += ' · '
                gestalt = match.group('gestalt')
                if not gestalt:
                    # It is a subfolder containing e.g. images of a publication.
                    continue
                links.append(
                        (
                        '<li class="card">'
                            '<span style="color:#444444">{av_after}: {end}. {gestalt} · {s}&#xa0;kB</span> '
                            '<small>({glosses_license}:&#xa0;<a href="https://creativecommons.org/licenses/{lic_url}/4.0/legalcode{lang}">CC&#x202F;{lic}</a>)</small>'
                        '</li>'
                        if end_of_retention else
                        '<li class="card">'
                            '<a href="/{lang_id}/{name_url}"><strong>{part}{gestalt} · {s}&#xa0;kB</strong></a> '
                            '<small>({glosses_license}:&#xa0;<a href="https://creativecommons.org/licenses/{lic_url}/4.0/legalcode{lang}">CC&#x202F;{lic}</a>)</small>'
                        '</li>'
                        ).format(
                            av_after = self.glosses['publication_available_after'][lang_id],
                            end = end_of_retention,
                            lang_id = lang_id,
                            name_url = quote(name),
                            part = part,
                            gestalt = gestalt,
                            s = round(size / 1000),
                            glosses_license = self.glosses['license'][lang_id],
                            lic_url = lic.lower(),
                            lic = lic,
                            lang = '.de' if lang_id == 'de' else '',
                        ))
            access = '<ul>{}</ul>'.format(''.join(links))
            citation = '''	<table class="mapping">
			<tr><th scope="row">Permalink</th><td><a href="{PERMA_DOMAIN}/{lang_id}/{pub_id}">{PERMA_DOMAIN}/{lang_id}/{pub_id}</a></td></tr>
			<tr><th scope="row">{glosses_author_s}</th><td>{linked_personstring}</td></tr>
			<tr><th scope="row">{glosses_title}</th><td>{title}</td></tr>
			<tr><th scope="row">{glosses_place}</th><td>{place}</td></tr>
			<tr><th scope="row">{glosses_year}</th><td>{year}</td></tr>
			<tr><th scope="row">{glosses_series}</th><td>{seriesstring}</td></tr>
			<tr><th scope="row">{glosses_serial_num}</th><td>{serial_num}</td></tr>
			<tr><th scope="row">{glosses_link}</th><td><a href="http://gateway-bayern.de/{pub_catid}">http://gateway-bayern.de/{pub_catid}</a></td></tr>
			<tr><th scope="row">{glosses_access}:</th><td>{access}</td></tr>
		</table>'''.format(
                    glosses_author_s = self.glosses['author_s'][lang_id],
                    glosses_title = self.glosses['title'][lang_id],
                    glosses_place = self.glosses['place'][lang_id],
                    glosses_year = self.glosses['year'][lang_id],
                    glosses_series = self.glosses['series'][lang_id],
                    glosses_serial_num = self.glosses['serial_num'][lang_id],
                    glosses_link = self.glosses['link_to_catalog'][lang_id],
                    glosses_access = self.glosses['access'][lang_id],
                    PERMA_DOMAIN = self.perma_url,
                    lang_id = lang_id,
                    pub_id = pub_id,
                    linked_personstring = linked_personstring,
                    title = title,
                    place = place,
                    year = year,
                    seriesstring = seriesstring,
                    serial_num = serial_num,
                    pub_catid = pub_catid,
                    access = access,
                    )
        elif cite_form == 'txt':
            # Join the main sections with an em-space (Unicode x2003) and
            # the subsections with an en-space (Unicode x2002).
            # Purpose: Avoid ambiguity.
            citation = '. '.join(
                    filter(None, (
                        (personstring + ':') if personstring else '',
                        title,
                        ' '.join(filter(None, (place, year))),
                        ': '.join(filter(None, (seriesstring, serial_num))),
                        self.perma_url + infix + pub_id,
                        ))).replace(':. ', ': ', 1)
        elif cite_form == 'ris':
            citation = '''TY - GEN
AU - {personstring}
T1 - {title}
CY - {place}
PY - {year}
T3 - {seriesstring}
VL - {serial_num}
UR - {PERMA_DOMAIN}{infix}{pub_id}
ER -
'''.format(
                    personstring = personstring.replace('; ', '\r\nAU - '),
                    title = title,
                    place = place,
                    year = year,
                    seriesstring = seriesstring,
                    serial_num = serial_num,
                    PERMA_DOMAIN = self.perma_url,
                    infix = infix,
                    pub_id = pub_id,
                    )
        elif cite_form == 'bib':
            bibtexkey = ((
                    ''.join(
                    name.split(',')[0] for name in personstring.split(';')
                    ) if personstring else '_') + year).replace(' ', '')
            citation = '''@misc&#123;{bibtexkey},
author  = "{personstring}",
title   = "{title}",
year    = "{year}",
address = "{place}",
series  = "{seriesstring}",
volume  = "{serial_num}",
url     = "{PERMA_DOMAIN}{infix}{pub_id}",
&#125;'''.format(
                    bibtexkey = bibtexkey,
                    personstring = personstring.replace('"', '{"}').replace('; ', ' and '),
                    title = title.replace('"', '{"}'),
                    place = place.replace('"', '{"}'),
                    year = year.replace('"', '{"}'),
                    seriesstring = seriesstring.replace('"', '{"}'),
                    serial_num = serial_num.replace('"', '{"}'),
                    PERMA_DOMAIN = self.perma_url.replace('"', '{"}'),
                    infix = infix,
                    pub_id = pub_id.replace('"', '{"}'),
                    )
        if unescaping:
            citation = unescape(citation)
        return citation.replace('\u202F', ' ').replace('\u2009', ' ').encode()

    def get_entry(self, pub_id: str, pub_catid: str
            ) -> '''Tuple[
            Optional[ET.Element],
            Iterable[Tupel[str, str, str, str, str]],
            Tuple]''':
        '''
        Get bibliographical information about the publication :param:`pub_id`
        from :param:`catalog`. Within this catalog, the publication is
        identified by :param:`pub_catid`. Return the whole entry as received
        from the catalog, the person-related information and a tuple of values
        extracted from the entry.
        '''
        entry = self.catalog.get_entry(self.query_key + '=' + pub_catid)

        if entry is None or not self.catalog.get_pub_id(entry):
            return (None, (), ())

        year = self.catalog.get_year(entry)
        year = (re.findall(r'\d\d\d\d?\d?', year) or [year])[0]

        place = self.catalog.get_place(entry)
        publisher = self.catalog.get_publisher(entry)

        title, sorttitle = catalog_io.format_title_sorttitle(
                *self.catalog.get_titles(entry))

        series = self.catalog.get_series_id_serial_num(entry)
        series = {
                sid: snum for svals in series.values() for sid, snum in svals }
        if series:
            # Look for a seriesstring first in the order given in the
            # configuration (which is preserved because configparser uses an
            # ordered dictionary).
            for series_id, seriesstring in self.series_titles.items():
                if series_id in series:
                    serial_num = series[series_id]
                    break
            else:
                # If no matching series ID is found in the configuration,
                # take any series ID of the catalog entry as a seriesstring.
                series_id, serial_num = series.popitem()
                seriesstring = series_id
        else:
            series_id = serial_num = seriesstring = ''

        persons = list(self.catalog.get_personal_names_ids_roles(entry))
        return entry, persons, (
                pub_id,
                pub_catid,
                title,
                sorttitle,
                place,
                publisher,
                year,
                seriesstring,
                serial_num,
                )

    def get_ersatzindex(self, lang_id: str) -> 'Tuple[str, bytes, str]':
        '''
        Get an ersatz for the datatable index. The latter one needs Javascript,
        the former one does not. Return:

        - the template id specifying the default template.
        - the ersatz as xHTML5 table in bytes.
        - the text language id, which is just `lang_id` in this case.
        '''
        with open(self.json_path, 'r', encoding = 'utf-8') as file:
            table = json.load(file)['data']
        table = sorted(
                ([ (value['_'] if isinstance(value, dict) else value).encode()
                    for key, value in sorted(row.items()) ]
                for row in table ), reverse = True)
        html = b'<table class="form mapping non-source"><tr><td>'\
                + b'</td></tr><tr><td>'.join(
                    b'</td><td>'.join(cells) for cells in table )\
                + b'</td></tr></table>'
        return 'text.tpl', html, lang_id

    def get_filenames_by_time(
            self,
            start: str,
            end: str,
            ) -> 'List[Tuple[str]]':
        '''
        Get the names of all files whose most recent modification date is
        greater than or the same as :param:`start` and lesser than or the
        same as :param:`end`.

        If :param:`start` is an empty string, there is no lower bound. If
        :param:`end` is an empty string, there is now upper bound. If not
        empty, the parameters must comply with the extended format of the
        ISO 8601 specification.
        '''
        if start:
            start = parse.iso2date(start)\
                    .replace(tzinfo = timezone.utc).timestamp()
        if end:
            end = parse.iso2date(end, (None, 12, None, 23, 59, 59, 999999))\
                    .replace(tzinfo = timezone.utc).timestamp()
        with DBCon(sqlite3.connect(self.meta_path)) as (connection, cur):
            if start and end:
                cur.execute(SQL_GET_FILENAMES_BY_TIMESTAMP_START_END,
                        (start, end))
            elif start:
                cur.execute(SQL_GET_FILENAMES_BY_TIMESTAMP_START, (start,))
            elif end:
                cur.execute(SQL_GET_FILENAMES_BY_TIMESTAMP_END, (end,))
            else:
                cur.execute(SQL_GET_FILENAMES)
            return cur.fetchall()

    def get_pub_text_html(self, filename: str) -> 'Tuple[str, str]':
        try:
            with open(self.repros_path + filename, 'rb') as infile:
                text = infile.read()
        except OSError:
            text = b''
        return text, self.default_lang_id

    def get_personstring(
            self,
            persons: 'Iterable[Tupel[str, str, str, str, str]]',
            ) -> str:
        '''
        Combine the information in :param:`persons` to a string that gives
        authorship information.
        '''
        names = []
        for name, name_enum, name_add, person_id, role in persons:
            name, *rest = name.split(',', 1)
            rest = ',' + rest[0] if rest else ''
            name = name + rest
            name = name.replace(', ', ',\u202F') # narrow no-break space
            name = name.replace(' ', '\u2009') # thin space
            if name_enum:
                name += '\u202F' + name_enum
            if name_add:
                name += ' ({})'.format(name_add)
            role = self.roles.get(role, '')
            if role == '0' or not name:
                continue
            elif role != '':
                role = ' [{}]'.format(role)
            name_role = '{}{}'.format(name, role)
            names.append(name_role)
        return ' ; '.join(names)

    def get_personstrings(
            self,
            persons: 'Iterable[Tupel[str, str, str, str, str]]'
            ) -> 'Tuple[str, str]':
        '''
        Combine the information in :param:`persons` to two strings:

        - a string that gives authorship information and is styled with
          HTML tags.
        - the same with a link derived from the GND, if present.
        '''
        names = []
        linked_names = []
        for name, name_enum, name_add, person_id, role in persons:
            name, *rest = name.split(',', 1)
            rest = ',' + rest[0] if rest else ''
            name = '<b>{}</b>'.format(name) + rest
            name = name.replace(', ', ',\u202F') # narrow no-break space
            name = name.replace(' ', '\u2009') # thin space
            if name_enum:
                name += '\u202F' + name_enum
            if name_add:
                name += ' ({})'.format(name_add)
            role = self.roles.get(role, '')
            if role == '0' or not name:
                continue
            elif role != '':
                role = ' [{}]'.format(role)
            name_role = '{}{}'.format(name, role)
            names.append(name_role)
            if person_id:
                linked_name = '{0} '\
                    '<a class="ref" href="http://d-nb.info/gnd/{1}">DNB</a>'\
                    .format(name_role, person_id)
            else:
                linked_name = name_role
            linked_names.append(linked_name)
        return ('; <br/>'.join(names), '; <br/>'.join(linked_names))

    def get_publication_by_pub_id(
            self,
            pub_id: str
            ) -> 'Optional[Tuple[str, ...]]':
        '''
        For the publication identified by :param:`pub_id` in the interchange
        database, get the entry with the bibliographical data about this
        publication.
        '''
        with DBCon(sqlite3.connect(self.meta_path)) as (connection, cur):
            cur.execute(SQL_GET_PUBLICATION_BY_PUB_ID, (pub_id,))
            return cur.fetchone()

    def make_temp_fileentry_marcxml(
            self,
            full_entry: 'ET.Element',
            pub_entry: 'Tuple',
            persons: 'Iterable[Tupel[str, str, str, str, str]]',
            filename: str,
            lic: str,
            timestamp: float,
            mapping_file_extensions_to_MARC_IDs_for_non_text:
                'Dict[str, str]' = {},
            role776: str = 'Reproduktion von',
            ) -> None:
        '''
        Adapt :param:`full_entry` reflecting that it does no longer originate
        from the external catalog (where the bibliographical data are stored)
        but now from our own publication website, when it is sent on requests
        e.g. via the OAI API used by the DNB to harvest our publications. For
        the adaption, already extracted and possibly adapted values are found
        in :param:`pub_entry`. The publication date is to be derived from the
        :param:`timestamp` of the file :param:`filename`; :param:`lic` is the
        license.

        Save the adapted entry as an XML fragment in :attr:`.temp_bibl_path`;
        for the filename, take :param:`filename`, but change its extension to
        ``'.xml'``.

        :param mapping_file_extensions_to_MARC_IDs_for_non_text: If there are
            publications which are not text, their MARC ID in the leader must
            not be ``'a'``. This mapping must contain the extensions of files
            of a different media type and the *jeweils* belonging MARC ID.
        :param role776: value to be inserted into MARC field 776, subfield i,
            if the publication is a retro-digitization. If not, this field is
            skipped anyways.

        .. note::
            About the adaptation:

            - Copy certain fields from :param:`full_entry` into a new element.
            - Copy and modify certain other fields: Leader. Controlfields
            - Add certain fields: about the publisher, the URL of file, rights
              regarding the use of the file, the original publication (if this
              is a retro-digitization).

            See e.g. `<http://nbn-resolving.de/urn:nbn:de:101-2017011938>`_.
        '''
        (
            pub_id,
            pub_catid,
            title,
            sorttitle,
            place,
            publisher,
            year,
            seriesstring,
            serial_num,
        ) = pub_entry
        is_retro = pub_id[0] in '0123456789'
        personstring = self.get_personstring(persons)
        pub_year = str(datetime.fromtimestamp(timestamp).year).zfill(4)[:4]
        file_ext = os.path.splitext(filename)[1]
        medium = mapping_file_extensions_to_MARC_IDs_for_non_text[file_ext]\
                if file_ext in mapping_file_extensions_to_MARC_IDs_for_non_text\
                else 'a' # Text
        status = 'm' # Monographie
        leader = xmlhtml.get_text(
                full_entry, './/marc:record/marc:leader',
                namespaces = {'marc': self.catalog.xmlns_default})
        leader = leader[:6] + medium + status + leader[8:] #TODO: Other changes?
        marc007 = 'cr|||||' # c: Elektronische Ressource; r: Fernzugriff.
        marc008 = xmlhtml.get_text(full_entry, './/*[@tag="008"]')
        if is_retro:
            old_year = marc008[7:11]
            marc008 = marc008[:6] + 'r' + pub_year + old_year + marc008[15:]
        else:
            # The following should just yield marc008 again, but we need
            # consistency with the field 264, subfield c in any case.
            marc008 = marc008[:6] + 's' + pub_year + '||||' + marc008[15:]
        record = ET.Element('record')
        child(record, 'leader').text = leader
        child(record, 'controlfield', {'tag': '007'}).text = marc007
        child(record, 'controlfield', {'tag': '008'}).text = marc008
        e = child(record, 'datafield', {'tag': '856', 'ind1': '4', 'ind2': '0'})
        child(e, 'subfield', {'code': 'u'}).text = self.perma_url + '/' + quote(
                filename)
        child(e, 'subfield', {'code': 'x'}).text = 'Transfer-URL'
        e = child(record, 'datafield', {'tag': '540', 'ind1': ' ', 'ind2': ' '})
        ##child(e, 'subfield', {'code': 'a'}).text = lic if lic else 'CC BY'
        ##e = child(record, 'datafield', {'tag': '264', 'ind1': ' ', 'ind2': '1'})
        child(e, 'subfield', {'code': 'a'}).text = self.publisher_name
        child(e, 'subfield', {'code': 'b'}).text = self.publisher_place
        child(e, 'subfield', {'code': 'c'}).text = pub_year
        if is_retro:
            e = child(record, 'datafield', {'tag':'776','ind1':' ','ind2':' '})
            child(e, 'subfield', {'code': 'a'}).text = personstring
            child(e, 'subfield', {'code': 'd'}).text = ', '.join(
                    (place, publisher, year))
            child(e, 'subfield', {'code': 'i'}).text = role776
            child(e, 'subfield', {'code': 't'}).text = title
        if seriesstring:
            e = child(record, 'datafield', {'tag':'490','ind1':'0','ind2':' '})
            child(e, 'subfield', {'code': 'a'}).text = seriesstring
            child(e, 'subfield', {'code': 'v'}).text = serial_num
        for num in (
                '041', # Sprachcode
                '082', # Notation nach der Dewey-Decimal-Classification
                '084', # Andere Notation (Klassifikationen / Thesauri)
                '100', # Personenname
                '110', # Körperschaftsname
                '245', # Titelangabe
                '246', # Titelvarianten
                '502', # Dissertationsvermerk
                '650', # Nebeneintragung unter einem Schlagwort - Sachschlagwort
                '653', # Indexierungsterm - nicht normiert
                '700', # Nebeneintragung - Personenname
                '710', # Nebeneintragung - Körperschaftsname
                ):
            for e in full_entry.iterfind('.//*[@tag="{}"]'.format(num)):
                xmlhtml.strip_namespaces(e)
                record.append(e)
        xmlhtml.indent(record)
        file_io.pave_dir(self.temp_bibl_path)
        with tempfile.NamedTemporaryFile(
                  'wb',
                  dir = self.temp_bibl_path,
                  delete = False,
                  ) as file:
            file.write(ET.tostring(record, encoding = 'utf-8'))
            name = file.name
        os.replace(name, self.temp_bibl_path + filename + '.xml')

def delete_entries_without_files(
        meta_path: str,
        repros_path: str,
        filename_re: 'Pattern[str]',
        ) -> None:
    '''
    Delete any bibliographical entry and any filename entry in the database
    at :param:`meta_path` for which no corresponding reproduction file
    in :param:`repros_path` exists.
    '''
    names = set()
    pub_ids = set()
    for name in os.listdir(repros_path):
        match = filename_re.search(name)
        if match:
            names.add(name)
            pub_ids.add(match.group('pub_id'))
    with DBCon(sqlite3.connect(meta_path)) as (connection, cur):
        cur.execute(SQL_GET_PUB_IDS)
        for (pub_id,) in cur.fetchall():
            if pub_id not in pub_ids:
                cur.execute(SQL_DELETE_PUBLICATION_BY_PUB_ID, (pub_id,))
                connection.commit()
        cur.execute(SQL_GET_FILENAMES)
        for (name,) in cur.fetchall():
            if name not in names:
                cur.execute(SQL_DELETE_FILE_BY_NAME, (name,))
                connection.commit()
        try:
            cur.execute("vacuum repros")
            connection.commit()
        except:
            pass # Sic.

def make_new_empty_db_if_absent(
        meta_path: str,
        wal_mode: bool,
        ) -> None:
    '''
    Make a new emtpy database at :param:`meta_path`, if none is
    present there or if the one present there is not readable.

    .. important::
        In the latter case, :param:`meta_path` is **replaced**
        by the new empty database.

    :param wal_mode: tells whether the WAL mode (i.e. write ahead logging)
        is used while operating on the SQLite database.

    .. important::
        WAL mode may be impossible e.g. on network drives.
        But non-WAL mode is not usable for production,
        because any write process blocks all read processes
        long enough to make the response fail.
    '''
    def build_db(wal_mode):
        with DBCon(sqlite3.connect(meta_path)) as (connection, cur):
            if wal_mode:
                cur.execute('PRAGMA journal_mode=WAL')
                print('A new SQLite db was built in WAL mode.')
            cur.execute('PRAGMA encoding = "UTF-8"')
            cur.execute(SQL_CREATE_TABLE_PUBLICATIONS)
            cur.execute(SQL_CREATE_TABLE_FILES)
            connection.commit()
    try:
        build_db(wal_mode)
    except sqlite3.DatabaseError:
        # Maybe the file at ``meta_path`` is not well formed
        # or the WAL mode does not match.
        os.remove(meta_path)
        build_db(wal_mode)
        print(
                'A DatabaseError occurred while building the SQLite db.'\
                ' Maybe the file at {} was not well-formed or the WAL mode'\
                ' ({}) did not match. The file was removed and built anew.'
                .format(meta_path, wal_mode))

def make_new_json_file(meta_path: str, json_path: str) -> None:
    '''
    Make a new json file caching the metadata so that they can directly be
    fed into the table of publications shown on the website.
    Extract the data from the database at :param:`meta_path` and store the
    json file at :param:`json_path`.
    '''
    retention_note = ' <b class="altattr" data-de="(Gesperrt bis {0})" '\
                                         'data-en="(Blocked until {0})"></b>'
    with tempfile.NamedTemporaryFile(
              'w',
              encoding = 'utf-8',
              dir = os.path.dirname(json_path),
              delete = False,
              ) as file:
        file.write('{"data":[\n')
        with DBCon(sqlite3.connect(meta_path)) as (connection, cur):
            file.write(',\n'.join(
                    json.dumps(
                        {
                        '0': year,
                        '1': seriesstring,
                        '2': {
                            '_': serial_num,
                            'sort': get_sortnumber(serial_num),
                            },
                        '3': personstring,
                        '4': {
                            '_': '<a href="../{}" class="source">{}</a>'.format(
                                pub_id, title),
                            'sort': sorttitle,
                            },
                        })
                    for (
                        pub_id,
                        pub_catid,
                        personstring,
                        linked_personstring,
                        title,
                        sorttitle,
                        place,
                        publisher,
                        year,
                        seriesstring,
                        serial_num
                        ) in cur.execute(SQL_GET_PUBLICATIONS) ))
        file.write('\n]}')
        tempname = file.name
    os.replace(tempname, json_path)

def refresh_forever(pid: int, config_path: str) -> None:
    '''
    Refresh the temporary database and the temporary files that store the
    metadata for the table of publications and for the pages dedicated to
    one specific publication:

    - Read the configuration given in :param:`config_path`.
    - Delete any entry (and its affiliated entries) that has no belonging
      reproduction.
    - Refresh the JSON file feeding the table of publications.
    - Exit, if no process with :param`pid` is running anymore.
    - Consult the library catalog; refresh the database and the XML files:

      - For any internal publication ID (used in the permalink), find the
        belonging catalog ID.
      - Get the catalog data for this entry, refresh the database with it
        and make an XML file of it for the OAI interface.
      - If no entry is returned:

        - The catalog may be unavailable at the moment. Skip this ID, try
          later. Make a log entry, which enables the second step:
        - The catalog ID may have been deleted: It permanently appears in
          the log. Human intervention is required to find the new catalog
          id and to adapt the ``[id_map]`` in the configuration.

    - Refresh the JSON file again.
    - Sleep a while.
    - Exit, if no process with :param`pid` is running anymore.
    - Start anew.

    :param config_path: absolute path to a file containing the configuration,
        e.g. the path to the very database being refreshed in this function.
    '''
    log = file_io.Log(__file__)
    sys.stderr = log
    sys.stdout = log
    dao = DB(config_path = config_path, is_refreshing = False)
    current_cycle = 1
    while True:
        delete_entries_without_files(
                dao.meta_path, dao.repros_path, dao.filename_re)
        make_new_json_file(dao.meta_path, dao.json_path)
        with DBCon(sqlite3.connect(dao.meta_path)) as (connection, cur):
            cur.execute(SQL_GET_FILENAMES)
            filenames_in_db = set(map(itemgetter(0), cur.fetchall()))
        num_of_changes = 0
        for name in os.listdir(dao.repros_path):
            path = dao.repros_path + name
            if not sys_io.pid_exists(pid):
                sys.exit(0)
            match = dao.filename_re.search(name)
            if not match:
                continue
            end_of_retention = match.group('end') or ''
            if current_cycle < dao.refresh_cycle and name in filenames_in_db:
                continue
            if end_of_retention:
                try:
                    enddate = datetime.strptime(end_of_retention, '%Y-%m-%d')
                except ValueError:
                    # ``end_of_retention`` cannot be understood as real date.
                    continue
                else:
                    if enddate <= datetime.utcnow():
                        # Force change of st_mtime, so that the API knows about
                        # the actual publication date (which is now).
                        os.utime(path)
                        os.replace(path,
                                dao.repros_path + dao.end_re.sub('', name))
                        end_of_retention = ''
            gestalt = match.group('gestalt')
            if not gestalt:
                # It is a subfolder containing e.g. images of a publication.
                continue
            pub_id = match.group('pub_id') or ''
            lic = match.group('lic') or ''
            pub_catid = dao.id_map.get(
                    pub_id, dao.catid_template.format(pub_id = pub_id))
            full_entry, persons, pub_entry = dao.get_entry(pub_id, pub_catid)
            # We do not want to overwrite an existing entry if ``pub_entry`` is
            # empty. That is the case when the catalog is unavailable.
            if full_entry and pub_entry:
                timestamp = os.stat(path, follow_symlinks = False).st_mtime
                if not end_of_retention:
                    dao.make_temp_fileentry_marcxml(
                            full_entry, pub_entry, persons, name, lic, timestamp
                            )
                personstr, linked_personstr = dao.get_personstrings(persons)
                pub_entry = pub_entry[:2] + (
                        personstr, linked_personstr) + pub_entry[2:]
                with DBCon(sqlite3.connect(dao.meta_path)) as (connection, cur):
                    cur.execute(
                            SQL_INSERT_OR_REPLACE_PUBLICATION, pub_entry
                            )
                    cur.execute(
                            SQL_INSERT_OR_IGNORE_FILE, (pub_id, name, timestamp)
                            )
                    connection.commit()
                num_of_changes += 1
                # Renew the json file after every nth change:
                if num_of_changes % 20 == 0:
                    make_new_json_file(dao.meta_path, dao.json_path)
            else:
                log.write('\n{}: The catalog makes no reply to {}.'.format(
                        pub_id, pub_catid))
        make_new_json_file(dao.meta_path, dao.json_path)
        if current_cycle == dao.refresh_cycle:
            current_cycle = 1
        else:
            current_cycle += 1
        end = time.time() + dao.refresh_pause
        while time.time() < end:
            if not sys_io.pid_exists(pid):
                sys.exit(0)
            time.sleep(2)
            # This loop is done instead of e.g. time.sleep(refresh_pause)
            # in order to avoid blocking and enable cancelling with ctrl-c.

if __name__ == '__main__':
    pid = int(sys.argv[1]) or os.getpid()
    config_path = sys.argv[2]
    refresh_forever(pid, config_path)
