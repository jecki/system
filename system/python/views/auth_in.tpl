% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2013 ff.
% # Template for a fulltext search form; gets filled into the base template.
% # In the example edition: http://localhost:8080/portal/auth_in.
% lang_id = kwargs.get('lang_id', '')
<main>
<article class="card non-source" role="search">
	<h1>{{dao.glosses['login'][lang_id]}}</h1>
	<form action="auth_in" method="post">
		<div class="table">
			<div>
				<label for="username">{{dao.glosses['username'][lang_id]}}:</label>
				<input aria-label="{{dao.glosses['login'][lang_id]}}" autofocus="" name="username" type="text"/>
			</div>
			<div>
				<label for="password">{{dao.glosses['password'][lang_id]}}:</label>
				<input aria-label="{{dao.glosses['password'][lang_id]}}" name="password" type="password"/>
			</div>
		</div>
		<p><button type="submit">{{dao.glosses['login'][lang_id]}}</button></p>
	</form>
</article>
</main>
% rebase('base.tpl', request = request, dao = dao, kwargs = kwargs)
