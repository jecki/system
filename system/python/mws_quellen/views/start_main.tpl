% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2013 ff.
% # Template for a portal overview; gets filled into the base template.
% # In the example edition: http://localhost:8080 (showing only one edition in
% # the row of editions).
% from re import compile
% regex = compile(b'<a[ /][^>]*?>')
% lang_id = kwargs.get('lang_id', '')
<main class="non-source flexcol">
% for target_site_id in dao.site_ids:
 % template_id, text, text_lang_id = dao.get_template_id_and_text(lang_id, target_site_id, dao.default_page_ids[target_site_id])
 % if text:
   % article = dao.ET.fromstring(text.decode()).find('article')
   % text = dao.xmlhtml.elem2string(article).encode() if article else b''
   % text = regex.sub(b'', text)
   % text = text.replace(b'</a>', b'')
   % text = text.replace(b'style="background:url(', b'style="background:url(/~/' + target_site_id.encode() + b'/')
   % text = text.replace(b' property="', b' data-property="')
   % text = text.strip()
   % if text:
<a class="card" style="max-width:700px" href="/{{lang_id}}/{{target_site_id}}/{{dao.default_page_ids[target_site_id]}}">
	<small>{{!text}}</small>
</a>
   % end
 % end
% end
% rebase('base.tpl', request = request, dao = dao, kwargs = kwargs)
</main>
