# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2013 ff.
import mmap
import os

import __init__
import file_io
from portal import dao_io

class Folder(dao_io.Folder):
    def __init__(
            self,
            path: str = __file__ + '/../../../../seite_mws_quellen/public',
            glosses_path: str = __file__ + '/../../../../glosses.xml',
            multisite: bool         = True,
            default_lang_id: str    = '',
            default_site_id: str    = 'portal',
            indices_folder_id: str  = 'indices',
            fulltext_index_id: str  = 'fulltext',
            config_page_id: str     = '_config',
            download_folder_id: str = 'opensource',
            download_file_id: str   = 'portal.zip',
            paths_for_download: 'List[str]' = [
                __file__ + '/../../portal',
                __file__ + '/../../views',
                __file__ + '/../../bottle.py',
                __file__ + '/../../bottleapp.py',
                __file__ + '/../../file_io.py',
                __file__ + '/../../rdf.py',
                __file__ + '/../../structs.py',
                __file__ + '/../../xhtml.py',
                __file__ + '/../../../cssjs',
                __file__ + '/../../../../glosses.xml',
                __file__ + '/../../../../seite_portal',
                ],
            ):
        '''
        The docstring of :class:`dao_io.Folder` is relevant.
        The docstring of :meth:`dao_io.Folder.__init__` is relevant.
        In addition:

        :param download_folder_id: name of a subfolder in the default folder.
            This subfolder will contain the extract of the system along with
            sample content. That is meant for download.
        :param download_file_id: name of the zipfile that will be saved in
            :param:`download_folder_id` and contain the extract of the sytem.
            If a file at this place already exists, it will be overwritten.
        :param paths_for_download: paths to be included in the aforementioned
            extract of the system with sample content.
        '''
        super().__init__(
                path = path,
                glosses_path = glosses_path,
                multisite = multisite,
                default_lang_id = default_lang_id,
                default_site_id = default_site_id,
                indices_folder_id = indices_folder_id,
                fulltext_index_id = fulltext_index_id,
                config_page_id = config_page_id
                )

        download_path = os.path.abspath(os.path.join(
                self.path, default_site_id, download_folder_id, download_file_id
                ))
        print('\nI save an example of my system for download at:\n{}\n'.format(
                download_path))
        file_io.zip_paths(paths_for_download, download_path)
        print('\n(Done.)\n\n')
