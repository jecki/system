# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
import os
import pip
import tkinter as tk
import tkinter.ttk as ttk
from collections import deque
from collections import OrderedDict
from functools import partial
from tkinter import filedialog, messagebox

import PyPDF2 as pypdf

import __init__
import file_io
import gui

class Window(tk.Tk):
    '''
    GUI for running certain tasks related to the publication website
    http://publikationen.badw.de of the Bavarian Academy of Sciences.
    '''
    def __init__(self):
        tasks = (
                self.add_metadata_specify,
                )
        self.metadata = {
        # Important: The key values, e.g. `/Lizenz_des_Textes`,
        # must start with a `/` and must not contain any whitespace.
                'license of content: CC BY 4.0': (
                    '/Lizenz_des_Textes',
                    'http://creativecommons.org/licenses/by/4.0'),
                'license of digital copy: CC0': (
                    '/Lizenz_des_Digitalisates',
                    'https://creativecommons.org/publicdomain/zero/1.0/'),
                }
        super().__init__()
        self.title('PDF-Metadata')
        frame = gui.lay(self, ttk.Frame, 0, 0, 1, 1)
        label = gui.lay(frame, ttk.Label, 0, 0, 1, 1,
                {'text': 'Please, click a task (or quit):'})
        self.tasks = tuple(
                (task,
                ' '.join((task.__doc__ or '').split('\n\n')[0].strip().split()))
                for task in tasks )
        for i, (task, description) in enumerate(self.tasks, start = 1):
            button = gui.lay(frame, ttk.Button, i, 0, 1, 1,
                    {'text': description})
            for event in ('<1>', '<Return>'):
                button.bind(event, task)

    def add_metadata(
            self,
            dirpath: str,
            frame: ttk.Frame,
            listbox: tk.Listbox,
            items_all_mark: str,
            event: tk.Event,
            ) -> None:
        '''See :meth:`self.metadata_specify`.'''
        items = { listbox.get(i) for i in listbox.curselection() }
        if items_all_mark in items:
            metadata = OrderedDict(sorted(self.metadata.values()))
        else:
            metadata = OrderedDict( self.metadata[item] for item in items
                    if item in self.metadata )
        label = gui.lay(frame, ttk.Label, 0, 0, 2, 1, {'text': 'Please, wait …'})
        self.update_idletasks()
        shared_outpath, size, results = file_io.run_over_files(
                shared_inpath = dirpath,
                include_in_inpaths_re = r'(?i)\.pdf$',
                results = deque(),
                function = partial(add_metadata, metadata = metadata),
                )
        # :param:`results` is an iterable of names of skipped files.
        frame.destroy()
        self.update_idletasks()
        ########## TODO: Message about outpath and skipped files ("results").

    def add_metadata_specify(self, event: tk.Event) -> None:
        '''
        Add metadata to all PDFs.

        Lay out the form for entering the specifications.
        '''
        items_all_mark = '[all]'
        dirpath = get_dirpath()
        if dirpath == '':
            return
        items = sorted(self.metadata.keys())
        frame = gui.lay(self, ttk.Frame, 0, 0, 1, 1, xweight = 1, yweight = 1)
        label = gui.lay(frame, ttk.Label, 0, 0, 1, 1, {'text': 'Specify:'})
        subframe = gui.lay(frame, ttk.Frame, 1, 0, len(self.tasks), 1,
                xweight = 1, yweight = 1)
        gui.lay(subframe, ttk.Label, 0, 0, 1, 1, {'text':
                 'Choose an item (multiple fields can be chosen by holding Ctrl'
                 ' while clicking):'})
        listbox = gui.lay(subframe, tk.Listbox, 1, 0, 1, 1,
                {'selectmode': 'extended'}, xweight = 1, yweight = 1)
        listbox.insert('end', items_all_mark)
        for item in items:
            listbox.insert('end', item)
        gui.lay_scrollbars(subframe, listbox, x_offset = 1)
        subsubframe = gui.lay(subframe, ttk.Frame, 0, 2, 2, 1, padx = 6)
        submit = gui.lay(subsubframe, ttk.Button, 4, 0, 1, 1,
                {'text': 'Submit & Start'}, pady = (5, 2))
        for event in ('<1>', '<Return>'):
            submit.bind(event, partial(
                    self.add_metadata,
                    dirpath,
                    frame,
                    listbox,
                    items_all_mark,
                    ))
        cancel = gui.lay(subsubframe, ttk.Button, 5, 0, 1, 1,
                {'text': 'Cancel', 'command': frame.destroy})
        self.update_idletasks()

def add_metadata(
        inpath: str,
        outpath: str,
        size: int,
        results: 'Deque[str]',
        metadata: 'Dict[str, str]' = {},
        ) -> 'Tuple[int, Deque[str]]':
    '''
    Add :param:`metadata` to the PDF file :param:`inpath` and save this file at
    :param:`outpath`. For the other parameters, see
    :func:`file_io.run_over_files`.
    '''
    merger = pypdf.PdfFileMerger()
    try:
        with open(inpath, 'rb') as infile:
            merger.append(infile)
            merger.addMetadata(metadata)
            merger.write(file_io.pave(outpath))
    except: # *All* exceptions must be caught here.
        results.append(inpath)
    return size, results

def get_dirpath() -> str:
    '''
    Let choose a folder containing the files to be processed.
    On cancel, the empty string is returned.
    '''
    return os.path.normcase(filedialog.askdirectory(
            initialdir = '.',
            title = 'Select the folder containing the files to be processed.'\
                    ' USE THE BUTTON. Double-click cannot work in this case!',
            mustexist = 1))

def main() -> None:
    window = Window()
    window.mainloop()

if __name__ == '__main__':
    main()
