# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
import os
import json
import urllib.parse

from collections import deque
from datetime import datetime

import __init__
import bottle
from bottle import HTTPError, redirect, request, static_file, template
import file_io
from file_io import log_ip_medium

class App(bottle.Bottle):
    '''
    A bottle application object.
    See the docstring of :func:`bottleapp.get` in :path:`../../bottleapp.py`.
    '''
    def __init__(
            self,
            dao_class: type,
            dao_kwargs: 'Dict[str, Any]',
            https: bool,
            host: str,
            port: str,
            debug: bool,
            server: str,
            views_paths: 'Sequence[str]',
            cssjs_path: str,
            ):
        super().__init__()
        self.https = https
        self.host = host
        self.port = port
        self.debug = debug
        self.server = server
        self.cssjs_path = os.path.abspath(cssjs_path) + os.sep
        self.views_paths = [ os.path.abspath(p) + os.sep for p in views_paths ]
        bottle.TEMPLATE_PATH = self.views_paths
        self.log = file_io.Log(
                __file__,
                log_folder_path = '../../../../../seite_ptolemaeus_zugriffe',
                log_file_name = \
                    datetime.now().isoformat().replace(':', '').split('.')[0] +
                    '--.log')
        self.dao = dao_class(**dao_kwargs)
        self.set_routes(self.dao)

    def log_access(self, request: 'bottle.LocalRequest') -> None:
        agent = request.environ.get('HTTP_USER_AGENT', '').split(None, 1)[0]
        if agent != 'bot':
            log_ip_medium(
                    self.log,
                    request.remote_addr,
                    '█' + agent + '█' + request.path
                    )

    def run(self) -> None:
        '''
        Run :param:`self` by calling :meth:`bottle.run`. Before or in this call,
        you may apply middleware, e.g. instead of mere ``app = self``, pass
        ``app = SessionMiddleware(self, self.dao.session_options)``.
        '''
        bottle.run(
                app = self,
                host = self.host,
                port = self.port,
                debug = self.debug,
                server = self.server(
                    self.host,
                    self.port,
                    **{
                        key:
                            (self.dao.paths.get(name, '') if self.https else '')
                        for key, name in (
                            ('certfile', 'ssl_cert_path'),
                            ('keyfile', 'ssl_key_path'),
                            ('chainfile', 'ssl_chain_path'),
                            )
                        }
                    )
                )

    def set_routes(self, dao) -> None:
        '''
        Define the request-handling functions and assign them to routes.
        '''
        @self.route('<path:path>/')
        def redirect_endslash(path):
            '''
            Redirect requests whose URL ends with `/`: Ignore the trailing `/`.
            '''
            redirect(path)

        @self.route('/')
        def redirect_short_url():
            '''
            Redirect requests which specify no page:
            Redirect to the default page :attr:`dao.default_page_id`.

            The redirected request will be dealt with by :func:`.return_page`.
            Further explanation is found in the docstring of that function.
            '''
            redirect('/' + dao.default_page_id)

        @self.route('/api/get_num_of_mss')
        def return_num_of_mss():
            return str(dao.get_num_of_mss())

        @self.route('/cssjs/<path:path>')
        def return_cssjs(path):
            '''
            Return a CSS or JS file for client-side styling or processing.
            For static content files, see :func:`.return_static_content`.

            Example URLs are:

            - `/cssjs/jquery/jquery.js`
            - `/cssjs/normalize/normalize.css`

            The path of the file on the server must be the joint of
            :attr:`self.cssjs_path` and :param:`path`.
            Otherwise, the access is denied.
            '''
            return static_file(path, self.cssjs_path)

        @self.route('/ersatzindex/<page_id>')
        def return_ersatzindex(page_id):
            '''
            Return an ersatz for the datatable that would otherwise be shown
            on the page :param:`page_id`.
            '''
            table = deque('<table class="form mapping non-source">')
            exists = False
            for row in dao.get_index(page_id):
                exists = True
                table.append('<tr><td>' + '</td><td>'.join(
                        val if isinstance(val, str) else val['_']
                        for _, val in sorted(row.items())
                        ) + '</td></tr>')
            if not exists:
                raise HTTPError(404, 'The page has not been found.')
            table.append('</table>')
            self.log_access(request)
            return template(
                    'text.tpl', request = request, dao = dao,
                    kwargs = {
                        'lang_id': 'en-PAL',
                        'text_lang_id': 'en-PAL',
                        'text': ''.join(table),
                        'ersatzlink': '',
                        })

        @self.route('/glossary/<concept_id>/<expression>')
        def return_glossary_entry(concept_id, expression):
            '''
            Return the lemma with the concept :param:`concept_id`
            and the expression :param:`expression`.
            '''
            lemma = dao.get_lemma(
                    concept_id,
                    expression,
                    item_id = None,
                    with_transl_froms = True,
                    with_transl_intos = True,
                    with_synonyms = True,
                    )
            if lemma:
                return template(
                        'lemma.tpl', request = request, dao = dao,
                        kwargs = {'lemma': lemma})
            else: raise HTTPError(404, 'The page has not been found.')

        @self.route('/indices/<kind>.json')
        def return_index(kind):
            '''
            Return JSON for a Datatable of e.g. MSS (or other entities,
            specified by :param:`kind`).
            '''
            return json.dumps(
                    {'data': tuple(dao.get_index(kind))},
                    separators = (',', ':'))

        @self.route('/jordanus')
        def redirect_short_url_jordanus():
            '''
            Redirect requests which specify no page:
            Redirect to the default page :attr:`dao.default_page_id`.

            The redirected request will be dealt with by :func:`.return_page`.
            Further explanation is found in the docstring of that function.
            '''
            redirect('/jordanus/' + dao.default_page_id)

        @self.route('/jordanus/indices/<kind>.json')
        def return_index_jordanus(kind):
            '''
            Return JSON for a Datatable of e.g. authors (or other entities,
            specified by :param:`kind`) of Jordanus.
            '''
            return json.dumps(
                    {'data': tuple(dao.get_index_jordanus(kind))},
                    separators = (',', ':'))

        @self.route('/jordanus/ms/<ms_id>')
        def return_ms_jordanus(ms_id):
            '''
            Return a catalog entry of Jordanus.
            '''
            entry = dao.get_ms_jordanus(ms_id)
            if entry:
                return template(
                        'ms_jordanus.tpl', request = request, dao = dao,
                        kwargs = {
                            'lang_id': 'en-PAL',
                            'entry': entry,
                            })
            else: raise HTTPError(404, 'The page has not been found.')

        @self.route('/jordanus/search')
        def return_search_form_jordanus():
            '''
            Return a page with a search form for the catalog of Jordanus.
            '''
            return template(
                    'search_form_jordanus.tpl', request = request, dao = dao,
                    kwargs = {'lang_id': 'en-PAL'})

        @self.route('/jordanus/search_results')
        def return_search_results_jordanus():
            '''
            Return a page containing the results of a search triggered by
            :func:`.return_search_form_jordanus`.
            '''
            return template(
                    'search_results_jordanus.tpl', request = request, dao = dao,
                    kwargs = {'lang_id': 'en-PAL'})

        @self.route('/jordanus/<page_id>')
        def return_page_jordanus(page_id):
            '''
            Return the page specified by the URL, of the site Jordanus.

            If :param:`page_id` does not denote a page of the site,
            raise the HTTP error 404 (`Not found`).
            '''
            template_id, text, text_lang_id = dao.get_template_id_and_text(
                    site_id = 'jordanus', page_id = page_id)
            if text:
                if text[:3] == b'\xef\xbb\xbf':
                    text = text[3:]
                self.log_access(request)
                return template(
                        template_id, request = request, dao = dao,
                        kwargs = {
                            'lang_id': 'en-PAL',
                            'base': 'base_jordanus.tpl',
                            'text_lang_id': text_lang_id,
                            'text': text,
                            })
            else: raise HTTPError(404, 'The page has not been found.')

        @self.route('/jobs')
        def return_jobs():
            '''
            Return a page describing the current job positions, announcing
            open jobs and providing a link to the jobs archive.
            '''
            general, vacancies = dao.get_jobs_information()
            return template(
                    'jobs.tpl', request = request, dao = dao,
                    kwargs = {
                        'general': general,
                        'vacancies': vacancies,
                        })

        @self.route('/jobs_archive')
        def return_jobs_archive():
            '''
            Return a page listing current and former vacancies.
            '''
            return template(
                    'jobs_archive.tpl', request = request, dao = dao,
                    kwargs = {'vacancies': dao.get_jobs()})

        @self.route('/job/<job_id>')
        def return_job(job_id):
            '''Return a page describing a current or a former vacancy.'''
            vacancies = dao.get_jobs(job_id = job_id)
            if vacancies:
                return template(
                        'job.tpl', request = request, dao = dao,
                        kwargs = {'vacancy': vacancies[0]})
            else: raise HTTPError(404, 'The page has not been found.')

        @self.route('/job')
        def redirect_ms(): redirect('/jobs')

        @self.route('/ms')
        def redirect_ms(): redirect('/manuscripts')

        @self.route('/news')
        def return_news():
            '''Return a page of dates, headers and links of all news items.'''
            return template(
                    'news.tpl', request = request, dao = dao,
                    kwargs = {'news': dao.get_news()})

        @self.route('/news/<news_id>')
        def return_news_item(news_id):
            '''
            Return a page for one news item specified by :param:`news_id`.
            '''
            news_item = dao.get_news_item(news_id)
            if news_item:
                return template(
                        'news_item.tpl', request = request, dao = dao,
                        kwargs = {'news_item': news_item})
            else: raise HTTPError(404, 'The page has not been found.')

        @self.route('/pal/<path:path>')
        def redirect_old_pal(path):
            '''
            Redirect requests for the old site:
            Redirect to the default page :attr:`dao.default_page_id`.

            The redirected request will be dealt with by :func:`.return_page`.
            Further explanation is found in the docstring of that function.
            '''
            redirect('/' + dao.default_page_id)

        @self.route('/project')
        def return_project():
            '''Return a page showing outline and description of the project.'''
            return template(
                    'project.tpl', request = request, dao = dao,
                    kwargs = {'project': dao.get_project()})

        @self.route('/remote_static/<path:path>')
        def return_remote_static_content(path):
            '''
            Similar to :func:`return_static_content`, but with another
            base folder (artefact of web2py).
            '''
            return static_file(
                    path,
                    dao.remote_static_path,
                    download = bool(request.query.download))

        @self.route('/repros/<path:path>')
        def return_repro(path):
            '''
            Return a scan (facsimile) located at :param:`path`,
            which is a relative path starting at :attr:`dao.repros_path`.
            '''
            return static_file(
                    urllib.parse.unquote(path),
                    dao.repros_path,
                    download = bool(request.query.download))

        @self.route('/start')
        def return_start():
            '''Return the startpage.'''
            text = dao.get_starttext()
            self.log_access(request)
            return template(
                    'start.tpl', request = request, dao = dao,
                    kwargs = {'text': text})

        @self.route('/team')
        def return_team():
            '''Return a page showing the team.'''
            return template(
                    'team.tpl', request = request, dao = dao,
                    kwargs = {'team': dao.get_team()})

        @self.route('/team/<user_id>')
        def return_user(user_id):
            '''Return a page describing the user :param:`user_id`.'''
            user = dao.get_user(user_id)
            if user and user['group_role'] != 'Former Researcher':
                return template(
                        'user.tpl', request = request, dao = dao,
                        kwargs = {'user': user})
            else: raise HTTPError(404, 'The page has not been found.')

        @self.route('/work')
        def redirect_work(): redirect('/works')

        @self.route('/<page_id>')
        def return_page(page_id):
            '''
            Return the page specified by the URL.

            If :param:`page_id` does not denote a page of the site,
            raise the HTTP error 404 (`Not found`).
            '''
            template_id, text, text_lang_id = dao.get_template_id_and_text(
                    page_id = page_id)
            if text:
                if text[:3] == b'\xef\xbb\xbf':
                    text = text[3:]
                self.log_access(request)
                return template(
                        template_id, request = request, dao = dao,
                        kwargs = {
                            'lang_id': 'en-PAL',
                            'text_lang_id': text_lang_id,
                            'text': text,
                            'ersatzlink': '/ersatzindex/' + page_id
                                    if template_id == 'index.tpl' else '',
                            })
            else: raise HTTPError(404, 'The page has not been found.')

        @self.route('/<doc_type:re:(ms|print|work)>/<doc_id>')
        def return_full_entry(doc_type, doc_id):
            '''
            Return one full catalog entry.
            '''
            template_id, entry = dao.get_template_id_and_entry(doc_type, doc_id)
            if entry:
                self.log_access(request)
                return template(
                        template_id, request = request, dao = dao,
                        kwargs = {'entry': entry})
            else: raise HTTPError(404, 'The page has not been found.')

        @self.route('/<doc_type:re:(ms|print)>/<doc_id>/<part_id>/transcription')
        def redirect_tran(doc_type, doc_id):
            redirect('/transcriptions')

        @self.route('/<doc_type:re:(ms|print)>/<doc_id>/<part_id>/<pagina>')
        def return_repro_entry(doc_type, doc_id, part_id, pagina):
            '''
            Return a page with the facsimile or the transcription (or both)
            belonging to the print or manuscript belonging to the given type,
            IDs and pagina.
            '''
            entry = dao.get_repro_entry(doc_type, doc_id, part_id, pagina)
            if entry:
                try:
                    img_scale = float(request.query['img_scale'])
                except: # Sic.
                    img_scale = 1
                self.log_access(request)
                return template(
                        'repro.tpl', request = request, dao = dao,
                        kwargs = {
                            'entry': entry,
                            'img_scale': img_scale,
                            'pagina': pagina,
                            })
            else: raise HTTPError(404, 'The page has not been found.')

        @self.route('/<doc_type:re:(ms|print)>/<doc_id>/<part_id>/transcription/<page_id>')
        def return_tran(doc_type, doc_id, part_id, page_id):
            '''
            Return the page :param:`page_id` of the fulltext transcription
            of the part :param:`part_id` of the print or manuscript with the
            ID :param:`doc_id`.
            '''
            text = dao.get_tran(doc_type, doc_id, part_id, page_id)
            if text:
                if text[:3] == b'\xef\xbb\xbf':
                    text = text[3:]
                self.log_access(request)
                return template(
                        'transcription.tpl', request = request, dao = dao,
                        kwargs = {'text': text})
            else: raise HTTPError(404, 'The page has not been found.')

        @self.route('/<doc_type:re:(ms|print)>/<doc_id>/<part_id>/transcription/<path:path>')
        def return_static_content_of_tran(doc_type, doc_id, part_id, path):
            '''
            Return a static content file, e.g. an image file,
            that is part of a transcription.

            If the URL contains a query string with a key `download` and a
            non-empty value, the static file is served for download.
            '''
            return static_file(
                    dao.get_repro_tran_path(doc_type, doc_id, part_id, path),
                    dao.repros_path,
                    download = bool(request.query.download))

        @self.route('/<path:path>')
        def return_static_content(path):
            '''
            Return a static content file, e.g. an image file.

            Example URLs are:

            - `/icons/shortcut.ico`
            - `/acta/secreta/secretissima/1890-03-20.pdf?download=true`

            If the URL contains a query string with a key `download` and a
            non-empty value, the static file is served for download.

            The path of the file on the server must be the joint of
            :attr:`dao.path` and :param:`path`. Otherwise, the access is denied.
            '''
            return static_file(
                    path,
                    dao.path,
                    download = bool(request.query.download))
