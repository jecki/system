import os
import re
import sys
from copy import copy

import openpyxl as op

import __init__
import file_io

def copy_rows(rows, ws_new):
    '''
    Copy :param:`rows` of cells into the worksheet :param:`ws_new`.
    '''
    for r, row in enumerate(rows, start = 1):
        for c, cell in enumerate(row, start = 1):
            cell_new = ws_new.cell(row = r, column = c, value = cell.value)
            if cell.has_style:
                # In the following, if you do not use :func:`copy`,
                # openpyxl throws an error.
                cell_new.alignment = copy(cell.alignment)
                cell_new.border = copy(cell.border)
                cell_new.font = copy(cell.font)
                cell_new.fill = copy(cell.fill)
                cell_new.number_format = copy(cell.number_format)
                cell_new.protection = copy(cell.protection)
    for col_cells in ws_new.columns:
        length = max( len(str(cell.value or '')) for cell in col_cells )
        ws_new.column_dimensions[col_cells[0].column].width = length

def main(
        filepath: str = '',
        filename_re: 'Pattern[str]' = re.compile(r'(?i)^\s*stellenplan\s*\d'),
        sheetname_re: 'Pattern[str]' = re.compile(r'(?i)^\s*stellenplan'),
        number_of_heading_rows: int = 3,
        institution_col: int = 6,
        institution_re: 'Pattern[str]' = re.compile(r'(?i)^\s*lrz\b'),
        post_id_col: int = 2,
        filename_differ: str = '#LRZ',
        ):
    '''
    Determine which files are to be processed: If :param:`filename` is
    non-empty, take it as the path to the (only) file to be processed.
    This exists for drag-and-drop functionality. If :param:`filename`,
    however, is empty, collect all files that:

    - are located in the same folder as this program,
    - have a base name ending with “.xlsx” (case-insensitive),
    - have a base name not ending with :param:`filename_differ`,
    - have a base name matching :param:`filename_re`.

    With the determined files, copy certain information from them into
    a new workbook and save it. More precisely:

    - In the first worksheet whose title matches :param:`sheetname_re`, copy:

      - the heading rows, i.e. the first :param:`number_of_heading_rows` rows.
      - rows whose value in column :param:`institution_col` (counted from 1!)
        matches :param:`institution_re`.

    - Secondly, copy sheets whose title (stripped and lowered) is equal to the
      (stripped and lowered) value in column :param:`post_id_col` (counted from
      1!) of any of the rows copied from the mentioned worksheet matching
      :param:`sheetname_re`.

    - Save the new workbook under a new name, which is the old one but
      suffixed by :param:`filename_differ`.
    '''
    if filepath:
        paths = [filepath]
    else:
        paths = []
        for file in os.scandir():
            base, ext = os.path.splitext(file.name)
            if ext.lower() == '.xlsx' and filename_re.search(base) and not\
                    base.endswith(filename_differ):
                paths.append(file.path)
    results = []
    for path in paths:
        wb = op.load_workbook(file.path, data_only = True)

        # Make a new workbook, in which the pertinent parts will be copied.
        wb_new = op.Workbook()
        for ws in wb.worksheets:
            if sheetname_re.search(ws.title):
                break
        else:
            continue
        ws_new = wb_new.active
        ws_new.title = ws.title

        # Pick the rows in the list that contains all posts in rows
        # and copy them into the new workbook.
        rows = []
        pertinent_post_ids = set()
        for n, row in enumerate(ws.iter_rows(), start = 1):
            if (n <= number_of_heading_rows or
                    institution_re.search(str(row[institution_col].value or ''))
                    ):
                rows.append(row)
                post_id = str(row[post_id_col - 1].value or '').strip().lower()
                if n > number_of_heading_rows and post_id:
                    pertinent_post_ids.add(post_id)
        copy_rows(rows, ws_new)

        # From the sheets that contain information about a single post each,
        # pick the pertinent ones and copy them into the new workbook.
        for ws in wb.worksheets:
            if ws.title.strip().lower() in pertinent_post_ids:
                copy_rows(ws.iter_rows(), wb_new.create_sheet(ws.title))

        results.append((wb_new, file.path))

    for wb, path in results:
        new_path = file_io.get_new_path(path, diff_char = filename_differ)
        wb.save(new_path)
        print('Ausgabe:', new_path)

if __name__ == '__main__':
    if len(sys.argv) > 1 and sys.argv[1].lower().endswith('.xlsx'):
        filepath = sys.argv[1]
    else:
        filepath = ''
    main(filepath = filepath)
