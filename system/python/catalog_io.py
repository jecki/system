﻿# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
'''
Access to a library catalog via URL query and extraction of certain data
from the response sent back by the catalog.
'''
import os
import re
import socket
import xml.etree.ElementTree as ET
from collections import deque
from urllib.parse import quote_plus
from urllib.request import urlopen

import compare
import xmlhtml

REPLACES = (('ß', 'ss'), ('æ', 'ae'), ('œ', 'oe'), ('þ', 'th'), ('ð', 'dh'))

class Catalog:
    '''
    Provide access to a library catalog via URL query. Provide methods for
    extracting data from the entries sent back by the catalog.

    .. important::
        - When extracting from the catalog entry, leading and trailing
          whitespace is removed from the field values always except for
          the case of identifiers like the publication key.
        - Except from that, the content of the field is given as-is;
          no unescaping is done.

    .. note::
        In order to abstract from implementation details of the catalog, you
        may want to use the methods without the implementation suffix, e.g.
        write ``get_titles(entry)`` instead of ``get_titles_marc(entry)``. If
        at some point in the future the implementation changes, it is sufficient
        to add new methods and use another implementation suffix in their names
        and to specify these suffixes in :param:`__init__.query_format` and
        :param:`__init__.entry_format` (described below).

    .. testcode::
        import xml.etree.ElementTree as ET
        from urllib.request import urlopen
        import catalog_io

        catalog = catalog_io.Catalog('http://bvbr.bib-bvb.de:5661/bvb01sru')
        query   = catalog.get_query('marcxml.idn=BV022951212')
        entry   = ET.fromstring(urlopen(catalog.url + query).read().decode())
        year    = catalog.get_year(entry)
        print(year)

    .. testoutput::
        2007
    '''
    def __init__(
            self,
            url: 'Optional[str]',
            query_format: str = 'sru',
            entry_format: str = 'marc',
            xmlns_default: str = 'http://www.loc.gov/MARC21/slim',
            ):
        '''
        :param url: URL to the library catalog, i.e. the URL of a valid query
            in this catalog *excluding* the query part starting with the
            question mark. If ``None``, no catalog gets connected. This option
            exists so that you can use the methods e.g. for conversion or
            extraction without a connection to an online catalog but instead
            with an export of catalog data.
        :param query_format: implementation suffix that determines how to query
            the catalog, especially how to build the query part of a query URL.
            At the moment, only methods for SRU are implemented.
            These methods end with ``_sru``, correspondingly, the
            string to be given to :param:`query_format` is ``'sru'``.
        :param entry_format: implementation suffix that determines how to
            process the response of a query, especially how to extract
            information from the serialized entries returned in the response.
            At the moment, only methods for MARC-XML are implemented.
            These methods end with ``_marc``, correspondingly, the
            string to be given to :param:`entry_format` is ``'marc'``.
        :param xmlns_default: default namespace URL valid for the XML tag names
            returned by the library catalog.
        '''
        methods = dir(self)
        self.url = url
        self.xmlns_default = xmlns_default
        self.query_format = query_format
        self.entry_format = entry_format
        methodmapping = {}
        for suffix in ('_' + query_format, '_' + entry_format):
            methodmapping.update(
                { name.rsplit(suffix, 1)[0]: name
                for name in methods if name.endswith(suffix) })
        for shortname, name in methodmapping.items():
            setattr(self, shortname, getattr(self, name))

    def get_edition_marc(self, entry: ET.Element) -> (str, str):
        '''
        Extract from :param:`entry` the so-called edition statement about the
        publication described by the entry.
        The edition statement consists of the statement itself and an optional
        remainder in another subfield. They are returned as a pair of strings.

        :param entry: A catalog entry in MARC-XML.
        '''
        return (
                xmlhtml.get_text(entry, './/*[@tag="250"]/*[@code="a"]').strip(),
                xmlhtml.get_text(entry, './/*[@tag="250"]/*[@code="b"]').strip(),
                )

    def get_entry(
            self,
            term: str,
            query_kwargs: 'Dict[str, str]'      = {},
            encoding: str                       = 'utf-8',
            timeout: 'Optional[float]'          = None,
            context: 'Optional[ssl.SSLContext]' = None,
            data                                = None,
            ) -> 'Optional[ET.Element]':
        '''
        Get the data of a certain entry in the library catalog.
        The entry is to be found via :param:`term`, e.g.
        ``'marcxml.idn=BV022951212'``.

        :param query_kwargs: Optional keyword arguments for :meth:`get_query`.
        :param encoding: Encoding used to read the data from the catalog.

        The other parameters are the same as in :func:`urllib.request.urlopen`.
        '''
        query = self.get_query(term, **query_kwargs)
        try:
            entry = ET.fromstring(urlopen(
                    self.url + query,
                    data = data,
                    timeout = timeout,
                    context = context,
                    ).read().decode(encoding))
        except: # Sic.
            entry = None
        return entry

    def get_extent_marc(self, entry: ET.Element) -> str:
        '''
        Extract from :param:`entry` the extent of the publication
        described by the entry.

        :param entry: A catalog entry in MARC-XML.
        '''
        extent = xmlhtml.get_text(entry, './/*[@tag="300"]/*[@code="a"]').strip()\
                 .replace(' ', '\u202f')
        if extent and extent[-1].isdigit():
            extent += '\u202fS.'
        return extent

    def get_extent_accompanying_material_marc(self, entry: ET.Element) -> str:
        '''
        Extract from :param:`entry` a statement about any accompanying material
        of the publication described by the entry.

        :param entry: A catalog entry in MARC-XML.
        '''
        return xmlhtml.get_text(entry, './/*[@tag="300"]/*[@code="e"]').strip()

    def get_extent_other_marc(self, entry: ET.Element) -> str:
        '''
        Extract from :param:`entry` a statement about further extensional
        features of the publication described by the entry, e.g., whether
        there are any illustrations.

        :param entry: A catalog entry in MARC-XML.
        '''
        return xmlhtml.get_text(entry, './/*[@tag="300"]/*[@code="b"]').strip()

    def get_genre_marc(
            self, entry: ET.Element) -> 'Generator[Tuple[str, str, str]]':
        '''
        Extract from :param:`entry` statements about the genre of the
        publication described in the entry. The statements include, if
        available, an identifying key for the genre, the genre as a verbal
        expression and a hint at the source from which the ID and the
        expression are taken.

        :param entry: A catalog entry in MARC-XML.
        '''
        for subentry in entry.iterfind('.//*[@tag="655"]'):
            genre_id = xmlhtml.get_text(subentry, './/*[@code="0"]').strip()
            genre_text = xmlhtml.get_text(subentry, './/*[@code="a"]').strip()
            genre_source = xmlhtml.get_text(subentry, './/*[@code="2"]').strip()
            yield genre_id, genre_text, genre_source

    def get_isbns_issns_marc(self, entry: ET.Element) -> 'Generator[str]':
        '''
        Extract from :param:`entry` the ISBNs or ISSNs of the publication
        described in the entry.

        :param entry: A catalog entry in MARC-XML.
        '''
        for subentry in entry.iterfind('.//*[@tag="020"]'):
            number = xmlhtml.get_text(subentry, './/*[@code="9"]').strip()
            if number:
                yield number

    def get_note_marc(self, entry: ET.Element) -> str:
        '''
        Extract from :param:`entry` a general note about the publication
        described by the entry.

        :param entry: A catalog entry in MARC-XML.
        '''
        return xmlhtml.get_text(entry, './/*[@tag="500"]/*[@code="a"]').strip()

    def get_personal_names_ids_roles_marc(
            self,
            entry: ET.Element,
            delete_from_id: 'Optional[Pattern[str]]' =
                    re.compile(r'^\(.*?\)'),
            ) -> 'Generator[Tupel[str, str, str, str, str]]':
        '''
        Extract from :param:`entry` for any recorded person the name and,
        if given, the person identifier (e.g. the GND) and the role this
        person plays with regard to the publication described by the entry
        (e.g. a contributor to a lexicon). The name consists of the name
        itself, optionally an enumeration (e.g. in ``'Friedrich II.'``) and
        optionally another addition associated with the name, e.g. a title.
        An example return value could be:
        ``[('Friedrich', 'II.', 'König von Preußen', '118535749', 'aut')]``

        :param entry: A catalog entry in MARC-XML.
        :param delete_from_id: If not ``None``, a pattern whose match is
            deleted from the key. The default deletes a leading parenthesis,
            so that e.g. ``'(DE-588)11875145X'`` becomes ``'BV006182977'``.
        '''
        for field_id in (100, 700):
            for subentry in entry.iterfind('.//*[@tag="{}"]'.format(field_id)):
                name = xmlhtml.get_text(subentry, './/*[@code="a"]').strip()
                name_enum = xmlhtml.get_text(subentry, './/*[@code="b"]').strip()
                name_add = xmlhtml.get_text(subentry, './/*[@code="c"]').strip()
                person_id = xmlhtml.get_text(subentry, './/*[@code="0"]').strip()
                role = xmlhtml.get_text(subentry, './/*[@code="4"]').strip()
                if delete_from_id:
                    person_id = delete_from_id.sub('', person_id.strip())
                yield (name, name_enum, name_add, person_id, role)

    def get_place_marc(self, entry: ET.Element) -> str:
        '''
        Extract from :param:`entry` the place of the publication
        described by the entry.

        :param entry: A catalog entry in MARC-XML.
        '''
        return xmlhtml.get_text(entry, './/*[@tag="264"]/*[@code="a"]').strip() \
            or xmlhtml.get_text(entry, './/*[@tag="260"]/*[@code="a"]').strip()

    def get_pub_id_marc(self, entry: ET.Element) -> str:
        '''
        Extract from :param:`entry` the catalog-specific key of the publication
        (e.g. the BV number of the B3Kat).

        :param entry: A catalog entry in MARC-XML.
        '''
        return xmlhtml.get_text(
                entry,
                './/marc:controlfield[@tag="001"]',
                {'marc': self.xmlns_default})

    def get_publisher_marc(self, entry: ET.Element) -> str:
        '''
        Extract from :param:`entry` the publisher of the publication
        described by the entry.

        :param entry: A catalog entry in MARC-XML.
        '''
        return xmlhtml.get_text(entry, './/*[@tag="264"]/*[@code="b"]').strip() \
            or xmlhtml.get_text(entry, './/*[@tag="260"]/*[@code="b"]').strip()

    def get_query_sru(
            self,
            query: str,
            maximumRecords: int   = 1,
            startRecord: int      = 1,
            extraRequestData: str = '',
            operation: str        = 'searchRetrieve',
            recordPacking: str    = 'xml',
            recordSchema: str     = 'marcxml',
            recordXPath: str      = '',
            resultSetTTL: int     = 0,
            sortKeys: str         = '',
            stylesheet: str       = '',
            version: str          = '1.1',
            ) -> str:
        '''
        Get a URL query string for a request that complies with one of the
        standards in http://www.loc.gov/standards/sru, depending on
        :param:`version`, e.g. http://www.loc.gov/standards/sru/sru-1-1.html
        for ``version == '1.1'``, which is the default.
        These standard descriptions explain all the parameters under the names
        used here except from the following:

        :param query: a CQL expression as described in:

            - http://loc.gov/standards/sru/cql/spec.html (more official)
            - http://zing.z3950.org/cql/intro.html (more readable)

            .. note::
                The CQL expression must not be escaped for the use in the URL
                (e.g. by percent encoding), because this is done here in the
                function.

            .. note::
                Two notes about the CQL syntax:

                - A simple query term consists only of the searched term. You
                  may enclose this term in double quotation marks; you must do
                  that if the term contains a space or a CQL control word
                  (like `all`).
                - If you want to limit the search to certain fields, you may
                  use an expression like ``'field=term'``. In this case, the
                  interface may require the specification of a namespace, e.g.
                  ``'marcxml.idn=BV042597690'``.

        :param maximumRecords: if ``0``, the query string does not specify a
            maximum number of records, and it is up to the server to deal with
            that. If it is greater than ``0``, the number is the number of
            records to be returned at most. Note, that the actual number of
            returned records can be limited by the service to which the request
            is sent (e.g. the service maybe disallows that more than 500
            records are sent back).
        
        For testcode, see the docstring of this class.
        '''
        # Get a dictionary of argument names and values for the parameters that
        # can be used for the URL query string:
        params = locals()
        del params['self']
        # Build the query string and encode the keys and values:
        return '?' + '&'.join( '{}={}'.format(
                key,
                quote_plus(str(val), safe = '')
                ) for key, val in params.items() if val )

    def get_series_id_serial_num_marc(
            self,
            entry: ET.Element,
            delete_from_id: 'Optional[Pattern[str]]' =
                    re.compile(r'(^\s+|\s+$|^\(.*?\))'),
            ) -> 'Dict[str, Deque[Tuple[str, str]]]':
        '''
        Extract from :param:`entry` the series keys and the serial numbers
        of the publication described by the entry.
        Get them from all of the catalog fields that may contain them.
        Consider multiple occurrences of the same field.
        Map the id of the field (e.g. ``'810'``) to all the pairs of series key
        and serial number which are found with this field id.
        Return this mapping.

        :param entry: A catalog entry in MARC-XML.
        :param delete_from_id: If not ``None``, a pattern whose match is
            deleted from the key. The default deletes a leading parenthesis,
            so that e.g. ``'(DE-604)BV006182977'`` becomes ``'BV006182977'``.
        '''
        result = {}
        for field_id in ('830', '811', '810', '773', '772'):
            subentries = entry.findall('.//*[@tag="{}"]'.format(field_id))
            subresults = deque()
            for subentry in subentries:
                series_id = xmlhtml.get_text(subentry, './/*[@code="w"]').strip()
                if not series_id:
                    continue
                subkey = 'g' if field_id in {'773', '772'} else 'v'
                serial_num = xmlhtml.get_text(
                        subentry,
                        './/*[@code="{}"]'.format(subkey)).strip()
                subresults.append((
                        delete_from_id.sub('', series_id)
                            if delete_from_id else series_id,
                        serial_num,
                        ))
            if subresults:
                result[field_id] = subresults
        return result

    def get_titles_marc(self, entry: ET.Element) -> 'Tuple[str, str, str]':
        '''
        Extract from :param:`entry` the toptitle and, if given, the subtitle
        and the sectiontitle of the publication described by the entry.
        For any of the three that is not given in the title field, the empty
        string is returned. If the field itself is not available, three
        empty strings are returned.

        :param entry: A catalog entry in MARC-XML.
        '''
        subentry = entry.find('.//*[@tag="245"]')
        if subentry is None:
            return '', '', ''
        toptitle = xmlhtml.get_text(subentry, './/*[@code="a"]').strip()
        subtitle = xmlhtml.get_text(subentry, './/*[@code="b"]').strip()
        sectiontitle = xmlhtml.get_text(subentry, './/*[@code="p"]').strip()
        return toptitle, subtitle, sectiontitle

    def get_uri_marc(self, entry: ET.Element) -> str:
        '''
        Extract from :param:`entry` an URI of the publication
        described by the entry.

        :param entry: A catalog entry in MARC-XML.
        '''
        return xmlhtml.get_text(entry, './/*[@tag="856"]/*[@code="u"]').strip()

    def get_year_marc(self, entry: ET.Element) -> str:
        '''
        Extract from :param:`entry` the year of the publication
        described by the entry.

        :param entry: A catalog entry in MARC-XML.
        '''
        marc008 = xmlhtml.get_text(entry, './/*[@tag="008"]')
        return xmlhtml.get_text(entry, './/*[@tag="264"]/*[@code="c"]').strip() \
            or xmlhtml.get_text(entry, './/*[@tag="260"]/*[@code="c"]').strip() \
            or (marc008[7:11] if len(marc008) > 10 else '')

def format_title_sorttitle(
        toptitle: str,
        subtitle: str,
        sectiontitle: str,
        punctuation_re: 'Optional[Pattern[str]]' =
                re.compile(r'[.:!?]\W*$'),
        ) -> 'Tuple[str, str]':
    '''
    Combine :param:`title`, :param:`subtitle`, :param:`sectiontitle`.
    Ensure, that the two latter ones start with an upper case letter.
    Join them with a `'.'`, but only if the preceding part does not
    end with any punctuation (possibly before other non-word characters).
    '''
    if toptitle and (subtitle or sectiontitle):
        if subtitle:
            subtitle = subtitle[0].upper() + subtitle[1:]
        if sectiontitle:
            sectiontitle = sectiontitle[0].upper() + sectiontitle[1:]
        if not punctuation_re.search(toptitle):
            toptitle += '.'
        toptitle += ' '
        if subtitle and sectiontitle:
            if not punctuation_re.search(subtitle):
                subtitle += '.'
            subtitle += ' '
    title = re.sub(r'\s+', ' ', toptitle + subtitle + sectiontitle)
    # Use the non-sorting marks:
    sorttitle = re.sub(r'\x98[^\x9c]*\x9c', '', title)
    sorttitle = compare.even_out(sorttitle, replaces = REPLACES).strip()
    sorttitle = re.sub(r' +', ' ', sorttitle)
    for old, new in (
            ('\x98', ''),
            ('\x9c', ''),
            (' ;', ';'),
            (' :', ':'),
            ):
        title = title.replace(old, new)
    return title, sorttitle

if __name__ == '__main__':
    bvs = r'''BV035282037'''
    # ('830', '811', '810', '773', '772')
    bvs = [ bv.strip() for bv in bvs.split() if bv.strip() ]
    catalog = Catalog('http://bvbr.bib-bvb.de:5661/bvb01sru')
    for rank, bv in enumerate(bvs):
        term = 'marcxml.idn=' + bv
        if 0:
            from pprint import pprint
            print(term)
            entry = catalog.get_entry(term)
            pprint(list(catalog.get_personal_names_ids_roles_marc(entry)))
        else:
            if rank == 0:
                print('<?xml version="1.0"?>')
            query = catalog.get_query(term)
            entry = urlopen(catalog.url + query).read().decode()
            entry = entry.replace('<?xml version="1.0"?>', '')
            print(entry)
