% # Licensed under http://www.apache.org/licenses/LICENSE-2.0.
% # Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
% lang_id = kwargs['lang_id']
% page_id = kwargs['page_id']
<main>
	<article class="card" style="max-width:1200px">
		{{!dao.get_citation(kwargs['pub_entry'], 'html_table', lang_id)}}
		<h3>{{dao.glosses['suggested_citations'][lang_id]}}</h3>
		<dl>
			<dt>{{dao.glosses['plain_text'][lang_id]}} <a class="ref" href="/{{lang_id}}/cite/{{page_id}}.txt">{{dao.glosses['as_file'][lang_id]}}</a></dt>
			<dd>{{!dao.get_citation(kwargs['pub_entry'], 'txt', lang_id)}}</dd>
			<dt>RIS <a class="ref" href="/{{lang_id}}/cite/{{page_id}}.ris">{{dao.glosses['as_file'][lang_id]}}</a></dt>
			<dd><pre>{{!dao.get_citation(kwargs['pub_entry'], 'ris', lang_id)}}</pre></dd>
			<dt>BibTex <a class="ref" href="/{{lang_id}}/cite/{{page_id}}.bib">{{dao.glosses['as_file'][lang_id]}}</a></dt>
			<dd>
				<pre>{{!dao.get_citation(kwargs['pub_entry'], 'bib', lang_id)}}</pre>
			</dd>
		</dl>
	</article>
</main>\\
% rebase('base.tpl', request = request, dao = dao, kwargs = kwargs)
