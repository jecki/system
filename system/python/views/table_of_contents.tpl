% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2013 ff.
% # Template for a table of contents; gets filled into the base template.
% # In the example edition: http://localhost:8080/de/example/table_of_contents.
% lang_id = kwargs.get('lang_id', '')
% site_id = kwargs.get('site_id', '')
<main>
<article class="non-source sheet">
	<h1>{{dao.glosses['table_of_contents'][lang_id]}}</h1>
	<nav>
	% for entry_lang_id, entry_file_id, entry_page_id, entry_element_link, entry_text, indent in dao.get_toc_entries(lang_id, site_id):
		<p{{!'' if indent else ' class="toc_entry_main"'}} style="padding-left:{{indent * 4}}%">
			{{!'' if (indent or entry_lang_id == dao.default_lang_ids[site_id]) else '<span class="local"><span class="tag">' + dao.glosses['lang_name'][entry_lang_id] + ':</span></span>'}}<a href="/{{entry_lang_id}}/{{site_id}}/{{entry_page_id}}{{entry_element_link}}">{{!entry_text}}</a>
		</p>
	% end
	</nav>
</article>
</main>
% rebase('base.tpl', request = request, dao = dao, kwargs = kwargs)
