% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
% entry = kwargs['entry']
% cat_lang = entry['cat_lang']
% modified_on = entry['ms_modified_on'].split()[0]
% modified_on_shown = '.'.join(reversed(modified_on.split('-')))
% place_name = entry['place_name'].lstrip('?')
% place_name = place_name + ', ' if place_name else ''
<main>
<article class="sheet sheetwide">
	<h1>{{place_name}}{{entry['lib_name']}}, {{entry['ms_shelfmark']}}</h1>
  % if cat_lang != 'Latin' and entry['ms_avail_source']:
	<p><small>[{{entry['ms_avail_source']}}]</small></p>
  % end
	% datestring = entry['ms_datestring']
	% place = entry['ms_origin']
	% provenance = entry['ms_provenance']
  % if cat_lang != 'Latin':
	<p class="flush_unhyph">{{entry['ms_info'] + ' ' if entry['ms_info'] else ''}}{{!'<b>Date:</b> ' + datestring if datestring else ''}}{{!'&#8193;<b title="Origin">Or.:</b> ' + place if place else ''}}{{!'&#8193;<b>Prov.:</b> ' + provenance if provenance else ''}}</p>
  % else:
	<p class="flush_unhyph">{{!entry['ms_datestring']}}{{!'&#8193;<b>Or.:</b> ' + place if place else ''}}{{!'&#8193;<b title="Provenance">Prov.:</b> ' + provenance if provenance else ''}}</p>
  % end
	<p class="flush_unhyph">\\
  % if cat_lang != 'Latin':
<b class="label" title="Codicology">Cod.</b>&#8193;\\
  % end
{{!entry['ms_codicology']}}</p>
	<p class="flush_unhyph">\\
  % if cat_lang != 'Latin':
<b class="label" title="Content">Cont.</b>&#8193;\\
  % end
{{!entry['ms_content']}}</p>
  % if entry['ms_note']:
	% paragraphs = entry['ms_note'].split('\n')
	% if len(paragraphs) == 1:
	<p class="flush_unhyph"><b class="label">Note</b>&#8193;{{!entry['ms_note']}}</p>
	% else:
	  % for rank, paragraph in enumerate(paragraphs, start = 1):
	<p class="flush_unhyph"><b class="label">Note&#160;{{rank}}</b>&#8193;{{!paragraph}}</p>
	  % end
	% end
  % end
	<p class="flush_unhyph"><b class="label">Lit.</b>&#8193;{{!entry['ms_bibliography']}}</p>
	<table class="parts">
	  % for mspart in entry['msparts']:
	  %     scan_link = mspart.get('scan_link', '')
	  %     tran_link = mspart.get('tran_link', '')
		<tr id="{{mspart['part_id']}}" role="row">
			<th scope="row">
				<p title="part id: {{mspart['part_id']}}">{{mspart['pagina_range']}}</p>
			  % if scan_link and tran_link:
				<p><a class="key key_dark" href="{{scan_link}}">Scans|Text</a></p>
			  % elif scan_link:
				<p><a class="key key_dark" href="{{scan_link}}">Scans</a></p>
			  % elif tran_link:
				<p><a class="key key_dark" href="{{tran_link}}">Text</a></p>
			  % end
			</th>
			<td>
				<div class="flush_unhyph">{{!mspart['mspart_quotation']}}</div>
				<div class="flush_unhyph">=&#8239;<a href="/work/{{mspart['work_id']}}">{{'' if 'Anonymous' in mspart['person_name'] else mspart['person_name'] + ', '}}{{!dao.format_title(mspart['work_title'])}} ({{mspart['work_siglum']}})</a>{{!mspart['mspart_note']}} {{!mspart['mspart_glosses']}}</div>
			</td>
		</tr>
	  % end
	</table>
  % if cat_lang != 'Latin' and entry['ms_bibl_special']:
	<p class="flush_unhyph"><b class="label" title="Entry-specific bibliography">Bibl.</b>&#8193;{{!entry['ms_bibl_special']}}</p>
  % end
	<aside class="citebox">
		<p>How to cite this entry?</p>
		<p>{{entry['user_firstname']}} {{entry['user_lastname']}}, ‘MS {{place_name}}{{entry['lib_name']}}, {{entry['ms_shelfmark']}}’ (update: <time datetime="{{modified_on}}">{{modified_on_shown}}</time>), <cite>Ptolemaeus Arabus et Latinus. Manuscripts</cite>, URL&#8239;=&#8239;http://ptolemaeus.badw.de/ms/{{entry['ms_id']}}.</p>
	</aside>
</article>
</main>\\
% rebase('base.tpl', request = request, dao = dao, kwargs = kwargs)