% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2013 ff.
% # Template for an index; gets filled into the base template.
% # In the example edition: http://localhost:8080/example/index_of_documents
% # and http://localhost:8080/example/index_of_persons.
% lang_id = kwargs.get('lang_id', '')
% text = kwargs['text']
% base = kwargs.get('base', 'base.tpl')
% prepath = kwargs.get('prepath', '/')
<script src="{{prepath}}cssjs/jquery/jquery.dataTables.min.js"></script>
<script src="{{prepath}}cssjs/jquery/dataTables.responsive.min.js"></script>
<script>
	$(document).ready(
		function() {
			var config = {{!dao.get_table_config_json(text, lang_id)}}
			// For a non-XML/HTML source, specify a function to be run after an HTML row has been built:
			if ('ajax' in config) { config.createdRow = addAttrs; };
			var table = $('#index').DataTable(config);
			// Connect the fulltext search for all columns:
			$('#global_term').on('keyup', function () { filterGlobal(); });
			// Connect the checkbox settings for all columns:
			$('#filter_global :checkbox').change(function () { filterGlobal(); });
			// Connect the fulltext search for a specific column:
			$('input.column').on('keyup', function () { filterColumn( parseInt( $(this).parents('tr').attr('data-column_rank') ) ); });
			// Connect the fulltext search for a specific column:
			$('input.column:checkbox').change(function () { filterColumn( parseInt( $(this).parents('tr').attr('data-column_rank') ) ); });
			// Set up the select boxes:
			$('table.form td select').each(
				function(i) {
					var select = $(this);
					var rank = parseInt( $(this).parents('tr').attr('data-column_rank') );
					// Connect the function in order to populate the drop-down menu:
					select.on('focus',
						function() {
							if (select[0].getAttribute('data-filled') != 'true') {
								var data = table.column(rank).data();
								var values = [];
								var reTrim = /\u003c(div|span) (class="trim"|hidden="")\u003e.*?\u003c\u002f(div|span)\u003e/g;
								var reTag = /\u003c.*?\u003e/g;
								var i = data.length;
								while (i--) {
									var value = data.pop();
									if (typeof value === 'object') {
										value = value._;
									};
									value = value.replace(reTrim, "");
									value = value.replace(reTag, "");
									var subvalues = value.split(';');
									var ii = subvalues.length;
									while (ii--) {
										var value = subvalues[ii].trim();
										if (values.indexOf(value) === -1) {
											values.push(value);
										}
									}
								};
								values.sort(function (a, b) {
									var a = a.toLowerCase();
									var b = b.toLowerCase();
									return a < b ? -1 : +(a > b);
								});
								for (var i = 0; i < values.length; i++) {
									select.append('<option>' + values[i] + '</option>');
								};
								select[0].setAttribute('data-filled', 'true');
							}
						}
					);
					// Connect the function in order to filter the table accordingly to the selection:
					select.on('change',
						function() {
							table.column(rank)
								.search(select.val() )
								.draw();
						}
					)
				}
			);
			// Enable fragment IDs even when using AJAX:
			$('#index').on( 'draw.dt',
				function() {
					var hash = window.location.hash.substring(1);
					if (hash != "" && hash != "top") {
						window.location.hash = hash;
					}
				}
			);
		}
	);
</script>
<noscript>
	<style>.js_only {display:none}</style>
	<main>
		<article class="sheet flush non-source">
			<p>{{dao.glosses['index_needs_js'][lang_id]}}</p>
		  % if 'ersatzlink' in kwargs:
			<p><a href="{{kwargs['ersatzlink']}}">{{!dao.glosses['or_click_here_for_ersatz'][lang_id]}}</a></p>
		  % end
		</article>
	</main>
</noscript>
<main class="js_only">
<form class="card non-source" role="search">
	<table class="form">
		% columns = [ (dao.xmlhtml.elem2string(e, inner = True), 'data-select' in e.attrib, rank) for rank, e in enumerate(dao.ET.fromstring(text[text.find(b'<thead>') + 7:text.find(b'</thead>')]).findall('th')) if 'data-no_search' not in e.attrib ]
		% gets_select_options = any( c[1] for c in columns )
		<thead>
			<tr>
				<th>
					<button type="reset" onclick="filterClear();">{{dao.glosses['reset'][lang_id]}}</button>
				</th>
				<th>{{dao.glosses['search'][lang_id]}} <a class="ref" href="{{('../' + dao.default_site_id + '/') if dao.multisite else ''}}user_guide#search_index">{{dao.glosses['user_guide'][lang_id]}}</a></th>
				<th id="no_case"  title="{{dao.glosses['no_case'][lang_id]}}">a|A</th>
				<th id="no_order" title="{{dao.glosses['no_order'][lang_id]}}">ab|ba</th>
				<th id="regex"    title="{{dao.glosses['regex'][lang_id]}}&#xd;{{dao.glosses['click_for_more_info'][lang_id]}}"><a class="ref" href="{{('../' + dao.default_site_id + '/') if dao.multisite else ''}}user_guide#regex">RE</a></th>
				% if gets_select_options:
				<th>{{dao.glosses['select_cell_value'][lang_id]}}</th>
				% end
			</tr>
		</thead>
		<tbody>
		  % if len(columns) > 1:
			<tr id="filter_global">
				<th scope="row">{{dao.glosses['all_columns'][lang_id]}}</th>
				<td><input id="global_term"      type="text"     aria-label="{{dao.glosses['search_term'][lang_id]}}"/></td>
				<td><input id="global_caseInsen" type="checkbox" aria-labeledby="no_case" checked="checked"  title="{{dao.glosses['no_case'][lang_id]}}"/></td>
				<td><input id="global_smart"     type="checkbox" aria-labeledby="no_order" checked="checked" title="{{dao.glosses['no_order'][lang_id]}}"/></td>
				<td><input id="global_regex"     type="checkbox" aria-labeledby="regex"                      title="{{dao.glosses['regex'][lang_id]}}"/></td>
				% if gets_select_options:
				<td></td>
				% end
			</tr>
		  % end
		  % for column_title, gets_select_option, column_rank in columns:
			<tr data-column_rank="{{column_rank}}">
				<th scope="row">{{!column_title}}</th>
				<td><input class="column" id="column{{column_rank}}_term"      type="text"     aria-label="{{dao.glosses['search_term'][lang_id]}}"/></td>
				<td><input class="column" id="column{{column_rank}}_caseInsen" type="checkbox" aria-labeledby="no_case"  title="{{dao.glosses['no_case'][lang_id]}}"  checked="checked"/></td>
				<td><input class="column" id="column{{column_rank}}_smart"     type="checkbox" aria-labeledby="no_order" title="{{dao.glosses['no_order'][lang_id]}}" checked="checked"/></td>
				<td><input class="column" id="column{{column_rank}}_regex"     type="checkbox" aria-labeledby="regex"    title="{{dao.glosses['regex'][lang_id]}}"/></td>
				% if gets_select_options:
				<td>\\
				  % if gets_select_option:
<select><option value=""></option></select>\\
				  % end
</td>
				% end
			</tr>
		  % end
		</tbody>
	</table>
</form>
{{!kwargs['text']}}
</main>\\
% rebase(base, request = request, dao = dao, kwargs = kwargs)
