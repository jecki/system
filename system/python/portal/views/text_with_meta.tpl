% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2013 ff.
% # Template for HTML-XML text with a meta data card taken from a data source
% # specified in the text; gets filled into the base template.
% # In the example edition: http://localhost:8080/de/example/00001.
% site_id = kwargs.get('site_id', '')
% text = kwargs['text']
<script>
$(document).ready(highlight);
</script>
<main>
% mapping = dao.get_mapping(text, site_id)
% if mapping:
<article class="card non-source">
<table class="mapping">
  % for key, value in mapping:
	<tr>{{!key.replace(' property="', ' data-property="')}}{{!value.replace(' property="', ' data-property="').replace(' class="cut_off"', '')}}</tr>
  % end
</table>
</article>
% end
{{!text}}
</main>\\
% rebase('base.tpl', request = request, dao = dao, kwargs = kwargs)