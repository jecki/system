# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2013 ff.
'''
Miscellaneous functions for frequent file operations, predominantly
in order to apply a given function to certain files, to collect results
or to modify the files, and saving the results or the modified files
under an automatically derived new name next to the input files and
with the same folder structure like the input files.
'''
import base64
import configparser
import csv
import io
import os
import pprint
import re
import sys
import traceback
import webbrowser
import zipfile
from collections import deque
from collections.abc import Iterable
from collections.abc import Mapping
from collections.abc import Sequence
from datetime import datetime
from itertools import zip_longest

DIFF_CHAR = '+'

class Log(io.BufferedWriter):
    def __init__(
            self,
            logged_path: str,
            log_folder_path: str = '../../../../__logs__',
            log_file_name: str = '',
            joint: str = '.',
            ) -> str:
        '''
        Make an object for saving log entries.

        :param logged_path: path to the program to be logged.
            The path will be mentioned in the beginning of the log.
        :param log_folder_path: must be a **relative** path from
            :param:`logged_path` to the **folder** in which the log shall
            be saved.
        :param log_file_name: the filename which the log shall get.
            If empty, the filename of the log will be derived from
            :param:`logged_path` by joining its parent-folder-name
            and its basename with :param:`joint`, removing any extension
            from the basename and adding the extension `.log`.
        :param joint: See :param:`log_file_name`.
        '''
        logged_path = os.path.abspath(logged_path)
        folderpath, filename = os.path.split(logged_path)
        self.path = get_new_path(os.path.abspath(os.path.join(
                logged_path,
                log_folder_path.lstrip('/' + os.sep),
                log_file_name or os.path.basename(folderpath) +
                    joint + filename.rsplit('.', 1)[0] + '.log',
                )))
        self.start = datetime.now().isoformat().replace(':', '').split('.')[0]
        with open(pave(self.path), 'w', encoding = 'utf-8') as file:
            file.write('ENCODING: UTF-8')
            file.write('\nPROGRAM: ' + logged_path)
            file.write('\nLOGSTART: ' + self.start + '\n\n')

    def write(
            self,
            entry: 'Union[str, Exception]',
            with_time: bool = False,
            ) -> None:
        '''
        Write :param:`entry` in the logfile.

        :param with_time: If True, the current time is prepended to the entry.
        '''
        if isinstance(entry, Exception):
            entry = traceback.format_exc() + '\n'
        if with_time:
            entry = datetime.now().isoformat() + '\t' + entry
        with open(self.path, 'a', encoding = 'utf-8') as file:
            file.write(entry)

    def write_line(
            self,
            entry: 'Union[str, Exception]',
            with_time: bool = False,
            prefix: str = '\n',
            ) -> None:
        '''
        Write :param:`entry` in the logfile.

        - Strip leading and trailing whitespace.
        - Replace each newline with a blank. The entry is a single line now.
        - Replace a sequence of two or more blanks with one blank.
        - Write the entry and a newline in the logfile.

        :param with_time: If True, the current time is prepended to the entry.
        '''
        if isinstance(entry, Exception):
            entry = traceback.format_exc()
        entry = entry.strip().replace('\n', ' ')
        if entry:
            while '  ' in entry:
                entry = entry.replace('  ', ' ')
            self.write(entry + '\n', with_time)

    def write_lines(
            self,
            entry: 'Union[str, Exception]',
            with_time: bool = False,
            prefix: str = '\n',
            ) -> None:
        '''
        Write :param:`entry` in the logfile.

        - Strip leading and trailing whitespace.
        - Write the entry and a newline in the logfile.

        :param with_time: If True, the current time is prepended to the entry.
        '''
        if isinstance(entry, Exception):
            entry = traceback.format_exc()
        entry = entry.strip()
        if entry:
            self.write(entry + '\n', with_time)

def csv_write(
        table: 'Sequence[Sequence[Union[str, numbers.Number]]]',
        new_path: str,
        encoding: str = 'utf-16',
        newline: str = '',
        ) -> None:
    '''
    Write :param:`table`, a table of rows of either strings or numbers, into
    :param:`new_path` as a CSV file that can be opened with e.g. Excel.
    '''
    with open(new_path, 'w', encoding = encoding, newline = newline) as file:
        writer = csv.writer(file, delimiter = '\t', quoting = csv.QUOTE_MINIMAL)
        writer.writerows(table)

def get_abspaths(
        config: 'Union[configparser.ConfigParser, Dict[str, Dict[str, str]]]',
        config_path: str,
        ) -> 'Dict[str, str]':
    '''
    Get absolute paths from the relative paths in the section `paths` of
    :param:`config`. Take the directory of :param:`config_path` as base path.
    Return a dictionary with the old keys labelling the paths.
    The order of the paths as given in :param:`config` is not preserved.
    '''
    basepath = os.path.dirname(config_path)
    return { key:
            os.path.abspath(os.path.join(basepath, path))
            for key, path in config['paths'].items() }

def get_config(
        config_path: str,
        encoding: str = 'utf-8',
        ) -> 'configparser.ConfigParser':
    config = configparser.ConfigParser(allow_no_value = True)
    config.optionxform = lambda key: key
    config.read(config_path, encoding = encoding)
    return config

def get_keys(path: str = '') -> 'Tuple[str, str]':
    '''
    Try to read a username and a password from the file :param:`path`
    and return them. They have to be stored in a certain form described
    in the docstring of :func:`.set_keys`.
    On any error, return two empty strings.
    '''
    try:
        with open(os.path.abspath(path), 'rb') as file:
            len_first, access_data = base64.b16decode(
                    file.read().strip()).decode().split(':', 1)
            return access_data[:int(len_first)], access_data[int(len_first):]
    except Exception as e:
        print(e)
        return '', ''

def get_new_path(
        path: str,
        ext: 'Optional[str]' = None,
        diff_char: str = DIFF_CHAR,
        ) -> str:
    '''
    Get a new and unused path name based on :param:`path`.
    For further information, see :func:`.get_new_paths`.
    '''
    return get_new_paths([path], ext = ext, diff_char = diff_char)[1]

def get_new_paths(
        paths: 'List[str]',
        ext: 'Optional[str]' = None,
        common_path: str = '',
        diff_char: str = DIFF_CHAR,
        ) -> 'Tuple[List[str], str]':
    '''
    Return a new and unused path name for each path in :param:`paths` and the
    common path of this new path names.

    The new path names are unique: For :param:`diff_char` is appended to the
    common part of the new path name(s) as often as necessary. Thus it must
    be allowed in file names in the current system; moreover, it is forced to
    be non-empty.

    For safer uniqueness, the new path names must be absolute. If the old path
    names are not absolute, they new path names are absolutized taking the
    filepath of this module as base.

    For the common path, :param:`common_path` is taken if it is a common part
    of :param:`paths` and a directory path, if not, the longest common path is
    taken.

    If :param:`paths` is empty, ``[], ''`` is returned.

    .. note::
        If you want to get a new unused name to save a single file, do
        ``new_filepath = get_new_paths([ANY_FILEPATH])[1]``; note the ``[1]``.

    :param ext: If ``None``, it is ignored, if not, it substitutes any extension
        (**including the leading dot!**) given in the corresponding old path.
    '''
    if not paths:
        return ([], '')
    if not diff_char:
        diff_char = DIFF_CHAR
    paths = [ os.path.abspath(path) for path in paths ]
    if ext != None:
        paths = [ os.path.splitext(path)[0] + ext for path in paths ]
    longest_common_path = (paths[0] if len(paths) == 1 else
                           os.path.dirname(os.path.commonprefix(paths)))
    if not (longest_common_path.startswith(common_path) and
            os.path.isdir(common_path)):
        common_path = longest_common_path
    path, extension = os.path.splitext(common_path)
    i = 0
    while True:
        new_common_path = path + diff_char * i + extension
        if not os.path.exists(new_common_path):
            break
        i += 1
    return ([ path.replace(common_path, new_common_path, 1) for path in paths ],
            new_common_path)

def get_paths(shared_inpath: str) -> 'Generator[str]':
    '''
    Yield :param:`shared_inpath`, if it is the path to a single file,
    yield the paths to all files in :param:`shared_inpath`, if it is a folder.
    The paths are absolutized and normalized, but not sorted.
    '''
    shared_inpath = os.path.abspath(shared_inpath)
    if os.path.isfile(shared_inpath):
        yield shared_inpath
    elif os.path.isdir(shared_inpath):
        for folderpath, _, filenames in os.walk(shared_inpath):
            for filename in filenames:
                yield os.path.join(folderpath, filename)

def get_paths_sorted(shared_inpath: str) -> 'List[str]':
    '''
    Return :param:`shared_inpath`, if it is the path to a single file,
    return the paths to all files in :param:`shared_inpath`, if it is a folder.
    The paths are absolutized, normalized and sorted.
    '''
    shared_inpath = os.path.abspath(shared_inpath)
    if os.path.isfile(shared_inpath):
        paths = [shared_inpath]
    elif os.path.isdir(shared_inpath):
        paths = sorted( os.path.join(folderpath, filename)
                for folderpath, _, filenames in os.walk(shared_inpath)
                for filename in filenames )
    else:
        paths = []
    return paths

def join(path: str, *paths: str) -> str:
    '''
    Get the folderpath of :param:`path`, join it with :param:`paths`, if
    there are any, and absolutize the resulting path.

    This is especially useful for the common case that you want to write
    a relative path like ``'../cssjs/portal.css'`` in a file and that the
    resulting path must stay the same when the file is imported (i.e. the
    current working directory is that of the calling module, not that of
    the file into which you wrote the path). For this case, simply do:

    .. testcode::
        import file_io
        print(file_io.join(__file__, '../cssjs/portal.css'))

    (The testoutput differs depending on the system and on the location
    of this module.)
    '''
    return os.path.abspath(os.path.join(os.path.dirname(path), *paths))

def log_ip_medium(
        log: Log,
        ip: str,
        rest: str,
        with_time: bool = True,
        ) -> None:
    '''
    Write :param:`ip` plus rest in :param:`log`. Before that, anonymize the IP:

    - IPv4: only the first 3 bytes are kept.
    - IPv6: only the first 6 (3 times 2) bytes are kept. (IPv6 addresses
      contain 128 bit of information, written as 8 blocks of 2 bytes, but
      this notation can be abbreviated.)

    .. note::
        For the anonymization of IPv6, see the following description on page 3
        at http://www.ietf.org/rfc/rfc2374.txt and the explanations on the
        subsequent pages::

            The aggregatable global unicast address format is as follows:

            | 3|  13 | 8 |   24   |   16   |          64 bits               |
            +--+-----+---+--------+--------+--------------------------------+
            |FP| TLA |RES|  NLA   |  SLA   |         Interface ID           |
            |  | ID  |   |  ID    |  ID    |                                |
            +--+-----+---+--------+--------+--------------------------------+

    :param with_time: The same as in :meth:`Log.write`.
    '''
    if ':' in ip:
        ip_parts = ip.split(':')
        diff = 8 - len(ip_parts)
        if diff and '' in ip_parts:
            ip_parts.insert(ip_parts.index(''), diff * '0')
        log.write('\n' + ':'.join(ip_parts[:4]) + rest, with_time)
    else:
        log.write('\n' + ip.rsplit('.', 1)[0] + rest, with_time)

def pave(filepath: str) -> str:
    '''
    Normalize and absolutize :param:`filepath` and make all intermediate-level
    directories needed to contain the leaf :param:`filepath`, as far as they do
    not already exist. Return the normalized and absolutized :param:`filepath`.
    '''
    filepath = os.path.abspath(filepath)
    pave_dir(os.path.dirname(filepath))
    return filepath

def pave_dir(dirpath: str) -> str:
    '''
    Normalize and absolutize :param:`dirpath` and make this directory as well as
    all intermediate-level directories needed to contain it, as far as they do
    not already exist. Return the normalized and absolutized :param:`dirpath`.
    '''
    dirpath = os.path.abspath(dirpath)
    try:
        os.makedirs(dirpath, mode = 0o777, exist_ok = True)
    except FileExistsError:
        os.makedirs(dirpath, mode = 0o555, exist_ok = True)
    return dirpath

def pave_new(
        path: str,
        ext: 'Optional[str]' = None,
        diff_char: str = DIFF_CHAR,
        ) -> str:
    '''
    Get a new and unused path name based on :param:`path`
    and make all intermediate-level directories of :param:`path`,
    as far as they do not already exist.
    For further information, see :func:`.get_new_paths`
    and :func:`pave`.
    '''
    return pave(get_new_path(path, ext, diff_char))

def put(
        results: 'Union[str, Sequence, Mapping]',
        filepath: str        = '',
        conv: bool           = False,
        pr: bool             = True,
        end: bool            = True,
        enc: str             = 'utf-8',
        ext: 'Optional[str]' = None,
        ) -> None:
    '''
    Give a quick look at :param:`results`. If this is not a string yet, it
    is converted depending on :param:`conv` and :param:`pr`, as is described
    below. (The short names are for the sake of easy input.)

    :param filepath: if the empty string, :param:`results` is encoded and
        shown at the standard output. Else, it has to be a filepath, e.g.
        the path to the calling module, i.e. ``__file__``, so that
        :param:`results` will be saved in the same folder; the file is
        opened after saving.
    :param conv: If ``True``, convert :param:`results` via :func:`.stringify`.
    :param pr: If ``True``, convert :param:`results` via pretty printing.
    :param end: If ``True``, terminate with ``sys.exit(0)``.
    :param enc: the encoding used to write :param:`results`.
    :param ext: the file extension used to write :param:`results`, must
        include the leading dot. Ignored, if it is ``None``.
    '''
    if not isinstance(results, str):
        if conv:
            results = stringify(results)
        elif pr:
            results = pprint.pformat(results)
        else:
            results = str(results)
    if filepath:
        outpath = get_new_path(filepath, ext)
        size = write(outpath, results, outencoding = enc)
        tell(outpath, size)
        if size > 0:
            webbrowser.open(outpath)
    else:
        try:
            sys.stdout.write(results + '\n')
        except:
            sys.stdout.buffer.write(
                    (results + '\n').encode(sys.stdout.encoding))
    if end:
        sys.exit(0)

def run_over_files(
        shared_inpath: 'Union[str, Sequence[str]]' = '',
        include_in_inpaths_re: str = r'',
        exclude_from_inpaths_re: str = r'',
        unsorted: bool = False,
        ext: 'Optional[str]' = None,
        diff_char: str = DIFF_CHAR,
        size: int = 0,
        results: 'Container' = deque(),
        function: '''
            Callable[
                [
                    Union[str, Sequence[str]],
                    Union[str, Sequence[str]],
                    int,
                    Container
                ],
                Tuple[Container, int]]''' = lambda *args: (deque(), 0),
        ) -> 'Tuple[str, int, Container]':
    '''
    Process file(s) using :param:`function`.
    (Examples can be found under ``if __name__ == '__main__'`` beneath.)
    Return:

    - :param:`shared_outpath`, a new path where to find modified file(s) or
      where to save :param:`results`, or an empty string, if no file(s) were
      found in :param:`shared_inpath`. Derived from :param:`shared_inpath`;
      derived from the last path, if :param:`shared_inpath` is a tuple of paths.
    - :param:`size`, which is possibly modified and returned by every call of
      :param:`function`. Depending on this function it may accumulate the
      entire size of the processed file(s). (The size of :param:`results` is
      better determined after :func:`.run_over_files`, when the results are
      stored.)
    - :param:`results`, which is possibly modified and returned by every call
      of :param:`function`. Depending on this function it may accumulate
      certain passages collected from the processed files and may be a deque of
      tuples with any tuple consisting of a label (being a string or a subtuple
      of strings) and a matching passage (being a string). It may be
      stringified and stored after :func:`.run_over_files`.

    :param shared_inpath: Usually a path to a file or to a folder of files,
        which will be processed by :param:`function`. But a tuple of such
        paths, when needed for special purposes, e.g. the comparison of
        files. In this case, the file(s) given by each path are combined
        with :func:`itertools.zip_longest` to a list of tuples of files.
    :param include_in_inpaths_re: Regular expression: only matching paths are
        included in the process. Ignored, if empty.
    :param exclude_from_inpaths_re: Regular expression: matching paths are
        excluded from the process. Ignored, if empty.
    :param unsorted: If a huge number of inpaths causes a memory issue, it
        might help to set this parameter to ``False``: The inpaths will be
        processed in unsorted order. This is not possible and will be ignored,
        if :param:`shared_inpath` consists of more than one path.
    :param ext: If ``None``, it is ignored, if not, it substitutes any extension
        (**including the leading dot!**) given in the corresponding old path.
    :param function: Usually the name of a function, otherwise
        a :func:`functools.partial` object.

        - If the former, the function :param:`function` is called beneath
          with and only with certain four positional arguments, which must
          have appeared in the definition of this function.
          These positional arguments are:

          - `inpath`, the path to the input file, or the tuple
            `inpaths`, if :param:`shared_inpath` is a tuple.
          - `outpath`, a path where to save an output file possibly created
            by :param:`function`, or the tuple `outpaths`, if
            :param:`shared_inpath` is a tuple.
          - `size`.
          - `results`.

        - If the latter, the function that is called is the first argument of
          the :func:`functools.partial` object, and it is called with the said
          four positional arguments plus the further arguments given in
          the :func:`functools.partial` object. Note:

          - In the :func:`functools.partial` object, the four positional
            arguments do not appear at all, only the other (positional or
            keyword) arguments.
          - In the called function, the four positional arguments must be the
            **last positional** arguments in the function signature. The other
            arguments appear before them, if positional, and after them, if not.

        The function has to return :param:`size` and :param:`results`.
        If the function processes file(s), it should write the new data
        into a file opened at the outpath that is one of the four positional
        arguments. If the function (also or exclusively) collects results,
        the collection is handed down from one file to another and should be
        written into a file after the run over all files is done.
    '''
    include = re.compile(include_in_inpaths_re).search
    exclude = re.compile(exclude_from_inpaths_re).search
    if isinstance(shared_inpath, str):
        inpaths = deque(get_paths(shared_inpath)) if unsorted \
                else get_paths_sorted(shared_inpath)
        if include_in_inpaths_re:
            inpaths = deque(filter(include, inpaths))
        if exclude_from_inpaths_re:
            inpaths = deque( p for p in inpaths if not exclude(p) )
        outpaths, shared_outpath = get_new_paths(
                inpaths, ext, shared_inpath, diff_char = diff_char)
        for inpath, outpath in zip(inpaths, outpaths):
            size, results = function(inpath, outpath, size, results)
    else:
        shared_inpaths = shared_inpath
        inpaths_groups = deque()
        outpaths_groups = deque()
        for shared_inpath in shared_inpaths:
            inpaths = get_paths_sorted(shared_inpath)
            if include_in_inpaths_re:
                inpaths = deque(filter(include, inpaths))
            if exclude_from_inpaths_re:
                inpaths = deque( p for p in inpaths if not exclude(p) )
            outpaths, shared_outpath = get_new_paths(
                    inpaths, ext, shared_inpath)
            inpaths_groups.append(inpaths)
            outpaths_groups.append(outpaths)
        for inpaths, outpaths in zip(
                zip_longest(*inpaths_groups),
                zip_longest(*outpaths_groups)):
            size, results = function(inpaths, outpaths, size, results)
    return shared_outpath, size, results

def set_keys(user: str = '', password: str = '') -> bytes:
    '''
    Combine :param:`user` and :param:`password` to a single string,
    prefix the string with the length of the first item plus a colon,
    return the base16-encoded representation of the whole string.
    The two original strings can be gained by :func:`.get_keys`.

    .. important::
        The encoding is not meant, of course, to serve a security
        purpose but to make the storage of the data more robust.
        Strictly speaking, the access data here are only as secure
        as the place where the resulting string is stored.
    '''
    len_first = str(len(user)) + ':'
    return base64.b16encode((len_first + user + password).encode())

def stringify(results: 'Union[str, Iterable, Mapping, Sequence]') -> str:
    '''
    Try to make a string from :param:`results` (in order to write it in
    a file or so).
    '''
    if not results:
        return ''
    if isinstance(results, str):
        return results
    if not isinstance(results, Iterable):
        return str(results)
    if not isinstance(results, Sequence):
        if isinstance(results, Mapping):
            results = results.items()
        try:
            results = sorted(results)
        except TypeError:
            pass
    if isinstance(results[0], str):
        return ('' if results[0].endswith('\n') else '\n').join(results)
    if not isinstance(results[0], Sequence):
        return '\n'.join( str(item) for item in results )
    # Else, try to join it under the assumption that it is an `Iterable`
    # of `Iterables` of the length two, namely a `label` and a `passage`;
    # the `label` may be an `Iterable` itself.
    return '\n'.join(
        (str(label) + '\t' +
         str(passage).replace('\n', '\n\t'))
            if (isinstance(label, str) or not isinstance(label, Iterable))
            else
        ('\t'.join( str(item) for item in label ) + '\t' +
         str(passage).replace('\n', '\n' + '\t' * len(label)))
        for label, passage in results )

def stringify_table(
        table: 'Sequence[Sequence[Union[str, numbers.Number]]]',
        ) -> str:
    return '\n'.join(
            '\t'.join(map(
                lambda cell: str(cell).replace('\t', '').replace('\n', ''),
                row)) for row in table )

def tell(outpath: str, size: int, results: 'Container' = ()):
    '''
    Give information about any output of :func:`.run_over_files`.
    (See the docstring of this function, particularly the last part.)
    '''
    if not outpath:
        print('\nNo input file(s) found in the given path.\n')
    elif not size and not results:
        print('\nNo new file(s), no collected results.\n')
    elif size:
        outpath = os.path.abspath(outpath)
        size = '\n(' + str(size) + ' B)' if size > 0 else ''
        print('\nOutput in:\n' + outpath + size)

def write(
        outpath: str,
        text: 'Union[str, bytes]' = '',
        outencoding: str = 'utf-8',
        ) -> int:
    '''
    Write :param:`text` in :param:`outpath`, either as bytes or with
    :param:`outencoding`, creating the file and all intermediate
    folders of the path, if necessary, according to :func:`.pave`.
    Return the file size as number of bytes.
    '''
    if isinstance(text, str):
        with open(pave(outpath), 'w', encoding = outencoding) as outfile:
            return outfile.write(text)
    else:
        with open(pave(outpath), 'wb') as outfile:
            return outfile.write(text)

def write_new(
        oldpath: str,
        text: 'Union[str, bytes]' = '',
        outencoding: str = 'utf-8',
        ext: 'Optional[str]' = None,
        diff_char: str = DIFF_CHAR,
        ) -> 'Tuple[str, int]':
    '''
    Write :param:`text` in a path derived from :param:`oldpath` with
    the help of :func:`.get_new_path`, and write it either as bytes or
    with :param:`outencoding`, creating the file and all intermediate
    folders of the path, if necessary, according to :func:`.pave`.
    For the other parameters, see :func:`.get_new_paths`.
    Return the derived path and the file size as number of bytes.
    '''
    newpath = get_new_path(oldpath, ext, diff_char = diff_char)
    return newpath, write(newpath, text, outencoding)

def write_tell(
        oldpath: str,
        text: 'Union[str, bytes]' = '',
        outencoding: str = 'utf-8',
        ext: 'Optional[str]' = None,
        diff_char: str = DIFF_CHAR,
        ) -> 'Tuple[str, int]':
    '''
    Apply :func:`write_new` with the same parameters, then :func:`tell`,
    and return the path of the written file and its size as number of bytes.
    '''
    newpath, size = write_new(
            oldpath,
            text = text,
            outencoding = outencoding,
            ext = ext,
            diff_char = diff_char,
            )
    tell(newpath, size)
    return newpath, size

def zip_paths(paths: 'Sequence[str]', save_path: str) -> None:
    '''
    Zip all files and folders found at or within any path of :param:`paths`.
    Retain the folder structure. Save the zipfile at :param:`save_path`.

    .. warning::
        A file or folder already existing at :param:`save_path` will be
        overwritten. You may use :func:`.get_new_path` to get an unused path.
    '''
    all_paths = deque()
    for file_or_folder_path in paths:
        for path in get_paths(file_or_folder_path):
            all_paths.append(path)
    prefix = os.path.dirname(os.path.commonprefix(all_paths))
    with zipfile.ZipFile(pave(save_path), 'w') as file:
        for path in all_paths:
            file.write(path, path.split(prefix, 1)[1].strip(os.sep))

##if __name__ == '__main__':
##
##    # Examples.
##
##    import compare
##    import file_io
##
##    # 1. An example for the usual case that :param:`common_input` is one path:
##
##    def process(
##            inpath: str,
##            outpath: str,
##            size: int,
##            results: 'Container',
##            ) -> 'Tuple[int, Container]':
##        '''
##        Process any text found in :param:`inpath`.
##        '''
##        print(inpath)
##        with open(inpath, 'r', encoding = 'utf-8') as infile:
##            text = infile.read()
##        # ... process text ...
##        size += file_io.write(outpath, text, 'utf-8')
##        return size, results
##
##    def main():
##        shared_outpath, size, results = file_io.run_over_files(
##            shared_inpath = '../../../PP/P',
##            exclude_from_inpaths_re = r'(?i)ind-?list',
##            function = process,
##            )
##        file_io.tell(shared_outpath, size, results)
##        if results:
##            outpath = file_io.get_new_path(shared_outpath, '.csv')
##            size = file_io.write(
##                    outpath,
##                    text = file_io.stringify(results),
##                    outencoding = 'utf-16',
##                    )
##            file_io.tell(outpath, size)
##
##    # 2. An example for the case of *two* :param:`common_input`-paths:
##
##    def collect_diffs(
##            inpaths: 'Sequence[str]',
##            outpaths: 'Sequence[str]',
##            size: int,
##            results: 'MutableSequence[Tuple[str, str]]',
##            ) -> 'Tuple[int, MutableSequence[Tuple[str, str]]]':
##        for path in inpaths:
##            print(path)
##        print()
##        with open(inpaths[0], 'r', encoding = 'cp437') as infile:
##            lines0 = infile.read().split('\n@H')[-1].strip().splitlines()
##        with open(inpaths[1], 'r', encoding = 'cp437') as infile:
##            lines1 = infile.read().split('\n@H')[-1].strip().splitlines()
##        diffs = compare.compare_lines(
##                lines0,
##                lines1,
##                level_line,
##                level_line,
##                level_line,
##                level_line,
##                )
##        if diffs:
##            results.append('\n▀▀▀▀ ○ {} ● {}'.format(inpaths[0], inpaths[1]))
##            results.append(diffs)
##        return size, results
##
##    def compare_two(path1: str, path2: str):
##        shared_outpath, size, results = file_io.run_over_files(
##                shared_inpath = (path1, path2),
##                include_in_inpaths_re = '',
##                function = collect_diffs,
##                )
##        file_io.tell(shared_outpath, size, results)
##        if results:
##            outpath = file_io.get_new_path(shared_outpath, '.csv')
##            size = file_io.write(
##                    outpath,
##                    text = '\n'.join( file_io.stringify(r) for r in results ),
##                    outencoding = 'utf-16',
##                    )
##            file_io.tell(outpath, size)
##
##    def level_line(line: str, other_line: str) -> str:
##        fs = line.split('\t')[:6]
##        ...
##        return '\t'.join(fs)
##
##    if __name__ == '__main__':
##        compare_two(
##                file_io.join(__file__, '../../mhd/comp'),
##                file_io.join(__file__, '../../mhd/compE'),
##                )
