# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2013 ff.
import re
from calendar import monthrange
from collections import Counter
from collections import defaultdict
from collections import deque
from datetime import MAXYEAR
from datetime import datetime
from functools import reduce
from functools import partial
from itertools import islice
from itertools import zip_longest
from operator import mul

ROMAN_NUMBERS_MAP = tuple(zip(
    (1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1),
    ('M', 'CM', 'D', 'CD', 'C', 'XC', 'L', 'XL', 'X', 'IX', 'V', 'IV', 'I')
)) # By Mark Pilgrim

def arabic2roman(
        number: int,
        numeral_map: 'Tuple[Tuple[int, str]]' = ROMAN_NUMBERS_MAP,
        ) -> str:
    '''
    Attribution notice:
    In essence by Mark Pilgrim, Paul Winkler, Tim Valenta, dln385.
    See http://code.activestate.com/recipes/81611-roman-numerals/.
    '''
    result = []
    for integer, numeral in numeral_map:
        count = number // integer
        result.append(numeral * count)
        number -= integer * count
    return ''.join(result)

def extract(pattern: str, text: str, search = re.search) -> 'Tuple[str, str]':
    '''
    Cut the first match for the regular expression :param:`pattern`
    out of :param:`text`. Return a tuple containing:

    - from the match a join of all text covered by capturing parentheses,
    - the text without the entire matching part.

    :param:`pattern` should contain at least one capturing group.
    If it does not or if :param:`pattern is not found in :param:`text`,
    return a tuple containing the empty string and the text unchanged.

    .. note::
        Capturing groups must not be nested. Inside a capturing group,
        use a non-capturing group instead, e.g. ``'(?:.*?)'``.

    .. testcode::
        import parse
        text = 'Hefe, Butter!, Zucker'
        print(parse.extract(r' ?([^,]+)!,?', text))

    .. testoutput::
        ('Butter', 'Hefe, Zucker')
    '''
    match = search(pattern, text)
    if match:
        span = match.span()
        return ''.join(match.groups()), text[:span[0]] + text[span[1]:]
    else:
        return '', text

def get_ngrams(sequence: 'Sequence[Any]', n: int) -> 'Generator[Tuple]':
    '''
    Get ngrams from :param:`sequence` with :param:`n` being the length of
    each ngram. At the start and at the end, the n-grams are *not* stuffed;
    i.e., the number of yielded ngrams is ``len(sequence) - n + 1``.
    '''
    if n == 1:
        for item in sequence:
            yield (item,)
    else:
        for i in range(len(sequence) - n + 1):
            yield tuple(sequence[i:i + n])

def get_ngram_ratios(
        sequence: 'Sequence[Any]',
        n: int = 2, # For n = 1, just use ``Counter(sequence)``.
        ) -> 'Generator[Tuple[Any, float]]':
    length = len(sequence)
    maximum = 2 # This is an arbitrary number fit to enter the first loop.
    while n <= length and maximum > 1:
        # If the length, i.e. the number of ngrams for each n, is greater than n
        # or if there are no longer repetitions of ngrams, the loop is left.
        counter = Counter(get_ngrams(sequence, n))
        maximum = max(counter.values())
        for ngram, number in counter.items():
            yield (ngram, number / length)
        length -= 1
        n += 1

def get_ngram_ratios_diffs(
        sequence: 'Sequence[Any]'
        ) -> 'Generator[Tuple[Any, float]]':
    length = len(sequence)
    mono_counter = {
            ngram: number / length
            for ngram, number in Counter(sequence).items() }
    for monogram, ratio in mono_counter.items():
        yield ((monogram,), 0)
    for ngram, ratio in get_ngram_ratios(sequence):
        monoratio = reduce(mul, ( mono_counter[item] for item in ngram ))
        yield (ngram, ratio - monoratio)

def get_passage(
        text: str,
        match: 'Match[str]',
        sections_before: int   = 0,
        sections_after: int    = 0,
        section_delimiter: str = '\n',
        chars_before: int      = 0,
        chars_after: int       = 0,
        ) -> str:
    '''
    Get a passage of :param:`text`:

    - If :param:`sections_before` or :param:`sections_after` is not ``0``,
      and if :param:`section_delimiter` is given, then get a passage starting
      from :param`match` and adding as many sections before and after as is
      specified by :param:`sections_before` and :param:`sections_after`.
      Where a section ends and another starts, that is determined by the
      :param:`section_delimiter`.
    - Else, if :param:`chars_before` or :param:`chars_after` is not ``0``,
      then get a passage starting from :param:`match` and adding as many
      characters as is specified by :param:`chars_before` and
      :param:`chars_after`.
    - Else, get only the passage matched by :param:`match`.
    '''
    if any((sections_before, sections_after)) and section_delimiter:
        if sections_before < 0:
            sections_after = max((sections_after, -sections_before))
            sections_before = 0
        elif sections_after < 0:
            sections_before = max((sections_before, -sections_after))
            sections_after = 0
        start = match.start()
        end = match.end()
        for _ in range(sections_before + 1):
            start = text.rfind(section_delimiter, 0, start)
            if start == -1:
                start = 0
                break
        else:
            start += len(section_delimiter)
        for _ in range(sections_after + 1):
            end = text.find(section_delimiter, end)
            if end == -1:
                end = len(text)
                break
            end += len(section_delimiter)
        else:
            end -= len(section_delimiter)
        return text[start:end]
    elif any((chars_before, chars_after)):
        return text[max((match.start() - chars_before, 0)):
                    min((match.end() + chars_after, len(text)))]
    else:
        return match.group()

def group(
        items: 'Iterable',
        n: int,
        discard: bool = True,
        fillvalue: 'Any' = None,
        ) -> 'Iterator[Tuple]':
    '''
    Group :param:`items` into an :param:`n`-tuples iterable.
    If :param:`discard` is ``True``, incomplete tuples are discarded.
    If not, they are replenished with :param:`fillvalue`.

    Attribution notice:
    by Brian Quinlan in 2004 (http://code.activestate.com/recipes/303060);
    upgraded and slightly extended by Stefan Müller in 2016.

    .. testcode::
        import parse
        print(list(parse.group(range(10), 3)))
        print(list(parse.group(range(10), 3, False)))

    .. testoutput::
        [(0, 1, 2), (3, 4, 5), (6, 7, 8)]
        [(0, 1, 2), (3, 4, 5), (6, 7, 8), (9, None, None)]
    '''
    myzip = zip if discard else partial(zip_longest, fillvalue = fillvalue)
    return myzip(*( islice(items, i, None, n) for i in range(n) ))

def iso2date(
        date: str,
        defaults: 'Tuple[Optional[int], int, Optional[int], int, int, int, int]'
            = (1, 1, 1, 0, 0, 0, 0),
        defaultsign: 'str' = '+',
        defaultzone: 'str' = 'Z',
        ) -> 'Optional[datetime]':
    '''
    Parse :param:`date` as a string conforming with the extended format of
    ISO 8601 and return the corresponding datetime object.

    At the moment, information about the sign and the zone is lost.

    :param defaults: default values of - in this order! - year, month, day
        and hour, minute, second, microsecond. The values chosen here as a
        default are minimum values for each time unit. In order to provide
        maximum defaults, pass the tuple:
        ``(None, 12, None, 23, 59, 59, 999999)``.
        The ``None`` will be replaced by the largest number possible: i.e.
        with the largest year number in the first case and with the number
        of days of the finally given month of the finally given year. If a
        value for year or month is derived from :param:`date`, there is no
        replacement.
    '''
    date = date.strip()
    sign = '-' if date.startswith('-') else defaultsign
    date = date.strip('-+')
    zone, date = extract(r'(Z|z|[-+]\d\d:\d\d)$', date)
    zone = zone if zone else defaultzone
    values = list(defaults)
    if date:
        for pos, value in enumerate(re.split(r'\D+', date)[:7]):
            values[pos] = int(value)
    if values[0] is None:
        values[0] = MAXYEAR
    if values[2] is None:
        values[2] = monthrange(values[0], values[1])[1]
    misecond = values[6]
    if misecond:
        values[6] = misecond * 1000 if misecond < 1000 else misecond
    return datetime(*values)

def iter_plaintext(
        text: str,
        startline: int = 0,
        startchar: int = 0,
        term_ends_with_termchar_re: str = r'[-\w]\Z',
        term_ends_with_hyphenchar_re: str = r'\w-\s*\Z',
        ) -> 'Tuple[str, Tuple[int, int], Tuple[int, int]]':
    '''
    Yield (iteratively, incrementally) every item of :param:`text` in a tuple
    like ``('item', (1, 2), (3, 4))``, which would mean: In :param:`text`,
    the item :str:`item` starts in line 1 at character 2 and ends in line 3
    at character 4.

    This iterator is intended for plain text.
    '''
    endline = startline
    endchar = startchar
    term_ends_with_termchar_comp = re.compile(term_ends_with_termchar_re)
    term_ends_with_hyphenchar_comp = re.compile(term_ends_with_hyphenchar_re)
    in_term = False
    term = ''
    for char in text:
        if char == '\r':
            continue
        elif (term_ends_with_termchar_comp.search(term + char) or
                (char == '\n' and term_ends_with_hyphenchar_comp.search(term))):
            term += char
            if not in_term:
                in_term = True
                startline = endline
                startchar = endchar
        elif in_term:
            yield (term, (startline, startchar), (endline, endchar))
            term = ''
            in_term = False
        if char == '\n':
            endline += 1
            endchar = 0
        else:
            endchar += 1

def iter_wblux(
        text: str,
        startline: int = 0,
        startchar: int = 0,
        term_ends_with_termchar_re: str = r'[^\s|]\Z',
        term_ends_with_hyphenchar_re: str = r'-(<[^>]*?>)*\Z',
        tagstarter: str = '<',
        tagcloser: str = '>',
        source_start_tags: 'Sequence[str]' = ('<tx>',),
        source_close_tags: 'Sequence[str]' = ('</tx>',),
        text_start_tags: 'Sequence[str]' = ('</S>', '</v>', '</tit>'),
        text_close_tags: 'Sequence[str]' = ('<S>', '<v>', '<tit>'),
        ) -> 'Tuple[str, Tuple[int, int], Tuple[int, int]]':
    '''
    Yield (iteratively, incrementally) every item of :param:`text` in a tuple
    like ``('item', (1, 2), (3, 4))``, which would mean: In :param:`text`,
    the item :str:`item` starts in line 1 at character 2 and ends in
    line 3 at character 4.

    .. note::
        This given range may in :param:`text` include more than just
        :str:`item`, e.g. it may also include a tagging which has been
        deliberately excluded from :str:`item`.

    This iterator is intended for the WBLux-project format, developed by
    Britta Weimann. With other values for the keyword arguments, it may be
    used for other formats, too.
    '''
    endline = startline
    endchar = startchar
    pretag_endline = endline
    pretag_endchar = endchar
    term_ends_with_termchar_comp = re.compile(term_ends_with_termchar_re)
    term_ends_with_hyphenchar_comp = re.compile(term_ends_with_hyphenchar_re)
    in_source = False
    in_text = False
    in_term = False
    in_tag = False
    term = ''
    tag = ''
    tag_in_term = ''
    for char in text:
        if char == '\r':
            continue
        elif char == tagstarter:
            in_tag = True
        if in_tag:
            tag += char
            if in_term:
                tag_in_term += char
        elif in_text and (term_ends_with_termchar_comp.search(term + char) or
                (char == '\n' and term_ends_with_hyphenchar_comp.search(term))):
            if tag_in_term:
                term += tag_in_term
                tag_in_term = ''
            term += char
            if not in_term:
                in_term = True
                startline = endline
                startchar = endchar
        elif in_term:
            yield (term, (startline, startchar),
                    (pretag_endline, pretag_endchar))
            term = ''
            tag_in_term = ''
            in_term = False
        if char == '\n':
            endline += 1
            endchar = 0
        else:
            endchar += 1
        if not in_tag:
            pretag_endline = endline
            pretag_endchar = endchar
        if char == tagcloser:
            tag = re.sub(r'\s', '', tag)
            if tag in source_start_tags:
                in_source = True
                in_text = True
            elif tag in source_close_tags:
                in_source = False
                in_text = False
            elif in_source and tag in text_start_tags:
                in_text = True
            elif in_source and tag in text_close_tags:
                in_text = False
            tag = ''
            in_tag = False

def pair(
        text: str,
        opener: str,
        closer: str,
        startpos: int = 0,
        ) -> 'Tuple[int, int]':
    '''
    Search in :param:`text` from :param:`startpos` on.
    Return the position where the first :param:`opener` starts that has
    a matching :param:`closer`, and return the position where this
    :param:`closer` ends. If no pair is found, ``(-1, -1)`` is returned.

    .. note::
        In the following testcode, the start position and the insensible
        delimiters have the single purpose to show the capability of the
        function in a nutshell. The text is a quotation from A. Stifter.

    .. testcode::
        from parse import pair
        text = '.und.. .in dem .brechenden.. Herzen.. bist du...'
        print(text[slice(*pair(text, '.', '..', 3))])

    .. testoutput::
        .in dem .brechenden.. Herzen..
    '''
    # Some precalculations to relieve the loops.
    len_text       = len(text)
    len_opener     = len(opener)
    len_closer     = len(closer)
    opener_shorter = len(opener) < len(closer)
    shorter        = opener if opener_shorter else closer
    longer         = closer if opener_shorter else opener
    len_shorter    = len(opener) if opener_shorter else len(closer)
    len_longer     = len(closer) if opener_shorter else len(opener)
    shorter_add    = 1 if opener_shorter else -1
    longer_add     = -1 if opener_shorter else 1
    endpos         = -1
    pos            = startpos
    level          = 0 # the nesting level
    # First, handle the case that opener and closer are identical;
    # thus, no nesting is possible.
    if opener == closer:
        pos = text.find(opener, startpos)
        if pos == -1:
            return -1, -1
        endpos = text.find(opener, pos + 1)
        if endpos == -1:
            return -1, -1
        return pos, endpos + len_closer
    # Handle the general case. First, find the first opener.
    while pos < len(text):
        if text[pos:pos + len_longer] == longer:
            startpos = pos
            pos += len_longer
            if longer == opener:
                level = 1
                break
        elif text[pos:pos + len_shorter] == shorter:
            startpos = pos
            pos += len_shorter
            if shorter == opener:
                level = 1
                break
        else:
            pos += 1
    if level == 1:
        while pos < len(text):
            if text[pos:pos + len_longer] == longer:
                level += longer_add
                pos += len_longer
            elif text[pos:pos + len_shorter] == shorter:
                level += shorter_add
                pos += len_shorter
            else:
                pos += 1
            if level == 0:
                endpos = pos
                break
    if endpos == -1:
        startpos = -1
    return startpos, endpos

##def pairs(
##        text: str,
##        delimiters: 'Pattern[str]',
##        opener_group_name: str = 'opener',
##        closer_group_name: str = 'closer',
##        ) -> 'Generator[Tuple[int, int, int, int, str]]':
##    '''
##    From :param:`text`, yield all matching pairs of delimiters described by
##    :param:`delimiters`. Delimiters may be nested. Nested delimiters are
##    yielded from the inside outwards; in order to obtain document order,
##    the yielded result just needs to be sorted. Because:
##
##    Each pair is a tuple consisting of:
##
##    - the integer position where the opening delimiter starts,
##    - the integer position where the opening delimiter ends,
##    - the integer position where the closing delimiter starts,
##    - the integer position where the closing delimiter ends,
##    - the so-called tag of this pair. The tag is derived from
##      :param:`delimiters`, which is explained next.
##
##    :param:`delimiters` must be built as follows (see also the testcode below):
##
##    - It consists entirely of one non-capturing group.
##    - This group contains exactly two alternatives.
##    - Each of these alternatives is a capturing named group.
##    - The one group is named with :param:`opener_group_name`.
##    - The other group is named with :param:`closer_group_name`.
##    - Note: Either of these two groups may come first; but if one
##      describes a subsequence of the other, it must come second.
##    - Each of the groups may contain one or more arbitrarily named
##      subgroups. If a group matches, its named subgroups are joined
##      together to a string, henceforth called `tag`.
##    - If there is no named subgroup, the tag is always the empty
##      string, and the pairing is not influenced by it.
##
##    An opening and a closing delimiter are paired if and only if
##
##    - they have the same tag (which may be the empty string) and
##    - they are on the same nesting level.
##
##    .. testcode::
##        import re
##        import parse
##
##        text = 'da steigt <edit mode="add">ja <name>der lange</edit>'\
##               ' <name>Nostitz</name></name> aus der Versenkung.'
##        # Note: This is neither XML nor HTML. Keep calm and carry on.
##
##        delimiters = re.compile(r"""(?sx)
##                (?:
##                    (?P<opener>
##                        <(?P<tag1>[^>/\s]+)([^"'>/]*?|".*?"|'.*?')*?>
##                    )
##                |
##                    (?P<closer>
##                        </(?P<tag2>[^>/\s]+)\s*>
##                    )
##                )
##                """)
##
##        for o_start, o_end, c_start, c_end, tag in parse.pairs(text, delimiters):
##            print(tag, '=', re.sub(r'<.*?>', '', text[o_end:c_start]))
##
##    .. testoutput::
##        edit = ja der lange
##        name = Nostitz
##        name = der lange Nostitz
##    '''
##    openers = defaultdict(deque)
##    for match in delimiters.finditer(text):
##        groups = match.groupdict(default = '')
##        opener = groups.pop(opener_group_name, '')
##        closer = groups.pop(closer_group_name, '')
##        tag = ''.join(groups.values())
##        span = match.span()
##        if opener:
##            openers[tag].append(span)
##        elif closer:
##            try:
##                opening_span = openers[tag].pop()
##            except IndexError:
##                # The closer has no corresponding opener.
##                continue
##            yield (*opening_span, *span, tag)

def pairs_first(
        text: str,
        delimiters: 'Pattern[str]',
        opener_group_name: str = 'opener',
        closer_group_name: str = 'closer',
        ) -> 'Tuple[int, int, int, int, str]':
    '''
    Get the first result of :func:`pairs`.
    If none is found at all, return ``(-1, -1, -1, -1, '')``.
    '''
    for result in pairs(
                text,
                delimiters,
                opener_group_name,
                closer_group_name,
                ):
        return result
    return (-1, -1, -1, -1, '')

def roman2arabic(
        number: str,
        numeral_map: 'Tuple[Tuple[int, str]]' = ROMAN_NUMBERS_MAP,
        ) -> int:
    '''
    Attribution notice:
    In essence by Mark Pilgrim, Paul Winkler, Tim Valenta, dln385.
    See http://code.activestate.com/recipes/81611-roman-numerals/.
    '''
    number = number.upper()
    i = result = 0
    for integer, numeral in numeral_map:
        while number[i:i + len(numeral)] == numeral:
            result += integer
            i += len(numeral)
    return result
