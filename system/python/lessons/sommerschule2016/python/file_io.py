# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Münchener Sommerschule, in 2016.

import csv
import os

def get_text(path, encoding = 'utf-8'):
    '''
    Get the text contained in :param:`path`.
    '''
    with open(path, 'r', encoding = encoding) as file:
        text = file.read()
    return text

def get_new_path(old_path, diff_sign = '+', ext = None):
    '''
    From :param:`old_path`, derive a path that does not yet exist.
    This path is the same as :param:`old_path`, if this path does
    not exist, or a path like the the old one, but with the basename
    extended by :param:`diff_sign` as many times as necessary to
    obtain a unique path.

    :param:`old_path` may be a relative path. The returned path is
    an absolute path.

    :param ext: If not ``None``, it is used instead of the
        extension of :param:`old_path`.
    '''
    old_path = os.path.abspath(old_path)
    base, extension = os.path.splitext(old_path)
    if ext is not None:
        extension = ext
    # There must be exactly one full stop at the start of the extension:
    extension = '.' + extension.lstrip('.')
    while os.path.exists(base + extension):
        base += diff_sign
    # At this point, ``base + extension`` is a unique path:
    return base + extension

def write_csv(path, table, encoding = 'utf-8', cell_delimiter = '\t'):
    '''
    Write the list of lists of strings :param:`table` in path
    as a csv file.
    '''
    with open(path, 'w', encoding = encoding, newline = '') as file:
        writer = csv.writer(file, delimiter = cell_delimiter)
        for row in table:
            writer.writerow(row)
    print('\nI have written a new csv file:')
    print('    Path:', path)
    print('    Encoding:', encoding)

def test():
    print('Das ist ein Test.')
    # In relative paths, `..` directs to the parent folder:
    path = '../mi.txt'
    new_path = get_new_path(path, ext = '.csv')
    test_table = [ ['Wert ä', 'Wert ö'], ['Wert Ä', 'Wert Ö'] ]
    write_csv(new_path, test_table, encoding = 'utf-16')
    # With these settings, the written file can be opened in e.g. Excel
    # for a quick look.

if __name__ == '__main__':
    test()
