# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2013 ff.
import math

def luxify_first_line(names: 'Iterable[Tuple[int, str]]') -> str:
    '''
    `[(1, "name1"), (2, "name2")]` -> `"§1(name1)2(name2)\n"`
    '''
    if names:
        return '§' + luxify_line(names)
    return '§\n'

def luxify_line(line: 'Iterable[Tuple[int, str]]') -> str:
    '''
    `[(1, "value1"), (2.1, "value2")]` -> `"1(value1)2.1(value2)\n"`
    '''
    if line:
        return ')'.join( str(key) + '(' +
                str(value).replace('#', '#1').replace('(', '#2')
                          .replace(')', '#3').replace('\n', '#4')
                for key, value in line ) + ')\n'
    return ''

def prettify_lux(iterator: 'Iterable[Iterable[Tuple[int, str]]]') -> str:
    '''
    lux file iterator ->
    "§key1(name1)  key2(name2)
    key1(value1)   key2(value2)  key3(value3)
    key1(value11)                key3(value13)
    "
    '''
    names = [ (math.trunc(key), value) for key, value in
            read_lux_first_line(next(iterator)) ]
    table = [ read_lux_line(line) for line in iterator ]
    table = [ [ (key, value.replace('#', '#1').replace('(', '#2')
            .replace(')', '#3').replace('\n', '#4')) for key, value in line ]
            for line in table ]
    lengths = { key: 0 for key, _ in names }
    for values in table:
        for key in lengths:
            lengths[key] = max([lengths[key]] +
                    [ len(value) + len(str(floatkey)) for floatkey, value
                      in values if math.trunc(floatkey) == key ])
    lengths = [ (key, lengths[key] + 4) for key, _ in names ]
    table_ = [luxify_first_line(names)[:-1]] # without linefeed char
    for values in table:
        values_ = []
        for key, length in lengths:
            values_.append(''.join( str(floatkey) + '(' + value + ')' for
                    floatkey, value in values if math.trunc(floatkey) ==
                    key ).ljust(length))
        table_.append(''.join(values_))
    return '\n'.join(table_)

def read_lux_first_line(first_line: str) -> 'Iterable[Tuple[int, str]]':
    '''
    `"§1(name1)2(name2)"` -> `[(1, "name1"), (2, "name2")]`
    That must not be transformed in a dictionary, because the given order is
    to be preserved.
    '''
    # Remove the ``§`` character and any BOM:
    return read_lux_line(first_line.split('§', 1)[1])

def read_lux_line(line: str) -> 'Iterable[Tuple[int, str]]':
    '''
    `"1(value1)2.1(value3)\n"` -> `[(1, "value1"), (2.1, "value2")]`
    To explain the use of floats here:

        ...
        2(value2)
        ...
            2.1(value3)
        ...
        2(value2)
        ...
            2.1(value3)
        ...

    That means:

        ...
        In category 2, value2 is set.
        ...
        In category 2, value3 is set and interrupts value2. (A mere `2(value3)`
        would have superseded value2, not interrupted it.)
        ...
        In category 2, value2 - the same as the value2 above - continues,
        interrupting value3.
        ...
        In category 2, value3 - the same as the value3 above - continues,
        interrupting value2 (if `2(value2)` occurs later on) or superseding it
        (if `2(value2)` does not occur later on).

    Thus, ignoring the special float-rules, i.e. treating a float like the
    rounded down int, e.g. `2.1` like `2`, one looses only the information if
    a value continues an earlier value as the same, not only equal value; but
    one does not err in the regard which value is valid for which position.
    '''
    values = line.split(')')
    # Remove last parenthesis and any linefeed or carriage-return character:
    values.pop()
    return [ (float(key) if '.' in key else int(key), value) for key, value in
            ( value.replace('#4', '\n').replace('#3', ')')
                   .replace('#2', '(').replace('#1', '#').split('(', 1)
              for value in values ) ]
