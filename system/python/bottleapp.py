# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2013 ff.
'''
:func:`get` builds and returns a bottle ap.
:func:`run` runs such an app.
See the docstrings of these functions.
'''
import os
import sys
import time

import bottle

def get(
        app_class: type,
        dao_class: type,
        dao_kwargs: 'Dict[str, Any]' = {},
        https: bool                  = False,
        host: str                    = 'localhost',
        port: int                    = 8080,
        debug: bool                  = True,
        server: bottle.ServerAdapter = bottle.WSGIRefServer,
        views_paths: 'Sequence[str]' = [__file__ + '/../views'],
        cssjs_path: str              = __file__ + '/../../cssjs',
        ) -> 'bottle.Bottle':
    '''
    Set up two objects, an app object and a data access object: The app object
    receives a request at a given domain and port, interprets it and exchanges
    data with the data access object, fills the data into a template and sends
    the template-generated HTML back as answer to the request. The data access
    object is an interface to a database or something equivalent.

    .. note::
        Spoken with the terms of the model-view-controller conception:

        - The model is the data access object.
        - The views are the templates found at :param:`views_paths`.
        - The controller is the app object (and this function itself).

    :param app_class: a class to build the app object.
        This object receives, routes and responds to all web requests.
        For a small example, see :class:`App` in :path:`portal/web_io.py`.
        This object is a kind of :class:`bottle.Bottle` object.
        One may further consult the documentation at http://bottlepy.org,
        notably http://bottlepy.org/docs/dev/api.html.
    :param dao_class: a class to build the data access object.
        This object exchanges data with the app object and handles the
        further input and output of the data into and from a database
        or something equivalent.
        For a small example, see :class:`Folder` in :path:`portal/dao_io.py`.
    :param dao_kwargs: contains names and values of keyword arguments with
        which the above mentioned data access object is initialized.
        See the ``__init__`` method of ``dao_class`` for further details.
    :param https: whether to use SSL or not, i.e., whether you provide an
        SSL credentials and the site is to be accessed at https.
        SSL credentials can be passed in the ``run`` method of ``app_class``.
    :param host: the host of the app, i.e. an IP address or ``'localhost'``.
    :param port: the port of the app, e.g. ``80`` or ``8080``.
    :param debug: If ``True``, an exception will be announced in the browser
        with full stack trace. That is desirable in development, but not in
        production. If ``False``, a short error message is shown. Moreover,
        the app is simply and immediately restarted if any exception occurs,
        which is not desirable in development, but arguably so in production.
        Further on, this parameter can be used in the templates to decide
        whether to include a meta tag like
        :html:`<meta name="robots" content="noindex, nofollow"/>`, which
        might be sensible for a mere test instance.
    :param server: the server that will be used to serve the app: a subclass
        of :class:`bottle.ServerAdapter`. Will be used in the method ``run``
        of :param:`app_class` and can be modified or totally re-built there
        for very specific settings.
    :param views_paths: An item of this sequence is a path to an (actual or
        virtual) directory containing templates used by the app.
        Note: A template will be looked up in the given order of the paths.
        Example: ``'base.tpl'`` is found in the first given directory, so this
        template is used, whereas ``'index.tpl'`` is not found in the first
        given directory but in the second, so the template in the second is
        used. Accordingly you may specify a directory containing customized
        templates first and a directory containing default templates second.
    :param cssjs_path: a path to the (actual or virtual) directory which is
        the root folder of the subfolders containing CSS or Javascript files
        for client-side styling or processing.
    '''
    return app_class(
            dao_class,
            dao_kwargs,
            https,
            host,
            port,
            debug,
            server,
            views_paths,
            cssjs_path,
            )

def run(app: 'bottle.Bottle') -> None:
    '''
    Run :param:`app`, an object like those returned by :func:`get`. If the app
    is not in debug mode, any exception is caught (and printed to the standard
    output), before the app is started again.
    '''
    if app.debug:
        app.run()
    else:
        while True:
            try:
                app.run()
            except: # Sic.
                time.sleep(1)
