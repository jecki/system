# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2013 ff.
import os

import __init__
import bottle
from bottle import HTTPError, redirect, request, static_file, template
import file_io
import rdf

class App(bottle.Bottle):
    '''
    A bottle application object.
    See the docstring of :func:`bottleapp.get` in :path:`../../bottleapp.py`.
    '''
    def __init__(
            self,
            dao_class: type,
            dao_kwargs: 'Dict[str, Any]',
            https: bool,
            host: str,
            port: str,
            debug: bool,
            server: str,
            views_paths: 'Sequence[str]',
            cssjs_path: str,
            ):
        super().__init__()
        self.https = https
        self.host = host
        self.port = port
        self.debug = debug
        self.server = server
        self.cssjs_path = os.path.abspath(cssjs_path) + os.sep
        self.views_paths = [ os.path.abspath(p) + os.sep for p in views_paths ]
        bottle.TEMPLATE_PATH = self.views_paths
        self.log = file_io.Log(__file__)
        self.dao = dao_class(**dao_kwargs)
        self.set_routes(self.dao)

    def run(self) -> None:
        '''
        Run :param:`self` by calling :meth:`bottle.run`. Before or in this call,
        you may apply middleware, e.g. instead of mere ``app = self``, pass
        ``app = SessionMiddleware(self, self.dao.session_options)``.
        '''
        bottle.run(
                app = self,
                host = self.host,
                port = self.port,
                debug = self.debug,
                server = self.server(
                    self.host,
                    self.port,
                    ),
                )

    def set_routes(self, dao) -> None:
        '''
        Define the request-handling functions and assign them to routes.
        '''
        @self.route('<path:path>/')
        def redirect_endslash(path):
            '''
            Redirect requests whose URL ends with `/`: Ignore the trailing `/`.
            '''
            redirect(path)

        @self.route('/cssjs/<path:path>')
        def return_cssjs(path):
            '''
            Return a CSS or JS file for client-side styling or processing.
            For static content files, see :func:`.return_static_content`.

            Example URLs are:

            - `/cssjs/jquery/jquery.js`
            - `/cssjs/normalize/normalize.css`

            The path of the file on the server must be the joint of
            :attr:`self.cssjs_path` and :param:`path`.
            Otherwise, the access is denied.
            '''
            return static_file(path, self.cssjs_path)

        @self.route('/')
        @self.route('/<site_id>')
        @self.route('/<site_id>/<page_id>')
        def redirect_short_url(lang_id = '', site_id = '', page_id = ''):
            '''
            Redirect requests which specify only `site_id` and `page_id` or
            only `site_id` or nothing at all:

            - If `site_id` is either lacking or not known in the portal,
              `dao.default_site_id` is assumed and written in the URL.
            - For `lang_id`, the default `lang_id` of the site is assumed and
              written in the URL.
            - For `page_id`, the default `page_id` of the site is assumed and
              written in the URL.

            The redirected request will be dealt with by :func:`.return_page`.
            Further explanation is found in the docstring of this function.
            '''
            if not (site_id and site_id in dao.site_ids):
                site_id = dao.default_site_id
            lang_id = dao.default_lang_ids[site_id]
            if not page_id:
                page_id = dao.default_page_ids[site_id]
            redirect('/{}/{}/{}'.format(lang_id, site_id, page_id))

        @self.route('/<lang_id>/{}/search_fulltext'.format(dao.default_site_id))
        def return_search_form(lang_id):
            '''
            Return a page with a full-text search form.

            :param lang_id: See :func:`.return_page`.
            '''
            return template(
                    'search_form.tpl', request = request, dao = dao,
                    kwargs = {
                        'text_lang_id': lang_id,
                        'lang_id': lang_id,
                        'site_id': dao.default_site_id,
                        'page_id': 'search_fulltext',
                        })

        @self.route('/<lang_id>/{}/search_results'.format(dao.default_site_id))
        def return_search_results(lang_id):
            '''
            Return a page containing the results of a search triggered by
            :func:`.return_search_form`.
            '''
            return template(
                    'search_results.tpl', request = request, dao = dao,
                    kwargs = {
                        'text_lang_id': lang_id,
                        'lang_id': lang_id,
                        'site_id': dao.default_site_id,
                        'page_id': 'search_results',
                        })

        @self.route('/<lang_id>/<site_id>/<page_id>')
        def return_page(lang_id, site_id, page_id):
            '''
            Return the page specified by the URL.

            If :param:`site_id` is not known within the portal,
            redirect to the default site.

            If :param:`lang_id` does not denote a supported language,
            redirect to its default language.

            If :param:`page_id` does not denote a page of the site,
            raise the HTTP error 404 (`Not found`).

            :param lang_id: As a rule, it is an id conforming to ISO 639 and
                http://tools.ietf.org/html/rfc5646, page 4 (‘shortest ISO 639
                code sometimes followed by extended language subtags’).
                It must not be str:`-` in order to exclude any confusion
                with the route of :func:`.return_static_code`.
            '''
            if site_id not in dao.site_ids:
                site_id = dao.default_site_id
                redirect('/{}/{}/{}'.format(
                        lang_id,
                        site_id,
                        dao.default_page_ids[site_id]))
            if lang_id not in dao.lang_ids[site_id]:
                redirect('/{}/{}/{}'.format(
                        dao.default_lang_ids[site_id],
                        site_id,
                        page_id))
            template_id, text, text_lang_id = dao.get_template_id_and_text(
                    lang_id, site_id, page_id)
            if text:
                if text[:3] == b'\xef\xbb\xbf':
                    text = text[3:]
                return template(
                        template_id, request = request, dao = dao,
                        kwargs = {
                            'text_lang_id': text_lang_id,
                            'lang_id': lang_id,
                            'site_id': site_id,
                            'page_id': page_id,
                            'text': text,
                            })
            else:
                raise HTTPError(404, dao.glosses['httperror404'][lang_id])

        @self.route('/<lang_id>/<site_id>/<page_id>/rdf.nt')
        def return_rdf_ntriples(lang_id, site_id, page_id):
            '''
            Return RDF triples as a UTF-8 encoded plain text of N-Triples.
            The triples are extracted from the RDFa annotation of the web page
            specified by :param:`lang_id`, :param:`site_id`, :param:`page_id`.
            '''
            try:
                doc = return_page(lang_id, site_id, page_id)
            except HTTPError:
                body = ''
            else:
                doc = dao.add_ajax_source(doc, site_id)
                parser = rdf.RDFaParser(base = '{}://{}/{}/{}/{}'.format(
                        request.urlparts[0],
                        request.urlparts[1],
                        # As lang_id, take the lang_id of the canonical page:
                        dao.get_text_lang_id_and_path(
                            lang_id, site_id, page_id)[0],
                        site_id,
                        page_id))
                parser.parse(doc)
                body = rdf.convert_triples2nt(parser.triples)
            return bottle.HTTPResponse(
                    body = body,
                    # black magic in bottle.py:
                    **{'Content-Type': 'text/plain; charset=utf-8'})

        @self.route('/<_>/<path:path>')
        def return_static_content(_, path):
            '''
            Return a static content file, e.g. an image file.

            Example URLs are:

            - `/de/site/icons/shortcut.ico`
            - `/de/acta/secreta/secretissima/1890-03-20.pdf?download=true`

            If the URL contains a query string with a key `download` and a
            non-empty value, the static file is served for download.

            The path of the file on the server must be the joint of
            :attr:`dao.path` and :param:`path`. Otherwise, the access is denied.

            :param _: is included in the route so that in a page with the URL
                `/de/mysite/start` a relative URL `icons/shortcut.ico` can be
                used. For the absolute version of this relative URL would be:
                `/de/mysite/icons/shortcut.ico`.
            '''
            return static_file(
                    path,
                    dao.path,
                    download = bool(request.query.download))
