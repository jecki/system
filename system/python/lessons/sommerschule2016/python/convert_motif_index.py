# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Münchener Sommerschule, in 2016.
import re

import file_io

def parse_text(text):
    '''
    Split the text in lines representing the entries of the motif index.
    '''
    lines = []
    for line in text.split('\n'):
        line = line.strip()
        # Now, some lines are just empty. Proceed only with a non-empty line:
        if line:
            # Some lines are separated by a newline but belong to the preceding
            # record and are not a record on their own. They do not start with
            # a sigle. Try to recognize them and add them to the preceding line.
            # Check before, whether there already is at least one line, to which
            # we can add. Otherwise, an IndexError would be thrown.
            if not re.search(r'^[A-Z]\d+\.', line) and lines:
                lines[-1] += ' ' + line
            else:
                lines.append(line)
    return lines

def parse_lines(lines):
    '''
    Split every line in :param:`lines` into siglum, name, references.
    '''
    entries = []
    for line in lines:
        # The siglum reaches to the next whitespace, the name to the next
        # full stop, the references are the rest of the string.
        match = re.search(r'(\S+)([^.]*)(.*)', line)
        siglum, name, references = match.group(1, 2, 3)
        # From name and references, strip the whitespace and the full stop.
        # They could have been excluded by the regular expression, but
        # then the expression gets rather complicated if we also care about
        # the cases when there are no references or, maybe, not even a name.
        # (In these cases and with the regex above, references or name are
        # an empty string.)
        name = name.strip()
        references = references.lstrip('.').strip()
        # Make a new empty and append it:
        entry = [siglum, name, references]
        entries.append(entry)
    return entries

def get_tables(entries):
    '''
    Derive multiple tables from :param:`entries`, namely:

    - A table of motifs.
    - A table of sources.
    - A table of secondary literature.
    - A table of relations between motifs and sources.
    - A table of relations between motifs and secondary literature.
    - A table of relations between motifs and other motifs, e.g.:

      - More generic motifs (e.g. A100 vs. A100.1).
      - Similar motifs (introduced by ``'Cf.'``).

    - #### To be done
    '''
    motif_table = entries # TODO: def get_motif_table(entries)
    # TODO: def get_source_table(entries)
    return [
            motif_table,
            # TODO: return more tables, see above.
            ]

def main(path):
    '''
    Convert a Nordic motif index into csv tables ready for
    being imported into a relational database.
    '''
    text = file_io.get_text(path)
    # Remove the BOM, if there is one:
    text = text.lstrip('\ufeff')
    lines = parse_text(text)
    entries = parse_lines(lines)
    tables = get_tables(entries)
    for table in tables:
        path = file_io.get_new_path(path, ext = '.csv')
        file_io.write_csv(path, table)
        # Make a second csv file which can be opened in Excel for a quick look:
        path = file_io.get_new_path(path, ext = '.csv')
        file_io.write_csv(path, table, encoding = 'utf-16')

if __name__ == '__main__':
    # In relative paths, `..` directs to the parent folder:
    path = '../motif-index.txt'
    main(path)
