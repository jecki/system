% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2013 ff.
% # Template for the title page, i.e. start page; gets filled into the base template.
% # Only necessary to set the RDFa subject, which must be the whole edition.
% # In the example edition: http://localhost:8080/example/start.
<main about="/{{kwargs.get('site_id', '')}}">
{{!kwargs['text']}}
</main>\\
% rebase('base_jordanus.tpl', request = request, dao = dao, kwargs = kwargs)