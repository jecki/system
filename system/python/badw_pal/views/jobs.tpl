% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
% vacancies = kwargs['vacancies']
<main>
<article class="sheet sheetwide">
	<section class="flush_unhyph">
	{{!kwargs['general']}}
	</section>
  % if vacancies:
	<h1>Currently available positions</h1>
  % else:
	<p>At the moment no position in our project is available.</p>
  % end
  % for vacancy in vacancies:
	<section>
		<h2>{{vacancy['job_title']}}</h2>
		<p><time>{{vacancy['job_posted']}}</time></p>
		{{!vacancy['job_description']}}
	</section>
  % end
	<p><a class="ref" href="/jobs_archive">Jobs Archive</a></p>
</article>
</main>\\
% rebase('base.tpl', request = request, dao = dao, kwargs = kwargs)