# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2017 (© http://badw.de)
'''
Start :meth:`db_io.DB.check_and_add` (see the docstring of this method).
'''
import getpass
import sys
import __init__
import sys_io
import file_io
from badw_reldb import db_io

def main(
        db_access_path: str,
        config_path: str,
        is_adding: bool = False,
        permaloop: bool = False,
        ):
    '''
        :param db_access_path: Relative path to a file containing a representation
            of user name and password of the database to be checked.
    '''
    dbuser, dbpassword = file_io.get_keys(db_access_path)
    if not (dbuser and dbpassword):
        dbuser = input('Database Username: ')
        dbpassword = getpass.getpass('Password: ')
    # Initialize database. See the description in :meth:`db_io.DB.__init__`.
    db = db_io.DB(
            dbhost     = 'mysql-badw1.sql.lrz.de',
            dbport     = 3306,
            dbname     = 'badw',
            dbuser     = dbuser,
            dbpassword = dbpassword,
            )
    db.check_and_add(
            config_path = config_path,
            is_adding = is_adding,
            permaloop = permaloop,
            )

main.__doc__ = db_io.DB.check_and_add.__doc__ + main.__doc__

if len(sys.argv) == 1:
    main(
            db_access_path = __file__ + '/../../../../../../6.1.13 Publikationsserver/db',
            config_path = __file__ + '/../../../../../../6.1.13 Publikationsserver/seite_publikationen/configuration.utf-8.ini',
            )
else:
    main(**sys_io.get_call_args_for(
            main,
            description = main.__doc__,
            ))
