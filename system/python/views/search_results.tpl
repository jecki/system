% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2013 ff.
% # Template for displaying search results; gets filled into the base template.
% # In the example edition:
% # http://localhost:8080/de/portal/search_results?term=wohlgebore%3Fn&no_case=on&regex=on&status_id=source&example=example.
% lang_id = kwargs.get('lang_id', '')
<main>
<article class="card non-source">
	<h1>{{dao.glosses['search_results'][lang_id]}}</h1>
  % for status, url, text, found_terms in dao.search_fulltext(request.query.term, no_case = bool(request.query.no_case), regex = bool(request.query.regex), single_words = bool(request.query.single_words), status_ids = request.query.getall('status_id'), site_ids = request.query.getall('site_id')):
	% text = text.replace('<', '‹')
	% text = text.replace('>', '›')
	% text = text.replace('&', '﹠')
	% for found_term in found_terms:
	  % found_term = found_term.replace('<', '‹')
	  % found_term = found_term.replace('>', '›')
	  % found_term = found_term.replace('&', '﹠')
	  % text = text.replace(found_term, '<mark>' + found_term + '</mark>')
	% end
	<p><a class="key" href="/{{url}}"><b>{{url}}:</b> <span class="{{status}}">{{!text}}<span></a></p>
  % end
</article>
</main>
% rebase('base.tpl', request = request, dao = dao, kwargs = kwargs)
