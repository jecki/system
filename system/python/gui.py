# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2013 ff.
import tkinter as tk
import tkinter.font as tkfont
import tkinter.ttk as ttk

def get_tlabel_font_text_width(text: str) -> int:
    return tkfont.Font(font = ttk.Style().lookup(
            'Button.TLabel','font')).measure(text)

def get_toplevel_decoration_height(window: tk.Toplevel) -> int:
    return (window.winfo_rooty() - window.winfo_y()
            # : height of the upper decoration of the window
            + window.winfo_rootx() - window.winfo_x())
            # : height of the lower decoration, guessed from the left one

def get_toplevel_decoration_width(window: tk.Toplevel):
    return (window.winfo_rootx() - window.winfo_x()) * 2
            # height of the left decoration, doubled

def lay(parent, kind, row, column, rowspan, columnspan,
        configurations = {}, sticky = 'wnes',
        # padx/y: tuple instead of number, if the paddings shall be unequal:
        padx = 0, pady = 0, ipadx = 0, ipady = 0,
        # with regard to the parent:
        xpad = 0, xminsize = 0, xweight = 0, xuniform = '',
        ypad = 0, yminsize = 0, yweight = 0, yuniform = ''):
    if any((xminsize, xweight, xuniform, xpad)):
        parent.columnconfigure(column, minsize = xminsize,
                weight = xweight, uniform = xuniform, pad = xpad)
    if any((yminsize, yweight, yuniform, ypad)):
        parent.rowconfigure(row, minsize = yminsize,
                weight = yweight, uniform = yuniform, pad = ypad)
    widget = kind(parent, **configurations)
    widget.grid(
            row = row,
            column = column,
            rowspan = rowspan,
            columnspan = columnspan,
            sticky = sticky,
            padx = padx,
            pady = pady,
            ipadx = ipadx,
            ipady = ipady,
            )
    return widget

def lay_scrollbars(frame, scrollable, x_offset = 0, y_offset = 0):
    scrollbar_h = lay(
            frame, ttk.Scrollbar, x_offset + 1, y_offset + 0, 1, 1,
            {'orient': 'horizontal', 'command': scrollable.xview},
            xweight = 1, yweight = 0)
    scrollbar_v = lay(
            frame, ttk.Scrollbar, x_offset + 0, y_offset + 1, 1, 1,
            {'orient': 'vertical', 'command': scrollable.yview},
            xweight = 0, yweight = 1)
    scrollable['xscrollcommand'] = scrollbar_h.set
    scrollable['yscrollcommand'] = scrollbar_v.set
