# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2013 ff.
import os
import statistics
from collections import defaultdict
from collections import deque
from decimal import Decimal as Dec
from operator import itemgetter

import __init__
import bottle

bottle.TEMPLATE_PATH = [os.path.abspath(os.path.join(__file__, '..', 'views'))]

def get_cell_formatted(
        ratio: 'Optional[float]',
        mad: float,
        total: float,
        tablemad: float,
        convert: 'Callable[[float], str]' =
                lambda ratio: '{:.0%}'.format(ratio)[:-1],
        cell_pattern: str       =
            '{spread_marker}{ratio_converted}{few_marker}{hint_at_total}',
        hint_at_total: str      = '<sub><i>{total}</i></sub>',
        few_limit: int          = 15,
        few_marker: str         = '<sub>?</sub>',
        too_few_limit: int      = 5,
        too_few_marker: str     = '?',
        too_few_background: str = '#ffffff',
        too_few_color: str      = '#000000',
        spread_marker: str      = '<sub>≈</sub>',
        text_bright: str        = '#ffffff',
        text_dark: str          = '#000000',
        ) -> 'Tuple[str, str, str]':
    '''
    Get a tuple consisting of:

    - a background color determined by :param:`ratio`: a grey tone
      corresponding with the ratio value, and white for a ratio of 0,
      black for a ratio of 1.
    - a text color: dark text for grey brighter than fifty percent, else
      bright text.
    - a value, converted from :param:`ratio` and formatted according to
      :param:`mad` and :param:`total`, compared with :param:`tablemad`,
      :param:`few_limit` and :param:`too_few_limit`. If :param:`total`
      is less than :param:`too_few_limit`, then:

      - the value is set to :param:`too_few_marker`,
      - the background is set to :param:`too_few_background`,
      - the text color is set to :param:`too_few_color`.

    If :param:`ratio` is ``None``, :param:`total` is expected to be ``0``.
    This case is handled like all cases in which :param:`total` is less
    than :param:`too_few_limit`, regardless of the actual value of this
    parameter.

    :param cell_pattern: becomes converted to the cell value with
        :meth:`str.format`, which takes the following keyword arguments:

        - ratio_converted, i.e :param:`ratio` after having been processed as
          argument of :param:`convert`.
        - the following three keyword arguments of this function itself:

    :param spread_marker: the empty string or :param:`spread_marker`, if the
        spread in the cell is greater than the spread in the whole table.
    :param few_marker: the empty string or :param:`few_marker`, if the
        total value is less than :param:`few_limit` and greater than
        :param:`too_few_limit`. This is skipped if the following parameter
        is not the empty string:
    :param hint_at_total: becomes converted to a hint at the total value of
        the cell with :meth:`str.format`, which takes the total value as
        keyword argument.
    '''
    if total < too_few_limit or ratio == None:
        ratio_converted = too_few_marker
        background      = too_few_background
        color           = too_few_color
        few_marker      = ''
        spread_marker   = ''
        hint_at_total   = hint_at_total.format(total = total) \
                          if hint_at_total else ''
    else:
        # For the grey scale, map the ratio to a corresponding
        # value between 10 and 245 (not 0 and 255 to avoid total
        # black and white).
        grey            = hex(int(10 + 235 - 235 * ratio))
        background      = '#{0:0>2}{0:0>2}{0:0>2}'.format(grey[2:])
        color           = text_dark if ratio < 0.5 else text_bright
        spread_marker   = spread_marker if mad > tablemad else ''
        ratio_converted = convert(ratio)
        if hint_at_total:
            hint_at_total = hint_at_total.format(total = total)
            few_marker    = ''
        else:
            few_marker    = few_marker if total < few_limit else ''
            hint_at_total = ''
    return background, color, cell_pattern.format(
            spread_marker   = spread_marker,
            ratio_converted = ratio_converted,
            hint_at_total   = hint_at_total,
            few_marker      = few_marker,
            )

def get_chi(table: 'Sequence[Sequence[float]]') -> float:
    '''
    Calculate a value like the chi-square value, but without squaring.

    .. note::
        This measure of spread is to be used in comparisons with other, in
        the same manner calculated values, e.g. in a comparison with the
        the measure of spread of the same data, grouped to a different table.

    :param table: a sequence of sequences which have the same length.
    '''
    total = sum( sum(row) for row in table )
    if total == 0:
        return 0.0
    else:
        columns = tuple(zip(*table))
        return sum( sum( abs(sum(row) * sum(columns[nth]) / total - cell)
                         for nth, cell in enumerate(row) )
                    for row in table
                    ) / (len(table) * len(columns))

def get_diffsum(
        table: 'Tuple[Tuple[float, float], Tuple[float, float]]',
        normalize_table: bool = True,
        ) -> float:
    '''
    From the four values in :param:`table`, get a value representing the
    contrast between the two rows:

    - ``0`` means, that there is no contrast, i.e. the values are equally
      distributed comparing the rows.
    - a value greater than ``0`` means that in the first row the first
      value is greater than the second, or that in the second row the second
      value is greater than the first, or both.
    - a value lesser than ``0`` means that in the first row the second
      value is greater than the first, or that in the second row the first
      value is greater than the second, or both.
    - If the table is normalized, ``2`` and ``-2`` indicate the maximum
      contrast.

    A table is normalized if the sum of each row is ``1`` (by using relative
    instead of absolute values).

    The normalization makes the resulting value comparable with any other
    value that likewise results from this function using a normalized table.

    :param normalize_table: If true, normalize :param:`table`.
    '''
    row0 = table[0]
    row1 = table[1]
    if normalize_table:
        message = 'The sum of a row is zero. The table is:\n{}'.format(table)
        sum0 = sum(row0)
        if sum0 == 0:
            raise Exception(message)
        row0 = (row0[0] / sum0, row0[1] / sum0)
        sum1 = sum(row1)
        if sum1 == 0:
            raise Exception(message)
        row1 = (row1[0] / sum1, row1[1] / sum1)
    return row0[0] - row0[1] + row1[1] - row1[0]

def get_heatmaps(
        results: 'Iterable[Hashable]',
        matchtable: 'Iterable[Iterable[Hashable]]',
        template_name: str,
        casegetter: itemgetter = itemgetter(0),
        matchgetter: itemgetter = itemgetter(1, 2),
        variantgetter: itemgetter = itemgetter(3),
        default_factory: 'Callable[[], float]' = lambda: 0,
        total: float = 0,
        format_config: 'Dict[str, Union[str, int, Callable[[float], str]]]' =
            {},
        ) -> str:
    '''
    Get HTML charts from :param:`results`, each structured like
    :param:`matchtable`; the cells are greyed proportionally to the
    cell total divided by the sum of all corresponding cell totals over
    all cases. The cases are drawn from the items of :param:`data` by
    :param:`casegetter`. For more details and for the other parameters,
    see :func:`.tabulate`, except for :param:`format_config`, whose
    function is to customize the formatting: Its entries are given as
    keyword arguments to :func:`.get_cell_formatted`. For more details,
    see this function.
    '''
    data = defaultdict(int)
    for key in results:
        data[key] += 1
    tables, total = tabulate(data, matchtable, casegetter, matchgetter,
            variantgetter, default_factory, total)
    totals_table = get_table_of_totals(tables)
    ratio_tables = get_tables_of_case_ratios(tables)
    stats_tables = { case:
            get_table_of_derived_cell_values(ratio_tables[case])
            for case in tables }
    total_tables = { case:
            get_table_of_derived_cell_values(
                tables[case], derive = lambda values: {'total': sum(values)})
            for case in tables }
    ratio_total_tables = get_tables_of_case_ratios(total_tables)
    tablestats = { case:
            get_mean_mad(deque( value
                for row in tables[case]
                for cell in row
                for value in (cell.values() or (0,)) ))
            for case in tables }
    charts = { case:
            ( get_cell_formatted(
                    ratiocell['total'],
                    madcell.get('mad', 0),
                    totalcell['total'],
                    tablestats[case].get('mad', 0),
                    **format_config)
                for ratiosrow, madsrow, totalsrow in zip(
                    ratio_total_tables[case],
                    stats_tables[case],
                    totals_table)
                for ratiocell, madcell, totalcell in zip(
                    ratiosrow, madsrow, totalsrow) )
            for case in tables }
    return bottle.template(template_name, charts = charts, total = total)

def get_integer_range(
        integer: int,
        integers_in_range: int,
        integers_in_superrange: int,
        ) -> 'Tuple[int, int]':
    '''
    Get a range that contains :param:`integer`, e.g. a range of years like
    1250 to 1275 for the year 1254, when a century is 100 years long.

    .. testcode:
        import chart
        print(chart.get_integer_range(1254, 25, 100))

    .. testoutput:
        (1250, 1275)
    '''
    begin = integer - integer % integers_in_superrange
    while True:
        if begin <= integer < begin + integers_in_range:
            break
        begin += integers_in_range
    return (begin, begin + integers_in_range)

def get_mean_mad(values: 'Sequence[float]') -> dict:
    '''
    Get the mean and the mean average deviation (MAD) of :param:`values`.
    '''
    if values:
        mean = statistics.mean(values)
        mad = statistics.mean( abs(value - mean) for value in values )
        return {'mad': mad, 'mean': mean}
    else:
        return {}

def get_ratios(
        cells: 'Dict[Hashable, Dict[Hashable, float]]',
        cases: 'Sequence[Hashable]',
        default: int = 0
        ) -> 'Dict[Hashable, Dict[Hashable, Optional[float]]]':
    '''
    For the values in :param:`cells`, get ratios.
    Each divisor is the total sum over :param:`cases` (see the testcode below).
    If such a total sum is ``0``, the ratio is ``None``.

    .. testcode::
        import chart
        cells = {
            'case a': {'txt1': 50, 'txt2': 100},
            'case b': {'txt1': 150, 'txt2': 400},
            'case c': {}}
        cases = deque(['case a', 'case b', 'case c'])
        ratios = get_ratios(cells, cases)
        for case in sorted(ratios.keys()):
            print(case + ':')
            print(ratios[case])

    .. testoutput::
        case a:
        {'txt1': 0.25, 'txt2': 0.2}
        case b:
        {'txt1': 0.75, 'txt2': 0.8}
        case c:
        {'txt1': 0.0, 'txt2': 0.0}
    '''
    variants = set( var for cell in cells.values() for var in cell.keys() )
    totals = { var: sum( abs(cells[case].get(var, default)) for case in cases )
            for var in variants }
    return { case:
            { var: (cells[case].get(var, default) / totals[var])
                if totals[var] else None for var in variants }
            for case in cases }

def get_table_of_derived_cell_values(
        table: 'Deque[Deque[Dict]]',
        derive: 'Callable[[Dict], Dict]' = get_mean_mad,
        ) -> 'Deque[Deque[Dict]]':
    return deque( deque( derive(cell.values())
            for cell in row ) for row in table )

def get_table_of_totals(
        tables: 'Dict[Hashable, Deque[Deque[Dict]]]'
        ) -> 'Deque[Deque[Dict]]':
    '''
    Get a table which is structured like one in :param:`tables` and
    contains as cell values a dictionary like ``{'total': x}`` with
    x being the total sum of the corresponding cells over all cases.

    :param tables: to be built by :func:`.tabulate`.
    '''
    return deque( deque( {'total': sum( value
            for cell in cells for value in cell.values() ) }
                for cells in zip(*rows) )
                    for rows in zip(*tables.values()) )

def get_tables_of_case_ratios(
        tables:
            'Dict[Hashable, Deque[Deque[Dict[Hashable, float]]]]'
        ) -> 'Dict[Hashable, Deque[Deque[Dict[Hashable, Optional[float]]]]]':
    '''
    Take a table like the ones built by :func:`.tabulate` and derive
    a table that contains ratios instead of the absolute cell values.
    Each ratio is the ratio of the absolute value of one case to the
    sum of the corresponding absolute values of all cases.
    If this sum is ``0``, the ratio is ``None``.

    .. testcode::
        import chart
        data = {
                ('case a', '1200', 'bair.', 'txt1', 'ycx'): 50,
                ('case a', '1200', 'bair.', 'txt2', 'xxc'): 25,
                ('case a', '1200', 'bair.', 'txt2', 'xxy'): 25,
                ('case a', '1200', 'alem.', 'txt3', 'xxx'): 10,
                ('case a', '1200', 'alem.', 'txt4', 'xcc'): 20,
                ('case a', '1250', 'bair.', 'txt5', 'xyc'): 100,
                ('case a', '1250', 'alem.', 'txt6', 'cxc'): 10,

                ('case b', '1200', 'bair.', 'txt1', 'yyy'): 150,
                ('case b', '1200', 'bair.', 'txt2', 'xxc'): 75,
                ('case b', '1200', 'bair.', 'txt2', 'xxy'): 75,
                ('case b', '1200', 'alem.', 'txt3', 'ycy'): 30,
                ('case b', '1200', 'alem.', 'txt4', 'ycc'): 60,
                ('case b', '1250', 'bair.', 'txt5', 'yyx'): 300,
                ('case b', '1250', 'alem.', 'txt6', 'ccy'): 40,

                ('case c', '1250', 'bair.', 'txt5', 'yyx'): 5,
                }
        matchtable = (
                (('1200', 'bair.'), ('1200', 'alem.')),
                (('1250', 'bair.'), ('1250', 'alem.')),
                )
        tables = chart.tabulate(data, matchtable)
        ratio_tables = get_tables_of_case_ratios(tables)
        for case in sorted(ratio_tables.keys()):
            print(case + ':')
            for row in ratio_tables[case]:
                for cell in row:
                    print(dict(cell), end = ' ')
                print()
            print()

    .. testoutput::
        case a:
        {'txt2': 0.25, 'txt1': 0.25} {'txt4': 0.25, 'txt3': 0.25} 
        {'txt5': 0.24691358024691357} {'txt6': 0.2} 

        case b:
        {'txt2': 0.75, 'txt1': 0.75} {'txt4': 0.75, 'txt3': 0.75} 
        {'txt5': 0.7407407407407407} {'txt6': 0.8} 

        case c:
        {'txt2': 0.0, 'txt1': 0.0} {'txt4': 0.0, 'txt3': 0.0} 
        {'txt5': 0.012345679012345678} {'txt6': 0.0} 

    '''
    cases = deque(tables.keys())
    ratio_tables = { case: deque() for case in cases }
    for r, row in enumerate(tables[cases[0]]):
        for case in cases:
            ratio_tables[case].append(deque())
        for c in range(len(row)):
            ratios = get_ratios({ case: tables[case][r][c] for case in cases },
                    cases)
            for case in cases:
                ratio_tables[case][-1].append(ratios[case])
    return ratio_tables

def tabulate(
        data: 'Dict[Sequence[Hashable], float]',
        matchtable: 'Iterable[Iterable[Hashable]]',
        casegetter: itemgetter = itemgetter(0),
        matchgetter: itemgetter = itemgetter(1, 2),
        variantgetter: itemgetter = itemgetter(3),
        default_factory: 'Callable[[], float]' = lambda: 0,
        total: float = 0,
        ) -> '''Tuple[
                    Dict[
                        Hashable,               # a case
                            Deque[              # a table
                                Deque[          # a row
                                    Defaultdict # a cell
                                    ]]],
                    int]''':
    '''
    Get a dictionary of tables and a total sum from :param:`data`.

    The dictionary consists of keys and values; the tables are the values,
    the so-called cases are the keys of the dictionary. The cases are drawn
    from the keys of :param:`data` by :param:`casegetter`. The shape of each
    table (i.e. the number of rows and the number of cells of each row) is
    determined by :param:`matchtable`. And the cells are filled as follows:
    For each entry in :param:`data`, the key tells:

    - which table is concerned: ``casegetter(key)``,
    - which cell of :param:`matchtable` corresponds: ``matchgetter(key)``,
    - which subcell of the cell is concerned: ``variantgetter(key)``.

    All values are added to :param:`total`, and the result is returned.

    All values for which the table, the cell and the subcell are equal, are
    **added up**, and the result of this addition becomes the value of the
    subcell.

    The key of the subcell is the outcome of ``variantgetter(key)``.

    The cell is a `collections.defaultdict` of such subcells. **Thus, beware
    of unintentionally adding keys afterward.**

    The row is a `collections.deque` of such cells.
    The table is a deque of such rows.

    :param default_factory: returns a starting value for each subcell.

    .. testcode::
        import chart
        data = {
                ('case a', '1200', 'bair.', 'txt1', 'ycx'): 50,
                ('case a', '1200', 'bair.', 'txt2', 'xxc'): 25,
                ('case a', '1200', 'bair.', 'txt2', 'xxy'): 25,
                ('case a', '1200', 'alem.', 'txt3', 'xxx'): 10,
                ('case a', '1200', 'alem.', 'txt4', 'xcc'): 20,
                ('case a', '1250', 'bair.', 'txt5', 'xyc'): 100,
                ('case a', '1250', 'alem.', 'txt6', 'cxc'): 10,

                ('case b', '1200', 'bair.', 'txt1', 'yyy'): 150,
                ('case b', '1200', 'bair.', 'txt2', 'xxc'): 75,
                ('case b', '1200', 'bair.', 'txt2', 'xxy'): 75,
                ('case b', '1200', 'alem.', 'txt3', 'ycy'): 30,
                ('case b', '1200', 'alem.', 'txt4', 'ycc'): 60,
                ('case b', '1250', 'bair.', 'txt5', 'yyx'): 300,
                ('case b', '1250', 'alem.', 'txt6', 'ccy'): 40,

                ('case c', '1250', 'bair.', 'txt5', 'yyx'): 5,
                }
        matchtable = (
                (('1200', 'bair.'), ('1200', 'alem.')),
                (('1250', 'bair.'), ('1250', 'alem.')),
                )
        tables = chart.tabulate(data, matchtable)
        for case in sorted(tables.keys()):
            print(case + ':')
            for row in tables[case]:
                for cell in row:
                    print(dict(cell), end = ' ')
                print()
            print()

    .. testoutput::
        case a:
        {'txt2': 50, 'txt1': 50} {'txt4': 20, 'txt3': 10} 
        {'txt5': 100} {'txt6': 10} 

        case b:
        {'txt2': 150, 'txt1': 150} {'txt4': 60, 'txt3': 30} 
        {'txt5': 300} {'txt6': 40} 

        case c:
        {} {} 
        {'txt5': 5} {} 

    '''
    data = data.items()
    # Provide an empty deque as the table for each case
    tables = { casegetter(key): deque() for key, _ in data }
    for matchrow in matchtable:
        for case in tables:
            # Provide an empty deque as the next row
            tables[case].append(deque())
        for matchcell in matchrow:
            for case in tables:
                # Provide an empty defaultdict as the next cell
                tables[case][-1].append(defaultdict(default_factory))
            for key, value in data:
                # Sum up all values of matching keys, but separated by variant
                if matchgetter(key) in matchcell:
                    tables[casegetter(key)][-1][-1][variantgetter(key)] += value
                    total += value
    return tables, total
