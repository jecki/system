% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2016 ff. (© http://badw.de)
% # Template for HTML-XML-text which gets filled into the base template without
% # any internal modification. In the example edition:
% # http://localhost:8080/de/example/introduction.
<script>
$(document).ready(highlight);
</script>
<main>
{{!kwargs['text']}}
</main>\\
% rebase('base.tpl', request = request, dao = dao, kwargs = kwargs)