% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
<main>
<article class="flush_unhyph sheet sheetwide">
	<h1>The Team</h1>
  % for group in kwargs['team']:
	% heading = group['group_heading']
	<h2>{{heading}}</h2>
	<ul>
	  % users = group['users']
	  % for user in users:
		% institution = user['user_institution']
		% title = user['user_title']
		% degree = user['user_degree']
		% name = (title + ' ' if title else '') + user['user_firstname'] + ' ' + user['user_lastname'] + (', ' + degree if degree else '')
		% institution = ' ({})'.format(institution) if institution else ''
		% if heading == 'Former Researchers':
		<li>{{name}}{{institution}}</li>
		% else:
		<li><p><a href="/team/{{user['user_id']}}">{{name}}</a>{{institution}}</p></li>
		% end
	  % end
	</ul>
  % end
</article>
</main>\\
% rebase('base.tpl', request = request, dao = dao, kwargs = kwargs)