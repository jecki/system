% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
% entry = kwargs['entry']
<main>
<article class="sheet sheetwide">
	<h1>{{entry[]}}</h1>
</article>
</main>\\
% rebase('base.tpl', request = request, dao = dao, kwargs = kwargs)