# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2018 ff. (© http://badw.de)

class DBCon():
    '''
    Context for a database connection.

    Use e.g. ``with DBCon(sqlite3.connect('db.sqlite')) as (connection, cur):``
    in order to get a connection and its cursor in a with-block.

    On exit: If an exception has occurred, a rollback is done. In any case, the
    connection is closed. (The rollback is done explicitly as it is not implied
    in the case that connection pooling is used.)

    A commit is not done automatically.
    '''
    def __init__(self, connection):
        self.connection = connection
    def __enter__(self):
        return (self.connection, self.connection.cursor())
    def __exit__(self, exception, value, traceback):
        if exception:
            self.connection.rollback()
        self.connection.close()
