% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
% first_pagina = entry['paginas'][0][0]
% last_pagina = entry['paginas'][-1][0]
			<div{{!' dir="rtl"' if rtl else ''}}>\\
% if pagina != first_pagina:
<a class="arrowleftstop key" href="/{{entry['doc_type']}}/{{entry['doc_id']}}/{{entry['part_id']}}/{{first_pagina}}{{query}}" rel="first">First</a>\\
% else:
<span class="arrowleftstop disabled key">First</span>\\
% end
% if entry['prev']:
<a class="arrowleft key" href="/{{entry['doc_type']}}/{{entry['doc_id']}}/{{entry['part_id']}}/{{entry['prev']}}{{query}}" rel="prev">Previous</a>\\
% else:
<span class="arrowleft disabled key">Previous</span>\\
% end
% if entry['next']:
<a class="arrowright key" href="/{{entry['doc_type']}}/{{entry['doc_id']}}/{{entry['part_id']}}/{{entry['next']}}{{query}}" rel="next">Next</a>\\
% else:
<span class="arrowright disabled key">Next</span>\\
% end
% if pagina != last_pagina:
<a class="arrowrightstop key" href="/{{entry['doc_type']}}/{{entry['doc_id']}}/{{entry['part_id']}}/{{last_pagina}}{{query}}" rel="last">Last</a>\\
% else:
<span class="arrowrightstop disabled key">Last</span>\\
% end
</div>
