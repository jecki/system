# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2013 ff.
import base64
import hashlib
import html
import json
import mmap
import os
import re
import tempfile
import unicodedata
import xml.etree.ElementTree as ET
from functools import partial
from itertools import permutations

import __init__
import file_io
import xmlhtml
from structs import DefaultkeyDict

class Folder():
    '''
    Class of a data access object that models a folder which contains
    the content of one or more websites.
    A class can inherit from this class and add or alter methods for e.g.
    database access.

    .. warning::
       XML files from an untrusted source must have been checked with regard to
       certain XML attacks not caught by the xml.etree parser. These attacks
       are the `billion laughs` attack and the `quadratic blowup` attack. See
       `http://docs.python.org/3/catalog/xml.html#xml-vulnerabilities`_.

    A simple and effective solution is to delete all entity declarations in
    XML files, *before* they are parsed here in :meth:`__init__`.
    Such deletion can be done as follows::

        text = re.sub(r'(?i)<\s*?!\s*?ENTITY[^>]*?>', '', text)

    Instead of own entity encodings, UTF-8 encoding should be used anyway.
    '''
    def __init__(
            self,
            path: str              = __file__ + '/../../../../seite_portal/public',
            glosses_path: str      = __file__ + '/../../../../glosses.xml',
            multisite: bool        = True,
            default_lang_id: str   = '',
            default_site_id: str   = 'portal',
            indices_folder_id: str = 'indices',
            fulltext_index_id: str = 'fulltext',
            config_page_id: str    = '_config',
            ):
        '''
        :param path: the path to the folder containing data which will be
            accessed via this object.
        :param glosses_path: path to the file containing all placeholder
            keywords and their corresponding translations, which are inserted
            into the served page according to the currently chosen language.
        :param multisite: If ``True``, the web presence consists of multiple
            sites, each comprising multiple pages.
            If ``False``, it is only one website with one or multiple pages.
            In the first case, the URL structure typically is something like
            ``'/lang_id/site_id/page_id'``, in the second case something like
            ``'/lang_id/page_id'``.
        :param default_lang_id: (sigil for) the default language of the website;
            may be left unspecified here and set later. Such sigils are used to
            choose a language specific version of a page or to choose a gloss
            for substituting placeholders in the template, e.g. in the menu.
            They mostly stem from URLs (e.g. ``'http://example.org/de/intro'``)
            and conform to ISO 639, http://tools.ietf.org/html/rfc5646, page 4
            (‘shortest ISO 639 code sometimes followed by extended language
            subtags’).
        :param default_site_id: (relevant only if :param:`multisite` is
            ``True``) is the distinctive name of a folder which is located
            within :param:`path` and represents the default website.
            The name must be unique within all websites served by the app.
            The default website is served, when the request does not specify
            the website.
        :param glosses_path: path to the file containing all placeholder
            keywords and their corresponding translations, which are inserted
            into the served page according to the currently chosen language.
        :param indices_folder_id: name of a subfolder in the default folder.
            This subfolder will contain search indices.
        :param fulltext_index_id: name of the file containing the fulltext
            search index. File and index will be made in this method.
        :param config_page_id: See :meth:`dao_io.Folder.config_from_file`.
        '''
        # To use of ET or xmlhtml in templates without import or parameter:
        self.ET = ET
        self.xmlhtml = xmlhtml

        self.path = os.path.abspath(path) + os.sep
        self.multisite = multisite
        self.default_lang_id = default_lang_id
        self.default_lang_ids = {'': default_lang_id}
        if multisite:
            self.default_site_id = default_site_id
            self.site_ids = tuple(sorted( name for name in os.listdir(self.path)
                    if os.path.isdir(self.path + name) ))
        else:
            self.default_site_id = ''
            self.site_ids = ('',)
        self.auth_path = os.path.abspath(self.path + '../auth.json')
        self.auth_salt_path = os.path.abspath(self.path + '../auth_salt')
        self.auth_secret_path = os.path.abspath(self.path + '../auth_secret')
        self.auth_sessions_path = os.path.abspath(self.path + '../sessions')
        with open(self.auth_salt_path, 'rb') as file:
            self.salt = file.read()
        with open(self.auth_secret_path, 'rb') as file:
            self.secret = file.read()

        self.config_from_file(config_page_id)
        self.default_lang_id = self.default_lang_ids[default_site_id]
        self.glosses = self.read_glosses(glosses_path)

        fulltext_index_path = file_io.pave(os.path.abspath(os.path.join(
                self.path, default_site_id, indices_folder_id, fulltext_index_id
                )))
        self.make_fulltext_index(fulltext_index_path, site_ids = self.site_ids)
        with open(fulltext_index_path, 'rb') as file:
            self.fulltext = mmap.mmap(
                    file.fileno(), 0, access = mmap.ACCESS_READ)

    def add_ajax_source(
            self,
            doc: str,
            site_id: str = '',
            table_config_re: 'Pattern[str]' = re.compile(
                r'(?s)data-table_config_json\s*?=\s*?(?P<a>["\'])(.*?)(?P=a)'),
            ) -> str:
        '''
        Add data from an ajax source to :param:`doc` from :param:`site_id`,
        if there is such a source given in :param:`doc`. Return the doc, be
        it complemented or be it unchanged.

        :param site_id: to be specified if :attr:`.multisite` is ``True``.
        '''
        if 'ajax' in doc and 'data-table_config_json' in doc:
            try:
                with open(
                        os.path.join(self.path, site_id, json.loads(
                            html.unescape(table_config_re.search(doc).group(2)))
                            .get('ajax', '')),
                        'r', encoding = 'utf-8') as file:
                    doc = doc.replace('</html>', '', 1) + ''.join(
                        ''.join(
                            '<td about="{}">'.format(row['rowAttrs']['about'])
                            + cell + '</td>' for cell in row.values()
                            if isinstance(cell, str) )
                        for row in json.load(file)['data']
                        ) + '</html>'
            except: # Sic. We prefer an empty page.
                pass
        return doc

    def auth_hash(self, losung: str) -> 'List[int]':
        return list(hashlib.pbkdf2_hmac(
                'sha512',
                losung.encode(encoding = 'utf-8'),
                self.salt,
                100001))

    def auth_in(self, user: str, word: str) -> bool:
        try:
            with open(self.auth_path, 'r', encoding = 'utf-8') as file:
                users = json.load(file)
            assert users[user]['losung'] == self.auth_hash(word)
        except:
            return False
        return True

    def auth_roles(self, user: 'str') -> 'Set[str]':
        with open(self.auth_path, 'r', encoding = 'utf-8') as file:
            return set(json.load(file).get(user, {}).get('roles', set()))

    def config_from_file(self, config_page_id: str) -> None:
        '''
        Set configurations taken from a file named :param:`config_page_id`.
        If :attr:`.multisite` is ``False``, there has to be such a file
        in the sole folder representing the website, otherwise there has
        to be such a file in each subfolder representing a subwebsite.
        (See :param:`self.__init__.multisite`.)

        The configurations are:

        - :attr:`.shortcut_icon_paths`
        - :attr:`.main_icons_path_link_flat_br`
        - :attr:`.menu_ids`
        - :attr:`.default_page_ids`
        - :attr:`.lang_ids`
        - :attr:`.default_lang_ids`
        '''
        self.shortcut_icon_paths = {}
        self.main_icons_path_link_flat_br = {}
        self.menu_ids = {}
        self.default_page_ids = {}
        self.lang_ids = {}
        # ``self.default_lang_ids`` is already set in :meth:`.__init__`.
        for site_id in self.site_ids:
            try:
                elem = ET.parse(
                        os.path.join(self.path, site_id, config_page_id))
                self.shortcut_icon_paths[site_id] = elem.find(
                        './/*[@id="shortcut_icon"]/tbody/tr/td').text or ''
                self.main_icons_path_link_flat_br[site_id] = tuple(
                        (path.strip(), link.strip(),
                            True if flat.strip().lower() == 'yes' else False,
                            True if br.strip().lower() == 'yes' else False)
                        for path, link, flat, br in
                        xmlhtml.elem2table(elem.find('.//*[@id="main_icons"]')) )
                menu_ids = xmlhtml.elem2tree(elem.find('.//*[@id="menu_ids"]'))
                self.menu_ids[site_id] = tuple(menu_ids.keys())
                self.default_page_ids[site_id] = next(
                        key for key, value in menu_ids.items()
                        if value.strip().lower() == 'default' )
                lang_ids = xmlhtml.elem2tree(elem.find('.//*[@id="lang_ids"]'))
                self.lang_ids[site_id] = tuple(lang_ids.keys())
                self.default_lang_ids[site_id] = next(
                        key for key, value in lang_ids.items()
                        if value.strip().lower() == 'default' )
            except Exception as e:
                print('\nAn Error occurred in:', site_id)
                raise e

    def get_mapping(
            self,
            text: bytes,
            site_id: str = '',
            keypath: str = './/thead/tr/th',
            valuepath: str = './/*[@id="{}"]/td',
            ajax_key: str = 'data-table_config_json',
            meta_source_re: 'Pattern[str]' = re.compile(
                r'''data-meta_source_id\s*?=\s*?(?P<a>["'])(.*?)(?P=a)'''),
            ) -> 'Tuple[Tuple[str, str], ...]':
        '''
        Extract a key-value mapping from an XML source, whose path is given
        in the attribute `data-meta_source_id` of the first element of
        :param:`text`. In the XML source, the keys are found under
        :param:`keypath`, and the values are found under :param:`valuepath`.
        Both parameters can contain ``{}``, which will be substituted by any
        fragment identifier found in the path given in `data-meta_source_id`.
        Alternatively, the values are to be found in an ajax source, whose
        path is given in an attribute :param:`ajax_key` of the root element.

        :param site_id: to be specified if :attr:`.multisite` is ``True``.
        '''
        try:
            meta_source_id, fragment_id = meta_source_re.search(
                    str(text.split(b'>', 1)[0], encoding = 'utf-8')
                    ).group(2).rsplit('#', 1)
            tree = ET.parse(os.path.join(self.path, site_id, meta_source_id))
            keys = tuple(map(xmlhtml.elem2string,
                    tree.findall(keypath.format(fragment_id))))
            ajax_element = tree.find('[@{}]'.format(ajax_key))
            ajax_path = json.loads(ajax_element.attrib.get(ajax_key)
                    ).get('ajax', '') if ajax_element else None
            if ajax_path:
                with open(os.path.join(self.path, site_id, ajax_path),
                        encoding = 'utf-8') as file:
                    ajax_source = json.load(file)
                for row in ajax_source['data']:
                    if row['rowAttrs']['id'] == fragment_id:
                        break
                values = ( '<td>' + row[str(i)] + '</td>'
                        for i in range(len(keys)) )
            else:
                values = map(xmlhtml.elem2string,
                        tree.findall(valuepath.format(fragment_id)))
            return tuple(zip(keys, values))
        except: # Sic.
            return ()

    def get_table_config_json(
            self,
            text: bytes,
            lang_id: str,
            table_config_re: 'Pattern[str]' = re.compile(
                r'(?s)data-table_config_json\s*?=\s*?(?P<a>["\'])(.*?)(?P=a)'),
            ) -> str:
        '''
        Get a json-string containing the configuration for a DataTables table
        (see http://www.datatables.net/): A basic configuration is specified;
        an additional configuration is taken from the JSON of the attribute
        `data-table_config_json` of the first element of :param:`text`, if
        possible; the basic configuration is replenished or overwritten with
        the additional configuration.
        '''
        table_config = {
            "deferRender": True,
            "dom": "ilptp",
            "language": {
                "aria": {
                    "sortAscending": ": " + self.glosses["aria_sortAscending"
                        ][lang_id],
                    "sortDescending": ": " + self.glosses["aria_sortDescending"
                        ][lang_id],
                    },
                "emptyTable": self.glosses["emptyTable"][lang_id],
                "info": self.glosses["info"][lang_id],
                "infoEmpty": self.glosses["infoEmpty"][lang_id],
                "infoFiltered": "(" + self.glosses["infoFiltered"][lang_id] +
                    ")",
                "infoPostFix": self.glosses["infoPostFix"][lang_id],
                "lengthMenu": self.glosses["lengthMenu"][lang_id],
                "loadingRecords": self.glosses["loadingRecords"][lang_id] +
                    " …",
                "paginate": {
                    "first": self.glosses["paginate_first"][lang_id],
                    "previous": self.glosses["paginate_previous"][lang_id],
                    "next": self.glosses["paginate_next"][lang_id],
                    "last": self.glosses["paginate_last"][lang_id],
                    },
                "processing": self.glosses["processing"][lang_id] + " …",
                "search": self.glosses["search"][lang_id] + ":",
                "thousands": "",
                "zeroRecords": self.glosses["zeroRecords"][lang_id],
                },
            "order": [[0, "asc"],],
            "paging": False,
            }
        try:
            table_config.update(json.loads(html.unescape(
                    table_config_re.search(
                    str(text.split(b'>', 1)[0], encoding = 'utf-8')).group(2))))
        except (AttributeError, ValueError) as e:
            pass
        # The configuration needs an attribute `columns` according to
        # http://www.datatables.net/examples/ajax/objects.html.
        # If not already given, I calculate it here, assuming, that the
        # columns in the JSON data are simply named after their rank:
        if not 'columns' in table_config:
            number_of_columns = text.count(b'<th>') + text.count(b'<th ')
            table_config['columns'] = [ {'data': str(i)}
                    for i in range(number_of_columns) ]
        return json.dumps(table_config, separators = (',', ':'))

    def get_template_id_and_text(
            self,
            lang_id: str = '',
            site_id: str = '',
            page_id: str = '',
            template_re: 'Pattern[str]' = re.compile(
                r'''data-template_id\s*?=\s*?(?P<a>["'])(.*?)(?P=a)'''),
            ) -> 'Tuple[str, bytes, str]':
        '''
        Return the template_id, the bytes of the file of a certain webpage
        and the text_lang_id. The template_id is taken from the attribute
        `data-template_id` of the first element of the xHTML5 file.
        The text_lang_id is taken from :meth:`get_text_lang_id_and_path`.
        The text language may differ from the currently chosen language of
        the user interface.

        :param site_id: to be specified if :attr:`.multisite` is ``True``.
        '''
        text_lang_id, path = self.get_text_lang_id_and_path(
                lang_id, site_id, page_id)
        try:
            with open(path, 'rb') as infile:
                text = infile.read()
        except OSError:
            text = b''
        match = template_re.search(
                str(text.split(b'>', 1)[0], encoding = 'utf-8'))
        return (match.group(2) if match else 'text.tpl', text, text_lang_id)

    def get_text_lang_id_and_path(
            self,
            lang_id: str = '',
            site_id: str = '',
            page_id: str = '',
            ) -> 'Tuple[str, str]':
        '''
        Put lang_id, site_id and page_id together to the path of the file of
        a certain webpage. The text_lang_id is the lang_id, if a file does
        exist for which the language lang_id is specified, otherwise, the
        text_lang_id is the lang_id of the default language specified for the
        whole site.

        :param site_id: to be specified if :attr:`.multisite` is ``True``.
        '''
        if site_id:
            path = self.path + site_id + '/' + page_id
        else:
            path = self.path + page_id
        langpath = path + '+' + lang_id
        if os.path.exists(langpath):
            path = langpath
        else:
            lang_id = self.default_lang_ids[site_id]
            # The text in the default language may be suffixed or not:
            langpath = path + '+' + lang_id
            if os.path.exists(langpath):
                path = langpath
        return lang_id, path

    def get_toc_entries(
            self,
            lang_id: str = '',
            site_id: str = '',
            ) -> 'Generator[Tuple[str, str, str, str, str, int]]':
        '''
        Yield (as a generator) for every page of the site :param:`site_id` and
        for every heading of this page the following data:
        `entry_lang_id`, `entry_file_id`, `entry_page_id`,
        `entry_element_link`, `entry_text`, `indent`.

        :param site_id: to be specified if :attr:`.multisite` is ``True``.
        '''
        path = self.path
        if site_id:
            path += site_id + '/'
        default_lang_id = self.default_lang_ids[site_id]
        for element in ET.parse(path + '_table_of_contents_ids'
                ).findall('.//tbody/tr/td'):
            entry_file_id = element.text or ''
            page_lang = entry_file_id.rsplit('+', 1)
            entry_lang_id = (page_lang[1] if len(page_lang) == 2
                    else default_lang_id)
            entry_page_id = page_lang[0]
            if entry_page_id in self.glosses:
                glosses = self.glosses[entry_page_id]
                entry_text = glosses.get(
                        entry_lang_id, glosses[default_lang_id])
            else:
                entry_text = entry_page_id
            yield (entry_lang_id, entry_file_id, entry_page_id, '', entry_text,
                    0)
            try:
                for _, element in ET.iterparse(
                        path + entry_file_id, events = ('start',)):
                    if element.tag in ('h1', 'h2', 'h3', 'h4', 'h5', 'h6'):
                        yield (entry_lang_id, entry_file_id, entry_page_id,
                                '#' + element.get('id', default = ''),
                                ''.join(element.itertext()),
                                int(element.tag[1]))
            except ET.ParseError as e:
                page = '/'.join( item for item in
                        (lang_id, site_id, entry_file_id) if item )
                print('\n\nAn Error occurred in: {}\n'.format(page))
                raise e

    def make_fulltext_index(
            self,
            fulltext_index_path: str,
            site_ids: 'Sequence[str]' = [''],
            ) -> None:
        '''
        Make a new fulltext search index at :param:`fulltext_index_path`.

        The index starts and ends with a newline. Between these newlines,
        there is an arbitrary number of records, delimited from each other
        by a newline. A record is structured as follows::

            status_id//lang_id/site_id/page_id#part_id//text-passage

        if :attr:`.multisite` is ``True``, else::

            status_id//lang_id/page_id#part_id//text-passage

        - `status_id` is a status flag; here: `source` or `non-source` or
          the empty string to indicate uncertainty of the status.
        - `lang_id/site_id/page_id#part_id` is the URL to `text-passage`;
          `#part_id` can be missing.
        - `text-passage` is a part of the fulltext.

        :param site_ids: a sequence of the IDs of all sites to be included
            in the fulltext index. To be left at the default value if
            :attr:`.multisite` is ``False``.
        '''
        print('\nI make a new fulltext-search index now at:\n',
                fulltext_index_path)
        preprocess_res = [ (re.compile(old), new) for old, new in
                (
                    # remove word-breaking hyphens at line ends:
                    (r'[-\xad]<br\s*?/>\s*', ''),
                ) ]
        postprocess_res = [ (re.compile(old), new) for old, new in
                (
                    (r'\s+', ' '),
                    # prevent DOS attacks of simple evil regular expressions:
                    (r'(.)\1{4,}', '\g<1>\g<1>\g<1>'),
                ) ]
        try:
            with open(fulltext_index_path, 'w', encoding = 'utf-8') as testfile:
                pass
        except OSError:
            raise Exception(
                    '\n\nAn error occurred:\n'
                    'The index file is open and cannot be overwritten.\n'
                    'Most probably the system is already running.\n'
                    'So, shut down the system and then start anew.\n')
        with open(fulltext_index_path, 'w', encoding = 'utf-8') as outfile:
            outfile.write('\n')
            for site_id in site_ids:
                path_head = self.path
                if site_id:
                    path_head += site_id + '/'
                for path_tail in ( name for name in os.listdir(path_head)
                        if os.path.isfile(path_head + name)
                        and name[0] != '_' ):
                    if '+' in path_tail:
                        page_id, lang_id = path_tail.rsplit('+', 1)
                    else:
                        page_id = path_tail
                        lang_id = self.default_lang_ids[site_id]
                    parser = xmlhtml.FragmentTextExtractor(tracked_attrs =
                            {'class': ('source', 'non-source')})
                    with open(path_head + path_tail, 'r', encoding = 'utf-8'
                            ) as infile:
                        try:
                            doc = infile.read()
                        except UnicodeDecodeError:
                            print('\nWARNING: {}/{} is not UTF-8 encoded and '
                                    'hence left out'.format(site_id, page_id))
                            continue
                    try:
                        for old, new in preprocess_res:
                            doc = old.sub(new, doc)
                        parser.parse(doc)
                        for extract in parser.extracts:
                            text = extract[1].strip()
                            if text:
                                for old, new in postprocess_res:
                                    text = old.sub(new, text)
                                part_id = extract[0]
                                status_id = extract[2].get('class', [''])[0]
                                outfile.write('{}//{}/{}{}//{}\n'.format(
                                        status_id,
                                        lang_id,
                                        site_id + '/' + page_id
                                            if self.multisite else page_id,
                                        '#' + part_id if part_id else '',
                                        unicodedata.normalize('NFKD', text),
                                        ))
                    except Exception as e:
                        print('\nA fatal error occurred in:',
                                '{}/{}'.format(site_id, page_id))
                        raise e
        print('\n(Done.)\n')

    def read_glosses(self, glosses_path: str) -> 'Dict[str, Dict[str, str]]':
        '''
        Get a dictionary of keywords mapped to a dictionary of language codes
        mapped to translations belonging to each keyword.
        '''
        return xmlhtml.elem2tree(
                ET.parse(os.path.abspath(glosses_path)).find('table'),
                get = lambda e: xmlhtml.elem2string(e, inner = True),
                Dict = partial(DefaultkeyDict, self.default_lang_id))

    def search_fulltext(
            self,
            term: str,
            no_case: bool = True,
            regex: bool = False,
            single_words: bool = False,
            status_ids: 'Sequence[str]' = [],
            site_ids: 'Sequence[str]' = [],
            at_least_one_letter_re: 'Pattern[str]' = re.compile(r'(?<!\\)\w'),
            flags_re: 'Pattern[str]' = re.compile(r'\(\?[aiLmsux]+?\)'),
            ) -> 'Tuple[str, str, str, AbstractSet[str]]':
        '''
        Search :param:`term` in the fulltext index.
        Yield (as a generator) tuples containing:

        - a status flag, either `source` or `non-source`.
        - the URL to the text passage containing the match.
        - the text passage containing the match.
        - a set of the matching substrings within the passage.
          This may be useful, when a search is done with a regular
          expression and the matching substrings shall be marked
          e.g. in an HTML output.

        .. important::
            At the moment, only the first 500 results are returned.
            This limitation is set to avoid performance issues both
            on the host computer and even more in e.g. the browser
            processing the results.

        .. todo::
            Remove the above mentioned limitation with the help of a
            solution via ajax and paging (or infinite scroll?).

        :param no_case: If true, upper and lower case are not distinguished.
        :param regex: If true, the :param:`term` is used as a regular
            expression.
        :param status_ids: a possibly empty list of status flags to restrict
            the search to records starting with one of these flags. If empty,
            no restriction takes place.
        :param site_ids: a possibly empty list of site ids to restrict
            the search to records whose URL points to any site having one
            of these site ids. If empty, no restriction takes place. The
            site_id is the name of the folder containing the site.
            To be left at the default value if :attr:`.multisite` is ``False``.
        '''
        term = unicodedata.normalize('NFKD', term)
        if not at_least_one_letter_re.search(term):
            term = r'\Z.'
            regex = True
        if single_words:
            term = r'(\b{}\b)'.format(
                   r'\b)|(\b'.join(map(
                   r'\b.*?\b'.join, permutations(
                       term.split() if regex else
                       map(re.escape, term.split())))))
        elif regex:
            term = flags_re.sub('', ' '.join(term.split()))
            term = ''.join( '({})'.format(c)
                    if ord(c) > 127 else c for c in term )
        else:
            term = re.escape(' '.join(term.split()))
        if no_case:
            term += '(?i)'
        try:
            term_re = re.compile(term, re.ASCII)
        except:
            term = r'\Z.'
            term_re = re.compile(term, re.ASCII)
        fullterm = r'\n{}//[^/\n]*/{}[^/\n]*//.*?({}).*'.format(
                r'(?:{})'.format('|'.join( s for s in status_ids ))
                        if len(status_ids) > 1 else
                    status_ids[0] if status_ids else
                    r'[^/\n]*',
                '' if not self.multisite else
                    r'(?:{})/'.format('|'.join( s for s in site_ids ))
                        if len(site_ids) > 1 else
                    site_ids[0] + '/' if site_ids else
                    r'[^/\n]*/',
                term
                )
        fullterm_b = fullterm.encode(encoding = 'utf-8', errors = 'ignore')
        for counter, match in enumerate(re.finditer(fullterm_b, self.fulltext)):
            status, url, text = match.group().decode().split('//', 2)
            matches = set( m.group() for m in term_re.finditer(text) )
            yield status, url, text, matches
            if counter > 500:
                yield '', '', 'Break: too many results.', {''}
                break
